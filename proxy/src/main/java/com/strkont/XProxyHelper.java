package com.strkont;

import com.strkont.proxy.ProxyCallback;
import com.strkont.proxy.ProxyManager;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
public class XProxyHelper {

    public static void init(ProxyCallback proxyCallback) {
        try {
            ProxyManager.steadStreamHandler();
            ProxyManager.initProxyCallback(proxyCallback);
        } catch (Throwable e) {
            MLog.e("abc", e);
        }
    }

}
