package com.strkont;

import android.util.Log;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe: Log工具类
 */
public class MLog {
    public static boolean a;

    public static String a(Throwable th) {
        return Log.getStackTraceString(th);
    }

    public static void e(String str, Throwable th) {
        Log.e(str, a(th));
    }
}