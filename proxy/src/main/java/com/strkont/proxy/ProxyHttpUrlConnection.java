package com.strkont.proxy;

import android.annotation.TargetApi;
import android.util.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Permission;
import java.util.List;
import java.util.Map;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
 public class ProxyHttpUrlConnection extends HttpURLConnection implements Closeable {

    private HttpURLConnection httpURLConnection;
    private OutputStream outputStream;
    private InputStream inputStream;

    ProxyHttpUrlConnection(URLConnection uRLConnection, URL url) {
        super(url);
        this.httpURLConnection = (HttpURLConnection) uRLConnection;
    }

    private void flush() throws IOException {
        if (this.outputStream != null) {
            this.outputStream.close();
        }
    }

    @Override public void addRequestProperty(String str, String str2) {
        this.httpURLConnection.addRequestProperty(str, ProxyManager.getRequestProperty(getURL(), str, str2));
    }

    @Override public void close() {
        this.outputStream = null;
        this.inputStream = null;
    }

    @Override public void connect() throws IOException {
        this.httpURLConnection.connect();
    }

    @Override public void disconnect() {
        this.httpURLConnection.disconnect();
    }

    @Override public boolean getAllowUserInteraction() {
        return this.httpURLConnection.getAllowUserInteraction();
    }

    @Override public int getConnectTimeout() {
        return this.httpURLConnection.getConnectTimeout();
    }

    @Override public Object getContent() throws IOException {
        return this.httpURLConnection.getContent();
    }

    @Override public Object getContent(Class[] clsArr) throws IOException {
        return this.httpURLConnection.getContent(clsArr);
    }

    @Override public String getContentEncoding() {
        return this.httpURLConnection.getContentEncoding();
    }

    @Override public int getContentLength() {
        return this.httpURLConnection.getContentLength();
    }

    @TargetApi(24)
    @Override public long getContentLengthLong() {
        return this.httpURLConnection.getContentLengthLong();
    }

    @Override public String getContentType() {
        return this.httpURLConnection.getContentType();
    }

    @Override public long getDate() {
        return this.httpURLConnection.getDate();
    }

    @Override public boolean getDefaultUseCaches() {
        return this.httpURLConnection.getDefaultUseCaches();
    }

    @Override public boolean getDoInput() {
        return this.httpURLConnection.getDoInput();
    }

    @Override public boolean getDoOutput() {
        return this.httpURLConnection.getDoOutput();
    }

    @Override public InputStream getErrorStream() {
        return this.httpURLConnection.getErrorStream();
    }

    @Override public long getExpiration() {
        return this.httpURLConnection.getExpiration();
    }

    @Override public String getHeaderField(int i) {
        return this.httpURLConnection.getHeaderField(i);
    }

    @Override public String getHeaderField(String str) {
        return this.httpURLConnection.getHeaderField(str);
    }

    @Override public long getHeaderFieldDate(String str, long j) {
        return this.httpURLConnection.getHeaderFieldDate(str, j);
    }

    @Override public int getHeaderFieldInt(String str, int i) {
        return this.httpURLConnection.getHeaderFieldInt(str, i);
    }

    @Override public String getHeaderFieldKey(int i) {
        return this.httpURLConnection.getHeaderFieldKey(i);
    }

    @TargetApi(24)
    @Override public long getHeaderFieldLong(String str, long j) {
        return this.httpURLConnection.getHeaderFieldLong(str, j);
    }

    @Override public Map<String, List<String>> getHeaderFields() {
        return this.httpURLConnection.getHeaderFields();
    }

    @Override public long getIfModifiedSince() {
        return this.httpURLConnection.getIfModifiedSince();
    }

    @Override public InputStream getInputStream() throws IOException {
        InputStream inputStream;
        flush();
        synchronized (this) {
            if (this.inputStream == null) {
                this.inputStream = new MyInputStream(this.httpURLConnection.getInputStream(), this);
            }
            inputStream = this.inputStream;
        }
        return inputStream;
    }

    @Override public boolean getInstanceFollowRedirects() {
        return this.httpURLConnection.getInstanceFollowRedirects();
    }

    @Override public long getLastModified() {
        return this.httpURLConnection.getLastModified();
    }

    @Override public OutputStream getOutputStream() throws IOException {
        OutputStream outputStream;
        synchronized (this) {
            if (this.outputStream == null) {
                this.outputStream = new MyOutputStream(this.httpURLConnection.getOutputStream(), this);
            }
            outputStream = this.outputStream;
        }
        return outputStream;
    }

    @Override public Permission getPermission() throws IOException {
        return this.httpURLConnection.getPermission();
    }

    @Override public int getReadTimeout() {
        return this.httpURLConnection.getReadTimeout();
    }

    @Override public String getRequestMethod() {
        String method = httpURLConnection.getRequestMethod();
        Log.e("abc", "request method=" + method);
        return method;
    }

    @Override public Map<String, List<String>> getRequestProperties() {
        return this.httpURLConnection.getRequestProperties();
    }

    @Override public String getRequestProperty(String str) {
        return this.httpURLConnection.getRequestProperty(str);
    }

    @Override public int getResponseCode() throws IOException {
        flush();
        return this.httpURLConnection.getResponseCode();
    }

    @Override public String getResponseMessage() throws IOException {
        flush();
        return this.httpURLConnection.getResponseMessage();
    }

    @Override public URL getURL() {
        return this.httpURLConnection.getURL();
    }

    @Override public boolean getUseCaches() {
        return this.httpURLConnection.getUseCaches();
    }

    @Override public void setAllowUserInteraction(boolean z) {
        this.httpURLConnection.setAllowUserInteraction(z);
    }

    @Override public void setChunkedStreamingMode(int i) {
        this.httpURLConnection.setChunkedStreamingMode(i);
    }

    @Override public void setConnectTimeout(int i) {
        this.httpURLConnection.setConnectTimeout(60000);
    }

    @Override public void setDefaultUseCaches(boolean z) {
        this.httpURLConnection.setDefaultUseCaches(z);
    }

    @Override public void setDoInput(boolean z) {
        this.httpURLConnection.setDoInput(z);
    }

    @Override public void setDoOutput(boolean z) {
        this.httpURLConnection.setDoOutput(z);
    }

    @Override public void setFixedLengthStreamingMode(int i) {
        this.httpURLConnection.setFixedLengthStreamingMode(i);
    }

    @TargetApi(19)
    @Override public void setFixedLengthStreamingMode(long j) {
        this.httpURLConnection.setFixedLengthStreamingMode(j);
    }

    @Override public void setIfModifiedSince(long j) {
        this.httpURLConnection.setIfModifiedSince(j);
    }

    @Override public void setInstanceFollowRedirects(boolean z) {
        this.httpURLConnection.setInstanceFollowRedirects(z);
    }

    @Override public void setReadTimeout(int i) {
        this.httpURLConnection.setReadTimeout(60000);
    }

    @Override public void setRequestMethod(String str) throws ProtocolException {
        Log.e("abc", "request method=" + str);
        this.httpURLConnection.setRequestMethod(str);
    }

    @Override public void setRequestProperty(String str, String str2) {
        this.httpURLConnection.setRequestProperty(str, ProxyManager.getRequestProperty(getURL(), str, str2));
    }

    @Override public void setUseCaches(boolean z) {
        this.httpURLConnection.setUseCaches(z);
    }

    @Override public String toString() {
        return super.toString();
    }

    @Override public boolean usingProxy() {
        return this.httpURLConnection.usingProxy();
    }
}
