package com.strkont.proxy;
import com.strkont.MLog;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLStreamHandler;
import java.util.Hashtable;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
public class ProxyManager {
    static ProxyCallback cb = ProxyCallback.callback;

    static final URL getUrl(URL url) {
        return cb.openConnection(url);
    }

    public static void steadStreamHandler() throws MalformedURLException, IllegalAccessException {
        URL url = new URL("http://www.baidu.com");
        url = new URL("https://www.baidu.com");
        Field field = getField(URL.class, "handlers");
        if (field == null) {
            field = getField(URL.class, "streamHandlers");
        }
        if (field == null) {
            throw new NullPointerException("适配失败");
        }
        //获取该类中的静态字段
        Hashtable hashtable = (Hashtable) field.get(null);
        for (Object next : hashtable.keySet()) {
            URLStreamHandler handler = (URLStreamHandler) hashtable.get(next);
            hashtable.put(next, new MyStreamHandler(handler));
        }
    }

    public static void initProxyCallback(ProxyCallback proxyCallback) {
        synchronized (ProxyManager.class) {
            if (proxyCallback == null) {
                try {
                    proxyCallback = ProxyCallback.callback;
                } catch (Throwable th) {
                    Class cls = ProxyManager.class;
                }
            }
            cb = proxyCallback;
        }
    }

    static final byte[] writeBody(URL url, byte[] bArr) {
        return cb.writeBody(url, bArr);
    }

    static final void readBody(URL url, byte[] bArr) {
        cb.readBody(url, bArr);
    }

    static final String getRequestProperty(URL url, String str, String str2) {
        return cb.addRequestProperty(url, str, str2);
    }

    public static Field getField(Class cls, String str) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Exception e) {
            return null;
        }
    }

    public static Method getMethod(Class cls, String str, Class... clsArr) {
        try {
            Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Throwable e) {
            MLog.e("abc", e);
            return null;
        }
    }

}