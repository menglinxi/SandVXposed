package com.strkont.proxy;

import android.annotation.TargetApi;
import android.util.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.Permission;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
 public final class ProxyHttpsUrlConnection extends HttpsURLConnection implements Closeable {
    private HttpsURLConnection httpsURLConnection;
    private OutputStream outputStream;
    private InputStream inputStream;

    protected ProxyHttpsUrlConnection(URLConnection uRLConnection, URL url) {
        super(url);
        this.httpsURLConnection = (HttpsURLConnection) uRLConnection;
    }

    private void flush() throws IOException {
        if (this.outputStream != null) {
            this.outputStream.close();
        }
    }

    @Override public void addRequestProperty(String str, String str2) {
        this.httpsURLConnection.addRequestProperty(str, ProxyManager.getRequestProperty(getURL(), str, str2));
    }

    @Override public void close() {
        this.outputStream = null;
        this.inputStream = null;
    }

    @Override public void connect() throws IOException {
        this.httpsURLConnection.connect();
    }

    @Override public void disconnect() {
        this.httpsURLConnection.disconnect();
    }

    @Override public boolean getAllowUserInteraction() {
        return this.httpsURLConnection.getAllowUserInteraction();
    }

    @Override public String getCipherSuite() {
        return this.httpsURLConnection.getCipherSuite();
    }

    @Override public int getConnectTimeout() {
        return this.httpsURLConnection.getConnectTimeout();
    }

    @Override public Object getContent() throws IOException {
        return this.httpsURLConnection.getContent();
    }

    @Override public Object getContent(Class[] clsArr) throws IOException {
        return this.httpsURLConnection.getContent(clsArr);
    }

    @Override public String getContentEncoding() {
        return this.httpsURLConnection.getContentEncoding();
    }

    @Override public int getContentLength() {
        return this.httpsURLConnection.getContentLength();
    }

    @TargetApi(24)
    @Override public long getContentLengthLong() {
        return this.httpsURLConnection.getContentLengthLong();
    }

    @Override public String getContentType() {
        return this.httpsURLConnection.getContentType();
    }

    @Override public long getDate() {
        return this.httpsURLConnection.getDate();
    }

    @Override public boolean getDefaultUseCaches() {
        return this.httpsURLConnection.getDefaultUseCaches();
    }

    @Override public boolean getDoInput() {
        return this.httpsURLConnection.getDoInput();
    }

    @Override public boolean getDoOutput() {
        return this.httpsURLConnection.getDoOutput();
    }

    @Override public InputStream getErrorStream() {
        return this.httpsURLConnection.getErrorStream();
    }

    @Override public long getExpiration() {
        return this.httpsURLConnection.getExpiration();
    }

    @Override public String getHeaderField(int i) {
        return this.httpsURLConnection.getHeaderField(i);
    }

    @Override public String getHeaderField(String str) {
        return this.httpsURLConnection.getHeaderField(str);
    }

    @Override public long getHeaderFieldDate(String str, long j) {
        return this.httpsURLConnection.getHeaderFieldDate(str, j);
    }

    @Override public int getHeaderFieldInt(String str, int i) {
        return this.httpsURLConnection.getHeaderFieldInt(str, i);
    }

    @Override public String getHeaderFieldKey(int i) {
        return this.httpsURLConnection.getHeaderFieldKey(i);
    }

    @TargetApi(24)
    @Override public long getHeaderFieldLong(String str, long j) {
        return this.httpsURLConnection.getHeaderFieldLong(str, j);
    }

    @Override public Map<String, List<String>> getHeaderFields() {
        return this.httpsURLConnection.getHeaderFields();
    }

    @Override public HostnameVerifier getHostnameVerifier() {
        return this.httpsURLConnection.getHostnameVerifier();
    }

    @Override public long getIfModifiedSince() {
        return this.httpsURLConnection.getIfModifiedSince();
    }

    @Override public InputStream getInputStream() throws IOException {
        InputStream inputStream;
        flush();
        synchronized (this) {
            if (this.inputStream == null) {
                this.inputStream = new MyInputStream(this.httpsURLConnection.getInputStream(), this);
            }
            inputStream = this.inputStream;
        }
        return inputStream;
    }

    @Override public boolean getInstanceFollowRedirects() {
        return this.httpsURLConnection.getInstanceFollowRedirects();
    }

    @Override public long getLastModified() {
        return this.httpsURLConnection.getLastModified();
    }

    @Override public Certificate[] getLocalCertificates() {
        return this.httpsURLConnection.getLocalCertificates();
    }

    @Override public Principal getLocalPrincipal() {
        return this.httpsURLConnection.getLocalPrincipal();
    }

    
    @Override public OutputStream getOutputStream() throws IOException {
        OutputStream outputStream;
        synchronized (this) {
            if (this.outputStream == null) {
                this.outputStream = new MyOutputStream(this.httpsURLConnection.getOutputStream(), this);
            }
            outputStream = this.outputStream;
        }
        return outputStream;
    }

    @Override public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
        return this.httpsURLConnection.getPeerPrincipal();
    }

    @Override public Permission getPermission() throws IOException {
        return this.httpsURLConnection.getPermission();
    }

    @Override public int getReadTimeout() {
        return this.httpsURLConnection.getReadTimeout();
    }

    @Override public String getRequestMethod() {
        String method = httpsURLConnection.getRequestMethod();
        Log.e("abc", "request method=" + method);
        return method;
    }

    @Override public Map<String, List<String>> getRequestProperties() {
        return this.httpsURLConnection.getRequestProperties();
    }

    @Override public String getRequestProperty(String str) {
        return this.httpsURLConnection.getRequestProperty(str);
    }

    @Override public int getResponseCode() throws IOException {
        flush();
        return this.httpsURLConnection.getResponseCode();
    }

    @Override public String getResponseMessage() throws IOException {
        flush();
        return this.httpsURLConnection.getResponseMessage();
    }

    @Override public SSLSocketFactory getSSLSocketFactory() {
        return this.httpsURLConnection.getSSLSocketFactory();
    }

    @Override public Certificate[] getServerCertificates() throws SSLPeerUnverifiedException {
        return this.httpsURLConnection.getServerCertificates();
    }

    @Override public URL getURL() {
        return this.httpsURLConnection.getURL();
    }

    @Override public boolean getUseCaches() {
        return this.httpsURLConnection.getUseCaches();
    }

    @Override public void setAllowUserInteraction(boolean z) {
        this.httpsURLConnection.setAllowUserInteraction(z);
    }

    @Override public void setChunkedStreamingMode(int i) {
        this.httpsURLConnection.setChunkedStreamingMode(i);
    }

    @Override public void setConnectTimeout(int i) {
        this.httpsURLConnection.setConnectTimeout(i);
    }

    @Override public void setDefaultUseCaches(boolean z) {
        this.httpsURLConnection.setDefaultUseCaches(z);
    }

    @Override public void setDoInput(boolean z) {
        this.httpsURLConnection.setDoInput(z);
    }

    @Override public void setDoOutput(boolean z) {
        this.httpsURLConnection.setDoOutput(z);
    }

    @Override public void setFixedLengthStreamingMode(int i) {
        this.httpsURLConnection.setFixedLengthStreamingMode(i);
    }

    @TargetApi(19)
    @Override public void setFixedLengthStreamingMode(long j) {
        this.httpsURLConnection.setFixedLengthStreamingMode(j);
    }

    @Override public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
        this.httpsURLConnection.setHostnameVerifier(hostnameVerifier);
    }

    @Override public void setIfModifiedSince(long j) {
        this.httpsURLConnection.setIfModifiedSince(j);
    }

    @Override public void setInstanceFollowRedirects(boolean z) {
        this.httpsURLConnection.setInstanceFollowRedirects(z);
    }

    @Override public void setReadTimeout(int i) {
        this.httpsURLConnection.setReadTimeout(i);
    }

    @Override public void setRequestMethod(String str) throws ProtocolException {
        Log.e("abc", "request method=" + str);
        this.httpsURLConnection.setRequestMethod(str);
    }

    @Override public void setRequestProperty(String str, String str2) {
        this.httpsURLConnection.setRequestProperty(str, ProxyManager.getRequestProperty(getURL(), str, str2));
    }

    @Override public void setSSLSocketFactory(SSLSocketFactory sSLSocketFactory) {
        this.httpsURLConnection.setSSLSocketFactory(sSLSocketFactory);
    }

    @Override public void setUseCaches(boolean z) {
        this.httpsURLConnection.setUseCaches(z);
    }

    @Override public String toString() {
        return super.toString();
    }

    @Override public boolean usingProxy() {
        return this.httpsURLConnection.usingProxy();
    }
}