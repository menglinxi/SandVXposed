package com.strkont.proxy;

import android.util.Log;

import com.strkont.MLog;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import javax.net.ssl.HttpsURLConnection;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
public class MyStreamHandler extends URLStreamHandler {
    static final Method METHOD_PARSE_URL = ProxyManager.getMethod(URLStreamHandler.class, "parseURL", URL.class, String.class, Integer.TYPE, Integer.TYPE);
    static final Method METHOD_SET_URL = ProxyManager.getMethod(URLStreamHandler.class, "setURL", URL.class, String.class, String.class, Integer.TYPE, String.class, String.class);
    static final Method METHOD_SET_URL_1 = ProxyManager.getMethod(URLStreamHandler.class, "setURL", URL.class, String.class, String.class, Integer.TYPE, String.class, String.class, String.class, String.class, String.class);
    static final Method METHOD_TO_EXTERNAL_FORM = ProxyManager.getMethod(URLStreamHandler.class, "toExternalForm", URL.class);
    static final Method METHOD_EQUALS = ProxyManager.getMethod(URLStreamHandler.class, "equals", URL.class, URL.class);
    static final Method METHOD_GET_DEFAULT_PORT = ProxyManager.getMethod(URLStreamHandler.class, "getDefaultPort", new Class[0]);
    static final Method METHOD_GET_HOST_ADDRESS = ProxyManager.getMethod(URLStreamHandler.class, "getHostAddress", URL.class);
    static final Method METHOD_HASH_CODE = ProxyManager.getMethod(URLStreamHandler.class, "hashCode", URL.class);
    static final Method METHOD_HOSTS_EQUALS = ProxyManager.getMethod(URLStreamHandler.class, "hostsEqual", URL.class, URL.class);
    static final Method METHOD_SAME_FILE = ProxyManager.getMethod(URLStreamHandler.class, "sameFile", URL.class, URL.class);
    private final URLStreamHandler urlStreamHandler;
    private final Field f;

    public MyStreamHandler(URLStreamHandler uRLStreamHandler) {
        this.urlStreamHandler = uRLStreamHandler;
        Field field = ProxyManager.getField(URL.class, "handler");
        if (field == null) {
            field = ProxyManager.getField(URL.class, "streamHandler");
        }
        this.f = field;
    }

    @Override
    protected URLConnection openConnection(URL url) {
//        Log.e("abc", MyStreamHandler.class.getName() + " openConnection url = " + url.toString());
        try {
            URL a = ProxyManager.getUrl(url);
            Method declaredMethod = URLStreamHandler.class.getDeclaredMethod("openConnection", new Class[]{URL.class});
            declaredMethod.setAccessible(true);
            URLConnection uRLConnection = (URLConnection) declaredMethod.invoke(this.urlStreamHandler, new Object[]{a});
            return (uRLConnection instanceof HttpsURLConnection) ?
                    (new ProxyHttpsUrlConnection(uRLConnection, a)) :
                    (uRLConnection instanceof HttpURLConnection ? new ProxyHttpUrlConnection(uRLConnection, a) : uRLConnection);
        } catch (Throwable e) {
            MLog.e("abc", e);
            return null;
        }
    }

    @Override
    protected URLConnection openConnection(URL url, Proxy proxy) {
        try {
            URL a = ProxyManager.getUrl(url);
            Method declaredMethod = URLStreamHandler.class.getDeclaredMethod("openConnection", new Class[]{URL.class, Proxy.class});
            declaredMethod.setAccessible(true);
            URLConnection uRLConnection = (URLConnection) declaredMethod.invoke(this.urlStreamHandler, new Object[]{a, proxy});
            return uRLConnection instanceof HttpsURLConnection ?
                    new ProxyHttpsUrlConnection(uRLConnection, a) :
                    uRLConnection instanceof HttpURLConnection ? new ProxyHttpUrlConnection(uRLConnection, a) : uRLConnection;
        } catch (Throwable e) {
            MLog.e("abc", e);
            return null;
        }
    }
}