package com.strkont.proxy;

import java.net.URL;
import java.util.HashMap;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
public abstract class ProxyCallback {

    public static final ProxyCallback callback = new ProxyCallback() {

    };
    protected static HashMap<URL, String> mAcceptCharset = new HashMap();
    protected static HashMap<URL, String> mContentEncoding = new HashMap();

    protected String addRequestProperty(URL url, String str, String str2) {
        return str2;
    }

    protected URL openConnection(URL url) {
        return url;
    }

    protected void readBody(URL url, byte[] bArr) {
    }

    protected byte[] writeBody(URL url, byte[] bArr) {
        return bArr;
    }
}
