package com.strkont.proxy;

import com.strkont.MLog;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
public class MyOutputStream extends OutputStream {
    final OutputStream outputStream;
    final Closeable closeable;
    ByteArrayOutputStream baos = null;
    boolean d = false;

    MyOutputStream(OutputStream outputStream, Closeable closeable) {
        this.outputStream = outputStream;
        this.closeable = closeable;
        this.baos = new ByteArrayOutputStream();
    }

    @Override
    public void close() {
        if (!this.d) {
            this.d = true;
            try {
                this.outputStream.write(ProxyManager.writeBody(((URLConnection) this.closeable).getURL(), this.baos.toByteArray()));
                this.outputStream.close();
                this.closeable.close();

            } catch (Throwable e) {
                MLog.e("abc", e);
            }finally {

            }
        }
    }

    @Override
    public void flush() throws IOException {
        super.flush();
    }

    @Override
    public void write(int i) {
        this.baos.write(i);
    }

    @Override
    public void write(byte[] bArr, int i, int i2) {
        this.baos.write(bArr, i, i2);
    }
}
