package com.strkont.proxy;

import com.strkont.MLog;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
 public class MyInputStream extends InputStream {
    InputStream inputStream;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    boolean isClose = false;

    MyInputStream(InputStream inputStream, Closeable closeable){

        byte[] bArr = new byte[4096];

        try {
            while (true) {
                try {
                    int read = inputStream.read(bArr);
                    if (read <= -1) {
                        break;
                    }
                    this.baos.write(bArr, 0, read);
                } catch (Throwable e) {
                    Throwable th = e;
                    bArr = null;
                    Throwable th2 = th;
                }
            }
            this.baos.flush();
            bArr = this.baos.toByteArray();
            ProxyManager.readBody(((URLConnection) closeable).getURL(), bArr);
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
            IOException th2 = e;
            try {
                MLog.e("abc", th2);
            } catch (Throwable th3) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e4) {
                    }
                }
            }
        }
        this.inputStream = new ByteArrayInputStream(bArr);
    }

    @Override public void close() throws IOException {
        if (!this.isClose) {
            this.isClose = true;
            this.inputStream.close();
            this.baos.close();
            super.close();
        }
    }

    @Override public int read() throws IOException {
        return this.inputStream.read();
    }

    @Override public int read(byte[] bArr, int i, int i2) throws IOException {
        return this.inputStream.read(bArr, i, i2);
    }
}
