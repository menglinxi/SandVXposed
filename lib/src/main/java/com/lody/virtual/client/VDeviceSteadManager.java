package com.lody.virtual.client;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;

import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.remote.VDeviceInfo;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import external.org.apache.commons.lang3.ClassUtils;


/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/18
 * className:
 * <p/>
 * describe: 偷天换日，修改设备信息
 */
public class VDeviceSteadManager {

    private static VDeviceSteadManager vManager = new VDeviceSteadManager();

    public static VDeviceSteadManager get(){

        if (vManager == null){
            synchronized (VDeviceSteadManager.class){
                if (vManager == null){
                    vManager = new VDeviceSteadManager();
                }
            }
        }
        return vManager;
    }

    /**
     * hook功能总开关
     * @param context
     * @param deviceInfo
     */
    public void startup(Context context, VDeviceInfo deviceInfo){
        //Feild
//        hookManufacturer(context, deviceInfo);
//        hookModel(context, deviceInfo);

        //CONNECTIVITY_SERVICE
        hookNetWorkType(context, deviceInfo);
        hookSSID(context, deviceInfo);
        hookBSSID(context, deviceInfo);

        hookRelease(context, deviceInfo);
        hookLine1Number(context, deviceInfo);
        //TELEPHONY_SERVICE
        hookSimSerialNumber(context, deviceInfo);

        hookDevice(context, deviceInfo);
        hookSerial(context, deviceInfo);    //serial
        hookDeviceId(context, deviceInfo);  //deviceid
        hookIMSI(context, deviceInfo);      //iccid(imsi)
        hookMacAddress(context, deviceInfo);//mac
        //SETTING
        hookAndroidId0(context, deviceInfo);//android_id
        hookAndroidId1(context, deviceInfo);

        //适配华为Mate10 serail 、udid 、imei(deviceid)、imsi(iccid)、mac、ip
        //hookSerial8(context, deviceInfo);
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O){
//            VLog.e("abc", "current sdk low than android O <sdk=26>");
//            //不能修改sdk版本号.
//            // (不修改设备SDK值的情况下，可调微信支付/网易支付/点数支付/QQ钱包支付/手机充值卡支付/银行卡支付)
//            //但是无法调起支付宝支付
//            hookSDK(context, deviceInfo);
//            hookSDK_INT(context, deviceInfo);
//        }
    }

    private void hookAndroidId1(Context context, VDeviceInfo deviceInfo) {
        runMethodHook1(context, "android.provider.Settings$Secure", "getString", deviceInfo.androidId);
    }

    private void hookAndroidId0(Context context, VDeviceInfo deviceInfo) {
        runMethodHook1(context, "android.provider.Settings$System", "getString", deviceInfo.androidId);
    }

    /**
     * hook MANUFACTURER （厂商）
     * @param context
     * @param deviceInfo
     */
    private void hookManufacturer(Context context, VDeviceInfo deviceInfo) {
        VLog.v("abc", "## stead manufacturter = %s ##", deviceInfo.manufacturer);
        runStaticFeildHook(context, "android.os.Build", "MANUFACTURER", deviceInfo.manufacturer);
    }


    /**
     * hook MODEL （机型）
     * @param context
     * @param deviceInfo
     */
    private void hookModel(Context context, VDeviceInfo deviceInfo) {
        VLog.v("abc", "## stead buildModel = %s ##", deviceInfo.model);
        runStaticFeildHook(context, "android.os.Build","MODEL", deviceInfo.model);
    }

    /**
     * hook SERIAL （序列号）
     * @param context
     * @param deviceInfo
     */
    private void hookSerial(Context context, VDeviceInfo deviceInfo) {
        VLog.v("abc", "## stead buildSerial = %s ##", deviceInfo.serial);
        runStaticFeildHook(context, "android.os.Build", "SERIAL", deviceInfo.serial);
    }

    /**
     * hook DEVICE （品牌）
     * @param context
     * @param deviceInfo
     */
    private void hookDevice(Context context, VDeviceInfo deviceInfo) {
        VLog.v("abc", "## stead buildDevice = %s ##", deviceInfo.brand);
        runStaticFeildHook(context, "android.os.Build", "DEVICE", deviceInfo.brand);
    }

    /**
     * hook RELEASE （发布版本号）
     * @param context
     * @param deviceInfo
     */
    private void hookRelease(Context context, VDeviceInfo deviceInfo) {
        VLog.v("abc", "## stead buildRelease = %s ##", deviceInfo.versionRelease);
        runStaticFeildHook(context, "android.os.Build$VERSION", "RELEASE", deviceInfo.versionRelease);
    }

    /**
     * hook SDK （sdk）
     * @param context
     * @param deviceInfo
     */
    private void hookSDK(Context context, VDeviceInfo deviceInfo) {
        VLog.v("abc", "## stead buildSDK = %s ##", deviceInfo.versionSdk);
        runStaticFeildHook(context, "android.os.Build$VERSION", "SDK", deviceInfo.versionSdk);
    }

    /**
     * hook SDK （sdk）
     * @param context
     * @param deviceInfo
     */
    private void hookSDK_INT(Context context, VDeviceInfo deviceInfo) {
        VLog.v("abc", "## stead buildSDK_INT = %s ##", deviceInfo.versionSdkInt);
        runStaticFeildHook(context, "android.os.Build$VERSION", "SDK_INT",
                deviceInfo.versionSdkInt<=Build.VERSION_CODES.KITKAT ? Build.VERSION_CODES.KITKAT : deviceInfo.versionSdkInt);
    }


    /**
     * hook SERIAL （序列号.android 8以上）
     * @param context
     * @param deviceInfo
     */
    private void hookSerial8(Context context, VDeviceInfo deviceInfo) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            VLog.v("abc", "## stead buildSerial8 = %s ##", deviceInfo.serial);
            runMethodHook(context, "android.os.Build", "getSerial", deviceInfo.serial);
        }
    }

    /**
     * hook deviceId  (IMEI:国际移动设备标识,是手机的识别)
     * @param context
     */
    private void hookDeviceId(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead DeviceId = %s ##", deviceInfo.deviceId);
        runMethodHook(context, "android.telephony.TelephonyManager", "getDeviceId", deviceInfo.deviceId);
    }

    /**
     * hook 手机sim卡号码    (本机号码)
     * * @param context
     */
    private void hookLine1Number(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead Line1Number = %s ##", deviceInfo.line1Number);
        runMethodHook(context, "android.telephony.TelephonyManager", "getLine1Number", deviceInfo.line1Number);
    }

    /**
     * hook IMSI (IMSI:国际移动用户识别码(就是识别你是哪个运营商的SIM卡))
     * @param context
     * @param deviceInfo
     */
    private void hookIMSI(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead imsi = %s ##", deviceInfo.iccId);
        runMethodHook(context, "android.telephony.TelephonyManager", "getSubscriberId", deviceInfo.imsi);
    }

    /**
     * hook SimSerialNumber (ICCID:集成电路卡识别码（固化在手机SIM卡中,就是SIM卡的序列号)
     * @param context
     * @param deviceInfo
     */
    private void hookSimSerialNumber(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead SimSerialNumber = %s ##", deviceInfo.line1Number);
        runMethodHook(context, "android.telephony.TelephonyManager", "getSimSerialNumber", deviceInfo.iccId);
    }

    /**
     * hook NetWorkType
     * @param context
     * @param deviceInfo
     */
    private void hookNetWorkType(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead NetWorkType = %s(default) ##", 1);
        runMethodHook(context, "android.net.NetworkInfo", "getType", 1);
    }

    /**
     * hook SSID
     * @param context
     * @param deviceInfo
     */
    private void hookSSID(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead SSID = %s ##", deviceInfo.ssid);
        runMethodHook(context, "android.net.wifi.WifiInfo", "getSSID", deviceInfo.ssid);
    }

    /**
     * hook BSSID
     * @param context
     * @param deviceInfo
     */
    private void hookBSSID(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead BSSID = %s ##", deviceInfo.bssid);
        runMethodHook(context, "android.net.wifi.WifiInfo", "getBSSID", deviceInfo.bssid);
    }


    /**
     * hook MAC
     * @param context
     * @param deviceInfo
     */
    private void hookMacAddress(Context context, final VDeviceInfo deviceInfo){
        VLog.v("abc", "## stead MacAddress = %s ##", deviceInfo.wifiMac);
        runMethodHook(context, "android.net.wifi.WifiInfo", "getMacAddress", deviceInfo.wifiMac);
    }

    /**
     * hook 方法体
     * @param context
     * @param className
     * @param methodName
     * @param steadValue
     */
    private void runMethodHook(Context context, String className, final String methodName, final Object steadValue){

        try{
            XposedHelpers.findAndHookMethod(
                    context.getClassLoader().loadClass(className),
                    methodName,
                    new XC_MethodHook(){
                        @Override
                        protected void beforeHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                            super.beforeHookedMethod(param);
                        }

                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            super.afterHookedMethod(param);
                            VLog.e("abc", "stead method = %s >> stead value = %s", methodName, steadValue);
                            param.setResult(steadValue);
                        }
                    });

        }
        catch (Throwable throwable){
            VLog.e("abc", throwable.toString());
        }
    }

    /**
     * hook 静态变量
     * @param context
     * @param value
     */
    private void runStaticFeildHook(Context context, String className, String feildName, Object value) {

        try{
            XposedHelpers.setStaticObjectField(
                    context.getClassLoader().loadClass(className), feildName, value);
        }
        catch (Throwable throwable){
            VLog.e("abc", throwable.toString());
        }
    }

    /**
     * hook 方法体
     * @param context
     * @param className
     * @param methodName
     * @param steadValue
     */
    private void runMethodHook1(Context context, String className, final String methodName, final Object steadValue){

        try{
            XposedHelpers.findAndHookMethod(
                    context.getClassLoader().loadClass(className),
                    methodName,
                    ContentResolver.class,
                    String.class,
                    new XC_MethodHook(){
                        @Override
                        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                            super.beforeHookedMethod(param);
                        }

                        @Override
                        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                            super.afterHookedMethod(param);
//                            VLog.e("abc", "stead type = %s >> stead value = %s", methodName, steadValue);
                            param.setResult(steadValue);
                        }
                    });

        }
        catch (Throwable throwable){
            VLog.e("abc", throwable.toString());
        }
    }

}
