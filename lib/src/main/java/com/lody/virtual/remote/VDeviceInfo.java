package com.lody.virtual.remote;

import android.os.Parcel;
import android.os.Parcelable;

import com.lody.virtual.os.VEnvironment;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * @author Lody
 */
public class VDeviceInfo implements Parcelable {

    public String androidId;
    public String bluetoothMac;
    public String brand;
    public String bssid;
    public String deviceId;
    public String gmsAdId;
    public String iccId;
    public String imei1;
    public String imei2;
    public String imsi;
    public String line1Number;
    public String manufacturer;
    public String meid;
    public String model;
    public String serial;
    public String ssid;
    public String versionRelease;
    public String versionSdk;
    public int versionSdkInt;
    public String wifiMac;

    public VDeviceInfo() {
    }

    public VDeviceInfo(Parcel in) {
        androidId = in.readString();
        bluetoothMac = in.readString();
        brand = in.readString();
        bssid = in.readString();
        deviceId = in.readString();
        gmsAdId = in.readString();
        iccId = in.readString();
        imei1 = in.readString();
        imei2 = in.readString();
        imsi = in.readString();
        line1Number = in.readString();
        manufacturer = in.readString();
        meid = in.readString();
        model = in.readString();
        serial = in.readString();
        ssid = in.readString();
        versionRelease = in.readString();
        versionSdk = in.readString();
        versionSdkInt = in.readInt();
        wifiMac = in.readString();
    }

    public static final Creator<VDeviceInfo> CREATOR = new Creator<VDeviceInfo>() {

        public VDeviceInfo get(Parcel parcel) {
            return new VDeviceInfo(parcel);
        }

        public VDeviceInfo[] get(int i) {
            return new VDeviceInfo[i];
        }

        @Override
        public VDeviceInfo createFromParcel(Parcel parcel) {
            return get(parcel);
        }

        @Override
        public VDeviceInfo[] newArray(int size) {
            return get(size);
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(this.deviceId);
        parcel.writeString(this.androidId);
        parcel.writeString(this.wifiMac);
        parcel.writeString(this.bluetoothMac);
        parcel.writeString(this.iccId);
        parcel.writeString(this.serial);
        parcel.writeString(this.gmsAdId);
        parcel.writeString(this.manufacturer);
        parcel.writeString(this.model);
        parcel.writeString(this.versionRelease);
        parcel.writeString(this.line1Number);
        parcel.writeString(this.imei1);
        parcel.writeString(this.imei2);
        parcel.writeString(this.meid);
        parcel.writeString(this.imsi);
        parcel.writeString(this.ssid);
        parcel.writeString(this.bssid);
        parcel.writeString(this.brand);
        parcel.writeString(this.versionSdk);
        parcel.writeInt(this.versionSdkInt);

    }

    public File getWifiFile(int userId) {
        File wifiMacFie = VEnvironment.getWifiMacFile(userId);
        if (!wifiMacFie.exists()) {
            try {
                RandomAccessFile file = new RandomAccessFile(wifiMacFie, "rws");
                file.write((wifiMac + "\n").getBytes());
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return wifiMacFie;
    }

    public File updateWifiMac(int i) {
        File file = VEnvironment.getWifiMacFile(i);
        if (file.exists()) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rws");
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(this.wifiMac);
                stringBuilder.append("\n");
                randomAccessFile.write(stringBuilder.toString().getBytes());
                randomAccessFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

}
