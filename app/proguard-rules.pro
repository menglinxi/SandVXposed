# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/lody/Desktop/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep   class com.amap.api.maps.**{*;}
-keep   class com.autonavi.**{*;}
-keep   class com.amap.api.trace.**{*;}

#定位
-keep class com.amap.api.location.**{*;}
-keep class com.amap.api.fence.**{*;}
-keep class com.autonavi.aps.amapapi.model.**{*;}

#搜索
-keep   class com.amap.api.services.**{*;}

#2D地图
-keep class com.amap.api.maps2d.**{*;}
-keep class com.amap.api.mapcore2d.**{*;}

#导航
-keep class com.amap.api.navi.**{*;}
-keep class com.autonavi.**{*;}

#lazyinject
-ignorewarning
-keepattributes *Annotation*
#保留部分泛型信息，必要!
-keepattributes Signature
#手动启用support keep注解
#http://tools.android.com/tech-docs/support-annotations
-dontskipnonpubliclibraryclassmembers
-keep,allowobfuscation @interface android.support.annotation.Keep

-keep @android.support.annotation.Keep class * {
*;
}

-keepclassmembers class * {
    @android.support.annotation.Keep *;
}
#手动启用Component注解
#http://tools.android.com/tech-docs/support-annotations
-keep,allowobfuscation @interface com.trend.lazyinject.annotation.Component

-keep,allowobfuscation @com.trend.lazyinject.annotation.Component class * {
*;
}

-keepclassmembers,allowobfuscation class * {
    @com.trend.lazyinject.annotation.Provide <methods>;
}

-keepclassmembers class * {
     @com.trend.lazyinject.annotation.Inject <fields>;
}

-keepclassmembers class * {
     @com.trend.lazyinject.annotation.InjectComponent <fields>;
}

-dontwarn javassist.**

# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
###################################################################################
############################VirtualApp代码区域######################################
###################################################################################

-keep class android.app.**{*;}
-keep class android.content.**{*;}
-keep class android.location.**{*;}
-keep class android.util.**{*;}

# 不混淆某个包下的类
-keep class com.lody.virtual.client.NativeEngine {
    *;
}

-keep class com.lody.virtual.client.core.** {
    *;
}

-keep class com.lody.virtual.client.env.** {
    *;
}

-keep class com.lody.virtual.client.fixer.** {
    *;
}

-keep class com.lody.virtual.client.hook.** {
    *;
}

-keep class com.lody.virtual.client.hook.base.** {
    *;
}

###
-keep class com.lody.virtual.client.hook.delegate.** {
    *;
}

-keep class com.lody.virtual.client.hook.providers.** {
    *;
}
###

-keep class com.lody.virtual.client.hook.proxies.** {
    *;
}

###
-keep class com.lody.virtual.client.hook.secondary.** {
    *;
}

-keep class com.lody.virtual.client.hook.utils.** {
    *;
}

-keep class com.lody.virtual.client.interface.** {
    *;
}
###

-keep class com.lody.virtual.client.ipc.** {
     *;
}

-keep class com.lody.virtual.client.natives.** {
    *;
}

###
-keep class com.lody.virtual.client.stub.** {
    *;
}

-keep class com.lody.virtual.helper.** {
    *;
}
###
-keep class com.lody.virtual.helper.compat.** {
    *;
}

-keep class com.lody.virtual.os.** {
    *;
}

-keep class com.lody.virtual.remote.** {
    *;
}

-keep class com.lody.virtual.server.** {
    *;
}

-keep class mirror.** {
    *;
}

-keep class mirror.android.** {
    *;
}

-keep class mirror.com.android.** {
    *;
}

-keep class mirror.Ref** {
    *;
}

-keep class mirror.Method** {
    *;
}

-keep class mirror.java.lang.** {
    *;
}

-keep class mirror.dalvik.system.**{
     *;
}

-keep class mirror.libcore.io.**{
     *;
}

-keep class mirror.** {
    *;
}


-keep class com.lody.virtual.client.hook.providers.MediaProviderHook {
    <methods>;
}
-keep class com.lody.virtual.client.hook.providers.QueryRedirectCursor {
    <methods>;
}
-keep class com.lody.virtual.client.hook.proxies.am.TransactionHandlerStub {
    <methods>;
}
-keep class com.lody.virtual.client.hook.proxies.content.** {
    *;
}
-keep class com.lody.virtual.client.hook.proxies.pm.** {
    *;
}
-keep class com.lody.virtual.helper.utils.EncodeUtils {
    <methods>;
}

-keep class com.lody.virtual.sandxposed.**{
    *;
}
###################################################################################
############################以上为VirtualApp代码区域################################
###################################################################################



###################################################################################
############################第三方代码库不混淆#######################################
###################################################################################

#-dontwarn android.strkont.proxy.**
#-keep class android.strkont.proxy.** {*;}
-dontwarn org.slf4j.**
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-dontwarn org.conscrypt.**
-dontwarn com.noober.menu.**
-dontwarn com.liulishuo.filedownloader.**
-dontwarn com.beardedhen.**
-dontwarn com.yinglan.alphatabs.**
-dontwarn com.github.CymChad.**
-dontwarn me.drakeet.materialdialog.**
-dontwarn com.github.bumptech.glide.**
-dontwarn com.afollestad.material-dialogs.**
-dontwarn io.virtualapp.**
-dontwarn me.weishu.epic.**

# rxjava2
-keep class rx.schedulers.Schedulers {
    public static <methods>;
}
-keep class rx.schedulers.ImmediateScheduler {
    public <methods>;
}
-keep class rx.schedulers.TestScheduler {
    public <methods>;
}
-keep class rx.schedulers.Schedulers {
    public static ** test();
}

#okhttp3
-dontwarn okhttp3.logging.**
-keep class okhttp3.internal.**{*;}
-dontwarn okio.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-keep class com.swift.sandhook.**{*;}
-keep interface com.swift.sandhook.** { *; }
-keep class com.trend.lazyinject.**{*;}
-keep interface com.trend.lazyinject.** { *; }

-dontwarn okhttp3.**

-keep class com.chad.library.adapter.** {
*;
}
-keep public class * extends com.chad.library.adapter.base.BaseQuickAdapter
-keep public class * extends com.chad.library.adapter.base.BaseViewHolder
-keepclassmembers  class **$** extends com.chad.library.adapter.base.BaseViewHolder {
     <init>(...);
}

-keep class com.google.**{*;}
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keep public class * implements java.io.Serializable {*;}

-keepclassmembers class * {
    public void *ButtonClicked(android.view.View);
}

#-keep class com.noober.menu.** {*;}
-keep class com.liulishuo.filedownloader.** {*;}
#-keep class com.beardedhen.** {*;}
#-keep class com.yinglan.alphatabs.** {*;}
#-keep class com.github.CymChad.** {*;}
#-keep class com.github.bumptech.** {*;}
#-keep class com.getkeepsafe.relinker.** {*;}
#-keep class me.drakeet.materialdialog.** {*;}
#-keep class com.github.bumptech.glide.** {*;}
#-keep class com.afollestad.material-dialogs.** {*;}
-keep class me.weishu.epic.** {*;}
#-keep class me.weishu.exposed.** {*;}
#-keep class me.zhanghai.android.** {*;}
#-keep class com.afollestad.** {*;}

#butterknife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}
-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

#bugly
-dontwarn com.tencent.bugly.**
-keep public class com.tencent.bugly.** {*;}

#####################################################
############## 星空助手业务模块  ######################
#####################################################

-keep class io.virtualapp.dev.** {*;}
-keep class io.virtualapp.down.** {*;}
-keep class io.virtualapp.abs.** {*;}
#-keep class io.virtualapp.delegate.**
-keep class io.virtualapp.entity.** {*;}
-keep class io.virtualapp.home.entity.** {*;}
#-keep class io.virtualapp.GameTool.** {*;}
#-keep class io.virtualapp.user.**
#-keep class io.virtualapp.VA.** {*;}
#-keep class io.virtualapp.home.adapter.**
-keep class io.virtualapp.home.gifts.**
#-keep class io.virtualapp.home.models.**
#-keep class io.virtualapp.home.callback.** {*;}
#-keep class com.strkont.**
-keep class andhook.lib.** {*;}
-keep class io.virtualapp.delegate.db.** {*;}

#-keep class io.virtualapp.view.** {*;}

# 加密工具类
-keep class io.virtualapp.tools.AESUtil {
    <fields>;
    <methods>;
}
-keep class io.virtualapp.tools.PhoneInfoCollect{
        <fields>;
        <methods>;
}


#中文混淆
-classobfuscationdictionary ./proguard-keys.txt
-packageobfuscationdictionary ./proguard-keys.txt
-obfuscationdictionary ./proguard-keys.txt
##把你的代码以及所使用到的各种第三方库代码统一移到同一个包下，加上包名可以更好的保护不能被混淆的类
#-repackageclasses newhome

####################################################


##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature


# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }


# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

##---------------End: proguard configuration for Gson  ----------

