package io.hook;

import android.annotation.SuppressLint;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.lody.virtual.helper.utils.VLog;
import com.service.MyMessageCode;
import com.service.MyMessengerReq;
import com.swift.sandhook.annotation.HookClass;
import com.swift.sandhook.annotation.HookMethod;
import com.swift.sandhook.annotation.HookMethodBackup;
import com.swift.sandhook.annotation.MethodParams;
import com.swift.sandhook.annotation.Param;
import com.swift.sandhook.annotation.ThisObject;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import io.virtualapp.VApp;


@HookClass(MediaRecorder.class)
public class HookMediaRecorder {
    private static Map<Integer, MediaRecordAudioParams> mParamsMap = new HashMap();

    private static class MediaRecordAudioParams {
        private Integer mAudioSource;
        private Integer mOutputFormat;
        private String mOutputFile;
        private Integer mAudioEncoder;
    }

    @SuppressLint("HandlerLeak")
    private static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.d("lhh", "Message coming");
            switch (msg.what) {
                case MyMessageCode.MYSERVICE_WHAT_MEDIARECORD_STARTED: {
                    VLog.d("lhh", "MYSERVICE_WHAT_MEDIARECORD_STARTED");
                    break;
                }
                case MyMessageCode.MYSERVICE_WHAT_MEDIARECORD_STOPPED: {
                    VLog.d("lhh", "MYSERVICE_WHAT_MEDIARECORD_STOPPED");
                    Bundle data = msg.getData();
                    String filePath = data.getString("param1");
                    VLog.d("lhh", "File Path = " + filePath);
                    break;
                }
                case MyMessageCode.MYSERVICE_WHAT_MEDIARECORD_ERROR_OCCURS: {
                    VLog.d("lhh", "MYSERVICE_WHAT_MEDIARECORD_ERROR_OCCURS");
                    break;
                }

            }
        }
    };

    @HookMethodBackup("start")
    static Method startBackup;

    @HookMethod("start")
    public static void start(@ThisObject MediaRecorder thiz) throws Throwable {
        VLog.e("abc", "hooked MediaRecorder Start success " + thiz + " messenger = " + VApp.getApp().mHomeServiceMessenger);
        MediaRecordAudioParams aParam = mParamsMap.get(thiz.hashCode());
        doRecord(aParam.mOutputFile, aParam.mOutputFormat, aParam.mAudioSource, aParam.mAudioEncoder);
//        SandHook.callOriginByBackup(startBackup, thiz);
    }

    @HookMethod("release")
    public static void insteadRelease(@ThisObject MediaRecorder thiz) {
        final String methodName = "release";

    }

    @HookMethod("stop")
    public static void insteadStop(@ThisObject MediaRecorder thiz) {
        final String methodName = "stop";
        mParamsMap.remove(thiz.hashCode());
        doStop();
        VLog.e("lhh", "let replace,stead method = %s ", methodName);

    }



    @HookMethod("setAudioSource")
    @MethodParams(int.class)
    public static void insteadSetAudioSource(@ThisObject MediaRecorder thiz, @Param int p) {

        setParamAudioSource(thiz, p);
        VLog.e("lhh", "let replace,stead method = %s ", "setAudioSource");

    }

    @HookMethod("setOutputFormat")
    @MethodParams(int.class)
    public static void insteadSetOutputFormat(@ThisObject MediaRecorder thiz, @Param int p) {

        setParamOutputFormat(thiz, p);
        VLog.e("lhh", "let replace,stead method = %s ", "setOutputFormat");

    }

    @HookMethod("setOutputFile")
    @MethodParams(String.class)
    public static void insteadSetOutputFile(@ThisObject MediaRecorder thiz, @Param String p) {

        setParamOutputFile(thiz, p);
        VLog.e("lhh", "let replace,stead method = %s ", "setOutputFile");

    }

    @HookMethod("setAudioEncoder")
    @MethodParams(int.class)
    public static void insteadSetAudioEncoder(@ThisObject MediaRecorder thiz, @Param int p) {

        setParamAudioEncoder(thiz, p);
        VLog.e("lhh", "let replace,stead method = %s ", "setAudioEncoder");

    }

    @HookMethod("prepare")
    public static void insteadPrepare(@ThisObject MediaRecorder thiz) {

    }


    static void doStop() {
        Message msg = Message.obtain(null, MyMessageCode.SERVICE_ORDER_MEDIARECORDER_STOP);
        msg.replyTo = VApp.getApp().mHomeServiceMessenger;
        try {
            VApp.getApp().mHomeServiceMessenger.send(msg);
            VLog.d("lhh", "Send Stop Messenger");
        } catch (RuntimeException re) {
            re.printStackTrace();
            VLog.d("lhh", "Send Messenger Error occur " + re.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            VLog.d("lhh", "Send Messenger Error occur " + e.getMessage());
        }

    }


    static void doRecord(String filePath, int outputFormat, int audioSource, int audioEncoder) {
        try {
            Message msg = Message.obtain(null, MyMessageCode.SERVICE_ORDER_MEDIARECORDER_START);
            msg.replyTo = VApp.getApp().mHomeServiceMessenger;

            Bundle data = new Bundle();

            data.putString("OutputFile", filePath);
            data.putInt("OutputFormat", outputFormat);
            data.putInt("AudioSource", audioSource);
            data.putInt("AudioEncoder", audioEncoder);

            MyMessengerReq msgReq = new MyMessengerReq();
            msgReq.setClientHandler(mHandler);
            //data.putSerializable("req",msgReq);
            msg.setData(data);

            VApp.getApp().mHomeServiceMessenger.send(msg);
            VLog.d("lhh", "Send Start Messenger");

        } catch (RuntimeException re) {
            re.printStackTrace();
            VLog.d("lhh", "Send Messenger Error occur " + re.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            VLog.d("lhh", "Send Messenger Error occur " + e.getMessage());
        }

    }

    private static void setParamAudioSource(MediaRecorder obj, int audioSource) {
        MediaRecordAudioParams param = mParamsMap.get(obj.hashCode());
        if (param == null) {
            param = new MediaRecordAudioParams();
            mParamsMap.put(obj.hashCode(), param);
        }
        param.mAudioSource = audioSource;

    }

    private static void setParamOutputFormat(MediaRecorder obj, int outputFormat) {
        MediaRecordAudioParams param = mParamsMap.get(obj.hashCode());
        if (param == null) {
            param = new MediaRecordAudioParams();
            mParamsMap.put(obj.hashCode(), param);
        }
        param.mOutputFormat = outputFormat;
    }

    private static void setParamOutputFile(MediaRecorder obj, String outputFile) {
        MediaRecordAudioParams param = mParamsMap.get(obj.hashCode());
        if (param == null) {
            param = new MediaRecordAudioParams();
            mParamsMap.put(obj.hashCode(), param);
        }
        param.mOutputFile = outputFile;
    }

    public static void setParamAudioEncoder(MediaRecorder obj, int audioEncoder) {
        MediaRecordAudioParams param = mParamsMap.get(obj.hashCode());
        if (param == null) {
            param = new MediaRecordAudioParams();
            mParamsMap.put(obj.hashCode(), param);
        }
        param.mAudioEncoder = audioEncoder;
    }
}
