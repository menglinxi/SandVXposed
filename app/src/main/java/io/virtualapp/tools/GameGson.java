package io.virtualapp.tools;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import io.virtualapp.entity.GameEntity;
import io.virtualapp.home.entity.HotGame;

import static io.virtualapp.home.WebConst.webUrlForGameList;
import static io.virtualapp.home.WebConst.webUrlForGameSearch;
import static io.virtualapp.home.WebConst.webUrlForHostGames;

/**
 * Created by chao on 2018/3/17.
 * 将json数据转换为Game实体类
 */
public class GameGson {

    // 静态的游戏数据，用于下载安装等
    private static List<GameEntity> list;
    private static int pageCount = 0;

    /**
     * 获取此页面的游戏数据
     * @param page
     * @return
     * @throws Exception
     */
    public static List<GameEntity> getAllGame(int page, int channelid) throws Exception {
        LogUtils.e("page --" + page);
        page = page > 0 ? page : 1;
        String webUrl = webUrlForGameList + "?page=" +  page  + "&channelid=" + channelid;
        list = new ArrayList<>();
        Gson gson = new Gson();
        String json = readUrl(webUrl);
        LogUtils.e("GameList","GamelistUrl = " + webUrl
                + "\nCurrentThread : " + Thread.currentThread().getName()
                + "\ngames json：\n" + json);
        JsonElement je = new JsonParser().parse(json);
        JsonElement items1 = je.getAsJsonObject().get("data");
        JsonElement items2 = items1.getAsJsonObject().get("pager");
        JsonElement items4 = items2.getAsJsonObject().get("dataList");
        JsonArray jsonArray = items4.getAsJsonArray();
        for (JsonElement js : jsonArray) {
            //将Json数据对象化
            GameEntity vGame = gson.fromJson(js, GameEntity.class);
            list.add(vGame);
        }
        return list;
    }

    public static List<GameEntity> getSearchGame(int page, int channelid, String key) throws Exception {

        String contnent = URLEncoder.encode(key, "UTF-8");
        String webUrl = webUrlForGameSearch + "?page=" +  page  + "&channelid=" + channelid + "&key=" + contnent;
        list = new ArrayList<>();
        Gson gson = new Gson();
        String json = readUrl(webUrl);
        LogUtils.e("SearchGame","searchUrl=" + webUrl
                + "\nCurrentThread : " + Thread.currentThread().getName()
                + "\nsearch json：\n" + json);
        JsonElement je = new JsonParser().parse(json);
        JsonElement items1 = je.getAsJsonObject().get("data");
        JsonElement items2 = items1.getAsJsonObject().get("pager");
        JsonElement items4 = items2.getAsJsonObject().get("dataList");
        JsonArray jsonArray = items4.getAsJsonArray();
        for (JsonElement js : jsonArray) {
            //将Json数据对象化
            GameEntity vGame = gson.fromJson(js, GameEntity.class);
            list.add(vGame);
        }

        return list;
    }

    public static List<HotGame> getHostGames() throws Exception {
        String webUrl = webUrlForHostGames;
        List<HotGame> list = new ArrayList<>();
        Gson gson = new Gson();
        String json = readUrl(webUrl);
        LogUtils.e("HostGames","hotGamesUrl = " + webUrl
                + "\nCurrentThread : " + Thread.currentThread().getName()
                + "\njson =" + json);

        JsonElement je = new JsonParser().parse(json);
        JsonElement items1 = je.getAsJsonObject().get("data");
        JsonElement items2 = items1.getAsJsonObject().get("hotgames");
        JsonArray jsonArray = items2.getAsJsonArray();
        for (JsonElement js : jsonArray) {
            //将Json数据对象化
            HotGame hGame = gson.fromJson(js, HotGame.class);
            list.add(hGame);
        }
        return list;
    }


    /**
     * 获得游戏列表页数
     *
     * @return
     */
    public static int getPageCount() {
        //已获取到页面数，则直接返回
        if (pageCount > 0) {
            return pageCount;
        }

        //线程中获取页面数
        new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    String webUrl = webUrlForGameList +"0";
                    String json = readUrl(webUrl);
                    JsonElement je = new JsonParser().parse(json);
                    JsonElement items1 = je.getAsJsonObject().get("data");
                    JsonElement items2 = items1.getAsJsonObject().get("pager");
                    JsonElement items3 = items2.getAsJsonObject().get("pager");
                    JsonElement items4 = items3.getAsJsonObject().get("pageCount");
                    pageCount = items4.getAsInt();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        return pageCount;

    }

    /**
     * 读取Url中的数据
     *
     * @param urlString
     * @return
     * @throws Exception
     */
    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
