package io.virtualapp.tools;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.LocaleList;
import android.provider.Settings;
import android.support.annotation.Keep;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.lody.virtual.helper.utils.VLog;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import io.virtualapp.VApp;


/**
 * @Author:$陈海楠
 * @Date:18-3-13
 * @Description:需要ACCESS_WIFI_STATE;READ_PHONE_STATE权限，使用时直接调用类的静态方法，返回一个包含类所有字段的Map
 */

@Keep
public class PhoneInfoCollect {

    private Context context;

    private static String TAG = "PhoneInfoCollect";

    private static PhoneInfoCollect phoneInfoCollect;

    //构建版本增量
    public String buildVersionIncremental = null;
    //软件当前版本号
    public String buildVersionRelease = null;
    //构建的SDK版本号
    public int buildVersionSdkInt = 0;
    //当前版本的名称
    public String buildVersionCodeName = null;
    //构建版本的SDK版本
    public String buildVersionSdk = null;
    //设置安全安卓ID
    public String settingsSecureAndroidId = null;  //不知道是啥玩意
    //设备ID
    public String telephonyGetDeviceId = null;
    //设备软件版本
    public String telephonyGetDeviceSoftwareVersion = null;
    //手机卡1电话号码
    public String telephonyGetLine1Number = null;
    //网络国家ISO
    public String telephonyGetNetworkCountryISO = null;
    //网络运营商
    public String telephonyGetNetworkOperator = null;
    //网络运营商名称
    public String telephonyGetNetworkOperatorName = null;
    //网络类型
    public String telephonyGetNetworkType = null;
    //手机类型
    public String telephonyGetPhoneType = null;
    //手机卡国家ISO
    public String telephonyGetSimCountryISO = null;
    //sim发行商
    public String telephonyGetSimOperator = null;
    //sim发行商名称
    public String telephonyGetSimOperatorName = null;
    //sim卡序列号
    public String telephonyGetSimSerialNumber = null;
    //sim卡状态
    public String telephonyGetSimState = null;
    //用户ID
    public String telephonyGetSubscriberId = null;
    //网络代理
    public String webUserAgent = null;
    //IMEI
    public String IMEI1 = null;
    public String IMEI2 = null;
    public String MEID = null;
    public String ANDROID_ID = null;

    //wifi名称,路由器广播地址
    public String wifiInfoGetSSID = null;
    //wifi的mac地址
    public String wifiInfoGetBSSID = null;
    //获取手机mac地址
    public String wifiInfoGetMacAddress = null;
    //获取网络ID
    public String wifiInfoGetNetworkId = null;
    //获取IP地址
    public String wifiInfoGetIpAddress = null;
    //显示DPI
    public String displayDpi = null;
    //cpu信息
    public String systemCpuInfo = null;
    //系统语言
    public String systemLanguage = null;
    //手机品牌
    public String phoneBrand = null;
    //手机型号
    public String phoneModel = null;
    //Manufacturer码
    public String phoneManufacturer = null;
    public String serial = null;


    private PhoneInfoCollect(Context context) {
        this.context = context;
        getBuildInfo();
        getTelephonyNetWorkInfo();
        getWifiInfo();
        getOthers();
    }

    public static PhoneInfoCollect get() {
        if (phoneInfoCollect == null) {
            phoneInfoCollect = new PhoneInfoCollect(VApp.getApp());
        }
        return phoneInfoCollect;
    }

    /**
     * 获取软件构建信息
     */
    public void getBuildInfo() {
        this.buildVersionIncremental = Build.VERSION.INCREMENTAL;
        //应该是获取软件的版本号
        try {
            PackageInfo packageInfo = context.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            this.buildVersionRelease = packageInfo.versionCode + "";  //当前软件的版本号
            this.buildVersionCodeName = packageInfo.versionName; //当前软件版本号名称
        } catch (PackageManager.NameNotFoundException e) {

        }
        this.buildVersionSdkInt = Build.VERSION.SDK_INT;
        this.buildVersionSdk = Build.VERSION.SDK;
    }

    /**
     * 获取手机移动网络信息
     */
    public void getTelephonyNetWorkInfo() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            this.telephonyGetDeviceId = telephonyManager.getDeviceId();
            this.telephonyGetDeviceSoftwareVersion = Build.VERSION.RELEASE;
            this.telephonyGetLine1Number = telephonyManager.getLine1Number();
            this.telephonyGetNetworkCountryISO = telephonyManager.getNetworkCountryIso();
            this.telephonyGetNetworkOperator = telephonyManager.getNetworkOperator();
            this.telephonyGetNetworkOperatorName = telephonyManager.getNetworkOperatorName();
            this.telephonyGetNetworkType = telephonyManager.getNetworkType() + "";
            this.telephonyGetPhoneType = telephonyManager.getPhoneType() + "";
            this.telephonyGetSimCountryISO = telephonyManager.getSimCountryIso();
            this.telephonyGetSimOperator = telephonyManager.getSimOperator();
            this.telephonyGetSimOperatorName = telephonyManager.getSimOperatorName();
            this.telephonyGetSimSerialNumber = telephonyManager.getSimSerialNumber();
            this.telephonyGetSimState = telephonyManager.getSimState() + "";
            this.telephonyGetSubscriberId = telephonyManager.getSubscriberId();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                this.IMEI1 = telephonyManager.getImei(TelephonyManager.PHONE_TYPE_NONE);
                this.IMEI2 = telephonyManager.getImei(TelephonyManager.PHONE_TYPE_GSM);
                this.MEID = telephonyManager.getMeid();
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                this.IMEI1 = telephonyManager.getDeviceId(TelephonyManager.PHONE_TYPE_NONE);
                this.IMEI2 = telephonyManager.getDeviceId(TelephonyManager.PHONE_TYPE_GSM);
                this.MEID = telephonyManager.getDeviceId(TelephonyManager.PHONE_TYPE_CDMA);
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Class clazz = telephonyManager.getClass();
                Method getImeiMethod = clazz.getDeclaredMethod("getImei", int.class);
                this.IMEI1 = (String) getImeiMethod.invoke(telephonyManager, TelephonyManager.PHONE_TYPE_NONE);
                this.IMEI2 = (String) getImeiMethod.invoke(telephonyManager, TelephonyManager.PHONE_TYPE_GSM);
                this.MEID = (String) getImeiMethod.invoke(telephonyManager, TelephonyManager.PHONE_TYPE_CDMA);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                this.webUserAgent = telephonyManager.getMmsUserAgent();
            }
        } catch (Exception e) {

        }
    }

    /**
     * 获取wifi信息
     */
    public void getWifiInfo() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        this.wifiInfoGetSSID = wifiInfo.getSSID();
        this.wifiInfoGetBSSID = wifiInfo.getBSSID();
        this.wifiInfoGetMacAddress = wifiInfo.getMacAddress();
        this.wifiInfoGetNetworkId = wifiInfo.getNetworkId() + "";
        this.wifiInfoGetIpAddress = getNetworkIpAddress();
    }

    public String getNetworkIpAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = isIPv4Address(sAddr);
                        if (isIPv4) {
                            this.wifiInfoGetIpAddress = sAddr;
                            return this.wifiInfoGetIpAddress;
                        }
                    }
                }
            }
        } catch (Exception e) {
            VLog.e("abc", e);
        }
        return null;
    }

    private static boolean isIPv4Address(String input) {
        Pattern IPV4_PATTERN = Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");
        return IPV4_PATTERN.matcher(input).matches();
    }

    public static String intIP2StringIP(int ip) {
        return (ip & 0xFF) + "." +
                ((ip >> 8) & 0xFF) + "." +
                ((ip >> 16) & 0xFF) + "." +
                (ip >> 24 & 0xFF);
    }

    /*
     *其余杂项
     */
    public void getOthers() {
        //获取设备显示DPI
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.displayDpi = displayMetrics.densityDpi + "";
        //获取cpu名称
        FileReader fr = null;
        BufferedReader reader = null;
        List<String> cpuInfo = new ArrayList<>();
        try {
            fr = new FileReader("/proc/cpuinfo");
            reader = new BufferedReader(fr);
            String line = "";
            while ((line = reader.readLine()) != null) {
                cpuInfo.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.systemCpuInfo = cpuInfo.get(cpuInfo.size() - 1);
        //获取系统语言和地区
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = LocaleList.getDefault().get(0);
        } else locale = Locale.getDefault();

        this.systemLanguage = locale.getLanguage() + "-" + locale.getCountry();

        this.phoneBrand = Build.BRAND;

        this.phoneModel = Build.MODEL;

        this.phoneManufacturer = Build.MANUFACTURER;

        this.serial = Build.VERSION.SDK_INT<=26 ? Build.SERIAL : Build.getSerial();


        this.ANDROID_ID = Settings.System.getString(this.context.getContentResolver(), Settings.Secure.ANDROID_ID);
        VLog.e("abc", "no: deviceId = %s --> imei1 = %s --> imei2 = %s --> meid = %s --> androidId = %s",
                this.telephonyGetDeviceId, this.IMEI1, this.IMEI2, this.MEID, this.ANDROID_ID);
    }

    /**
     * 返回收集到的信息
     */
    public static Map returnPhoneInfo(Context context) {
        Map<String, String> phoneInfo = new HashMap();   //要返回的Map
        PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();   //实例化类获取信息
        Field[] fields = phoneInfoCollect.getClass().getFields();    //反射获取类对象中的所有字段到一个集合中
        Log.d(TAG, "returnPhoneInfo: " + fields.length);   //打印测试反射获取是否成功
        for (Field field : fields) {
            //遍历所有依次取出所有字段
            //field.setAccessible(true);   //设置当前对象可访问
            try {
                //如果字段值为null或为“”则不存入Map中
                if (field.get(phoneInfoCollect) != null && !"".equals(field.get(phoneInfoCollect).toString())) {
                    phoneInfo.put(field.getName(), field.get(phoneInfoCollect).toString());  //将字段的名称作为key值，值作为value存入Map中
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return phoneInfo;
    }

}
