package io.virtualapp.tools;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.lody.virtual.client.core.VirtualCore;

import java.text.DecimalFormat;

import io.virtualapp.down.DownloadUtil;
import io.virtualapp.home.activity.HomeActivity;
import io.virtualapp.home.callback.GameDownloadCallback;

/**
 * Created by chao on 2018/3/17.
 */

public class GameDownLoad {

    private static int taskId;
    private static int taskId1;
    String urlString;
    private static Context context;

    public static void setContext(Context context) {
        GameDownLoad.context = context;
    }

    /**
     * 使用陈海南提供的下载工具
     *
     * @param downloadUrl
     */
    public static void download(String downloadUrl, boolean isPause, GameDownloadCallback callback, int position) {

        DownloadUtil.downSingleTask(downloadUrl, isPause, new FileDownloadListener() {
            //task.getPath();可以获取当前下载任务的保存路径
            //task.getSpeed();可以获取当前下载的任务的实时下载速度，如果任务完成则返回下载平均速度
            //task.getId();用于获取下载任务在下载数据库中的唯一标识，暂停以及取消，删除都需要该ID
            //创建任务之前请注意网络和SD卡权限
            @Override
            protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                //开始下载
                show("准备下载游戏 ...");
            }

            @Override
            protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                //soFarBytes当前已经下载字节数, totalBytes下载文件总字节数
                //下载进度 soFarBytes*100/totalBytes
                // int progress = (int) ((soFarBytes * 100) / totalBytes);//直接使用会溢出
                LogUtils.e("Thread -->" + Thread.currentThread().getName());
                float progress = ((float) soFarBytes / totalBytes) * 100;
                taskId1 = task.getId();
                if (taskId==0 || taskId1==taskId){
                    callback.update(progress);
                }
                taskId = taskId1;

                DecimalFormat df = new DecimalFormat("0.00");
                String str = df.format(progress);
                Log.d("游戏下载", "下载进度：" + str + "%," + soFarBytes + "(" + totalBytes + ")");
                show( str + "%");
            }

            @Override
            protected void completed(BaseDownloadTask task) {
                //下载完成
                Log.d("游戏下载", "下载完成！");
                show("下载完成,正在安装...");
                try {
                    callback.complete();
                    VirtualCore.get().installPackage(task.getPath(), 0);
                    show("游戏安装完成！");
                } catch (Exception e) {
                    show("游戏安装失败！");
                }
            }

            @Override
            protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
                //下载暂停
                //soFarBytes当前已经下载字节数, totalBytes下载文件总字节数
                //下载进度 soFarBytes*100/totalBytes
                show("暂停下载");
                float progress = ((float) soFarBytes / totalBytes) * 100;
                callback.pause(progress);
            }

            @Override
            protected void error(BaseDownloadTask task, Throwable e) {
                //下载出现错误,
                //e抛出错误
                e.printStackTrace();
                String errorString = "下载失败";
                if (e.toString().contains("FileDownloadOutOfSpaceException")) {
                    errorString += ",储存空间不足!";
                } else if (e.toString().contains("UnknownHostException")) {
                    errorString += ",网络错误!";
                } else {
                    errorString += ",请检查应用权限!";
                }

                callback.error(errorString);
                show(errorString);
            }

            @Override
            protected void warn(BaseDownloadTask task) {
                //在下载队列中(正在等待/正在下载)已经存在相同下载链接与相同存储路径的任务,如果任务未完成则会自动下载
                show("正在继续下载...");
            }
        });
    }

    /**
     * 通过广播的方式显示下载信息
     */
    public static void show(String string) {
        try {
            Intent intent = new Intent();
            intent.setAction(HomeActivity.ACTION_UPDATEUI);
            intent.putExtra("pre", string);
            //发送广播
            context.sendBroadcast(intent);
        } catch (Exception e) {
            Log.d("游戏下载进度显示", "显示失败！");
        }
    }
}
