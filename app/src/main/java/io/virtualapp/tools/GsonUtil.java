package io.virtualapp.tools;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil {
    private static GsonUtil instance;
    private Gson gson;

    public Gson getGson() {
        return gson;
    }

    public static GsonUtil getInstance() {
        if (instance == null)
            instance = new GsonUtil();
        return instance;
    }

    private GsonUtil() {
        // gson = new GsonBuilder().registerTypeAdapter(Date.class,new
        // StringToDateTypeAdapter()).disableHtmlEscaping().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        gson = new GsonBuilder().disableHtmlEscaping()
                .setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    }

    public static <T> T convertJsonWithoutException(String json, Class clazz) {
        try {
            return (T) new Gson().fromJson(json, clazz);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
