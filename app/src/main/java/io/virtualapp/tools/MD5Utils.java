package io.virtualapp.tools;

import com.lody.virtual.helper.utils.VLog;

import java.security.MessageDigest;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/16
 * className:
 * <p/>
 * describe:
 */
public final class MD5Utils {
    private MD5Utils() {
    }

    public static String MD5(String str) {
        try {
            return a(MessageDigest.getInstance("MD5").digest(str.getBytes("utf-8")));
        } catch (Throwable e) {
            VLog.e("abc", e);
            return null;
        }
    }

    private static String a(byte[] bArr) {
        char[] toCharArray = "0123456789ABCDEF".toCharArray();
        StringBuilder stringBuilder = new StringBuilder(bArr.length * 2);
        for (int i = 0; i < bArr.length; i++) {
            stringBuilder.append(toCharArray[(bArr[i] >> 4) & 15]);
            stringBuilder.append(toCharArray[bArr[i] & 15]);
        }
        return stringBuilder.toString();
    }
}
