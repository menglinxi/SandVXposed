package io.virtualapp.tools;

import android.support.annotation.Keep;
import android.util.Base64;

import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

@Keep
public class AESUtil {

    public static final String charset = "UTF-8";
    private static String transformation = "AES/CBC/PKCS5Padding";
    private static String algorithm = "AES";
    private static final byte[] ASE_KEY = {18, 52, 86, 120, 18, 52, 86, 120};

    public static String encrypt(String content, String key) {
        try {
            return encrypt(content.trim().getBytes(charset), key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String encrypt(byte[] content, String key) {
        try {
            byte[] rawBytes = getRawKey(content);
            SecretKeySpec skey = new SecretKeySpec(rawBytes, algorithm);
//            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), algorithm);
//            IvParameterSpec iv = new IvParameterSpec(key.getBytes());
            Cipher cipher = Cipher.getInstance(transformation);
            // 初始化
            cipher.init(Cipher.ENCRYPT_MODE, skey);
//            return Base64.encodeToString(encryptShifts(cipher.doFinal(content)), Base64.NO_WRAP);     //yin
            return new String(Base64.encode(cipher.doFinal(content), Base64.NO_WRAP));                  //jianshu
//            return Base64.getEncoder().encodeToString(encryptShifts(cipher.doFinal(content)));        //java
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     *
     * @param encryptData
     * @return
     */
    public static String decrypt(String encryptData, String key) {
        try {
            return decrypt(encryptData.getBytes(charset), key);
//            return decrypt(encryptData, key);
        } catch (Exception e) {
            return "";
        }
    }

    public static String decrypt(byte[] encryptData, String key) {
        try {
            byte[] tmp = decryptShifts(Base64.decode(encryptData, Base64.NO_WRAP));
//            byte[] tmp = decryptShifts(Base64.getDecoder().decode(encryptData));
            byte[] iv = new byte[16];
            Arrays.fill(iv, (byte) 0);
            // IvParameterSpec zeroIv = new IvParameterSpec(iv);
            IvParameterSpec zeroIv = new IvParameterSpec(key.getBytes());
            SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance(transformation);
            // 与加密时不同MODE:Cipher.DECRYPT_MODE
            cipher.init(Cipher.DECRYPT_MODE, keySpec, zeroIv);
            byte[] decryptedData = cipher.doFinal(tmp);
            return new String(decryptedData, charset).trim();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 字节数组转成16进制字符串
     *
     * @param b
     * @return
     */
    public static String byte2hex(byte[] b) { // 一个字节的数，
        StringBuffer sb = new StringBuffer(b.length * 2);
        String tmp = "";
        for (int n = 0; n < b.length; n++) {
            // 整数转成十六进制表示
            tmp = (Integer.toHexString(b[n] & 0XFF));
            if (tmp.length() == 1) {
                sb.append("0");
            }
            sb.append(tmp);
        }
        return sb.toString().toUpperCase(); // 转成大写
    }

    /**
     * 将hex字符串转换成字节数组
     *
     * @param inputString
     * @return
     */
    private static byte[] hex2byte(String inputString) {
        if (inputString == null || inputString.length() < 2) {
            return new byte[0];
        }
        inputString = inputString.toLowerCase();
        int l = inputString.length() / 2;
        byte[] result = new byte[l];
        for (int i = 0; i < l; ++i) {
            String tmp = inputString.substring(2 * i, 2 * i + 2);
            result[i] = (byte) (Integer.parseInt(tmp, 16) & 0xFF);
        }
        return result;
    }

    private static byte[] encryptShifts(byte[] buffer) {
        int len = buffer.length;
        for (int i = 0; i < len; i++) {
            buffer[i] = (byte) (~((buffer[i] + 7) % 256));
        }
        return buffer;
    }

    private static byte[] decryptShifts(byte[] buffer) {
        int len = buffer.length;
        for (int i = 0; i < len; i++) {
            buffer[i] = (byte) ((~buffer[i] - 7) % 256);
        }
        return buffer;
    }

    public static String genKey(String udid, String devid) {
        String key = byte2hex(ASE_KEY);
        if (udid != null && !"".equals(udid)) {
            key = autoFillKey(udid);
        } else if (devid != null && !"".equals(devid)) {
            key = autoFillKey(devid);
        }
        return key;
    }

    private static String autoFillKey(String key) {
        int len = key.length();
        if (len < 16) {
            key = String.format("%0" + (16 - len) + "d%s", 0, key);
        } else if (len > 16) {
            key = key.substring(0, 16);
        }
        return key;
    }

    public static void main(String[] args) {
        String content = "wo我……";
        String key = genKey("123", "345");
        System.out.println("key = " + key);
        String enContent = encrypt(content.trim(), key);
        String deContent = decrypt(enContent, key);
        System.out.println("enContent = " + enContent + " --> deContent = " + deContent);
        System.out.println(content.equals(content));
    }

    private static byte[] getRawKey(byte[] seed) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        sr.setSeed(seed);
        kgen.init(128, sr); // 192 and 256 bits may not be available
        SecretKey skey = kgen.generateKey();
        byte[] raw = skey.getEncoded();
        return raw;
    }

}