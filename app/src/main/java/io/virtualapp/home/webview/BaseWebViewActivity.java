package io.virtualapp.home.webview;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.home.WebConst;
import io.virtualapp.home.activity.BaseActivity;
import io.virtualapp.home.util.StatusBarUtils;

/**
 * Created by zhangweibo on 2017/3/6.
 */
public class BaseWebViewActivity extends BaseActivity implements IWebView{

    @BindView(R.id.webview)
    BaseWebView webView;
    @BindView(R.id.pb_progress)
    WebViewProgressBar pbProgress;
    private String loadUrl;//需要加载的URL;
    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        /* 视频为了避免闪屏和透明问题
//        （这个对宿主没什么影响，建议声明）*/
//        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        super.onCreate(savedInstanceState);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.theme_color);
        actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setTitle(R.string.user_rule);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_web_view;
    }

    @Override
    public void initData() {

    }

    @Override
    public void initViews() {
        showProgressBar();
        startProgress();
        initWebView();
        loadContentOrUrl();
    }

    /**
     * 加载网页内容或者url
     */
    public void loadContentOrUrl() {
        //加载url或者html内容
        String loadData = WebConst.SERVICE_URL;
        loadData(loadData);
    }


    @Override
    protected void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
    }

    @Override
    public void showProgressBar() {
        pbProgress.setVisibility(View.VISIBLE);
    }


    @Override
    public void hideProgressBar() {
        pbProgress.setVisibility(View.GONE);
    }

    private void initWebView() {

        pbProgress.setVisibility(View.VISIBLE);

        WebSettings settings = webView.getSettings();
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        settings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        //缩放操作
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true); //支持缩放，默认为true。是下面那个的前提。
        settings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不
        settings.setJavaScriptCanOpenWindowsAutomatically(true); //支持通过JS打开新窗口
        settings.setLoadsImagesAutomatically(true); //支持自动加载图片
        settings.setDomStorageEnabled(true);
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setSupportZoom(false);

        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());
//        webView.addJavascriptInterface(defineScripObject(), "object");
    }

    @Override
    public void progressChanged(int newProgress) {
        pbProgress.updateProgress(newProgress);
    }


    @Override
    public boolean loadUrl(String url) {
        this.loadUrl = url;
        webView.loadUrl(url);
        return false;
    }


    @Override
    public void loadData(String data) {
          webView.loadUrl(data);
          finishProgress();
          hideProgressBar();
//        webView.loadDataWithBaseURL(null, data, "text/html", "utf-8", null);
    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public String getWebViewLoadUrl() {
        return loadUrl;
    }

    public WebView getWebView() {
        return webView;
    }

    @Override
    public void startProgress() {
        pbProgress.startProgress();
    }

    @Override
    public void finishProgress() {
        pbProgress.finishProgress();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.destroy();
            webView = null;
        }
    }
}
