package io.virtualapp.home.webview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;

/**
 * time 2017/11/21
 * <p>describe:
 * </p>
 *
 * @author qiurong
 * @since 2.1.4-2
 */
public class BaseWebView extends WebView {

    private OnScrollChangedCallback mOnScrollChangedCallback;
    private FullscreenHolder fullscreenContainer;
    private View customView;
    /**
     * 视频全屏参数
     */
    protected static final FrameLayout.LayoutParams COVER_SCREEN_PARAMS
            = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    public BaseWebView(Context context) {
        super(context);
        init();
    }

    public BaseWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        WebSettings ws = getSettings();
        ws.setJavaScriptEnabled(true);
        ws.setSupportZoom(true);
        ws.setBuiltInZoomControls(false);
        ws.setSavePassword(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //适配5.0不允许http和https混合使用情况
//            ws.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        } 

        ws.setTextZoom(100);
        ws.setDatabaseEnabled(true);
        ws.setAppCacheEnabled(true);
        ws.setLoadsImagesAutomatically(true);
        ws.setSupportMultipleWindows(false);
        //是否阻塞加载网络图片  协议http or https
        ws.setBlockNetworkImage(false);
        //允许加载本地文件html  file协议
        ws.setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            //通过 file url 加载的 Javascript 读取其他的本地文件 .建议关闭
            ws.setAllowFileAccessFromFileURLs(false);
            //允许通过 file url 加载的 Javascript 可以访问其他的源，包括其他的文件和 http，https 等其他的源
            ws.setAllowUniversalAccessFromFileURLs(false);
        }
        ws.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            ws.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        } else {
            ws.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }
        ws.setLoadWithOverviewMode(true);
        ws.setUseWideViewPort(true);
        ws.setDomStorageEnabled(true);
        ws.setNeedInitialFocus(true);
        //设置编码格式
        ws.setDefaultTextEncodingName("utf-8");
        ws.setDefaultFontSize(16);
        //设置 WebView 支持的最小字体大小，默认为 8
        ws.setMinimumFontSize(12);
        ws.setGeolocationEnabled(true);
        //缓存文件最大值
        ws.setAppCacheMaxSize(Long.MAX_VALUE);
        setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                if (!TextUtils.isEmpty(url)) {
                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
//        if (mOnScrollChangedCallback != null) {
//            boolean toEnd = !getView().canScrollVertically(1);
//            if (toEnd) {
//                // 已经处于底端
//                mOnScrollChangedCallback.onPageEnd(this, t);
//            } else if (getScrollY() == 0) {
//                mOnScrollChangedCallback.onPageTop(this, t);
//            } else {
//                mOnScrollChangedCallback.onScroll(l - oldl, t - oldt);
//            }
//        }
    }

    /**
     * 全屏容器界面
     */
    static class FullscreenHolder extends FrameLayout {

        public FullscreenHolder(Context ctx) {
            super(ctx);
            setBackgroundColor(Color.BLACK);
        }

        @Override
        public boolean onTouchEvent(MotionEvent evt) {
            return true;
        }
    }

    private void setStatusBarVisibility(boolean visible) {
        int flag = visible ? 0 : WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setFlags(flag, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private Window getWindow() {
        return ((Activity) getContext()).getWindow();
    }

    public void onDestroy() {
        loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
        clearHistory();
        ViewParent parent = getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this);
        }
        destroy();
    }

    public void setOnScrollChangedCallback(
            final OnScrollChangedCallback onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    public interface OnScrollChangedCallback {
        /**
         * 滚动回调
         *
         * @param dx 横向滚动值
         * @param dy 纵向滚动值
         */
        void onScroll(int dx, int dy);

        /**
         * 已经滑动到底部
         *
         * @param parent view本身
         * @param top    top
         */
        void onPageEnd(View parent, int top);

        /**
         * 已经滑动到顶部
         *
         * @param parent view本身
         * @param top    top
         */
        void onPageTop(View parent, int top);
    }
}
