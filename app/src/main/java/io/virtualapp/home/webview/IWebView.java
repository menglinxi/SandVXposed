package io.virtualapp.home.webview;

import android.content.Context;

/**
 * Created by zhangweibo on 2017/3/6.
 */

public interface IWebView {


    /**
     * 显示进度条
     */
    void showProgressBar();
    /**
     * 隐藏进度条
     */
    void hideProgressBar();

    /**
     * 进度条变化时调用
     */
    void progressChanged(int newProgress);

    /**
     * 开始加载
     */
    void startProgress();

    /**
     * 结束加载
     */
    void finishProgress();

    /**
     *获取加载的url
     */
    String getWebViewLoadUrl();

    /**
     * 加载URL地址
     */
    boolean loadUrl(String url);

    /**
     * 加载内容数据
     *
     * @param data
     */
    void loadData(String data);

    Context getContext();

}
