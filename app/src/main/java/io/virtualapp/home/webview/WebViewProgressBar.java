package io.virtualapp.home.webview;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/**
 * Created by zhangweibo on 2017/3/6.
 */

public class WebViewProgressBar extends ProgressBar {

    private Handler mHandler;
    private ValueAnimator animator;
    private int maxValue=1000;
    private int minValue=100;
    public WebViewProgressBar(Context context) {
        super(context);
        init();
    }

    public WebViewProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void init(){
        this.setMax(maxValue);
        animator=ValueAnimator.ofInt(minValue,maxValue);
        animator.setDuration(500);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value=(Integer) animation.getAnimatedValue();
                WebViewProgressBar.this.setProgress(value);
                if (value==maxValue){
                    WebViewProgressBar.this.setVisibility(GONE);
                }
            }
        });
    }

    public void updateProgress(int progress){
        this.setProgress(progress);
    }

    public void startProgress(){
        animator.cancel();
        this.setProgress(0);
        this.setVisibility(VISIBLE);
    }

    public void finishProgress(){
        animator.start();
    }

    @Override
    protected void onAttachedToWindow() {
        if (mHandler==null){
            mHandler=new Handler();
        }
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mHandler!=null){
            mHandler.removeCallbacks(null);
        }
        super.onDetachedFromWindow();
    }
}
