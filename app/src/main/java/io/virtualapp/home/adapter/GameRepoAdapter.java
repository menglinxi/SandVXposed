package io.virtualapp.home.adapter;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lid.lib.LabelImageView;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.utils.VLog;

import java.util.List;

import io.virtualapp.R;
import io.virtualapp.entity.GameEntity;
import io.virtualapp.home.callback.UpdateDownLoadListener;
import io.virtualapp.home.util.TasksManager;
import io.virtualapp.home.weight.RoundProgressBarWidthNumber;
import io.virtualapp.home.weight.StateTextView;
import io.virtualapp.tools.LogUtils;

/**
 * Created by lanpan on 2018/4/17.
 */

public class GameRepoAdapter extends BaseQuickAdapter<GameEntity, BaseViewHolder> {

    private static final int MSG_PROGRESS_OK = 0x200;
    private static final int MSG_PROGRESS_WARNNING = 0x404;
    private SparseArray<RoundProgressBarWidthNumber> progressBarArray = new SparseArray<>();
    private static int MSG_PROGRESS_UPDATE;
    private RoundProgressBarWidthNumber itemProgressBar;
    //    private RoundProgressBarWidthNumber realProgressBar;
    public static boolean isLoading;

    public UpdateDownLoadListener loadListener;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_PROGRESS_OK: {
                    int position = (int) msg.obj;
                    RoundProgressBarWidthNumber realProgressBar = progressBarArray.get(position);
                    if (isLoading) {
                        realProgressBar.setProgress(MSG_PROGRESS_UPDATE);
                    }
                    if (realProgressBar.getProgress() >= 100) {
                        mHandler.removeMessages(MSG_PROGRESS_UPDATE);
                        realProgressBar.setVisibility(View.INVISIBLE);
                        isLoading = false;
                        return;
                    }
                    mHandler.sendEmptyMessageDelayed(MSG_PROGRESS_UPDATE, 100);
                    break;
                }
                case MSG_PROGRESS_WARNNING: {
                    mHandler.removeMessages(MSG_PROGRESS_UPDATE);
                    int position = (int) msg.obj;
                    RoundProgressBarWidthNumber realProgressBar = progressBarArray.get(position);
                    if (realProgressBar!=null){
                        realProgressBar.setVisibility(View.INVISIBLE);
                    }
                    break;
                }
            }
        }
    };

    public GameRepoAdapter(int layoutResId, @Nullable List<GameEntity> data, UpdateDownLoadListener loadListener) {
        super(layoutResId, data);
        this.loadListener = loadListener;
    }

    public Bitmap greyBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Bitmap faceIconGreyBitmap = Bitmap
                .createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(faceIconGreyBitmap);
        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);
        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);
        paint.setColorFilter(colorMatrixFilter);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return faceIconGreyBitmap;
    }

    public Bitmap sourceBitmap(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Bitmap faceIconGreyBitmap = Bitmap
                .createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(faceIconGreyBitmap);
        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(1);
        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);
        paint.setColorFilter(colorMatrixFilter);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        return faceIconGreyBitmap;
    }


    @Override
    protected void convert(BaseViewHolder helper, GameEntity item) {
        Glide.with(mContext).load(item.getPic()).into(new SimpleTarget<Drawable>() {
//            @Override
//            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                if (VirtualCore.get().isAppInstalled(item.getApkpackage()) && resource instanceof BitmapDrawable) {
//                    ((ImageView) helper.getView(R.id.game_logo)).setImageDrawable(resource);
//                } else {
//                    BitmapDrawable bitmapDrawable = (BitmapDrawable) resource;
//                    Bitmap bitmap = greyBitmap(bitmapDrawable.getBitmap());
//                    ((ImageView) helper.getView(R.id.game_logo)).setImageBitmap(bitmap);
//                }
//            }

            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                LabelImageView imageView =  helper.getView(R.id.game_logo);
                if (VirtualCore.get().isAppInstalled(item.getApkpackage()) && resource instanceof BitmapDrawable) {
                    imageView.setLabelText("已安装");
                    imageView.setLabelBackgroundColor(Color.parseColor("#67e667"));
                    imageView.setImageDrawable(resource);
                } else {
                    imageView.setLabelText("未下载");
                    imageView.setLabelBackgroundColor(Color.parseColor("#C2185B"));
                    imageView.setImageDrawable(resource);
                }
            }
        });

        helper.setText(R.id.installed_game_name, item.getGamename());
        helper.setTextColor(R.id.installed_game_name, mContext.getResources().getColor(R.color.black_de));
        helper.itemView.setId(item.getId());
        StateTextView textView = helper.getView(R.id.installed_game_name);
        textView.setText(item.getGamename());
        textView.setNormal();
        itemProgressBar = helper.getView(R.id.download_progressbar);
        itemProgressBar.setVisibility(View.GONE);
        String url = item.getDlurl();
        if (url==null){
            VLog.e("abc","url not exist");
            return;
        }
        String path = TasksManager.getImpl().getPath(url);
        String pkgName = item.getApkpackage();
        final int id = FileDownloadUtils.generateId(url, path);

        final int status = VirtualCore.get().isAppInstalled(pkgName) ? FileDownloadStatus.INVALID_STATUS : TasksManager.getImpl().getStatus(id, path);
        helper.itemView.setTag(item);
//        VLog.e("abc", "%s(%s) , convert status = %s --> %s", item.getGamename(), item.getId(), status, helper.itemView);
        loadListener.getGameViews().put(item.getId(), helper.itemView);
        if (status == FileDownloadStatus.pending || status == FileDownloadStatus.started || status == FileDownloadStatus.connected || status == FileDownloadStatus.progress) {
            long soFarBytes = TasksManager.getImpl().getSoFar(id);
            long totalBytes = TasksManager.getImpl().getTotal(id);
            loadListener.updateDownLoading(status, item, soFarBytes, totalBytes);
        }
        if (status == FileDownloadStatus.paused || status == FileDownloadStatus.completed) {
            long soFarBytes = TasksManager.getImpl().getSoFar(id);
            long totalBytes = TasksManager.getImpl().getTotal(id);
            loadListener.updateNotDownloaded(status, item, soFarBytes, totalBytes);
        }
    }

    public void startProgressBar(int position) {
        LogUtils.e("start gameid --> " + position);
        RoundProgressBarWidthNumber realProgressBar = progressBarArray.get(position);
        realProgressBar.setVisibility(View.VISIBLE);
        realProgressBar.start();
        isLoading = true;
    }

    public void pauseProgressBar(int position) {
        LogUtils.e("pause gameid --> " + position);
        RoundProgressBarWidthNumber realProgressBar = progressBarArray.get(position);
        realProgressBar.pause();
        isLoading = false;
    }

    public void setProgressData(int position, Float update) {
        this.MSG_PROGRESS_UPDATE = Math.round(update);
        LogUtils.e("线程：" + Thread.currentThread().getName() + "\n下载进度：" + Math.round(update));
        Message msg = new Message();
        msg.what = MSG_PROGRESS_OK;
        msg.obj = position;
        mHandler.sendMessage(msg);
    }

    public void setDownloadStop(int position, float update) {
        MSG_PROGRESS_UPDATE = Math.round(update);
        Message msg = new Message();
        msg.what = MSG_PROGRESS_WARNNING;
        msg.obj = position;
        mHandler.sendMessage(msg);
    }
}


