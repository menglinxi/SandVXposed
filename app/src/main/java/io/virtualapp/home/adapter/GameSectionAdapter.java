package io.virtualapp.home.adapter;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import io.virtualapp.entity.GameSection;

/**
 * Created by lanpan on 2018/4/18.
 */

public class GameSectionAdapter extends BaseSectionQuickAdapter<GameSection, BaseViewHolder> {

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param layoutResId      The layout resource id of each item.
     * @param sectionHeadResId The section head layout id for each item
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public GameSectionAdapter(int layoutResId, int sectionHeadResId, List<GameSection> data) {
        super(layoutResId, sectionHeadResId, data);
    }


    @Override
    protected void convertHead(BaseViewHolder helper, GameSection item) {
//        helper.setText(R.id.head_section, item.header);
//        helper.setVisible(R.id.head_more, item.isMore());
//        helper.addOnClickListener(R.id.head_more);
    }

    @Override
    protected void convert(BaseViewHolder helper, GameSection item) {
//        GameEntity data = item.t;
//        switch (helper.getLayoutPosition() % 2) {
//            case 0:
//                Glide.with(mContext).load(data.getPic()).into((ImageView) helper.getView(R.id.game_logo));
//                break;
//            case 1:
//                Glide.with(mContext).load(data.getPic()).into((ImageView) helper.getView(R.id.game_logo));
//                break;
//
//        }
//        helper.setText(R.id.installed_game_name, data.getGamename());
    }
}
