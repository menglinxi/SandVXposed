package io.virtualapp.home.adapter;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import io.virtualapp.R;
import io.virtualapp.home.entity.GameGifts;

/**
 * Created by lanpan on 2018/4/17.
 */

public class GameGiftsAdapter extends BaseQuickAdapter<GameGifts, BaseViewHolder> {

    public GameGiftsAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, GameGifts item) {
        helper.setText(R.id.tv_game_name, item.getGamename());
        helper.setText(R.id.tv_presents_count, item.getGifts().size()+"");
        helper.setText(R.id.tv_use_method, item.getGifts().get(0).getRemark());
        Glide.with(mContext).load(item.getPic()).into((ImageView) helper.getView(R.id.iv_game_icon));
    }
}
