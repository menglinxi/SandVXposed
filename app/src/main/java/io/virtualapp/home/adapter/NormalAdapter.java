package io.virtualapp.home.adapter;

import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import io.virtualapp.R;
import io.virtualapp.entity.va.AppInfo;

/**
 * Created by lanpan on 2018/4/17.
 */

public class NormalAdapter extends BaseQuickAdapter<AppInfo, BaseViewHolder> {

    public NormalAdapter(int layoutResId, @Nullable List<AppInfo> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, AppInfo item) {
        Glide.with(mContext).load(item.getIcon()).into((ImageView) helper.getView(R.id.game_logo));
        helper.setText(R.id.installed_game_name, item.getName());
    }
}
