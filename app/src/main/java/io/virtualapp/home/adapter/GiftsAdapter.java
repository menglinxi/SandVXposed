package io.virtualapp.home.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import io.virtualapp.R;
import io.virtualapp.home.entity.Gifts;
import io.virtualapp.home.models.GiftsModel;
import io.virtualapp.home.weight.SnackbarUtils;
import io.virtualapp.tools.LogUtils;

/**
 * Created by lanpan on 2018/7/17.
 */

public class GiftsAdapter extends BaseQuickAdapter<Gifts, BaseViewHolder>{

    private String gameIcon;
    private String uid;


    public GiftsAdapter(int layoutResId, List<Gifts> list, String gameIcon) {
        super(layoutResId, list);
        this.gameIcon = gameIcon;
    }

    @Override
    protected void convert(BaseViewHolder helper, Gifts item) {
        helper.setText(R.id.tv_game_name, item.getGiftname());
        helper.setText(R.id.tv_gift_code, item.getCode());
        helper.setText(R.id.tv_gift_detail, item.getContext());
        Glide.with(mContext).load(gameIcon).into((ImageView) helper.getView(R.id.iv_game_icon));

        TextView tvCode = helper.getView(R.id.tv_gift_code);
        TextView tvCopy =  helper.getView(R.id.tv_copy);
        TextView snackbarView = helper.getView(R.id.snackbar_dialog);

        tvCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String code = tvCode.getText().toString();

                //获取剪贴板管理器：
                ClipboardManager cm = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);

                // 创建普通字符型ClipData
                ClipData mClipData = ClipData.newPlainText("Label", code);
                // 将ClipData内容放到系统剪贴板里。
                cm.setPrimaryClip(mClipData);

                SnackbarUtils.Short(snackbarView, "礼包码已复制成功")
                        .info()
                        .margins(30,0,30, getNavigationBarHeight())
                        .show();

                GiftsModel.submitCode(uid, item.getCode(), item.getId()+"");
            }
        });

    }

    public void setUid(String uid){
        this.uid = uid;
    }

    private int getNavigationBarHeight() {
        Resources resources = mContext.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height","dimen", "android");
        int height = resources.getDimensionPixelSize(resourceId);
        LogUtils.e("navigation_bar_height", "Navi height:" + height);
        return height;
    }

}
