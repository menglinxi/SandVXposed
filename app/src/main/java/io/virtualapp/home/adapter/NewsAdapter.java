package io.virtualapp.home.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import io.virtualapp.R;
import io.virtualapp.home.entity.news.NewsInfo;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/24
 * className:
 * <p/>
 * describe:
 */
public class NewsAdapter extends BaseQuickAdapter<NewsInfo, BaseViewHolder>{

    public NewsAdapter(int layoutResId, @Nullable List<NewsInfo> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NewsInfo item) {
        helper.setText(R.id.tv_title, item.getTitle());
//        String timeDescription = TimeUtil.getDescriptionTime(item.getCt());
//        if (timeDescription==null){
//            helper.setText(R.id.tv_time, item.getCt());
//        }else{
//            helper.setText(R.id.tv_time, timeDescription);
//        }
        helper.setText(R.id.tv_time, item.getCt());
        helper.setText(R.id.tv_game_name, item.getGamename());
    }
}
