package io.virtualapp.home.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.google.gson.Gson;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.utils.VLog;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.virtualapp.BuildConfig;
import io.virtualapp.R;
import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.dev.page.DevActivity;
import io.virtualapp.home.activity.HomeActivity;
import io.virtualapp.home.callback.TraceCallback;
import io.virtualapp.home.callback.UserCallback;
import io.virtualapp.home.entity.UserInfo;
import io.virtualapp.home.fragment.base.ThemeFragment;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.tools.AESUtil;
import io.virtualapp.tools.DESEncrypt;
import io.virtualapp.tools.GsonUtil;
import io.virtualapp.tools.PhoneInfoCollect;
import io.virtualapp.user.activity.LoginActivity;
import io.virtualapp.user.activity.ResetPwdActivity;
import io.virtualapp.user.models.EmailPwdModel;
import io.virtualapp.user.models.LoginUtils;

import static io.virtualapp.home.WebConst.feedbackLink;

/**
 * Created by lanpan on 2018/4/17.
 */

public class UserInfoFragment extends ThemeFragment {

    private static final int MSG_SWITCH_ACCOUNT_SUCCESS = 0x100;
    private static final int MSG_SWITCH_ACCOUNT_FAILURE = 0x200;

    /**
     * 页面控件
     */
    @BindView(R.id.info_logo)
    BootstrapCircleThumbnail infoLogo;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.switch_account)
    TextView switchAccount;
    @BindView(R.id.info_phone)
    TextView infoPhone;
    @BindView(R.id.info_exit)
    Button infoExit;
    @BindView(R.id.change_password)
    TextView changePassword;
    @BindView(R.id.userinfo_container)
    RelativeLayout userInfoContainer;
    @BindView(R.id.version)
    TextView version;
    @BindView(R.id.feedback)
    TextView feedback;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_game_pwd)
    TextView tvGamePwd;
    @BindView(R.id.tv_update_email_pwd)
    TextView tvUpdateEmailPwd;

    @BindView(R.id.btn_show_list)
    Button mTestButton;

    @BindView(R.id.txt_channel)
    TextView mTxtChannel;
    /**
     * 变量
     */
    private List<String> accountList = new ArrayList<>();
    private Map<String, String> accounts = new HashMap<>();
    private List<String> userInfoes;
    private static String enUserInfojson;
    private String name = null;
    private String phone = null;
    private String pwd;
    private List<String> userObjs;
    private String gameUser;
    private String gamePwd;
    private int uid;
    private TextView tvResetEmail;
    private EditText etEmailPwd;
    private CheckBox cbVisible;
    private TextView tvPwdLength;
    private String newPwd;
    private int dev = 0;

    /**
     * 点击事件
     */
    @OnClick(R.id.switch_account)   //切换账号
    public void switchAccount() {
        inflateSwitchAccountView();
    }

    @OnClick(R.id.change_password)  //修改密码
    public void changePassword() {
        hideUserContent();
        startActivity(new Intent(getActivity(), ResetPwdActivity.class));
    }

    @OnClick(R.id.info_exit)        //退出登录
    public void exitLogin() {
//        inflateLoginView();
        hideUserContent();
        if (pwd != null && !pwd.equals("")) {
            LoginUtils.getImpl(getActivity()).saveCurrentUser(name + ":" + pwd);
        } else {
            LoginUtils.getImpl(getActivity()).saveCurrentUser(name);
        }
        LoginUtils.getImpl(getActivity()).saveState(false);
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    @OnClick(R.id.tv_update_email_pwd)
    public void updatePwd(){
        pupUpdateEmailPwdWindow();
    }

    @OnClick(R.id.info_logo)
    public void intoDev(){
        dev++;
        if (dev==6){
            gotoActivity(DevActivity.class, false);

        }
        if(dev==7){
            if(dialog==null){
                View listView = getLayoutInflater().inflate(R.layout.order_info,null);
                dialog = new OrderDialog(UserInfoFragment.this.getActivity());
                dialog.setContentView(listView);
            }
            dialog.show();
            dev = 0;
        }
    }

    /**
     * 更新邮箱的密码
     */
    private void pupUpdateEmailPwdWindow() {

        MaterialDialog materialDialog = new MaterialDialog.Builder(getActivity())
                .title("温馨提示")
                .customView(R.layout.custom_dialog, false)
                .canceledOnTouchOutside(false)
                .positiveText("确定修改")
                .negativeText("取消")
                .positiveColor(getResources().getColor(R.color.colorPrimaryDark))
                .negativeColor(getResources().getColor(R.color.bootstrap_gray))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        VLog.i("abc", "new email pwd = %s", etEmailPwd.getText().toString());
                        newPwd = etEmailPwd.getText().toString();
                        if (newPwd.length() < 6){
                            ToastUtils.showToast("请输入不少于6位数密码");
                            return;
                        }

                        if (newPwd.length() > 16){
                            ToastUtils.showToast("密码不能超过16位数");
                            return;
                        }

                        HashMap<String, String> param = new HashMap<>();
                        param.put("uid", String.valueOf(uid));
                        param.put("email", gameUser);
                        param.put("pwd0", etEmailPwd.getText().toString());
                        param.put("pwd1", etEmailPwd.getText().toString());
                        String json = GsonUtil.getInstance().getGson().toJson(param);
                        VLog.i("abc", "json = %s", json);

                        try{
                            DESEncrypt des = new DESEncrypt();
                            String key = des.genKey(PhoneInfoCollect.get().ANDROID_ID, PhoneInfoCollect.get().serial);
                            String KEY = des.encryption(json, key);
                            VLog.i("abc", "KEY = %s", KEY);

                            String dec = des.decryption(KEY, key);
                            VLog.i("abc", "decrypt = %s", dec);

                            HashMap<String, String> param1 = new HashMap<>();
                            param1.put("ANDROID_ID", PhoneInfoCollect.get().ANDROID_ID);
                            param1.put("serial", PhoneInfoCollect.get().serial);
                            param1.put("KEY", KEY);
                            String keyJson = GsonUtil.getInstance().getGson().toJson(param1);
                            VLog.i("abc", "keyJson = %s", keyJson);
                            EmailPwdModel.get().updateEmailPwd(keyJson, new TraceCallback() {
                                @Override
                                public void resp() {
                                    tvGamePwd.setText(newPwd);
                                }
                            });

                        }catch (Exception e){
                            VLog.e("abc", "error = %s", e.toString());
                        }
                    }
                })
                .build();

        tvResetEmail = materialDialog.getCustomView().findViewById(R.id.tv_email);
        etEmailPwd = materialDialog.getCustomView().findViewById(R.id.et_new_pwd);
        cbVisible = materialDialog.getCustomView().findViewById(R.id.cb_visible_pwd);
        tvPwdLength = materialDialog.getCustomView().findViewById(R.id.tv_pwd_length);

        tvResetEmail.setText("* email:" + gameUser);
        etEmailPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                VLog.i("abc", "start = %s， before = %s, count = %s", start, before, count);
                tvPwdLength.setText(count + "/16");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        cbVisible.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                etEmailPwd.setInputType(
                        !isChecked ? InputType.TYPE_TEXT_VARIATION_PASSWORD : InputType.TYPE_CLASS_TEXT);
                etEmailPwd.setTransformationMethod(
                        !isChecked ? PasswordTransformationMethod.getInstance() : null);
            }
        });
        materialDialog.show();

    }

    /**
     * Fragment对象实例化
     *
     * @param userinfo
     * @return
     */
    public static UserInfoFragment newInstance(String userinfo) {
        enUserInfojson = userinfo;
        return new UserInfoFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (name != null && phone != null) {
            tvUserName.setText(name);
            infoPhone.setText(phone);
            tvEmail.setText(gameUser);
            tvGamePwd.setText(gamePwd);
//            tvUserName.setText("admin@xxx.com");
        }
        if (userInfoContainer != null) {
            userInfoContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void hideUserContent() {
        userInfoContainer.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void initData() {
        String htmlRes = "<a href='" + feedbackLink +"'>"+ getResources().getString(R.string.feedback)+"</a>";
        feedback.setText(Html.fromHtml(htmlRes));
        feedback.setMovementMethod(LinkMovementMethod.getInstance());
        version.setText("版本：" + BuildConfig.VERSION_NAME);
        LoginUtils utils = LoginUtils.getImpl(getActivity());
        userObjs = utils.getAllUserInfo();
        for (String json : userObjs) {
            LoginUtils.User u = new Gson().fromJson(json, LoginUtils.User.class);
            accountList.add(u.getUser());
        }
        userInfoes = accountList;
        setUserInfo();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_user_info;
    }

    OrderDialog dialog;
    @Override
    protected void initViews() {

        mTxtChannel.setText("数据标识："+BuildConfig.CHANNEL);


    }

    private void setUserInfo() {
        if (enUserInfojson != null) {
            JsonUtils jsonUtils = new JsonUtils(enUserInfojson);
            PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
            String loginJson = AESUtil.decrypt(jsonUtils.key("data").key("login").stringValue(), (new StringBuffer(AESUtil.genKey(phoneInfoCollect.ANDROID_ID, phoneInfoCollect.IMEI1)).reverse()).toString());
            UserInfo.DataBean.LoginBean loginBean = new Gson().fromJson(loginJson, UserInfo.DataBean.LoginBean.class);

            if (loginBean!=null){
                name = loginBean.getBoxname();
                phone = loginBean.getVd().getLine1Number();
                pwd = loginBean.getBoxpwd();
                gameUser = loginBean.getGameuser();
                gamePwd = loginBean.getGamepwd();
                uid = loginBean.getUserid();
            }
        }
    }

    /**
     * 加载切换账号页面
     */
    private void inflateSwitchAccountView() {
        int index = 0;
        if (accountList != null) {
            for (int i = 0; i < accountList.size(); i++) {
                if (name.equals(accountList.get(i))) {
                    index = i;
                    break;
                }
            }
        }
        new MaterialDialog.Builder(getActivity())
                .title(R.string.switch_account)
                .items(accountList)
                .itemsColor(Color.parseColor("#FF696969"))
                .itemsCallbackSingleChoice(index, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (name.equals(text.toString())) {
                            return true;
                        }
                        ToastUtils.showToast("正在切换账号，请稍等！");
                        LoginUtils.User u = new Gson().fromJson(userObjs.get(which), LoginUtils.User.class);
                        String password = u.getPwd();
                        String json = LoginUtils.getImpl(getActivity()).getUploadJson(text.toString(), password);
                        LoginUtils.getImpl(getActivity()).login(json, new UserCallback() {
                            @Override
                            public void successed(String content) {
                                Message msg = new Message();
                                msg.what = MSG_SWITCH_ACCOUNT_SUCCESS;
                                msg.obj = content;
                                handler.sendMessage(msg);
                                LoginUtils.getImpl(getActivity()).saveCurrentUser(text.toString(), password);
                            }

                            @Override
                            public void failed(String error) {
                                handler.sendEmptyMessage(MSG_SWITCH_ACCOUNT_FAILURE);
                            }
                        });
                        return false;
                    }
                })
                .positiveColor(Color.parseColor("#FF696969"))
                .positiveText(R.string.ok)
                .show();
    }


    private class UserInfoHandler extends Handler {

        private final WeakReference<HomeActivity> mActivity;

        public UserInfoHandler(HomeActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            HomeActivity activity = mActivity.get();
            if (activity != null) {
                switch (msg.what) {
                    case MSG_SWITCH_ACCOUNT_SUCCESS:
                        enUserInfojson = (String) msg.obj;
                        VLog.e("abc","relogin_en --> %s",enUserInfojson);
                        VUiKit.defer().when(() -> {
                            SharedPreferences sharedPreferences = VirtualCore.get().getContext().getSharedPreferences("postResult", Context.MODE_PRIVATE);
                            String oldUserInfoJson = sharedPreferences.getString("info", null);
                            LoginUtils.getImpl(getActivity()).checkUserInfo(oldUserInfoJson, enUserInfojson);
                        }).done(res -> {
                            ToastUtils.showToast("账号切换成功");
                            setUserInfo();
                            tvUserName.setText(name);
                            infoPhone.setText(phone);
//                            tvUserName.setText("admin2@xxx.com");
                            LoginUtils.getImpl(getActivity()).save(enUserInfojson);
                        });
                        break;
                    case MSG_SWITCH_ACCOUNT_FAILURE:
                        ToastUtils.showToast("账号切换失败");
                        break;
                }
            }
        }
    }

    private final UserInfoHandler handler = new UserInfoHandler(HomeActivity.INSTANCE);

}
