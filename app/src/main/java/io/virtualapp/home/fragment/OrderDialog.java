package io.virtualapp.home.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.virtualapp.R;
import io.virtualapp.delegate.db.OrderTraceHandle;
import io.virtualapp.entity.OrderTrace;

public class OrderDialog extends Dialog {
    private View mRootView;
    private TextView mTxtTotal;
    private TextView mTxtNoSendCount;
    private TextView mTxtSendFailCount;
    private RecyclerView mList;
    private MyAdapter mListAdapter;

    public OrderDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    public void show() {
        initView();
        super.show();


    }

    public OrderDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected OrderDialog(@NonNull Context context, boolean cancelable, @NonNull DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    public void setContentView(@NonNull View view) {
        super.setContentView(view);
        mRootView = view;
//        initView();
    }

    private void initView() {
        mTxtTotal = mRootView.findViewById(R.id.txt_total_count);
        mTxtNoSendCount = mRootView.findViewById(R.id.txt_upload_count);
        mTxtSendFailCount = mRootView.findViewById(R.id.txt_upload_fail_count);


        mList = mRootView.findViewById(R.id.list_never_upload);

        List<OrderTrace> totalData = OrderTraceHandle.get().query();


        List<OrderTrace> sendFailData = new ArrayList<>();
        List<OrderTrace> noSendData =new ArrayList<>();

        for(OrderTrace d:totalData){
            if(d.getSendStatus()==0){
                noSendData.add(d);
                continue;
            }
            if(d.getSendStatus()<0){
                sendFailData.add(d);
                continue;
            }
        }



        mTxtTotal.setText(String.valueOf(totalData.size()));
        mTxtSendFailCount.setText(String.valueOf(sendFailData.size()));
        mTxtNoSendCount.setText(String.valueOf(noSendData.size()));

        mListAdapter = new MyAdapter(totalData);
        mList.setAdapter(mListAdapter);
        mList.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mList.invalidate();
    }

    private class MyAdapter extends RecyclerView.Adapter {
        private List<OrderTrace> mData;

        MyAdapter(List<OrderTrace> data) {
            mData = data;
        }

        void setData(List<OrderTrace> data) {
            mData = data;
        }


        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_list_item, viewGroup, false);
            return new MyItemHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            ((MyItemHolder) viewHolder).setData(mData.get(position), position);
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

    private class MyItemHolder extends RecyclerView.ViewHolder {
        private View mRootView;

        private TextView mTxtCreateTime;
        private TextView mTxtOrderId;
        private TextView mTxtSendStatus;
        private TextView mTxtPayMethod;
        private TextView mTxtAppName;
        private TextView mTxtPkgName;
        private TextView mTxtPayStatus;
        private TextView mTxtChannel;
        private TextView mTxtChannelId;


        public MyItemHolder(@NonNull View itemView) {
            super(itemView);
            mRootView = itemView;
            mTxtCreateTime = itemView.findViewById(R.id.txt_item_create_time);
            mTxtOrderId = itemView.findViewById(R.id.txt_item_id);
            mTxtSendStatus = itemView.findViewById(R.id.txt_send_status);
            mTxtPayMethod = itemView.findViewById(R.id.txt_pay_method);
            mTxtPayStatus = itemView.findViewById(R.id.txt_pay_status);
            mTxtPkgName = itemView.findViewById(R.id.txt_pkg_name);
            mTxtAppName = itemView.findViewById(R.id.txt_app_name);
            mTxtChannel = itemView.findViewById(R.id.txt_channel);
            mTxtChannelId = itemView.findViewById(R.id.txt_channel_id);
        }

        public void setData(OrderTrace data, int position) {
            int createTime = data.getCreate_time();
            String orderId = data.getOrderId();
            int orderStatus = data.getStatus();

            mTxtSendStatus.setText(String.valueOf(orderStatus));

            mTxtOrderId.setText(orderId);
            Date d = new Date(createTime * 1000L);
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            mTxtCreateTime.setText(format.format(d));

            if(data.getPay_method()!=null)
                mTxtPayMethod.setText(data.getPay_method());

            if(data.getApp_name()!=null)
                mTxtAppName.setText(data.getApp_name());

            if (data.getPkg_name()!=null)
                mTxtPkgName.setText(data.getPkg_name());

            if(data.getChannel()!=null)
                mTxtChannel.setText(data.getChannel());

            if(data.getChannel_id()!=null)
                mTxtChannelId.setText(data.getChannel_id());

            mTxtPayStatus.setText(String.valueOf(data.getStatus()));

        }
    }
}
