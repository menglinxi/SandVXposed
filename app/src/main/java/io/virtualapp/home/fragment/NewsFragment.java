package io.virtualapp.home.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.lody.virtual.helper.utils.VLog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.entity.va.AppInfo;
import io.virtualapp.entity.va.AppManager;
import io.virtualapp.home.fragment.base.ThemeFragment;
import io.virtualapp.home.weight.SimpleViewpagerIndicator;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/24
 * className:
 * <p/>
 * describe:
 */
public class NewsFragment extends ThemeFragment {

    private static final int TYPE_NEWS = 0;
    private static final int TYPE_STRATEGY = 1;
    private static final int TYPE_NOTICE = 2;
    private static final int TYPE_GUILD = 3;

    @BindView(R.id.svi_title_indicator)
    SimpleViewpagerIndicator sviTitleIndicator;
    @BindView(R.id.vp_news)
    ViewPager vpNews;

    private static String[] titles = {"资讯", "攻略", "主播", "公会"};
    private static String enUserInfojson;
    private List<Fragment> fragments = new ArrayList<>();
    private AppManager appManager;
    private List<AppInfo> mApps;
    private String packages;
    private VpAdapter vpAdapter;
    private NewsxFragment[] newsFragment = new NewsxFragment[4];

    @Override
    protected void initData() {
        packages = getInstalledPackage();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_news;
    }

    public static NewsFragment newInstance(String userinfo) {
        enUserInfojson = userinfo;
        return new NewsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (packages==null || packages.equals("")){
            packages = getInstalledPackage();
            vpAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void initViews() {
        sviTitleIndicator.setExpand(true)
                .setIndicatorColor(getResources().getColor(R.color.col_white))
                .setSelectedTabTextColor(getResources().getColor(R.color.col_white));
//                .setBackgroundColor(getResources().getColor(R.color.theme_color));

        vpAdapter = new VpAdapter(getActivity().getSupportFragmentManager());
        vpNews.setAdapter(vpAdapter);
        vpNews.addOnPageChangeListener(vpAdapter);
        vpNews.setOffscreenPageLimit(3);
        sviTitleIndicator.setViewPager(vpNews);
        sviTitleIndicator.setOnItemClickListener(new SimpleViewpagerIndicator.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ((NewsxFragment)fragments.get(position)).setType(position);
            }
        });
    }

    class VpAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {
        VpAdapter(FragmentManager fm) {
            super(fm);
            newsFragment[TYPE_NEWS] = new NewsxFragment(packages);
            newsFragment[TYPE_STRATEGY] = new NewsxFragment(packages);
            newsFragment[TYPE_NOTICE] = new NewsxFragment(packages);
            newsFragment[TYPE_GUILD] = new NewsxFragment(packages);
            fragments.add(newsFragment[TYPE_NEWS]);
            fragments.add(newsFragment[TYPE_STRATEGY]);
            fragments.add(newsFragment[TYPE_NOTICE]);
            fragments.add(newsFragment[TYPE_GUILD]);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            VLog.e("abc", "fragment = %s, position = %s", fragments.get(position).toString(), position);
            ((NewsxFragment)fragments.get(position)).setType(position);
//            switch (position){
//                case 0:
//                    ((NewsxFragment)fragments.get(position)).setType(TYPE_NEWS);
//                    break;
//                case 1:
//                    ((NewsxFragment)fragments.get(position)).setType(TYPE_STRATEGY);
//                    break;
//                case 2:
//                    ((NewsxFragment)fragments.get(position)).setType(TYPE_NOTICE);
//                    break;
//
//                default:
//                    ((NewsxFragment)fragments.get(position)).setType(TYPE_NEWS);
//                    break;
//            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    /**
     * 获取已安装游戏的包名
     *
     * @return
     */
    private String getInstalledPackage() {

        if (appManager == null) {
            appManager = new AppManager(getActivity());
        }
        mApps = appManager.geInstalledApps();
        StringBuilder sb = new StringBuilder();
        if (mApps != null && mApps.size() > 0) {
            for (int i = 0; i < mApps.size(); i++) {
                if (i != mApps.size() - 1) {
                    sb.append("'" + mApps.get(i).getPkgName() + "',");
                } else {
                    sb.append("'" + mApps.get(i).getPkgName() + "'");
                }
            }
        }
        return sb.toString();
    }

}
