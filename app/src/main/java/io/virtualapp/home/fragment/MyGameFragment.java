package io.virtualapp.home.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.helper.utils.VLog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.virtualapp.R;
import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.entity.va.AppInfo;
import io.virtualapp.entity.va.AppManager;
import io.virtualapp.home.activity.GiftsActivity;
import io.virtualapp.home.adapter.NormalAdapter;
import io.virtualapp.home.entity.UserInfo;
import io.virtualapp.home.fragment.base.ThemeFragment;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.tools.AESUtil;
import io.virtualapp.tools.PhoneInfoCollect;
import io.virtualapp.view.utils.CheckDoubleClickUtils;

/**
 * Created by lanpan on 2018/4/17.
 */

public class MyGameFragment extends ThemeFragment {


    @BindView(R.id.installed_rv_content)
    RecyclerView content;
    @BindView(R.id.empty_repostory)
    TextView emptyNotice;
    @BindView(R.id.tv_presents)
    TextView tvPresents;

    private List<AppInfo> mApps;
    private AppManager appManager;
    private NormalAdapter normalAdapter;
    private static String userinfo;
    private ArrayList<String> packages = new ArrayList<>();
    private int uid;

    public static MyGameFragment newInstance(String userInfo){
        userinfo = userInfo;
        return new MyGameFragment();
    }


    @OnClick(R.id.tv_presents)
    public void getPresents(){

        if (mApps==null && mApps.size()==0) return;

        packages.clear();

        for (AppInfo appInfo: mApps) {
            packages.add(appInfo.getPkgName());
        }

        Intent intent = new Intent(getActivity(), GiftsActivity.class);
        intent.putStringArrayListExtra("apps", packages);
        intent.putExtra("uid", uid+"");
        startActivity(intent);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        JsonUtils jsonUtils = new JsonUtils(userinfo);
        PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
        String loginJson = AESUtil.decrypt(jsonUtils.key("data").key("login").stringValue(), (new StringBuffer(AESUtil.genKey(phoneInfoCollect.ANDROID_ID, phoneInfoCollect.IMEI1)).reverse()).toString());
        UserInfo.DataBean.LoginBean loginBean = new Gson().fromJson(loginJson, UserInfo.DataBean.LoginBean.class);
        if (loginBean!=null){
            uid = loginBean.getUserid();
        }
    }

    @Override
    protected void initData() {
        if (appManager == null) {
            appManager = new AppManager(getActivity());
        }
        mApps = appManager.geInstalledApps();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_my_game;
    }

    @Override
    protected void initViews() {
        initRecylerView();
    }

    private void initRecylerView() {
        content.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        normalAdapter = new NormalAdapter(R.layout.rv_mygame_item, mApps);
        content.setAdapter(normalAdapter);
        normalAdapter.setOnItemClickListener((BaseQuickAdapter adapter, View view, int position) -> {
            if (CheckDoubleClickUtils.isDoubleClick()) {
                return;
            }
            Intent intent = VirtualCore.get().getLaunchIntent(mApps.get(position).getPkgName(), 0);
            VActivityManager.get().startActivity(intent, 0);
            ToastUtils.showToast("正在启动：" + mApps.get(position).getName());
        });

        normalAdapter.setOnItemLongClickListener((BaseQuickAdapter adapter, View view, int position) -> {
            new MaterialDialog.Builder(getActivity())
                    .title(R.string.uninstall_game)
                    .content("是否卸载\"" + mApps.get(position).getName() + "\"?")
                    .positiveText(R.string.uninstall)
                    .positiveColor(Color.parseColor("#FF733FF4"))
                    .negativeText(R.string.cancel)
                    .negativeColor(Color.parseColor("#FF733FF4"))
                    .onPositive((MaterialDialog dialog, DialogAction which) -> {
                        VUiKit.defer().when(() -> {
                            AppInfo appInfo = mApps.get(position);
                            VirtualCore.get().uninstallPackage(appInfo.getPkgName());
                        }).done(result -> {
                            mApps.remove(position);
                            if (mApps.size() <= 0) {
                                emptyNotice.setVisibility(View.VISIBLE);
                                content.setVisibility(View.GONE);
                            }
                            normalAdapter.notifyDataSetChanged();
                        });
                    }).show();
            return false;
        });

        if (mApps==null || mApps.size() == 0) {
            emptyNotice.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        VLog.i("abc", MyGameFragment.class.getName()+" onResume");
        initData();
        if (mApps != null && mApps.size() > 0) {
            normalAdapter.setNewData(mApps);
            normalAdapter.notifyDataSetChanged();
            content.setVisibility(View.VISIBLE);
            emptyNotice.setVisibility(View.GONE);
        }
    }

}
