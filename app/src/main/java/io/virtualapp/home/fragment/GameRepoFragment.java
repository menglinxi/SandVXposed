package io.virtualapp.home.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.google.gson.Gson;
import com.lid.lib.LabelImageView;
import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.remote.InstallResult;
import com.lody.virtual.remote.InstalledAppInfo;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.VCommends;
import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.entity.GameEntity;
import io.virtualapp.home.activity.HomeActivity;
import io.virtualapp.home.adapter.GameRepoAdapter;
import io.virtualapp.home.callback.ConnentListener;
import io.virtualapp.home.callback.UpdateDownLoadListener;
import io.virtualapp.home.entity.HotGame;
import io.virtualapp.home.entity.UserInfo;
import io.virtualapp.home.fragment.base.ThemeFragment;
import io.virtualapp.home.util.DynamicTimeFormat;
import io.virtualapp.home.util.TasksManager;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.home.weight.RoundProgressBarWidthNumber;
import io.virtualapp.home.weight.StateTextView;
import io.virtualapp.tools.AESUtil;
import io.virtualapp.tools.ClearDataUtils;
import io.virtualapp.tools.GameGson;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.tools.PhoneInfoCollect;
import io.virtualapp.view.utils.CheckDoubleClickUtils;

/**
 * Created by lanpan on 2018/4/17.
 */

public class GameRepoFragment extends ThemeFragment implements UpdateDownLoadListener {

    //常量
    private final static int MSG_SHOW_ALL_GAME = 0x01;
    private final static int MSG_LOAD_HOTGAME_FINISHED = 0x02;
    private final static int MSG_BIND_SERVICE_SECCUSSED = 0x03;
    private static final int MSG_UPDATE_GAME_NAME = 0x04;
    private final static int MSG_LOAD_SEARCHGAME_FINISHED = 0x05;

    //控件变量
    @BindView(R.id.game_repo_rv_content)
    RecyclerView gameRepoRvContent;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout mRefreshLayout;
    private ClassicsHeader mClassicsHeader;
    private Drawable mDrawableProgress;
    private SparseArray<View> gameViews = new SparseArray<>();

    //普通变量
    public int page = 1;
    private List<GameEntity> loadedGames;
    private static String adUrl = "http://f.jiawanhd.com/group1/M00/00/60/rBEABFqyOY-AQZOTAAJP1XWW25g5..jpeg";
    private GameRepoAdapter gameRepoAdapter;
    private static String enUserInfojson;
    private int channelId;
    private boolean isNoMoreData;
    private List<HotGame> hotGames;
    private NumberProgressBar hotgame_progressbar;
    private int hotPosition;
    private StateTextView hotgame_name;
    private Banner banner;
    private View headerAdView;

    @Override
    public SparseArray<View> getGameViews() {
        return gameViews;
    }

    /**
     * Fragment对象实例化
     *
     * @param userinfo
     * @return
     */
    public static GameRepoFragment newInstance(String userinfo) {
        enUserInfojson = userinfo;
        return new GameRepoFragment();
    }

    private void initRefreshLayout() {
        int delta = new Random().nextInt(7 * 24 * 60 * 60 * 1000);
        mClassicsHeader = (ClassicsHeader) mRefreshLayout.getRefreshHeader();
        mClassicsHeader.setLastUpdateTime(new Date(System.currentTimeMillis() - delta));
        mClassicsHeader.setTimeFormat(new SimpleDateFormat("更新于 MM-dd HH:mm", Locale.CHINA));
        mClassicsHeader.setTimeFormat(new DynamicTimeFormat("更新于 %s"));

//        mDrawableProgress = mClassicsHeader.getProgressView().getDrawable();
        mDrawableProgress = ((ImageView) mClassicsHeader.findViewById(ClassicsHeader.ID_IMAGE_PROGRESS)).getDrawable();
        if (mDrawableProgress instanceof LayerDrawable) {
            mDrawableProgress = ((LayerDrawable) mDrawableProgress).getDrawable(0);
        }

        //上拉刷新
        mRefreshLayout.setOnLoadMoreListener((RefreshLayout refreshLayout) -> {
            page++;
            getAllGame();
            refreshLayout.finishLoadMore();
        });

        //下拉刷新
        mRefreshLayout.setOnRefreshListener(
                new OnRefreshListener() {

                    @Override
                    public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                        //禁止用户连续刷新
                        if (CheckDoubleClickUtils.isDoubleClick()) {
                            return;
                        }
                        cleanAllData();
                        isNoMoreData = false;
                        page = 1;
                        getHostGames();
                        getAllGame();
                    }
                });
    }

    private void getHostGames() {
        VUiKit.defer().when(() -> {
            try {
                hotGames = GameGson.getHostGames();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).done(result -> {
            handler.sendEmptyMessage(MSG_LOAD_HOTGAME_FINISHED);
        });
    }

    @Override
    protected void initData() {
        isNoMoreData = false;
        if (enUserInfojson != null) {
            JsonUtils jsonUtils = new JsonUtils(enUserInfojson);
            PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
            String loginJson = AESUtil.decrypt(jsonUtils.key("data").key("login").stringValue(), (new StringBuffer(AESUtil.genKey(phoneInfoCollect.ANDROID_ID, phoneInfoCollect.IMEI1)).reverse()).toString());
            UserInfo.DataBean.LoginBean loginBean = new Gson().fromJson(loginJson, UserInfo.DataBean.LoginBean.class);
            if (loginBean!=null){
                channelId = loginBean.getChannelid();
            }
        }
        mRefreshLayout.autoRefresh(0, 1000, 1.0f);
        mRefreshLayout.setEnableLoadMore(true);
        mRefreshLayout.setEnableAutoLoadMore(true);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game_repository;
    }

    @Override
    protected void initViews() {
        loadedGames = new ArrayList<>();
        TasksManager.getImpl().onCreate(new WeakReference<>(HomeActivity.INSTANCE), new ConnentListener() {
            @Override
            public void callback() {

            }
        });
        initRefreshLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = VirtualCore.get().getContext().getSharedPreferences("postResult", Context.MODE_PRIVATE);
        String currentUserInfoJson = sharedPreferences.getString("info", null);
        if (currentUserInfoJson != null && !currentUserInfoJson.equals(currentUserInfoJson)) {
            JsonUtils jsonUtils = new JsonUtils(currentUserInfoJson);
            PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
            String loginJson = AESUtil.decrypt(jsonUtils.key("data").key("login").stringValue(), (new StringBuffer(AESUtil.genKey(phoneInfoCollect.ANDROID_ID, phoneInfoCollect.IMEI1)).reverse()).toString());
            UserInfo.DataBean.LoginBean loginBean = new Gson().fromJson(loginJson, UserInfo.DataBean.LoginBean.class);
            if (channelId != loginBean.getChannelid() && loadedGames != null && mRefreshLayout != null) {
                loadedGames.clear();
                isNoMoreData = false;
                enUserInfojson = currentUserInfoJson;
                channelId = loginBean.getChannelid();
                mRefreshLayout.autoRefresh(0, 1000, 1.0f);
                mRefreshLayout.setEnableLoadMore(true);
                mRefreshLayout.setEnableAutoLoadMore(true);
            }
        }
    }

    private void initRecylerView() {
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
        gameRepoRvContent.setLayoutManager(manager);
        gameRepoAdapter = new GameRepoAdapter(R.layout.rv_game_repo_item, loadedGames, this);
        gameRepoRvContent.setAdapter(gameRepoAdapter);
        gameRepoAdapter.addHeaderView(getAdHeaderView());
        gameRepoAdapter.addHeaderView(getTitleHeaderView());

        //下载游戏
        gameRepoAdapter.setOnItemClickListener((BaseQuickAdapter adapter, View view, int position) -> {
            if (CheckDoubleClickUtils.isDoubleClick()) {
                return;
            }
            //初始化游戏资源
            GameEntity game = (GameEntity) view.getTag();
            VLog.e("abc", "position = %s >> id = %s", position, getId());
            String gameName = game.getGamename();
            int gameid = game.getId();
            String url = game.getDlurl();
            String path = TasksManager.getImpl().getPath(url);
            VLog.e("abc", "url = %s >> path = %s", url, path);

            if (VirtualCore.get().isAppInstalled(game.getApkpackage())) {
                Intent intent = VirtualCore.get().getLaunchIntent(game.getApkpackage(), 0);
                VActivityManager.get().startActivity(intent, 0);
                ToastUtils.showToast("正在启动：" + gameName);
                return;
            }
            VLog.e("abc", "downloadStatus = %s", FileDownloader.getImpl().getStatus(url, path));
            byte b = FileDownloader.getImpl().getStatus(url, path);
            if (b == FileDownloadStatus.completed) {
                Message msg = new Message();
                msg.what = MSG_UPDATE_GAME_NAME;
//                Status s = new Status();
//                s.setEntity(game);
//                s.setStatus("completed");
                game.setStatus("completed");
                msg.obj = game;
                handler.sendMessage(msg);
                return;
            }
//            if (b == FileDownloadStatus.INVALID_STATUS) {
//                VLog.e("abc", "delete file");
//                File file = new File(path);
//                if (file.exists()) {
//                    file.delete();
//                }
//            }

            int id = FileDownloadUtils.generateId(url, path);
            if (TasksManager.getImpl().getStatus(id, path) == FileDownloadStatus.progress) {
                //暂停下载
                FileDownloader.getImpl().pause(id);
                return;
            }
            if (TasksManager.getImpl().isReady()) {
                //开始下载
                BaseDownloadTask task = TasksManager.getImpl().getDownloadTask(url, path, dlCallback);
//                TasksManager.getImpl().addTask(game.getId(), task);
                task.setTag(game);
                task.start();
            }

            LogUtils.e("RecylerView had been initializated");
        });

        //上拉刷新
        gameRepoRvContent.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!ViewCompat.canScrollVertically(recyclerView, 1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (isNoMoreData) {
                        LogUtils.e("没有更多游戏了");
                        ToastUtils.showToast("没有更多游戏了");
                        mRefreshLayout.setEnableLoadMore(false);
                        mRefreshLayout.setEnableAutoLoadMore(false);
                    } else {
                        mRefreshLayout.autoLoadMore(500, 1000, 1.2f);
                    }
                }
            }
        });
    }

    private View getAdHeaderView() {
        headerAdView = getActivity().getLayoutInflater()
                .inflate(R.layout.game_repo_header_ad, (ViewGroup) gameRepoRvContent.getParent(), false);
        banner = headerAdView.findViewById(R.id.gamerepo_ad);

        if (hotGames != null && hotGames.size() != 0) {
            //设置图片集合
            List<String> images = getHotImages();
            banner.setImages(images);
        } else {
            List<String> localImages = new ArrayList<>();
            localImages.add(adUrl);
            banner.setImages(localImages);
        }

        //设置图片加载器
        banner.setImageLoader(new GlideLoader());

        //设置自动轮播，默认为true
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(5 * 1000);
        //圆点指示器
        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        banner.start();

        banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                try {

                    if (hotGames.size()==0) return;

                    if (VirtualCore.get().isAppInstalled(hotGames.get(position).getApkpackage())) {
                        Intent intent = VirtualCore.get().getLaunchIntent(hotGames.get(position).getApkpackage(), 0);
                        VActivityManager.get().startActivity(intent, 0);
                        ToastUtils.showToast("正在启动：" + hotGames.get(position).getGamename());
                        return;
                    }

                    LogUtils.e("hotGames.size - " + hotGames.size());
                    if (hotGames == null || hotGames.size() == 0) return;

                    if (TasksManager.getImpl().isReady()) {
                        String url = hotGames.get(position).getDlurl();
                        String path = TasksManager.getImpl().getPath(url);
                        //开始下载
                        BaseDownloadTask task = TasksManager.getImpl().getDownloadTask(url, path, dlCallback);
                        task.setTag(hotGames.get(position));
                        task.start();
                        LogUtils.e("start download hot game " + hotGames.get(position).getGamename());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                hotPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return headerAdView;
    }

    private List<String> getHotImages() {
        List<String> images = new ArrayList<>();
        for (int i = 0; i < hotGames.size(); i++) {
            images.add(hotGames.get(i).getHotimg());
        }
        return images;
    }

    public View getTitleHeaderView() {
        View view = getActivity().getLayoutInflater()
                .inflate(R.layout.game_repo_header_title, (ViewGroup) gameRepoRvContent.getParent(), false);
        hotgame_progressbar = view.findViewById(R.id.hotgame_progressbar);
        hotgame_name = view.findViewById(R.id.hot_game_name);
        return view;
    }

    @Override
    public void onDestroy() {
        gameViews.clear();
        FileDownloader.getImpl().pauseAll();
        loadedGames.clear();
        super.onDestroy();
    }

    @Override
    public void updateDownLoading(int status, GameEntity game, long soFarBytes, long totalBytes) {
        float progress = ((float) soFarBytes / totalBytes) * 100;
        Message msg = new Message();
        msg.what = MSG_UPDATE_GAME_NAME;
        game.setProgress(progress);
        switch (status) {
            case FileDownloadStatus.pending:
                game.setStatus("pending");
                break;
            case FileDownloadStatus.started:
                game.setStatus("started");
                break;
            case FileDownloadStatus.connected:
                game.setStatus("connected");
                break;
            case FileDownloadStatus.progress:
                game.setStatus("progress");
                break;
        }
        msg.obj = game;
        handler.sendMessage(msg);
    }

    @Override
    public void updateNotDownloaded(final int status, GameEntity game, final long sofar, final long total) {

        VLog.e("abc", "updateNotDownloaded status = %s --> (%s,%s)", status, sofar, total);
        float progress = 0;
        if (sofar > 0 && total > 0) {
            progress = ((float) sofar / total) * 100;
        }
        Message msg = new Message();
        msg.what = MSG_UPDATE_GAME_NAME;
        game.setProgress(progress);
        switch (status) {
            case FileDownloadStatus.warn:
                game.setStatus("warn");
                break;
            case FileDownloadStatus.error:
                game.setStatus("error");
                break;
            case FileDownloadStatus.paused:
                game.setStatus("paused");
                break;
            case FileDownloadStatus.completed:
                game.setStatus("completed");
                break;
        }
        msg.obj = game;
        handler.sendMessage(msg);
    }

    /**
     * 下载的回调
     */
    private FileDownloadListener dlCallback = new FileDownloadListener() {
        @Override
        protected void pending(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            updateDownLoading(FileDownloadStatus.pending, (GameEntity) task.getTag(), soFarBytes, totalBytes);
        }

        @Override
        protected void connected(BaseDownloadTask task, String etag, boolean isContinue, int soFarBytes, int totalBytes) {
            updateDownLoading(FileDownloadStatus.connected, (GameEntity) task.getTag(), soFarBytes, totalBytes);
        }

        @Override
        protected void progress(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            updateDownLoading(FileDownloadStatus.progress, (GameEntity) task.getTag(), soFarBytes, totalBytes);
        }

        @Override
        protected void completed(BaseDownloadTask task) {
            updateNotDownloaded(FileDownloadStatus.completed, (GameEntity) task.getTag(), 0, 0);
        }

        @Override
        protected void paused(BaseDownloadTask task, int soFarBytes, int totalBytes) {
            updateNotDownloaded(FileDownloadStatus.paused, (GameEntity) task.getTag(), soFarBytes, totalBytes);
        }

        @Override
        protected void error(BaseDownloadTask task, Throwable e) {
            updateNotDownloaded(FileDownloadStatus.error, (GameEntity) task.getTag(), task.getLargeFileSoFarBytes(), task.getLargeFileTotalBytes());
        }

        @Override
        protected void warn(BaseDownloadTask task) {
            updateNotDownloaded(FileDownloadStatus.warn, (GameEntity) task.getTag(), task.getLargeFileSoFarBytes(), task.getLargeFileTotalBytes());
        }
    };


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void getAllGame() {
        VUiKit.defer().when(() -> {
            try {
                List<GameEntity> games = GameGson.getAllGame(page, channelId);
                if (games.size() == 0) {
                    page--;
                    isNoMoreData = true;
                    return;
                }

                loadedGames.addAll(games);
                isNoMoreData = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).done(result -> {
            Message message = new Message();
            message.what = MSG_SHOW_ALL_GAME;
            handler.sendMessage(message);
        });
    }


    private class GameRepoHandler extends Handler {

        InstallResult installResult;

        @Override
        public void handleMessage(Message msg) {
            Activity activity = getActivity();
            if (activity != null) {
                switch (msg.what) {
                    case MSG_SHOW_ALL_GAME:
                        mRefreshLayout.finishRefresh();
                        if (gameRepoAdapter == null) {
                            initRecylerView();
                        }
                        gameRepoAdapter.notifyDataSetChanged();
                        mRefreshLayout.setEnableLoadMore(true);
                        resetSearched();
                        break;

                    case MSG_LOAD_HOTGAME_FINISHED:

                        break;

                    case MSG_LOAD_SEARCHGAME_FINISHED:
                        LogUtils.e("Thread:" + Thread.currentThread() + "\nsearch game had been add.");
                        gameRepoAdapter.notifyDataSetChanged();
                        LogUtils.e("game repostory adapter had been set changed.");
                        mRefreshLayout.setEnableLoadMore(false);
                        mRefreshLayout.setEnableAutoLoadMore(false);
                        break;

                    case MSG_BIND_SERVICE_SECCUSSED:
                        if (gameRepoAdapter != null) {
                            gameRepoAdapter.notifyDataSetChanged();
                        }
                        break;

                    case MSG_UPDATE_GAME_NAME:
                        GameEntity state = (GameEntity) msg.obj;
                        View itemView = gameViews.get(state.getId());
                        int isHot = state.getIshot();
                        if (isHot == 1 && itemView == null) {

                            if (hotGames.get(hotPosition).getId() != state.getId()) {
                                hotgame_progressbar.setVisibility(View.INVISIBLE);
                                hotgame_name.setVisibility(View.INVISIBLE);
                                return;
                            }

                            if (state.getStatus().equals("completed")) {
                                installHotGame(state);
                            } else if (state.getStatus().equals("error")) {
                                ToastUtils.showToast(state.getGamename() + "下载错误");
                            } else if (state.getStatus().equals("progress")) {
                                int hotGameId = hotGames.get(hotPosition).getId();
                                if (hotGameId == state.getId()) {
                                    hotgame_name.setVisibility(View.VISIBLE);
                                    hotgame_name.setOK();
                                    hotgame_name.setText(state.getGamename());
                                    hotgame_progressbar.setVisibility(View.VISIBLE);
                                    hotgame_progressbar.setProgress(Math.round(state.getProgress()));
                                }
                                VLog.e("abc", "hot game " + state.getGamename() + ", progress " + state.getProgress());
                            }
                            return;
                        }
                        GameEntity vGame = (GameEntity) gameViews.get(state.getId()).getTag();
                        if (itemView == null || !state.getGamename().equals(vGame.getGamename())) {
                            return;
                        }
                        StateTextView textView = itemView.findViewById(R.id.installed_game_name);

                        if (state.getStatus().equals("progress")) {
                            textView.setText("正在下载..");
                            textView.setOK();
                            RoundProgressBarWidthNumber itemProgressBar = gameViews.get(state.getId()).findViewById(R.id.download_progressbar);
                            itemProgressBar.setVisibility(View.VISIBLE);
                            itemProgressBar.setProgress(Math.round(state.getProgress()));
                            itemProgressBar.start();
                        } else if (state.getStatus().equals("paused")) {
                            textView.setText(state.getGamename());
                            textView.setWarn();
                            RoundProgressBarWidthNumber itemProgressBar = itemView.findViewById(R.id.download_progressbar);
                            itemProgressBar.setVisibility(View.VISIBLE);
                            itemProgressBar.setProgress(Math.round(state.getProgress()));
                            itemProgressBar.pause();
                        } else if (state.getStatus().equals("warn")) {
                            gameRepoAdapter.setDownloadStop(state.getId(), state.getProgress());
                        } else if (state.getStatus().equals("error")) {
                            textView.setError();
                        } else if (state.getStatus().equals("completed")) {
                            textView.setText("正在安装..");
                            textView.setOK();
                            RoundProgressBarWidthNumber itemProgressBar = itemView.findViewById(R.id.download_progressbar);
                            itemProgressBar.pause();
                            itemProgressBar.setVisibility(View.INVISIBLE);
                            VUiKit.defer().when(() -> {
                                VLog.e("abc", "when");
                                installResult = VirtualCore.get().installPackage(TasksManager.getImpl().getPath(state.getDlurl()), VCommends.flags);
                                if (!installResult.isSuccess) {
                                    Toast.makeText(activity, "安装: " + state.getGamename() + "失败！", Toast.LENGTH_SHORT).show();
                                    throw new IllegalStateException();
                                }
                                ClearDataUtils.clearExternalDirs(installResult.packageName, false);
                            }).done(res -> {
                                VLog.e("abc", "done");
                                try {
                                    textView.setText(state.getGamename());
                                    textView.setNormal();
                                    LabelImageView imageView = gameViews.get(state.getId()).findViewById(R.id.game_logo);
                                    String path = TasksManager.getImpl().getPath(state.getDlurl());
                                    File file = new File(path);
                                    if (file.exists()) {
                                        file.delete();
                                        Toast.makeText(activity, "删除: " + state.getGamename() + "安装包！", Toast.LENGTH_SHORT).show();
                                    }
                                    List<InstalledAppInfo> installedApps = VirtualCore.get().getInstalledApps(0);
                                    PackageManager packageManager = activity.getPackageManager();
                                    for (InstalledAppInfo installedApp : installedApps) {
                                        int[] installedUsers = installedApp.getInstalledUsers();
                                        for (int installedUser : installedUsers) {
                                            ApplicationInfo applicationInfo = installedApp.getApplicationInfo(installedUser);
                                            if (applicationInfo.packageName.equals(installResult.packageName)) {
                                                imageView.setLabelText("已安装");
                                                imageView.setLabelBackgroundColor(Color.parseColor("#67e667"));
                                                imageView.setImageDrawable(applicationInfo.loadIcon(packageManager));
                                                break;
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    VLog.e("abc", e);
                                }
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(activity, "安装: " + state.getGamename() + "成功！", Toast.LENGTH_SHORT).show();
                                    }
                                }, 5L * 100);

                            });
                        }
                        break;
                }
            }
        }
    }

    private void installHotGame(GameEntity state) {
        hotgame_name.setText("正在安装..");
        VUiKit.defer().when(() -> {
            VLog.e("abc", "when");
            InstallResult installResult = VirtualCore.get().installPackage(TasksManager.getImpl().getPath(state.getDlurl()), VCommends.flags);
            if (!installResult.isSuccess) {
                ToastUtils.showToast("安装: " + state.getGamename() + "失败！");
                throw new IllegalStateException();
            }
            ClearDataUtils.clearExternalDirs(installResult.packageName, false);
        }).done(res -> {
            VLog.e("abc", "done");
            hotgame_progressbar.setVisibility(View.INVISIBLE);
            hotgame_name.setVisibility(View.INVISIBLE);
            try {
                String path = TasksManager.getImpl().getPath(state.getDlurl());
                File file = new File(path);
                if (file.exists()) {
                    file.delete();
                    ToastUtils.showToast("删除: " + state.getGamename() + "安装包！");
                }

            } catch (Exception e) {
                VLog.e("abc", e);
            }
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ToastUtils.showToast("安装: " + state.getGamename() + "成功！");
                }
            }, 5L * 100);
        });
    }

    public void showSearchData(List<GameEntity> searchGames) {
        isNoMoreData = false;
        cleanAllData();
        loadedGames.addAll(searchGames);
        handler.sendEmptyMessage(MSG_LOAD_SEARCHGAME_FINISHED);
    }

    private final GameRepoHandler handler = new GameRepoHandler();

//    static class Status {
//        String status;
//        float progress;
//        GameEntity entity;
//
//        public String getStatus() {
//            return status;
//        }
//
//        public void setStatus(String status) {
//            this.status = status;
//        }
//
//        public GameEntity getEntity() {
//            return entity;
//        }
//
//        public void setEntity(GameEntity entity) {
//            this.entity = entity;
//        }
//
//        public float getProgress() {
//            return progress;
//        }
//
//        public void setProgress(float progress) {
//            this.progress = progress;
//        }
//    }

    public void cleanAllData() {
        if (loadedGames != null) {
            loadedGames.clear();
        }
    }

    public void resetSearched() {
        HomeActivity.INSTANCE.resetSearch();
    }

    public void refresh() {
        initData();
    }


    class GlideLoader extends ImageLoader {

        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load((String) path).into(imageView);
        }
    }

}




