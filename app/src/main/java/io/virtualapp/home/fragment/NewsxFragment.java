package io.virtualapp.home.fragment;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.gson.Gson;
import com.lody.virtual.helper.utils.VLog;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.VApp;
import io.virtualapp.home.activity.NewsDetailActivity;
import io.virtualapp.home.adapter.NewsAdapter;
import io.virtualapp.home.callback.OkHttpCallback;
import io.virtualapp.home.entity.news.NewsInfo;
import io.virtualapp.home.entity.news.NewsInfoes;
import io.virtualapp.home.fragment.base.ThemeFragment;
import io.virtualapp.home.models.NewsModel;
import io.virtualapp.home.util.ScreenUtils;
import io.virtualapp.view.utils.CheckDoubleClickUtils;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/24
 * className:
 * <p/>
 * describe:
 */
public class NewsxFragment extends ThemeFragment {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.tv_notice)
    TextView tvNotice;
    @BindView(R.id.fl_empty)
    FrameLayout flEmpty;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout mRefreshLayout;

    private static final int TYPE_NEWS = 0;
    private static final int TYPE_STRATEGY = 1;
    private static final int TYPE_NOTICE = 2;
    private static final int TYPE_GUILD = 3;

    private int MARGIN = ScreenUtils.dp2px(VApp.getApp(), 5);
    private static String packages;
    private static int type;
    private int page = 1;
    private NewsInfoes newsdetail;
    private NewsAdapter newsAdapter;
    private List<NewsInfo> dataList = new ArrayList<>();
    private boolean isRefresh;

    public NewsxFragment() {
    }

    public void setType(int type) {
        this.type = type;
    }

    @SuppressLint("ValidFragment")
    public NewsxFragment(String pkgs) {
        this.packages = pkgs;
    }

    @Override
    protected void initData() {
        VLog.i("abc", "type = %s", type);

        if (type==TYPE_GUILD){
            flEmpty.setVisibility(View.VISIBLE);
            tvNotice.setText("公会功能正在开发中...");
            return;
        }
        mRefreshLayout.autoRefresh(0, 1000, 1.0f);
        mRefreshLayout.setEnableLoadMore(true);
        mRefreshLayout.setEnableAutoLoadMore(true);
    }

    /**
     * 获取游戏资讯
     */
    private void getNews() {
        Map<String, String> map = new HashMap<>();
        map.put("key", "");
        map.put("type", String.valueOf(type));
        map.put("page", String.valueOf(page));
        map.put("package", packages);

        String param = new Gson().toJson(map);

        NewsModel.get().getNewsList(param, new OkHttpCallback<NewsInfoes>() {
            @Override
            public void response(NewsInfoes newsInfoes) {
                newsdetail = newsInfoes;
                //结束刷新状态
                finishLoadData();
                if (newsdetail==null || newsdetail.getDataList().size()==0){
                    flEmpty.setVisibility(View.VISIBLE);
                    return;
                }else{
                    flEmpty.setVisibility(View.GONE);
                }
                dataList.addAll(newsdetail.getDataList());
                //隐藏空白页面
                flEmpty.setVisibility(View.GONE);
                //填充数据到列表
                setNewsList(dataList);
                page = newsdetail.getPager().getPageNumber();
            }

            @Override
            public void errors(Throwable t) {
                finishLoadData();
                VLog.e("abc", "connent error = %s", t);
                flEmpty.setVisibility(View.VISIBLE);
            }
        });
    }

    private void finishLoadData() {
        mRefreshLayout.finishRefresh();
        mRefreshLayout.finishLoadMore();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_newsx;
    }

    @Override
    protected void initViews() {
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                //禁止用户连续刷新
                if (CheckDoubleClickUtils.isDoubleClick()) {
                    return;
                }

                if (type==TYPE_GUILD){
                    flEmpty.setVisibility(View.VISIBLE);
                    tvNotice.setText("公会管理与公会论坛功能开发中...");
                    mRefreshLayout.finishRefresh();
                    return;
                }

                isRefresh = true;
                clearList();
                getNews();
            }
        });

        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

                if (type==TYPE_GUILD){
                    flEmpty.setVisibility(View.VISIBLE);
                    tvNotice.setText("公会管理与公会论坛功能开发中...");
                    mRefreshLayout.finishLoadMore();
                    return;
                }

                if (newsdetail!=null && page==newsdetail.getPager().getPageCount()){
                    mRefreshLayout.finishLoadMore();
                    return;
                }
                getNews();
            }
        });
    }

    /**
     * 清空列表
     */
    private void clearList() {

        if (newsAdapter!=null){
            VLog.e("abc", "clean news");
            dataList.clear();
        }
    }

    private void setNewsList(List<NewsInfo> dataList) {
        if (newsAdapter==null){
            newsAdapter = new NewsAdapter(R.layout.item_news, dataList);
            rvContent.setLayoutManager(new LinearLayoutManager(getContext()));
            rvContent.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    if (parent.getChildAdapterPosition(view) == 0) {
                        outRect.set(MARGIN, MARGIN, MARGIN, MARGIN);
                    } else {
                        outRect.set(MARGIN, 0, MARGIN, MARGIN);
                    }
                }
            });
            rvContent.setAdapter(newsAdapter);
            newsAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("news_id", dataList.get(position).getId());
                    gotoActivity(NewsDetailActivity.class, bundle, false);
                }
            });
        }else {
            if (isRefresh){
                newsAdapter.replaceData(dataList);
                isRefresh = false;
            }else{
                newsAdapter.addData(dataList);
            }
            newsAdapter.notifyDataSetChanged();
        }
    }

}
