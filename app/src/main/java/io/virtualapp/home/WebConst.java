package io.virtualapp.home;

/**
 * Created by lanpan on 2018/4/25.
 */

public class WebConst {

//    public static final String baseUrl = "https://www.17sy8.cn";
    public static final String baseUrl = "https://box.jiawanhd.com";
    public static final String localbaseUrl = "http://192.168.1.170:8080";

    public static final String webUrlForLogin = baseUrl + "/box/login";                      //登陆上传文件地址
    public static final String webUrlForRegister = baseUrl + "/box/boxreg";                  //注册上传文件地址
    public static final String webUrlForResetPassword = baseUrl + "/box/updpwd";             //修改账号密码
    public static final String webUrlForGameList = baseUrl + "/box/gamelist";                //获取游戏列表
    public static final String webUrlForGameSearch = baseUrl + "/box/search";                //获取查询列表
    public static final String webUrlForHostGames = baseUrl + "/box/hotgames";               //获取热门游戏
    public static final String getGiftUrl = baseUrl + "/boxutil/getgift";                   //获取游戏礼包
    public static final String submitCodeUrl = baseUrl+ "/boxutil/ugift";                   //提交已使用的游戏礼包
//    public static final String webUrlForNews = localbaseUrl + "/gamenews/list";                       //获取新闻资讯
//    public static final String webUrlForNewsDetail = localbaseUrl+ "/gamenews/getnews";                       //获取单条新闻资讯
//    public static final String webUrlForEmailPwd = localbaseUrl + "/boxutil/updemail_pwd";//修改邮箱的密码
    public static final String webUrlForFindPwd= baseUrl + "/boxutil/findpwd";              //找回密码
    public static final String webUrlForSendCode = baseUrl + "/boxutil/sendcode";           //发送验证码
    public static final String webUrlForRegCode = baseUrl + "/boxutil/mobilecode";           //注册验证码
    public static final String webUrlForRegV1 = baseUrl + "/box/boxregv1";           //注册验证码

    public static final String webUrlForNews = baseUrl + "/gamenews/list";                       //获取新闻资讯
    public static final String webUrlForNewsDetail = baseUrl + "/gamenews/getnews";              //获取单条新闻资讯
    public static final String webUrlForEmailPwd = baseUrl + "/boxutil/updemail_pwd";           //修改邮箱的密码
//    public static final String webUrlForFindPwd= baseUrl + "/boxutil/findpwd";              //找回密码
//    public static final String webUrlForSendCode = baseUrl + "/boxutil/sendcode";           //发送验证码

    //about hook net api
    public static final String webUrlForGameLogin = "https://box.jiawanhd.com/box/gameloginlog";
    public static final String webUrlForPay = "https://box.jiawanhd.com/pay/payv1";


    public static final String feedbackLink = "https://jq.qq.com/?_wv=1027&k=5WB56B5";       //游戏反馈群链接

    public static final int SPLASH_ACTIVITY_TASK = 0x001;
    public static final int HOME_ACTIVITY_TASK = 0x002;
    public static final int RESETPWD_ACTIVITY_TASK = 0x003;
    public static final int REGISTER_ACTIVITY_TASK = 0x004;
    public static final int FORGET_ACTIVITY_TASK = 0x005;
    public static final int LOGIN_ACTIVITY_TASK = 0x006;

    //服务协议
    public final static String SERVICE_URL = "file:///android_asset/html/service.html";


}
