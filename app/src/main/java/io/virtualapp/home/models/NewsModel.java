package io.virtualapp.home.models;

import com.google.gson.Gson;
import com.lody.virtual.helper.utils.VLog;

import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.OkHttpCallback;
import io.virtualapp.home.callback.RespCallback;
import io.virtualapp.home.entity.news.NewsBean;
import io.virtualapp.home.entity.news.NewsDetail;
import io.virtualapp.home.entity.news.NewsDetailBean;
import io.virtualapp.home.entity.news.NewsInfoes;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.user.models.SendInfoWithOkHttp;


/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/25
 * className:
 * <p/>
 * describe:
 */
public class NewsModel {

    private static NewsModel newsModel = new NewsModel();

    public static NewsModel get(){
        return newsModel;
    }

    public void getNewsList(String param, OkHttpCallback<NewsInfoes> callback){
        VLog.i("abc", "news params = %s", param);
        SendInfoWithOkHttp.sendHttpPostRequest(param, WebConst.webUrlForNews, new RespCallback() {
            @Override
            public void response(String resp) {
                VLog.i("abc", "news content = %s", resp);
                NewsBean newsData = new Gson().fromJson(resp, NewsBean.class);
                NewsInfoes newsInfoes = newsData.getData().getPager();
                callback.response(newsInfoes);
            }

            @Override
            public void errors(Throwable t) {
                LogUtils.e("receive news errors : " + t.toString());
                callback.errors(t);
            }
        });
    }

    public void getNewDetail(String param, OkHttpCallback<NewsDetail> callback){

        String path = WebConst.webUrlForNewsDetail + "?id=" + param;
        SendInfoWithOkHttp.sendHttpGetRequest(path, new RespCallback() {
            @Override
            public void response(String resp) {
                VLog.i("abc", resp);
                NewsDetailBean newsDetailBean = new Gson().fromJson(resp, NewsDetailBean.class);
                NewsDetail newsDetail = newsDetailBean.getData().getData().get(0);
                callback.response(newsDetail);
            }

            @Override
            public void errors(Throwable t) {
                callback.errors(t);
                LogUtils.e("receive news errors : " + t.toString());
            }
        });
    }

}
