package io.virtualapp.home.models;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.GiftsCallback;
import io.virtualapp.home.entity.CommonBean;
import io.virtualapp.home.entity.GiftData;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.user.models.SendInfoWithOkHttp;


/**
 * Created by lanpan on 2018/7/16.
 */

public class GiftsModel {

    public static void getPresents(String uid, String pakges, GiftsCallback callback){

            VUiKit.defer().when(()->{
                try {
                    Map<String, String> params = new HashMap<>();
                    params.put("uid", uid);
                    params.put("pakges", pakges);
                    String json = new Gson().toJson(params);
                    String resultjson = SendInfoWithOkHttp.sendInfo(json, WebConst.getGiftUrl);
                    GiftData gift = new Gson().fromJson(resultjson, GiftData.class);
                    if (gift.getOperationState().equals("SUCCESS")){
                        callback.success(gift.getData().getGamegifts());
                    }
                    else if (gift.getOperationState().equals("EXCEPTION")){
                        callback.errors(gift.getErrors()[0]);
                    }
                    else{
                        callback.failure("暂时没有礼包哦~");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    callback.errors("连接失败~");
                }

            }).done(result -> {

            });
    }

    public static void submitCode(String uid, String code, String giftId){

        VUiKit.defer().when(()->{
            try {
                Map<String, String> params = new HashMap<>();
                params.put("uid", uid);
                params.put("code", code);
                params.put("giftid", giftId);
                String json = new Gson().toJson(params);
                String resultjson = SendInfoWithOkHttp.sendInfo(json, WebConst.submitCodeUrl);
                CommonBean commonBean = new Gson().fromJson(resultjson, CommonBean.class);
                if (commonBean.getOperationState().equals("SUCCESS")) {
                    LogUtils.i("gift", "礼品兑换码已提交成功");
                }else{
                    LogUtils.i("gift", "礼品兑换码提交失败");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).done(result -> {

        });

    }

}
