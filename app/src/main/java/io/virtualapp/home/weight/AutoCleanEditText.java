package io.virtualapp.home.weight;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

public class AutoCleanEditText extends EditText {

    private Drawable drawableRight;

    final int DRAWABLE_LEFT = 0;
    final int DRAWABLE_TOP = 1;
    final int DRAWABLE_RIGHT = 2;
    final int DRAWABLE_BOTTOM = 3;
    private boolean hasFocus;
    private OnCustomFocusChangeListener onCustomFocusChangeListener;
    private OnCustomTextChangeListener onCustomTextChangeListener;
    private OnDeleteClickListener onDeleteClickListener;
    @SuppressLint("NewApi")
    public AutoCleanEditText(Context context, AttributeSet attrs, int defStyleAttr,
                             int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public AutoCleanEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public AutoCleanEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public AutoCleanEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        drawableRight = getCompoundDrawables()[DRAWABLE_RIGHT];
        drawableRight.setBounds(0, 0, drawableRight.getMinimumWidth(), drawableRight.getMinimumHeight());

        this.setCompoundDrawables(null, null, null, null);
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (onCustomFocusChangeListener != null) {
                    onCustomFocusChangeListener.onFocusChangee(v, hasFocus);
                }
                AutoCleanEditText.this.hasFocus = hasFocus;
                if (hasFocus && getText().length() > 0) {
                    AutoCleanEditText.this.setCompoundDrawables(null, null, drawableRight, null);
                } else {
                    AutoCleanEditText.this.setCompoundDrawables(null, null, null, null);
                }
            }
        });

        this.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (onCustomTextChangeListener!=null){
                    onCustomTextChangeListener.afterTextChanged();
                }
                if (AutoCleanEditText.this.getText().length() > 0 && hasFocus) {
                    AutoCleanEditText.this.setCompoundDrawables(null, null, drawableRight, null);
                } else {
                    AutoCleanEditText.this.setCompoundDrawables(null, null, null, null);
                }
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                Drawable drawableRight = getCompoundDrawables()[DRAWABLE_RIGHT];

                if (drawableRight != null && event.getX() >= (getMeasuredWidth() - drawableRight.getBounds().width()-getPaddingRight())) {
                    this.setText("");
                    AutoCleanEditText.this.setCompoundDrawables(null, null, null, null);
                    if (onDeleteClickListener!=null){
                        onDeleteClickListener.onDeleteClick();
                    }
                }
                break;
        }
        return super.onTouchEvent(event);
    }


    public void setOnCustomFocusChangeListener(OnCustomFocusChangeListener onCustomFocusChangeListener) {
        this.onCustomFocusChangeListener = onCustomFocusChangeListener;
    }

    public void setOnCustomTextChangeListener(OnCustomTextChangeListener onCustomTextChangeListener) {
        this.onCustomTextChangeListener = onCustomTextChangeListener;
    }


    public interface OnDeleteClickListener{
        void onDeleteClick();
    }

    public void setOnDeleteClickListener(OnDeleteClickListener onDeleteClickListener) {
        this.onDeleteClickListener = onDeleteClickListener;
    }
}