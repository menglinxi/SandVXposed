package io.virtualapp.home.weight;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import io.virtualapp.R;

/**
 * Created by lanpan on 2018/5/16.
 */

@SuppressLint("AppCompatCustomView")
public class StateTextView extends TextView{

    enum state{
        OK,
        WARN,
        ERROR,
        NORMAL
    }

    private state currentState;

    public StateTextView(Context context) {
        super(context);
    }

    public StateTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public StateTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        Paint mPaint = new Paint();
//        mPaint.setStyle(Paint.Style.FILL);
//        mPaint.setAntiAlias(true);
//        canvas.save();
        if (currentState== state.OK){
            setTextColor(getResources().getColor(R.color.holo_green_dark));
//            mPaint.setColor(getResources().getColor(R.color.holo_green_dark));
        }
        else if (currentState== state.NORMAL){
            setTextColor(getResources().getColor(R.color.black_de));
//            mPaint.setColor(getResources().getColor(R.color.black_de));
        }
        else  if (currentState== state.WARN){
            setTextColor(getResources().getColor(R.color.col_orange_1));
//            mPaint.setColor(getResources().getColor(R.color.col_orange_1));

        }
        else if (currentState== state.ERROR){
            setTextColor(getResources().getColor(R.color.holo_red_dark));
//            mPaint.setColor(getResources().getColor(R.color.holo_red_dark));

        }else{
            setTextColor(getResources().getColor(R.color.black_de));
//            mPaint.setColor(getResources().getColor(R.color.black_de));
        }
//        canvas.drawText(getText().toString(), 0, getText().length(), mPaint);
//        canvas.restore();
        super.onDraw(canvas);
    }

    public void setNormal(){
        currentState = state.NORMAL;
    }

    public void setOK(){
        currentState = state.OK;
    }

    public void setWarn(){
        currentState = state.WARN;
    }

    public void setError(){
        currentState = state.ERROR;
    }

}
