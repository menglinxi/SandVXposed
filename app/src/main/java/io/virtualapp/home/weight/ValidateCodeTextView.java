package io.virtualapp.home.weight;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import io.virtualapp.R;

/**
 * Created by zhangweibo on 2017/1/22.
 * 验证码
 */

public class ValidateCodeTextView extends TextView implements View.OnClickListener {
    //绑定的控件输入不匹配时
    public final int TYPE_GET_CODE_UNFORMATE=1;
    //获取验证码
    public final int TYPE_GET_CODE=2;
    //获取成功,并加载中
    public final int TYPE_CODE_LOADING=3;
    //重新获取验证码
    public final int TYPE_GET_CODE_AGAIN=4;
    //定时器
    private Timer mTimer;

    private int type=TYPE_GET_CODE;

    private int[] textColor=new int[]{Color.parseColor("#30e1fd"),Color.parseColor("#afafaf")};

    private onValidateClickListenr mOnValidateClickListenr;
    //绑定的edit
    private EditText mBindEdit;
    private  boolean isBackground;
    public ValidateCodeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ValidateCodeTextView, 0, 0);
        isBackground=a.getBoolean(R.styleable.ValidateCodeTextView_isBackground,true);
        a.recycle();
        init();
    }

    public void init(){
        mTimer=new Timer(90000,1000);
        if (isBackground){
            this.setBackgroundResource(R.drawable.sel_btn_bg_validate);
        }
        this.setOnClickListener(this);
        setStatusByType();
    }

    @Override
    public void onClick(View v) {
        if (mOnValidateClickListenr!=null){
            mOnValidateClickListenr.onValidateClick();
        }
    }

    /**
     * 绑定输入框
     * @param view
     */
    public void bindInputView(EditText view){
        this.mBindEdit=view;
        this.type=TYPE_GET_CODE_UNFORMATE;
        setStatusByType();
        this.mBindEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //正在加载中直接跳过
                if (type==TYPE_CODE_LOADING){
                    return;
                }
                if (isPhoneNumber(mBindEdit.getText().toString())){
                    setEnabled(true);
                }else {
                    setEnabled(false);
                }
            }
        });
    }

    /**
     *开始计时
     */
    public void startTimer(){
        type=TYPE_CODE_LOADING;
        setStatusByType();
        mTimer.start();
    }

    class Timer extends CountDownTimer{

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public Timer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            String str=String.format(getResources().getString(R.string.reagain_obtain),millisUntilFinished/1000);
            setText(str);
        }

        @Override
        public void onFinish() {
            type=TYPE_GET_CODE_AGAIN;
            setStatusByType();
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled){
            this.setTextColor(textColor[0]);
        }else {
            this.setTextColor(textColor[1]);
        }
    }

    /**
     * 根据状态设置验证码的视图
     */
    private void setStatusByType(){
        switch (type){
            case TYPE_GET_CODE:
                this.setEnabled(true);
                this.setText("获取验证码");
                break;
            case TYPE_CODE_LOADING:
                this.setEnabled(false);
                break;
            case TYPE_GET_CODE_AGAIN:
                this.setEnabled(true);
                this.setText("重新获取");
                break;
            case TYPE_GET_CODE_UNFORMATE:
                this.setEnabled(false);
                this.setText("获取验证码");
                break;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mTimer!=null){
            mTimer.cancel();
        }
    }

   public interface  onValidateClickListenr{

        void onValidateClick();
    }

    public void setOnValidateClickListenr(onValidateClickListenr mOnValidateClickListenr) {
        this.mOnValidateClickListenr = mOnValidateClickListenr;
    }

    public static boolean isPhoneNumber(String number) {
        return number.matches("^1[34578]\\d{9}$");
    }
}
