package io.virtualapp.home.weight;

import android.view.View;

public interface OnCustomFocusChangeListener {
        void onFocusChangee(View v, boolean hasFocus);
    }