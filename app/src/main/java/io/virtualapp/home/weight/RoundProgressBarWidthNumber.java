package io.virtualapp.home.weight;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;

import io.virtualapp.R;


public class RoundProgressBarWidthNumber extends HorizontalProgressBarWithNumber {

    /**
     * mRadius of view
     */
    private int mRadius = dp2px(30);
    private int default1dp = dp2px(1);
    private int default2dp = dp2px(2);
    private int default3dp = dp2px(3);
    private int default4dp = dp2px(4);
    private int default5dp = dp2px(5);

    private int mMaxPaintWidth;
    private int realWidth;
    private int rectColor;
    private int rectCorner = dp2px(5);
    private int foreginCircleColor;

    private boolean isPause = false;

    public RoundProgressBarWidthNumber(Context context) {
        this(context, null);
    }

    public RoundProgressBarWidthNumber(Context context, AttributeSet attrs) {
        super(context, attrs);

        mReachedProgressBarHeight = (int) (mUnReachedProgressBarHeight * 2.5f);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.RoundProgressBarWidthNumber);
        mRadius = (int) ta.getDimension(R.styleable.RoundProgressBarWidthNumber_radius, mRadius);
        rectColor = (int) ta.getColor(R.styleable.RoundProgressBarWidthNumber_rect_color, Color.BLACK);
        foreginCircleColor = (int) ta.getColor(R.styleable.RoundProgressBarWidthNumber_foreign_circle_color, Color.TRANSPARENT);
        rectCorner = (int) ta.getColor(R.styleable.RoundProgressBarWidthNumber_rect_corner, rectCorner);

        ta.recycle();

        mPaint.setStyle(Style.STROKE);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStrokeCap(Cap.ROUND);

    }

    /**
     * 这里默认在布局中padding值要么不设置，要么全部设置
     */
    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        mMaxPaintWidth = Math.max(mReachedProgressBarHeight, mUnReachedProgressBarHeight);
        int expect = mRadius * 2 + mMaxPaintWidth + getPaddingLeft() + getPaddingRight();
        int width = resolveSize(expect, widthMeasureSpec);
        int height = resolveSize(expect, heightMeasureSpec);
        realWidth = Math.min(width, height);

        mRadius = (realWidth - getPaddingLeft() - getPaddingRight() - mMaxPaintWidth) / 2;

        setMeasuredDimension(realWidth, realWidth);

    }


    @Override
    protected synchronized void onDraw(Canvas canvas)
    {
        String text = getProgress() + "%\n";
        float textWidth = mPaint.measureText(text);
        float textHeight = (mPaint.descent() + mPaint.ascent()) / 2;

        canvas.save();

        //1.绘制矩形
        RectF rectf = new RectF(0, 0, realWidth, realWidth);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Style.FILL);
        mPaint.setColor(rectColor);
        mPaint.setAlpha(255/2);

        Path mPath = new Path();
        mPath.setFillType(Path.FillType.INVERSE_WINDING);
        mPath.addCircle(realWidth/2, realWidth/2, (realWidth/2)-dp2px(5), Path.Direction.CW);
        canvas.clipPath(mPath);
        canvas.drawRoundRect(rectf, rectCorner, rectCorner, mPaint);
        canvas.restore();

        //把原点位移到中心点
        canvas.save();
        canvas.translate(getPaddingLeft() + mMaxPaintWidth / 2, getPaddingTop() + mMaxPaintWidth / 2);

        if (isPause){
            drawPause(canvas);
        }
        else{
            drawLoading(canvas, text, textWidth, textHeight);
        }

        invalidate(0, 0, mRadius*2, mRadius*2);
    }

    //绘制暂停状态
    private void drawPause(Canvas canvas) {

        float runSweap = getProgress() * 1.0f / getMax() * 360;

        //绘制内圆
        mPaint.setAntiAlias(true);
        mPaint.setColor(mUnReachedBarColor);
        mPaint.setStyle(Style.FILL);
        mPaint.setAlpha(255/2);
        mPaint.setStrokeWidth(mUnReachedProgressBarHeight);

        Path p1 = new Path();
        p1.setFillType(Path.FillType.INVERSE_WINDING);
        p1.addRect(mRadius-dp2px(6), mRadius-dp2px(6), mRadius-dp2px(2), mRadius+dp2px(6), Path.Direction.CCW);
        p1.addRect(mRadius+dp2px(2), mRadius-dp2px(6), mRadius+dp2px(6), mRadius+dp2px(6), Path.Direction.CCW);
        canvas.clipPath(p1);
        canvas.drawCircle(mRadius, mRadius, mRadius, mPaint);

        mPaint.setColor(Color.RED);
        mPaint.setStyle(Style.STROKE);
        mPaint.setStrokeWidth(default4dp);
        canvas.drawArc(new RectF(-default1dp,-default1dp,mRadius*2+default1dp, mRadius*2+default1dp), -90, runSweap, false, mPaint);

        canvas.restore();
    }

    //绘制loading状态
    private void drawLoading(Canvas canvas, String text, float textWidth, float textHeight) {
        //绘制内圆
        mPaint.setAntiAlias(true);
        mPaint.setColor(mUnReachedBarColor);
        mPaint.setStyle(Style.FILL);
        mPaint.setAlpha(255/2);
        mPaint.setStrokeWidth(mUnReachedProgressBarHeight);
        canvas.drawCircle(mRadius, mRadius, mRadius, mPaint);

        //绘制进度条
        mPaint.setAntiAlias(true);
        mPaint.setColor(mReachedBarColor);
        mPaint.setAlpha(255/2);
        mPaint.setStrokeWidth(mReachedProgressBarHeight);
        float sweepAngle = getProgress() * 1.0f / getMax() * 360;
        canvas.drawArc(new RectF(0, 0, mRadius*2, mRadius*2), -90, sweepAngle, true, mPaint);

        //绘制文字
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Style.FILL);
        mPaint.setColor(Color.WHITE);
        mPaint.setTextSize(mTextSize);
        canvas.drawText(text, mRadius - textWidth / 2, mRadius - textHeight, mPaint);
        canvas.restore();
    }

    public void pause(){
        this.isPause = true;
    }

    public void start(){
        isPause = false;
    }

}
