package io.virtualapp.home.callback;

import java.util.List;

import io.virtualapp.home.entity.GameGifts;

/**
 * Created by lanpan on 2018/7/16.
 */

public interface GiftsCallback {

    void success(List<GameGifts> gamegifts);

    void failure(String msg);

    void errors(String errors);

}
