package io.virtualapp.home.callback;

/**
 * Created by lanpan on 2018/4/18.
 */

public interface GameDownloadCallback {

    void update(Float progress);

    void complete();

    void pause(Float progress);

    void error(String errorInfo);

}
