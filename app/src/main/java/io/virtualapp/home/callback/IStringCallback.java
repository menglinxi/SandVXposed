package io.virtualapp.home.callback;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/19
 * className:
 * <p/>
 * describe:
 */
public interface IStringCallback {

    void getStrResp(String str);
}
