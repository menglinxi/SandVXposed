package io.virtualapp.home.callback;

/**
 * Created by lanpan on 2018/4/25.
 */

public interface UserCallback {

    void successed(String content);

    void failed(String error);

}
