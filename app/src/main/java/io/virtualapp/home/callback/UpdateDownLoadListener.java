package io.virtualapp.home.callback;

import android.util.SparseArray;
import android.view.View;

import io.virtualapp.entity.GameEntity;

public interface UpdateDownLoadListener {

    public void updateDownLoading(int status, GameEntity game, long soFarBytes, long totalBytes);

    public void updateNotDownloaded(final int status, GameEntity game, final long sofar, final long total);

    public SparseArray<View> getGameViews();
}
