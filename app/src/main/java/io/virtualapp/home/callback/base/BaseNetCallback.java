package io.virtualapp.home.callback.base;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/25
 * className:
 * <p/>
 * describe:
 */
public interface BaseNetCallback {

    void errors(Throwable t);

}
