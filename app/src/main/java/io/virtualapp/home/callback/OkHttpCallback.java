package io.virtualapp.home.callback;

import io.virtualapp.home.callback.base.BaseNetCallback;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/24
 * className:
 * <p/>
 * describe:
 */
public interface OkHttpCallback<T extends Object> extends BaseNetCallback {

    void response(T t);

}
