package io.virtualapp.home.util;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lanpan on 2018/7/17.
 */

public class TimerDelayUtil {

    private static final int MSG_SET_TEXT = 0;
    private Timer timer;
    private TimerTask timerTask;
    private int defaulteTime = 3;

    private static TimerDelayUtil instance;

    public static TimerDelayUtil get() {
        if (instance==null){
            instance = new TimerDelayUtil();
        }
        return instance;
    }

    /**
     * 响应时长提示
     */
    public void startTimeDelay(ITimer iTimer){

        reset();

        if (timer==null){
            timer = new Timer();
        }

        if (timerTask==null){
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    defaulteTime--;
                    iTimer.callback(defaulteTime);

                    if (defaulteTime==0){
                        cancelTimer();
                    }
                }
            };
        }

        timer.schedule(timerTask, 1000, 1000);
    }


    /**
     * 响应时长提示
     */
    public void startTimeDelay(boolean isCode, ITimer iTimer){

        if (isCode){
            defaulteTime = 60;
        }

        if (timer==null){
            timer = new Timer();
        }

        if (timerTask==null){
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    defaulteTime--;
                    iTimer.callback(defaulteTime);

                    if (defaulteTime==0){
                        cancelTimer();
                    }
                }
            };
        }

        timer.schedule(timerTask, 1000, 1000);
    }

    private void reset() {
        defaulteTime = 3;
    }

    public void cancelTimer(){
        if (timerTask!=null){
            timerTask.cancel();
            timerTask = null;
        }
    }

    public void stopTimer(){
        cancelTimer();
        if (timer!=null){
            timer.cancel();
        }
        timer = null;
    }

    public interface ITimer{

        void callback(int time);

    }




}
