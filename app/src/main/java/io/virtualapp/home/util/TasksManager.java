package io.virtualapp.home.util;

import android.app.Activity;
import android.os.Environment;
import android.text.TextUtils;
import android.util.SparseArray;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadConnectListener;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.model.FileDownloadStatus;
import com.liulishuo.filedownloader.util.FileDownloadUtils;

import java.lang.ref.WeakReference;

import io.virtualapp.home.callback.ConnentListener;

/**
 * Created by lanpan on 2018/4/27.
 */

public class TasksManager {

    private static SparseArray<BaseDownloadTask> taskSparseArray = new SparseArray<>();

    public static TasksManager getImpl() {
        return new TasksManager();
    }

    public BaseDownloadTask getDownloadTask(String url, String path, FileDownloadListener taskDownloadListener){
        return FileDownloader.getImpl().create(url)
                .setPath(path)
                .setCallbackProgressTimes(500) //进度返回次数
                .setAutoRetryTimes(3)   //自动重试次数
                .setSyncCallback(true)  //设置回调在子线程
                .setListener(taskDownloadListener);
    }

    /**
     * 文件的路径
     * （文件名没有通过MD5加密）
     * @param downloadUrl
     * @return
     */
    public String getPath(String downloadUrl){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath()
                + downloadUrl.substring(downloadUrl.lastIndexOf("/"));
    }

    /**
     * 文件的路径
     * （文件名经过MD5加密）
     * @param url
     * @return
     */
    public String getMD5NamePath(final String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        return FileDownloadUtils.getDefaultSaveFilePath(url);
    }

    public void addTask(int id, final BaseDownloadTask task) {
        taskSparseArray.put(id, task);
    }

    public void removeTask(final int id) {
        taskSparseArray.remove(id);
    }

    public BaseDownloadTask getTask(final int id) {
        return taskSparseArray.get(id);
    }

    public SparseArray<BaseDownloadTask> getAllTask(){
        return taskSparseArray;
    }

    public void releaseTask() {
        taskSparseArray.clear();
    }

    private FileDownloadConnectListener listener;

    private void registerServiceConnectionListener(final WeakReference<? extends Activity> activityWeakReference, ConnentListener connentListener) {
        if (listener != null) {
            FileDownloader.getImpl().removeServiceConnectListener(listener);
        }

        listener = new FileDownloadConnectListener() {

            @Override
            public void connected() {
                if (activityWeakReference == null || activityWeakReference.get() == null) {
                    return;
                }
                connentListener.callback();

            }

            @Override
            public void disconnected() {
                if (activityWeakReference == null
                        || activityWeakReference.get() == null) {
                    return;
                }

                connentListener.callback();
            }
        };

        FileDownloader.getImpl().addServiceConnectListener(listener);
    }

    private void unregisterServiceConnectionListener() {
        FileDownloader.getImpl().removeServiceConnectListener(listener);
        listener = null;
    }

    public void onCreate(final WeakReference<? extends Activity> activityWeakReference, ConnentListener listener) {
        if (!FileDownloader.getImpl().isServiceConnected()) {
            FileDownloader.getImpl().bindService();
            registerServiceConnectionListener(activityWeakReference, listener);
        }
    }

    public void onDestroy() {
        unregisterServiceConnectionListener();
        releaseTask();
    }

    public boolean isReady() {
        return FileDownloader.getImpl().isServiceConnected();
    }

    /**
     * @param status Download Status
     * @return has already downloaded
     * @see FileDownloadStatus
     */
    public boolean isDownloaded(final int status) {
        return status == FileDownloadStatus.completed;
    }

    public int getStatus(final int id, String path) {
        return FileDownloader.getImpl().getStatus(id, path);
    }

    public long getTotal(final int id) {
        return FileDownloader.getImpl().getTotal(id);
    }

    public long getSoFar(final int id) {
        return FileDownloader.getImpl().getSoFar(id);
    }

}
