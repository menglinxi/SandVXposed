package io.virtualapp.home.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.virtualapp.home.callback.UserCallback;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.user.models.SendInfoWithOkHttp;

import static io.virtualapp.home.WebConst.webUrlForResetPassword;

/**
 * Created by lanpan on 2018/4/28.
 */

public class ResetPwdUtil {


    private final Context context;

    public static ResetPwdUtil getImpl(Context context){
        return new ResetPwdUtil(context);
    }

    private ResetPwdUtil(Context context){
        this.context = context;
    }

    /**
     * @Date: 2018/4/25 8:36
     * @Param:
     * @Description: 登陆
     */
    public void resetPwd(String json, UserCallback callback){
        new Thread(() ->{
            try {
                String postResult = SendInfoWithOkHttp.sendInfo(json,webUrlForResetPassword);
                if (isResetSuccess(postResult)){
                    callback.successed(postResult);
                    save(postResult); }else{
                    if (postResult==null){
                        callback.failed(postResult);
                        return;

                    }
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(postResult);
                        String error = jsonObject.getString("errors");
                        JSONArray jsonArray = new JSONArray(error);
                        if (jsonArray.length()==0){
                            callback.failed(null);
                            return;
                        }
                        callback.failed((String) jsonArray.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                LogUtils.d( "change pwd result: "+ postResult);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

    }
    /**
     * @param:postResult,登陆时服务器返回的json数据
     * @Description:根据服务器返回的数据来判断用户是否登陆成功
     */
    public boolean isResetSuccess(String postResult) {
        //如果返回的json数据中没有errors则认为成功登陆
        String error = "errors";
        Pattern pattern = Pattern.compile(error);
        Matcher matcher = pattern.matcher(postResult);
        return !matcher.find();
    }

    /**
     * @param:postResult,服务器回传的json数据
     * @Description:保存服务器回传的json数据至本地
     */
    public void save(String postResult) {
        SharedPreferences sp = context.getSharedPreferences("ResetResult", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("info", postResult);
        editor.commit();
    }

    /**
     * @Date: 2018/4/25 8:25
     * @Param:
     * @Description: 生成登陆时上传的json文件
     */
    public String getUploadJson(String name,String oldpwd, String newpwd, String confirmpwd){

        Map<String, String> resetPwdMap = new HashMap<>();
        resetPwdMap.put("name",name);
        resetPwdMap.put("oldpwd",oldpwd);
        resetPwdMap.put("newpwd",newpwd);
        resetPwdMap.put("cnewpwd",confirmpwd);
        return new Gson().toJson(resetPwdMap);
    }

}
