package io.virtualapp.home.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.liulishuo.filedownloader.FileDownloader;
import com.lody.virtual.helper.utils.VLog;
import com.yinglan.alphatabs.AlphaTabView;
import com.yinglan.alphatabs.AlphaTabsIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.VApp;

import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.entity.GameEntity;
import io.virtualapp.home.entity.UserInfo;
import io.virtualapp.home.fragment.GameRepoFragment;
import io.virtualapp.home.fragment.MyGameFragment;
import io.virtualapp.home.fragment.UserInfoFragment;
import io.virtualapp.home.util.StatusBarUtils;
import io.virtualapp.home.util.TasksManager;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.home.weight.NoTouchViewPager;
import io.virtualapp.tools.AESUtil;
import io.virtualapp.tools.GameGson;
import io.virtualapp.tools.PhoneInfoCollect;
import io.virtualapp.user.models.LoginUtils;

import static io.virtualapp.home.WebConst.HOME_ACTIVITY_TASK;

public class HomeActivity extends BaseActivity {

    public static final String ACTION_UPDATEUI = "action.updateUI";
    private GameRepoFragment gameRepoFragment = null;

    @BindView(R.id.main_viewpager)
    NoTouchViewPager mainViewpager;
    @BindView(R.id.main_alphatabs_indicator)
    AlphaTabsIndicator mainAlphatabsIndicator;
    @BindView(R.id.main_installed)
    AlphaTabView mainInstalled;
    @BindView(R.id.main_game_repo)
    AlphaTabView mainGameRepo;
    @BindView(R.id.main_user_info)
    AlphaTabView mainUserInfo;

    private List<Fragment> mainfragments = new ArrayList<>();
    private MainAdapter mainAdapter;
    private ActionBar actionBar;
    public static HomeActivity INSTANCE;
    private String userInfo;
    private EditText findGames;
    public boolean searchOk;
    private boolean isLive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VApp.activityContainer.put(HOME_ACTIVITY_TASK, this);
        INSTANCE = this;
        StatusBarUtils.setWindowStatusBarColor(this, R.color.theme_color);
        actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setTitle(R.string.title_news);
        VLog.i("abc", "Home page oncreate");
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public void initData() {
        userInfo = getIntent().getStringExtra("userinfo");
        if (userInfo==null){
            userInfo = LoginUtils.getImpl(this).getPostResult();
        }
    }

    @Override
    public void initViews() {
        mainAdapter = new MainAdapter(getSupportFragmentManager());
        mainViewpager.setAdapter(mainAdapter);
        mainViewpager.addOnPageChangeListener(mainAdapter);
        mainViewpager.setOffscreenPageLimit(3);
        mainAlphatabsIndicator.setViewPager(mainViewpager);
    }

    class MainAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private boolean isEdit;
        private String oldContent;

        public MainAdapter(FragmentManager fm) {
            super(fm);
            gameRepoFragment = GameRepoFragment.newInstance(userInfo);
            mainfragments.add(MyGameFragment.newInstance(userInfo));
            mainfragments.add(gameRepoFragment);
//            mainfragments.add(NewsFragment.newInstance(userInfo));
            mainfragments.add(UserInfoFragment.newInstance(userInfo));
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            VLog.e("abc", "onPageSelected = %s", position);
            switch (position) {

                case 0:
                    actionBar.show();
                    actionBar.setTitle(R.string.title_game_my);
                    actionBar.setCustomView(null);
                    getItem(position).onResume();
                    break;

                case 1:
                    actionBar.show();
                    if (actionBar.getCustomView() == null || !actionBar.isShowing()) {
                        actionBar.setTitle(null);
                        actionBar.setCustomView(R.layout.search_layout);
                        actionBar.setDisplayShowCustomEnabled(true);
                        View view = actionBar.getCustomView();
                        ImageView iconLeft = view.findViewById(R.id.icon_search_left);
                        ImageView iconRight = view.findViewById(R.id.icon_search_right);
                        findGames = view.findViewById(R.id.find_games);
                        TextView cancel = view.findViewById(R.id.cancel_textView);
                        findGames.setOnFocusChangeListener((View v, boolean hasFocus) -> {
                            if (hasFocus) {
                                iconLeft.setVisibility(View.VISIBLE);
                                iconRight.setVisibility(View.GONE);
                                cancel.setVisibility(View.VISIBLE);
                                cancel.setText(R.string.search);

                            } else {
                                iconLeft.setVisibility(View.GONE);
                                iconRight.setVisibility(View.VISIBLE);
                                cancel.setVisibility(View.GONE);
                            }
                        });
                        cancel.setOnClickListener((View v) -> {
                            String type = cancel.getText().toString();
                            if (type.equals("取消")) {
                                findGames.setText("");
                            } else if (type.equals("搜索")) {
                                String content = findGames.getText().toString();
                                if (content.equals(oldContent) && searchOk) {
                                    ToastUtils.showToast("已经为您找到了" + content);
                                    return;
                                }

                                oldContent = content;
                                int page = gameRepoFragment.page;
                                JsonUtils jsonUtils = new JsonUtils(userInfo);
                                PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
                                String loginJson = AESUtil.decrypt(jsonUtils.key("data").key("login").stringValue(), (new StringBuffer(AESUtil.genKey(phoneInfoCollect.ANDROID_ID, phoneInfoCollect.IMEI1)).reverse()).toString());
                                UserInfo.DataBean.LoginBean loginBean = new Gson().fromJson(loginJson, UserInfo.DataBean.LoginBean.class);
                                hideInputKeyBoard();
                                getSearchGame(page, loginBean.getChannelid(), content);
                            }
                        });
                        findGames.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if (findGames.hasFocus() && count == 0) {
                                    gameRepoFragment.cleanAllData();
                                    gameRepoFragment.refresh();
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });


                    }
                    getItem(position).onResume();
                    break;

                case 2:
                    actionBar.show();
                    actionBar.setTitle(R.string.title_news);
                    actionBar.setCustomView(null);
                    getItem(position).onResume();
                    break;

                case 3:
                    actionBar.show();
                    actionBar.setTitle(R.string.title_my_info);
                    actionBar.setCustomView(null);
                    getItem(position).onResume();
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        @Override
        public Fragment getItem(int position) {
            return mainfragments.get(position);
        }

        @Override
        public int getCount() {
            return mainfragments.size();
        }

//        @Override
//        public int getItemPosition(Object object) {
//            VLog.e("abc", "getItemPosition");
//            return POSITION_NONE;
//        }
    }

    public void getSearchGame(int page, int channelId, String content) {
        VUiKit.defer().when(() -> {
            try {
                List<GameEntity> games = GameGson.getSearchGame(page, channelId, content);
                if (games.size() == 0) {
                    runOnUiThread(() -> {
                        ToastUtils.showToast("没有搜到" + content);
                        hideInputKeyBoard();
                    });
                    return;
                }
                gameRepoFragment.resetSearched();
                gameRepoFragment.showSearchData(games);
                searchOk = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).done(result -> {

        });
    }

    private void hideInputKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(findGames.getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VApp.activityContainer.remove(HOME_ACTIVITY_TASK);
        VLog.e("abc", HomeActivity.class.getName() + " onDestroy");
        TasksManager.getImpl().onDestroy();
        FileDownloader.getImpl().pauseAll();
    }

    public void resetSearch() {
        searchOk = false;
//        findGames.setFocusableInTouchMode(false);
//        findGames.setText("");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
//            Intent home = new Intent(Intent.ACTION_MAIN);
//            home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            home.addCategory(Intent.CATEGORY_HOME);
//            startActivity(home);
            return true;
        }
        return super.onKeyDown(keyCode,event);
    }




}
