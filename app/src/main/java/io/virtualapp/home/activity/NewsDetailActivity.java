package io.virtualapp.home.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.zzhoujay.richtext.ImageHolder;
import com.zzhoujay.richtext.RichText;
import com.zzhoujay.richtext.RichTextConfig;
import com.zzhoujay.richtext.callback.Callback;
import com.zzhoujay.richtext.callback.DrawableGetter;
import com.zzhoujay.richtext.callback.ImageFixCallback;
import com.zzhoujay.richtext.callback.OnImageClickListener;
import com.zzhoujay.richtext.callback.OnUrlClickListener;

import java.util.List;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.home.callback.OkHttpCallback;
import io.virtualapp.home.entity.news.NewsDetail;
import io.virtualapp.home.models.NewsModel;
import io.virtualapp.home.util.StatusBarUtils;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/25
 * className:
 * <p/>
 * describe:
 */
public class NewsDetailActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_author)
    TextView tvAuthor;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.line)
    View line;
    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    private ActionBar actionBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.theme_color);
        actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setTitle(R.string.detail_news);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_news_detail;
    }

    @Override
    public void initData() {

        int newsId = getIntent().getExtras().getInt("news_id");
        int newsType = getIntent().getExtras().getInt("news_type");
        NewsModel.get().getNewDetail(newsId+"", new OkHttpCallback<NewsDetail>() {
            @Override
            public void response(NewsDetail newsDetail) {
                if (tvEmpty.isShown()){
                    tvEmpty.setVisibility(View.GONE);
                }
                tvTitle.setText(newsDetail.getTitle());
                tvAuthor.setText(newsDetail.getAuthor());
//                String timeDescription = TimeUtil.getDescriptionTime(newsDetail.getCt());
//                if (timeDescription==null){
//                    tvTime.setText(newsDetail.getCt());
//                }else{
//                    tvTime.setText(timeDescription);
//                }
                tvTime.setText(newsDetail.getCt());
                setHtmlToTextView(newsDetail.getContext());
            }

            @Override
            public void errors(Throwable t) {
                tvEmpty.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * 加载html页面
     * @param content
     */
    private void setHtmlToTextView(String content) {
        RichText.fromHtml(content)
                .autoFix(true) // 是否自动修复，默认true
                .autoPlay(true) // gif图片是否自动播放
//                .showBorder(true) // 是否显示图片边框
//                .borderColor(getResources().getColor(R.color.fixed_status_color)) // 图片边框颜色
                .borderSize(10) // 边框尺寸
                .borderRadius(50) // 图片边框圆角弧度
                .scaleType(ImageHolder.ScaleType.fit_xy) // 图片缩放方式
                .size(ImageHolder.MATCH_PARENT, ImageHolder.WRAP_CONTENT) // 图片占位区域的宽高
                .fix(imageFixCallback) // 设置自定义修复图片宽高
//                .fixLink(linkFixCallback) // 设置链接自定义回调
                .clickable(true) // 是否可点击，默认只有设置了点击监听才可点击
                .imageClick(onImageClickListener) // 设置图片点击回调
                .urlClick(onURLClickListener) // 设置链接点击回调
//                .urlLongClick(onUrlLongClickListener) // 设置链接长按回调
                .placeHolder(new DrawableGetter() {
                    @Override
                    public Drawable getDrawable(ImageHolder imageHolder, RichTextConfig richTextConfig, TextView textView) {
                        return getResources().getDrawable(R.mipmap.img_placeor);
                    }
                }) // 设置加载中显示的占位图
                .errorImage(new DrawableGetter() {
                    @Override
                    public Drawable getDrawable(ImageHolder imageHolder, RichTextConfig richTextConfig, TextView textView) {
                        return getResources().getDrawable(R.mipmap.vv_kong);
                    }
                }) // 设置加载失败的错误图
//                .imageGetter(yourImageGetter) // 设置图片加载器，默认为DefaultImageGetter，使用okhttp实现
//                .imageDownloader(yourImageDownloader) // 设置DefaultImageGetter的图片下载器
                .bind(this) // 绑定richText对象到某个object上，方便后面的清理
                .done(new Callback() {
                    @Override
                    public void done(boolean b) {//TODO 隐藏加载框

                    }
                }) // 解析完成回调
                .into(tvContent);
    }

    private OnUrlClickListener onURLClickListener = new OnUrlClickListener() {
        @Override
        public boolean urlClicked(String s) {

            //return true代表消费事件
            return false;
        }
    };

    /**
     * Html上面的图片点击回调
     */
    private OnImageClickListener onImageClickListener = new OnImageClickListener() {
        @Override
        public void imageClicked(List<String> list, int pos) {
            //TODO
        }
    };

    /**
     * 设置自定义修复图片宽高
     */
    private ImageFixCallback imageFixCallback = new ImageFixCallback() {
        @Override
        public void onInit(ImageHolder imageHolder) {

        }

        @Override
        public void onLoading(ImageHolder imageHolder) {

        }

        @Override
        public void onSizeReady(ImageHolder imageHolder, int i, int i1, ImageHolder.SizeHolder sizeHolder) {

        }

        @Override
        public void onImageReady(ImageHolder imageHolder, int i, int i1) {

        }

        @Override
        public void onFailure(ImageHolder imageHolder, Exception e) {

        }
    };

    @Override
    public void initViews() {
        RichText.initCacheDir(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RichText.clear(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
