package io.virtualapp.home.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import io.virtualapp.R;
import io.virtualapp.delegate.service.RoundService;
import io.virtualapp.home.util.StatusBarUtils;

/**
 * Created by lanpan on 2018/4/17.
 */

public abstract class BaseActivity extends AppCompatActivity {
    private final long ORDER_CHECK_INTERNAL = 10 * 60 * 1000L; //每间隔一段时间检查是否有订单需要上报
    private Timer mTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.transparent);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        setContentView(getLayoutId());
    }


    /**
     * 启动后台订单自提交服务
     */
    private void startOrderTranceService() {
        Intent intent = new Intent(this, RoundService.class);
        startService(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mTimer != null) {
            //关闭当前时钟，
            mTimer.cancel();
        }
        mTimer = new Timer();



        //重新激活时钟
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                startOrderTranceService();
            }
        }, 0, ORDER_CHECK_INTERNAL);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mTimer != null) {
            //关闭上报时钟
            mTimer.cancel();
        }
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
        initData();
        initViews();
    }

    public abstract int getLayoutId();

    public abstract void initData();

    public abstract void initViews();

    public void gotoActivity(Class<? extends Activity> clazz) {
        gotoActivity(clazz, false);
    }

    public void gotoActivity(Class<? extends Activity> clazz, boolean finish) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
        if (finish) {
            finish();
        }
    }

    public void gotoActivity(Class<? extends Activity> clazz, Bundle bundle, boolean finish) {
        Intent intent = new Intent(this, clazz);
        if (bundle != null) intent.putExtras(bundle);
        startActivity(intent);
        if (finish) {
            finish();
        }
    }

}
