package io.virtualapp.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import io.virtualapp.R;
import io.virtualapp.VApp;
import io.virtualapp.home.util.StatusBarUtils;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.user.activity.LoginActivity;
import io.virtualapp.user.models.LoginUtils;

import static io.virtualapp.home.WebConst.HOME_ACTIVITY_TASK;
import static io.virtualapp.home.WebConst.SPLASH_ACTIVITY_TASK;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VApp.activityContainer.put(SPLASH_ACTIVITY_TASK, this);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.qmui_config_color_transparent);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public void initData() {

    }

    @Override
    public void initViews() {
        ToastUtils.init(this);
        new Handler().postDelayed(()->{
            HomeActivity homeActivity = (HomeActivity) VApp.activityContainer.get(HOME_ACTIVITY_TASK);
            if (LoginUtils.getImpl(SplashActivity.this).getState() && homeActivity!=null){
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                finish();
            }else{
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }, 500);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VApp.activityContainer.remove(SPLASH_ACTIVITY_TASK);
    }
}
