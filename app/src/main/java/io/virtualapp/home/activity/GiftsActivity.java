package io.virtualapp.home.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;

import java.util.List;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.home.adapter.GameGiftsAdapter;
import io.virtualapp.home.adapter.GiftsAdapter;
import io.virtualapp.home.callback.GiftsCallback;
import io.virtualapp.home.entity.GameGifts;
import io.virtualapp.home.entity.Gifts;
import io.virtualapp.home.models.GiftsModel;
import io.virtualapp.home.util.ScreenUtils;
import io.virtualapp.home.util.StatusBarUtils;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.view.utils.CheckDoubleClickUtils;

public class GiftsActivity extends BaseActivity{

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.empty)
    TextView tvEmpty;
    @BindView(R.id.rl_gift_container)
    RelativeLayout rlGiftContainer;

    private ActionBar actionBar;
    private GameGiftsAdapter gameGiftsAdapter;
    private String uid;
    private List<String> apps;
    private List<GameGifts> giftList;
    private View rootView;
    public static GiftsActivity INSTANCE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActionbar();
        INSTANCE = this;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_gifts;
    }

    private void addSpaceHeaderView(View targetView,  BaseQuickAdapter adapter) {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ScreenUtils.dp2px(this, 20));
        targetView.setLayoutParams(layoutParams);
        if (adapter!=null){
            adapter.addHeaderView(targetView, 0);
        }
    }


    /**
     * 初始化ActionBar
     */
    private void initActionbar() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.theme_color);
        actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.my_gift));
//        actionBar.setHomeAsUpIndicator(R.mipmap.left);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initData();
    }

    @Override
    public void initData() {
        uid = getIntent().getStringExtra("uid");
        apps = getIntent().getStringArrayListExtra("apps");

        if (apps.size()>0){
            String appsJson = getPakgJson();
            LogUtils.e("param json : " + appsJson);

            GiftsModel.getPresents(uid, appsJson, new GiftsCallback() {
                @Override
                public void success(List<GameGifts> gamegifts) {
                    LogUtils.e("gift", Thread.currentThread().getName());

                    runOnUiThread(()-> {
                        if (gamegifts == null || gamegifts.size() == 0) {
                            rvContent.setVisibility(View.GONE);
                            tvEmpty.setVisibility(View.VISIBLE);
                        } else {
                            giftList = gamegifts;
                            gameGiftsAdapter.setNewData(gamegifts);
                        }
                    });

                }

                @Override
                public void failure(String msg) {
                    runOnUiThread(()-> {
                        rvContent.setVisibility(View.GONE);
                        tvEmpty.setVisibility(View.VISIBLE);
                    });

                }

                @Override
                public void errors(String errors) {
                    runOnUiThread(()-> {
                        rvContent.setVisibility(View.GONE);
                        tvEmpty.setVisibility(View.VISIBLE);
                        tvEmpty.setText("网络好像出现了点问题~");
                    });

                }
            });
        }
        else{
            rvContent.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }

    }

    @NonNull
    private String getPakgJson() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < apps.size(); i++) {
            if (i != apps.size()-1){
                sb.append("'" + apps.get(i) + "',");
            }else {
                sb.append("'" + apps.get(i) + "'");
            }
        }

        return sb.toString();
    }

    @Override
    public void initViews() {
        rvContent.setBackgroundColor(getResources().getColor(R.color.bootstrap_gray_lighter));
        rvContent.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        gameGiftsAdapter = new GameGiftsAdapter(R.layout.item_game_gifts);
        rvContent.setAdapter(gameGiftsAdapter);
        gameGiftsAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                if (CheckDoubleClickUtils.isDoubleClick()) {
                    return;
                }

                if (rootView==null){
                    rootView = getLayoutInflater().inflate(R.layout.window_gifts, null);
                }
                rlGiftContainer.removeView(rootView);
                GameGifts gameGifts = giftList.get(position);
                List<Gifts> gifts = gameGifts.getGifts();
                GiftsAdapter giftsAdapter = new GiftsAdapter(R.layout.item_gifts, gifts, gameGifts.getPic());
                giftsAdapter.setUid(uid);
                RecyclerView rvGifts = rootView.findViewById(R.id.rv_gifts);
                rvGifts.setLayoutManager(new LinearLayoutManager(GiftsActivity.this, LinearLayoutManager.VERTICAL, false));
                rvGifts.setAdapter(giftsAdapter);

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                int margin = ScreenUtils.dp2px(GiftsActivity.this, 30);
                int mTop = ScreenUtils.dp2px(GiftsActivity.this, 120);

                layoutParams.setMargins(margin, mTop, margin, margin);

                rlGiftContainer.addView(rootView, layoutParams);

                ImageView ivColse = rootView.findViewById(R.id.iv_colse);
                ivColse.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (rootView!=null) {
                            rlGiftContainer.removeView(rootView);
                        }
                        return true;
                    }
                });
            }
        });

        rvContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (rootView!=null){
                    rlGiftContainer.removeView(rootView);
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (rootView!=null && rootView.isShown()){
            rlGiftContainer.removeView(rootView);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public View getGiftsView(){
        return rootView;
    }

}
