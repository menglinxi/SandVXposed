package io.virtualapp.home.entity;

/**
 * Created by lanpan on 2018/7/17.
 */

public class Gifts {


    /**
     * id : 17
     * giftname : 阴阳师测试礼包1
     * gamepkg : com.netease.yys
     * gameid : 19
     * codetype : 2
     * code : qb34317200
     * remark : 点击设置->礼包码输入->粘贴礼包码->获取礼包
     * context : 大血瓶X1,急救丸X1,回血丹X1
     * status : 0
     * channelcode : 0
     */

    private int id;
    private String giftname;
    private String gamepkg;
    private int gameid;
    private int codetype;
    private String code;
    private String remark;
    private String context;
    private int status;
    private String channelcode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGiftname() {
        return giftname;
    }

    public void setGiftname(String giftname) {
        this.giftname = giftname;
    }

    public String getGamepkg() {
        return gamepkg;
    }

    public void setGamepkg(String gamepkg) {
        this.gamepkg = gamepkg;
    }

    public int getGameid() {
        return gameid;
    }

    public void setGameid(int gameid) {
        this.gameid = gameid;
    }

    public int getCodetype() {
        return codetype;
    }

    public void setCodetype(int codetype) {
        this.codetype = codetype;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getChannelcode() {
        return channelcode;
    }

    public void setChannelcode(String channelcode) {
        this.channelcode = channelcode;
    }
}
