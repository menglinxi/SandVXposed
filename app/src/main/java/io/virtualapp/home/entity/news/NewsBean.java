package io.virtualapp.home.entity.news;

import io.virtualapp.home.entity.CommonBean;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/24
 * className:
 * <p/>
 * describe:
 */
public class NewsBean extends CommonBean {


    /**
     * data : {"pager":{"pager":{"pageNumber":1,"pageSize":15,"pageCount":1,"recordCount":2},"dataList":[{"id":5,"title":"测试资讯梦幻","author":"admin","type":1,"ct":"2018-10-23 15:18:41","gamename":"梦幻西游手游"},{"id":8,"title":"大话西游资讯","author":"admin","type":1,"ct":"2018-10-23 15:18:41","gamename":"大话西游"}]}}
     */

    private NewsWrapper data;

    public NewsWrapper getData() {
        return data;
    }

    public void setData(NewsWrapper data) {
        this.data = data;
    }

}
