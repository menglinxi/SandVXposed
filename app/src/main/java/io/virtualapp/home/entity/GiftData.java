package io.virtualapp.home.entity;

import java.util.List;

/**
 * Created by lanpan on 2018/7/17.
 */

public class GiftData extends CommonBean {

    /**
     * data : {"gamegifts":[]}
     * operationState : SUCCESS
     * errors : []
     */

    private GiftBean data;


    public GiftBean getData() {
        return data;
    }

    public void setData(GiftBean data) {
        this.data = data;
    }

    public static class GiftBean {

        private List<GameGifts> gamegifts;

        public List<GameGifts> getGamegifts() {
            return gamegifts;
        }

        public void setGamegifts(List<GameGifts> gamegifts) {
            this.gamegifts = gamegifts;
        }
    }
}
