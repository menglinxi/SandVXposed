package io.virtualapp.home.entity.news;

import java.util.List;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/25
 * className:
 * <p/>
 * describe:
 */
public class NewsDetailWrapper {
    private List<NewsDetail> data;

    public List<NewsDetail> getData() {
        return data;
    }

    public void setData(List<NewsDetail> data) {
        this.data = data;
    }

}
