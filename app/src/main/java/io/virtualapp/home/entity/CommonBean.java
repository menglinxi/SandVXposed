package io.virtualapp.home.entity;

/**
 * Created by lanpan on 2018/7/18.
 */

public class CommonBean {

    private String operationState;
    private String[] errors;

    public String[] getErrors() {
        return errors;
    }

    public void setErrors(String[] errors) {
        this.errors = errors;
    }

    public String getOperationState() {
        return operationState;
    }

    public void setOperationState(String operationState) {
        this.operationState = operationState;
    }
}
