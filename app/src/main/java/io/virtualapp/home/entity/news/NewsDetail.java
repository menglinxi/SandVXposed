package io.virtualapp.home.entity.news;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/25
 * className:
 * <p/>
 * describe:
 */
public class NewsDetail {
    /**
     * id : 5
     * title : 测试资讯梦幻
     * author : admin
     * context : 咨询
     * gamepackage : com.netease.my
     * type : 1
     * ct : 2018-10-23 15:18:41
     */

    private int id;
    private String title;
    private String author;
    private String context;
    private String gamepackage;
    private int type;
    private String ct;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getGamepackage() {
        return gamepackage;
    }

    public void setGamepackage(String gamepackage) {
        this.gamepackage = gamepackage;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCt() {
        return ct;
    }

    public void setCt(String ct) {
        this.ct = ct;
    }

}
