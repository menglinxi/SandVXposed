package io.virtualapp.home.entity;

import io.virtualapp.entity.GameEntity;

/**
 * Created by lanpan on 2018/5/8.
 */

public class HotGame extends GameEntity {


    /**
     * id : 26
     * gamename : 迷雾求生
     * pic : http://www.17sy8.cn:80/uploadimg/1523600811936.png
     * dlurl : https://g88.gdl.netease.com/g88_netease_netease.lpwl_cps_dev_1.0.30_com.netease.mwqs_18Z3kvM.apk
     * channel : 1
     * createtime : 2018-04-13 14:27:14
     * apkpackage : com.netease.mwqs
     * sort : 500
     * ishot : 1
     * hotimg : aaaa
     */

    private String hotimg;

    public String getHotimg() {
        return hotimg;
    }

    public void setHotimg(String hotimg) {
        this.hotimg = hotimg;
    }
}
