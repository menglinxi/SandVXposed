package io.virtualapp.home.entity.news;

import java.util.List;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/24
 * className:
 * <p/>
 * describe:
 */
public class NewsInfoes {


    /**
     * pager : {"pageNumber":1,"pageSize":15,"pageCount":1,"recordCount":2}
     * dataList : [{"id":5,"title":"测试资讯梦幻","author":"admin","type":1,"ct":"2018-10-23 15:18:41","gamename":"梦幻西游手游"},{"id":8,"title":"大话西游资讯","author":"admin","type":1,"ct":"2018-10-23 15:18:41","gamename":"大话西游"}]
     */

    private Pager pager;
    private List<NewsInfo> dataList;

    public Pager getPager() {
        return pager;
    }

    public void setPager(Pager pager) {
        this.pager = pager;
    }

    public List<NewsInfo> getDataList() {
        return dataList;
    }

    public void setDataList(List<NewsInfo> dataList) {
        this.dataList = dataList;
    }

}
