package io.virtualapp.home.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lanpan on 2018/7/17.
 */

public class GameGifts {

    /**
     * id : 19
     * gamename : 阴阳师
     * pic : http://f.jiawanhd.com/group1/M00/00/74/rBEABFtC7F2AVrnhAAAYuViON1U519.png
     * package : com.netease.yys
     * gifts : [{"id":17,"giftname":"阴阳师测试礼包1","gamepkg":"com.netease.yys","gameid":19,"codetype":2,"code":"qb34317200","remark":"点击设置->礼包码输入->粘贴礼包码->获取礼包","context":"大血瓶X1,急救丸X1,回血丹X1","status":0,"channelcode":"0"}]
     */

    @SerializedName("id")
    private int id;
    @SerializedName("gamename")
    private String gamename;
    @SerializedName("pic")
    private String pic;
    @SerializedName("package")
    private String packageX;
    @SerializedName("gifts")
    private List<Gifts> gifts;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGamename() {
        return gamename;
    }

    public void setGamename(String gamename) {
        this.gamename = gamename;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPackageX() {
        return packageX;
    }

    public void setPackageX(String packageX) {
        this.packageX = packageX;
    }

    public List<Gifts> getGifts() {
        return gifts;
    }

    public void setGifts(List<Gifts> gifts) {
        this.gifts = gifts;
    }
}
