package io.virtualapp.home.entity.news;

import io.virtualapp.home.entity.CommonBean;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/25
 * className:
 * <p/>
 * describe:
 */
public class NewsDetailBean extends CommonBean {

    /**
     * data : {"data":[{"id":5,"title":"测试资讯梦幻","author":"admin","context":"咨询","gamepackage":"com.netease.my","type":1,"ct":"2018-10-23 15:18:41"}]}
     */

    private NewsDetailWrapper data;

    public NewsDetailWrapper getData() {
        return data;
    }

    public void setData(NewsDetailWrapper data) {
        this.data = data;
    }

}
