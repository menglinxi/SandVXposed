package io.virtualapp.home.entity;

/**
 * Created by lanpan on 2018/4/25.
 */

public class UserInfo {

    /**
     * data : {"login":{"boxname":"admin","boxpwd":"123456","channelcode":"netease.lpwl_cps_dev","channelid":1,"vd":{"buildManufacturer":"Xiaomi","buildModel":"MI NOTE LTE","buildSerial":"q928cca2eord3xaqh4uh","buildVersionRelease":"Android 4.4.0","androidId":"3snvfk2ps6f78wm","deviceId":"869237104785454","line1Number":"15629299087","networkType":"13","simNo":"40062594299767843943","iccid":"27489385659531039490","ssid":"TP-LINK_KXZ7FGTDG","macaddress":"00:11:8B:5B:45:34","createtime":"2018-03-26 10:46:56","id":1},"gameuser":"mcjds4oaug1l@163.com","gamepwd":"ltb13579","userid":2}}
     * operationState : SUCCESS
     */

    private DataBean data;
    private String operationState;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getOperationState() {
        return operationState;
    }

    public void setOperationState(String operationState) {
        this.operationState = operationState;
    }

    public static class DataBean {
        /**
         * login : {"boxname":"admin","boxpwd":"123456","channelcode":"netease.lpwl_cps_dev","channelid":1,"vd":{"buildManufacturer":"Xiaomi","buildModel":"MI NOTE LTE","buildSerial":"q928cca2eord3xaqh4uh","buildVersionRelease":"Android 4.4.0","androidId":"3snvfk2ps6f78wm","deviceId":"869237104785454","line1Number":"15629299087","networkType":"13","simNo":"40062594299767843943","iccid":"27489385659531039490","ssid":"TP-LINK_KXZ7FGTDG","macaddress":"00:11:8B:5B:45:34","createtime":"2018-03-26 10:46:56","id":1},"gameuser":"mcjds4oaug1l@163.com","gamepwd":"ltb13579","userid":2}
         */

        private LoginBean login;

        public LoginBean getLogin() {
            return login;
        }

        public void setLogin(LoginBean login) {
            this.login = login;
        }

        public static class LoginBean {

            /**
             * boxname : admin
             * boxpwd : 123456
             * channelcode : netease.lpwl_cps_dev
             * channelid : 1
             * vd : {"buildManufacturer":"Xiaomi","buildModel":"MI NOTE LTE","buildSerial":"q928cca2eord3xaqh4uh","buildVersionRelease":"Android 4.4.0","androidId":"3snvfk2ps6f78wm","deviceId":"869237104785454","line1Number":"15629299087","networkType":"13","simNo":"40062594299767843943","iccid":"27489385659531039490","ssid":"TP-LINK_KXZ7FGTDG","macaddress":"00:11:8B:5B:45:34","createtime":"2018-03-26 10:46:56","id":1}
             * gameuser : mcjds4oaug1l@163.com
             * gamepwd : ltb13579
             * userid : 2
             */

            private String boxname;
            private String boxpwd;
            private String channelcode;
            private int channelid;
            private VdBean vd;
            private String gameuser;
            private String gamepwd;
            private int userid;

            public String getBoxname() {
                return boxname;
            }

            public void setBoxname(String boxname) {
                this.boxname = boxname;
            }

            public String getBoxpwd() {
                return boxpwd;
            }

            public void setBoxpwd(String boxpwd) {
                this.boxpwd = boxpwd;
            }

            public String getChannelcode() {
                return channelcode;
            }

            public void setChannelcode(String channelcode) {
                this.channelcode = channelcode;
            }

            public int getChannelid() {
                return channelid;
            }

            public void setChannelid(int channelid) {
                this.channelid = channelid;
            }

            public VdBean getVd() {
                return vd;
            }

            public void setVd(VdBean vd) {
                this.vd = vd;
            }

            public String getGameuser() {
                return gameuser;
            }

            public void setGameuser(String gameuser) {
                this.gameuser = gameuser;
            }

            public String getGamepwd() {
                return gamepwd;
            }

            public void setGamepwd(String gamepwd) {
                this.gamepwd = gamepwd;
            }

            public int getUserid() {
                return userid;
            }

            public void setUserid(int userid) {
                this.userid = userid;
            }

            public static class VdBean {
                /**
                 * buildManufacturer : Xiaomi
                 * buildModel : MI NOTE LTE
                 * buildSerial : q928cca2eord3xaqh4uh
                 * buildVersionRelease : Android 4.4.0
                 * androidId : 3snvfk2ps6f78wm
                 * deviceId : 869237104785454
                 * line1Number : 15629299087
                 * networkType : 13
                 * simNo : 40062594299767843943
                 * iccid : 27489385659531039490
                 * ssid : TP-LINK_KXZ7FGTDG
                 * macaddress : 00:11:8B:5B:45:34
                 * createtime : 2018-03-26 10:46:56
                 * id : 1
                 */

                private String buildManufacturer;
                private String buildModel;
                private String buildSerial;
                private String buildVersionRelease;
                private String androidId;
                private String deviceId;
                private String line1Number;
                private String networkType;
                private String simNo;
                private String iccid;
                private String ssid;
                private String macaddress;
                private String createtime;
                private int id;

                public String getBuildManufacturer() {
                    return buildManufacturer;
                }

                public void setBuildManufacturer(String buildManufacturer) {
                    this.buildManufacturer = buildManufacturer;
                }

                public String getBuildModel() {
                    return buildModel;
                }

                public void setBuildModel(String buildModel) {
                    this.buildModel = buildModel;
                }

                public String getBuildSerial() {
                    return buildSerial;
                }

                public void setBuildSerial(String buildSerial) {
                    this.buildSerial = buildSerial;
                }

                public String getBuildVersionRelease() {
                    return buildVersionRelease;
                }

                public void setBuildVersionRelease(String buildVersionRelease) {
                    this.buildVersionRelease = buildVersionRelease;
                }

                public String getAndroidId() {
                    return androidId;
                }

                public void setAndroidId(String androidId) {
                    this.androidId = androidId;
                }

                public String getDeviceId() {
                    return deviceId;
                }

                public void setDeviceId(String deviceId) {
                    this.deviceId = deviceId;
                }

                public String getLine1Number() {
                    return line1Number;
                }

                public void setLine1Number(String line1Number) {
                    this.line1Number = line1Number;
                }

                public String getNetworkType() {
                    return networkType;
                }

                public void setNetworkType(String networkType) {
                    this.networkType = networkType;
                }

                public String getSimNo() {
                    return simNo;
                }

                public void setSimNo(String simNo) {
                    this.simNo = simNo;
                }

                public String getIccid() {
                    return iccid;
                }

                public void setIccid(String iccid) {
                    this.iccid = iccid;
                }

                public String getSsid() {
                    return ssid;
                }

                public void setSsid(String ssid) {
                    this.ssid = ssid;
                }

                public String getMacaddress() {
                    return macaddress;
                }

                public void setMacaddress(String macaddress) {
                    this.macaddress = macaddress;
                }

                public String getCreatetime() {
                    return createtime;
                }

                public void setCreatetime(String createtime) {
                    this.createtime = createtime;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }
            }
        }
    }
}
