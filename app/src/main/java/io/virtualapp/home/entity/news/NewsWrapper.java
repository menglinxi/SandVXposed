package io.virtualapp.home.entity.news;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/24
 * className:
 * <p/>
 * describe:
 */
public class NewsWrapper {

    private NewsInfoes pager;

    public NewsInfoes getPager() {
        return pager;
    }

    public void setPager(NewsInfoes pager) {
        this.pager = pager;
    }
}
