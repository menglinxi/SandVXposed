package io.virtualapp;

import com.lody.virtual.client.core.InstallStrategy;

/**
 * @author Lody
 */
public class VCommends {

	public static final String TAG_NEW_VERSION = "First launch new Version";
	public static final String TAG_SHOW_ADD_APP_GUIDE = "Should show add app guide";

	public static final int REQUEST_SELECT_APP = 5;

	public static final String EXTRA_APP_INFO_LIST = "va.extra.APP_INFO_LIST";

	public static final String TAG_ASK_INSTALL_GMS = "va.extra.ASK_INSTALL_GMS";


	public static final int RECHARGE_INTERVAL_DELAY = 5;
	public static final int RECHARGE_INTERVAL_PERIOD = 300;
	public static final String SP_VBOX_DEVICE_INFO = "sp_vbox_device_info";
	public static final String SP_VBOX_RECHARGE_RECORD = "sp_vbox_recharge_record";
	public static final String UM_EVENT_DOWNLOADED = "game_downloaded";
	public static final String UM_EVENT_HOT_DOWNLOADED = "hot_game_downloaded";
	public static final String UM_EVENT_HOT_INSTALLED = "hot_game_installed";
	public static final String UM_EVENT_INSTALLED = "game_installed";
	public static final String UM_EVENT_LIST_OPENED = "list_game_opened";
	public static final String UM_EVENT_MY_OPENED = "my_game_opened";
	public static final String UM_EVENT_UNINSTALLED = "game_uninstalled";
//	public static final int flags = 72;


	public static final int flags = InstallStrategy.COMPARE_VERSION | InstallStrategy.SKIP_DEX_OPT;

}
