package io.virtualapp.user.models;

import com.lody.virtual.helper.utils.VLog;

import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.IStringCallback;
import io.virtualapp.home.callback.RespCallback;
import io.virtualapp.home.util.ToastUtils;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/19
 * className:
 * <p/>
 * describe:
 */
public class RegModel {

    private static RegModel regModel = new RegModel();

    public static RegModel get(){
        return regModel;
    }

    public void getCode(String userName, String mobile, IStringCallback callback){

        String webUrl = WebConst.webUrlForSendCode + "?uname=" + userName + "&mobile=" + mobile;

        SendInfoWithOkHttp.sendHttpGetRequest(webUrl, new RespCallback() {
            @Override
            public void response(String resp) {
                VLog.i("abc", "getCode resp = %s", resp);
                if (isSuccess(resp)){
                    callback.getStrResp(resp);
                }else {
                    ToastUtils.showToast("获取验证码失败");
                }
            }

            @Override
            public void errors(Throwable t) {
                ToastUtils.showToast(t.getMessage());
                VLog.e("abc", "getCode resp = %s", t.getMessage());
            }
        });
    }

    public void register(String json, IStringCallback callback){
        SendInfoWithOkHttp.sendHttpPostRequest(json, WebConst.webUrlForFindPwd, new RespCallback() {
            @Override
            public void response(String resp) {
                VLog.i("abc", "register resp = %s", resp);
                if (isSuccess(resp)){
                    callback.getStrResp(resp);
                }else{
                    ToastUtils.showToast("注册失败.");
                }
            }

            @Override
            public void errors(Throwable t) {
                VLog.e("abc", "register resp = %s", t.getMessage());
            }
        });
    }

    public boolean isSuccess(String resp){
        JsonUtils jsonUtils = new JsonUtils(resp);
        String success = jsonUtils.key("operationState").stringValue();
        if (success.equals("SUCCESS")) return true;
        else return false;
    }

}
