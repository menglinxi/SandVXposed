package io.virtualapp.user.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.lody.virtual.helper.utils.VLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.IStringCallback;
import io.virtualapp.home.callback.RespCallback;
import io.virtualapp.home.callback.UserCallback;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.tools.GsonUtil;
import io.virtualapp.tools.PhoneInfoCollect;

import static io.virtualapp.home.WebConst.webUrlForRegV1;

/**
 * @Author:陈海楠
 * @Date:2018/4/25
 * @Description:
 */
public class RegisterUtils {

    public static String register(String username, String pwd, String mobile, String channelcode, String code, Context context, UserCallback callback) {
        String TAG= "abc";
        final SharedPreferences sp = context.getSharedPreferences("register",Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor =sp.edit();
        new Thread(() -> {
            Map device = PhoneInfoCollect.returnPhoneInfo(context);
            VLog.e("abc","device = %s",device);
            device.put("username", username);
            device.put("pwd", pwd);
            device.put("mobile", mobile);
            device.put("channelcode", channelcode);
            device.put("code", code);
            try {
                String registerResult = SendInfoWithOkHttp.sendInfo(new Gson().toJson(device), webUrlForRegV1);

                if (isSuccess(registerResult)){
                    callback.successed(registerResult);
                }else{
                    if (registerResult==null){
                        callback.failed(registerResult);
                        return;
                    }
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = new JSONObject(registerResult);
                        String error = jsonObject.getString("errors");
                        JSONArray jsonArray = new JSONArray(error);
                        callback.failed((String) jsonArray.get(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                editor.putString("registerResult",registerResult);
                editor.commit();
            } catch (IOException e) {
                Log.d(TAG, "register: "+e.toString());
            }
        }).start();
        return sp.getString("registerResult",null);
    }

    public static void getCode(String userName, String mobile, IStringCallback callback){

        String webUrl = WebConst.webUrlForRegCode + "?uname=" + userName + "&mobile=" + mobile;

        SendInfoWithOkHttp.sendHttpGetRequest(webUrl, new RespCallback() {
            @Override
            public void response(String resp) {
                try{
                    VLog.i("abc", "getCode resp = %s", resp);
                    HashMap<String, Object> map = GsonUtil.getInstance().getGson().fromJson(resp, HashMap.class);
                    if (isSuccess(resp)){
                        callback.getStrResp(resp);
                    }else {
                        List<String> errorinfo = (ArrayList)map.get("errors");
                        ToastUtils.showToast(errorinfo.get(0));
                    }
                }catch (Exception e){
                    VLog.e("abc", "error = %s", e.toString());
                    ToastUtils.showToast("获取验证码失败");
                }
            }

            @Override
            public void errors(Throwable t) {
                ToastUtils.showToast(t.getMessage());
                VLog.e("abc", "getCode resp = %s", t.getMessage());
            }
        });
    }

    public static boolean isSuccess(String resp){
        JsonUtils jsonUtils = new JsonUtils(resp);
        String success = jsonUtils.key("operationState").stringValue();
        if (success.equals("SUCCESS")) return true;
        else return false;
    }
}
