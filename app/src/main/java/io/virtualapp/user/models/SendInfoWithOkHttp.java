package io.virtualapp.user.models;

import android.support.annotation.NonNull;

import com.lody.virtual.helper.utils.VLog;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.virtualapp.home.callback.RespCallback;
import io.virtualapp.home.util.LogInterceptor;
import okhttp3.CacheControl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * @Author:陈海楠
 * @Date:18-3-28
 * @Description: 上传文件函数
 */
public class SendInfoWithOkHttp {
    
    private static String TAG = "okhttp";

    public static String sendInfo(final String json, final String webUrl) throws IOException {
        final int CONNECT_TIMEOUT = 6 * 1000;                    //连接超时时间
        final int READ_TIMEOUT = 10 * 1000;                      //读取超时时间
        final int WRITE_TIMEOUT = 10 * 1000;                      //写入超时时间
        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");   //上传文件的格式

        Request request = new Request.Builder()
                .url(webUrl)
                .post(RequestBody.create(mediaType, json))
                .cacheControl(CacheControl.FORCE_NETWORK)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(new LogInterceptor())
                .build();

        return client.newCall(request).execute().body().string();

    }
    
    public static void sendHttpPostRequest(final String json, final String webUrl, RespCallback callback){

        Observable.create(new ObservableOnSubscribe<String>() {
                    @Override
                    public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                        String resp = thisOkHttpClient()
                                .newCall(postOkHttpRequest(json, webUrl))
                                .execute()
                                .body()
                                .string();
                        e.onNext(resp);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String data) throws Exception {
                        VLog.e(TAG, "subscribe 线程:" + Thread.currentThread().getName() + "\n");
                        callback.response(data);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        VLog.e(TAG, "subscribe 线程:" + Thread.currentThread().getName() + "\n");
                        callback.errors(throwable);
                    }
                });

    }


    public static void sendHttpGetRequest(final String webUrl, RespCallback callback){

        Observable.create(new ObservableOnSubscribe<String>() {
                    @Override
                    public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                        String resp = thisOkHttpClient()
                                .newCall(getOkHttpRequest(webUrl))
                                .execute()
                                .body()
                                .string();
                        e.onNext(resp);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<String>() {
                    @Override
                    public void accept(@NonNull String data) throws Exception {
                        VLog.e(TAG, "subscribe 线程:" + Thread.currentThread().getName() + "\n");
                        callback.response(data);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                        VLog.e(TAG, "subscribe 线程:" + Thread.currentThread().getName() + "\n");
                        callback.errors(throwable);
                    }
                });

    }

    private static OkHttpClient thisOkHttpClient(){

        final int CONNECT_TIMEOUT = 6 * 1000;                    //连接超时时间
        final int READ_TIMEOUT = 10 * 1000;                      //读取超时时间
        final int WRITE_TIMEOUT = 10 * 1000;                      //写入超时时间

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(new LogInterceptor())
                .build();

        return client;
    }

    private static Request getOkHttpRequest(final String webUrl, String...params){

        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");   //上传文件的格式
        Request request = new Request.Builder()
                .url(webUrl)
                .get()
                .cacheControl(CacheControl.FORCE_NETWORK)
                .build();

        return request;
    }

    private static Request postOkHttpRequest(final String json, final String webUrl){

        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");   //上传文件的格式
        Request request = new Request.Builder()
                .url(webUrl)
                .post(RequestBody.create(mediaType, json))
                .cacheControl(CacheControl.FORCE_NETWORK)
                .build();

        return request;
    }
}
