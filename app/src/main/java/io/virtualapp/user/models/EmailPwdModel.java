package io.virtualapp.user.models;

import com.lody.virtual.helper.utils.VLog;

import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.RespCallback;
import io.virtualapp.home.callback.TraceCallback;
import io.virtualapp.home.util.ToastUtils;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/18
 * className:
 * <p/>
 * describe:
 */
public class EmailPwdModel {

    private static EmailPwdModel emailPwdModel = new EmailPwdModel();

    public static EmailPwdModel get(){
        return emailPwdModel;
    }

    public void updateEmailPwd(String json, TraceCallback callback){
        SendInfoWithOkHttp.sendHttpPostRequest(json, WebConst.webUrlForEmailPwd, new RespCallback() {
            @Override
            public void response(String resp) {
                VLog.i("abc", "updateEmailPwd resp = %s", resp);
                JsonUtils jsonUtils = new JsonUtils(resp);
                String msg =  jsonUtils.key("data").key("msg").stringValue();
                callback.resp();
                ToastUtils.showToast(msg);
            }

            @Override
            public void errors(Throwable t) {
                ToastUtils.showToast(t.getMessage());
            }
        });
    }
}
