package io.virtualapp.user.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.remote.InstalledAppInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.home.callback.UserCallback;
import io.virtualapp.tools.AESUtil;
import io.virtualapp.tools.ClearDataUtils;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.tools.PhoneInfoCollect;

import static io.virtualapp.home.WebConst.webUrlForLogin;

/**
 * @Author:陈海楠
 * @Date:2018/4/24
 * @Description:登陆工具类
 */
public class LoginUtils {
    String TAG = this.getClass().toString();
    private Context context;
    private String postResult;

    public static LoginUtils getImpl(Context context) {
        return new LoginUtils(context);
    }

    private LoginUtils(Context context) {
        this.context = context;
    }

    /**
     * @Date: 2018/4/25 8:10
     * @Param:
     * @Description: 保存用户账号密码至本地
     */
    public void saveUserInfo(String user, String password) {

        List<String> users = getAllUserInfo();
        if (users.size() >= 3) {
            users = deleteFirstRow(users, user);
        }

        Set set = new HashSet<>();

        //初次登录
        if (users.size() == 0) {
            User u = new User();
            u.setUser(user);
            u.setPwd(password);
            String userJson = new Gson().toJson(u);
            addInfo(userJson + "\n");
            return;
        }

        //把账户都拿出来，然后判断是否登录过
        for (int i = 0; i < users.size(); i++) {
            User u = new Gson().fromJson(users.get(i), User.class);
            set.add(u.getUser());
        }

        if (!set.contains(user)) {
            User u1 = new User();
            u1.setUser(user);
            u1.setPwd(password);
            String userJson = new Gson().toJson(u1);
            addInfo(userJson + "\n");
        }
    }

//    public void updatePwd(String username, String password){
//        List<String> users = getAllUserInfo();
//        Gson gson = GsonUtil.getInstance().getGson();
//        for (String struser : users) {
//            User user = gson.fromJson(struser, User.class);
//            if (user.getUser().equals(username)){
//                if (!user.getPwd().equals(password)){
//                    user.setPwd(password);
//                    String updateUser = gson.toJson(user);
//                    addInfo(updateUser + "\n");
//                    break;
//                }
//            }
//        }
//    }

    private List<String> deleteFirstRow(List<String> users, String nuser) {

        //把账户都拿出来，然后判断是否登录过
        for (int i = 0; i < users.size(); i++) {
            User u = new Gson().fromJson(users.get(i), User.class);
            if (u.getUser().equals(nuser))
                return users;
        }

        deleteUserFile();
        if (users.size() >= 3) {
            users.remove(0);
        }
        for (String user : users) {
            addInfo(user + "\n");
        }
        return users;
    }

    private void addInfo(String data) {
        FileOutputStream out = null;
        BufferedWriter writer = null;
        try {
            out = context.openFileOutput("userInfo", Context.MODE_APPEND);
            writer = new BufferedWriter(new OutputStreamWriter(out));
            writer.write(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            {
                try {
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @Date: 2018/4/25 8:23
     * @Param:
     * @Description: 从文件中读取所有的用户账号和密码
     */
    public List<String> getAllUserInfo() {
        FileInputStream in = null;
        BufferedReader reader = null;
        List<String> allUser = new ArrayList<>();
        try {
            in = context.openFileInput("userInfo");
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";

            StringBuilder stringBuilder = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                LogUtils.e("line ->" + stringBuilder.append(line + "\n"));
                allUser.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return allUser;
    }


    public User getUserObj(String uname){

        Gson gson = new Gson();
        List<String> users= getAllUserInfo();
        if(users==null||users.size()<1)
        {
            return null;
        }

        for(String stemp:users) {
            String user = "";

            User u = gson.fromJson(stemp, User.class);
            user = u.getUser();
            if(uname.equals(user)) {
                return u;
            }
        }

        return null;
    }

    /**
     * @Date: 2018/4/25 8:25
     * @Param:
     * @Description: 生成登陆时上传的json文件
     */
    public String getUploadJson(String user, String pwd) {
        Map deviceInfo = PhoneInfoCollect.returnPhoneInfo(context);
        deviceInfo.put("name", user);
        deviceInfo.put("pwd", pwd);
        return new Gson().toJson(deviceInfo);
    }

    /**
     * @Date: 2018/4/25 8:36
     * @Param:
     * @Description: 登陆
     */
    public void login(String json, UserCallback callback) {
        VUiKit.defer().when(() -> {
            try {
                postResult = SendInfoWithOkHttp.sendInfo(json, webUrlForLogin);
            } catch (Exception e) {
                LogUtils.e("errors",  e);
                postResult = null;
            }
        }).done(res -> {
            if (isLoginSuccess(postResult)) {
                callback.successed(postResult);
            } else {
                if (postResult == null) {
                    callback.failed(postResult);
                    return;
                }
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(postResult);
                    String error = jsonObject.getString("errors");
                    JSONArray jsonArray = new JSONArray(error);
                    callback.failed((String) jsonArray.get(0));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * @param:postResult,登陆时服务器返回的json数据
     * @Description:根据服务器返回的数据来判断用户是否登陆成功
     */
    public boolean isLoginSuccess(String postResult) {
        //如果返回的json数据中没有errors则认为成功登陆
        if (postResult != null) {
            String error = "errors";
            Pattern pattern = Pattern.compile(error);
            Matcher matcher = pattern.matcher(postResult);
            return !matcher.find();
        } else {
            return false;
        }
    }

    /**
     * @param:postResult,服务器回传的json数据
     * @Description:保存服务器回传的json数据至本地
     */
    public void save(String postResult) {
        SharedPreferences sp = context.getSharedPreferences("postResult", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("info", postResult);
        editor.commit();
    }

    /**
     * @param:postResult,服务器回传的json数据
     * @Description:保存服务器回传的json数据至本地
     */
    public void saveState(Boolean online) {
        SharedPreferences sp = context.getSharedPreferences("BOX_USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("online", online);
        editor.commit();
    }

    public boolean getState() {
        return context.getSharedPreferences("BOX_USER", Context.MODE_PRIVATE).getBoolean("online", false);
    }

    /**
     * @Date: 2018/4/25 8:12
     * @Param:
     * @Description: 加密或者解密字符串
     */
    public String enCodeOrUnCodeString(String string) {
        char[] chars = string.toCharArray();
        for (char c : chars) {
            c = (char) (c ^ 10000);
        }
        return new String(chars);
    }

    public void save(boolean isLogin) {
        SharedPreferences sp = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("isLogin", isLogin);
        editor.commit();
    }

    public boolean isLogin() {
        SharedPreferences sp = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        return sp.getBoolean("isLogin", false);
    }

    public String getPostResult() {
        SharedPreferences sp = context.getSharedPreferences("postResult", Context.MODE_PRIVATE);
        return sp.getString("info", null);
    }

    public void saveCurrentUser(String user, String pwd) {
        SharedPreferences sp = context.getSharedPreferences("BOX_USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("CURRENT_USER", user + ":" + pwd);
        editor.commit();
    }

    public void saveCurrentUser(String userAndPwd) {
        SharedPreferences sp = context.getSharedPreferences("BOX_USER", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("CURRENT_USER", userAndPwd);
        editor.commit();
    }

    public String getCurrentUser() {
        return context.getSharedPreferences("BOX_USER", Context.MODE_PRIVATE).getString("CURRENT_USER", "");
    }

    public void updatePwd(String user, String pwd) {
        List<String> users = getAllUserInfo();

        List<String> newUserJson = new ArrayList<>();

        for (int i = 0; i < users.size(); i++) {
            User u = new Gson().fromJson(users.get(i), User.class);
            if (u.getUser().equals(user)) {
                User u1 = new User();
                u1.setUser(user);
                u1.setPwd(pwd);
                String userJson = new Gson().toJson(u1);
                newUserJson.add(userJson);
            } else {
                newUserJson.add(users.get(i));
            }
        }

        //delete old file
        deleteUserFile();

        for (String newJson : newUserJson) {
            addInfo(newJson + "\n");
        }

    }

    private void deleteUserFile() {
        File file = new File(context.getFilesDir(), "userInfo");
        if (file.exists() && !file.isDirectory()) {
            file.delete();
        }
    }

    private boolean isUserFileExist() {
        return new File(context.getFilesDir(), "userInfo").exists();
    }

    public void checkUserInfo(String oldUserInfoJson, String newUserInfoJson) {
        VLog.e("abc","old = %s --> new = %s",oldUserInfoJson,newUserInfoJson);
        PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
        JsonUtils oldUserInfoJsonUtils = new JsonUtils(oldUserInfoJson);
        String oldLoginJson = AESUtil.decrypt(oldUserInfoJsonUtils.key("data").key("login").stringValue(), (new StringBuffer(AESUtil.genKey(phoneInfoCollect.ANDROID_ID, phoneInfoCollect.IMEI1)).reverse()).toString());
        String oldGameUserStr = new JsonUtils(oldLoginJson).key("gameuser").stringValue();
        JsonUtils userInfoJsonUtils = new JsonUtils(newUserInfoJson);
        String loginJson = AESUtil.decrypt(userInfoJsonUtils.key("data").key("login").stringValue(), (new StringBuffer(AESUtil.genKey(phoneInfoCollect.ANDROID_ID, phoneInfoCollect.IMEI1)).reverse()).toString());
        String gameUserStr = new JsonUtils(loginJson).key("gameuser").stringValue();
        VLog.e("abc", "oldGameUserStr = %s --> gameUserStr = %s", oldGameUserStr, gameUserStr);
        if (!gameUserStr.equals(oldGameUserStr)) {
            List<InstalledAppInfo> appInfos = VirtualCore.get().getInstalledApps(0);
            if (appInfos != null) {
                for (InstalledAppInfo appInfo : appInfos) {
                    VLog.e("abc", "清除缓存 = %s", appInfo.packageName);
                  //  VirtualCore.get().clearPackage(appInfo.packageName);
                    ClearDataUtils.clearExternalDirs(appInfo.packageName, true);
                }
            }
            VirtualCore.get().killAllApps();
        }
    }

    public class User {
        String user;
        String pwd;

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPwd() {
            return pwd;
        }

        public void setPwd(String pwd) {
            this.pwd = pwd;
        }

    }


}
