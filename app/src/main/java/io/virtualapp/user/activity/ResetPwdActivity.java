package io.virtualapp.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.virtualapp.R;
import io.virtualapp.VApp;
import io.virtualapp.home.activity.HomeActivity;
import io.virtualapp.home.callback.UserCallback;
import io.virtualapp.home.util.ResetPwdUtil;
import io.virtualapp.home.util.StatusBarUtils;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.user.models.LoginUtils;
import io.virtualapp.view.utils.CheckDoubleClickUtils;

import static io.virtualapp.home.WebConst.RESETPWD_ACTIVITY_TASK;

public class ResetPwdActivity extends AppCompatActivity {

    @BindView(R.id.reset_user_account)
    EditText resetUserAccount;
    @BindView(R.id.reset_user_old_password)
    EditText resetUserOldPassword;
    @BindView(R.id.reset_user_newpassword)
    EditText resetUserNewpassword;
    @BindView(R.id.reset_user_confirm_password)
    EditText resetUserConfirmPassword;
    @BindView(R.id.cp_btn_confirm)
    Button cpBtnConfirm;
    @BindView(R.id.iv_back)
    ImageView ivBack;

    String account, oldpwd, newpwd, confirmpwd;

    @OnClick(R.id.cp_btn_confirm)
    public void resetPwd(){

        if (CheckDoubleClickUtils.isDoubleClick(3000)) {
            return;
        }

        account = resetUserAccount.getText().toString();
        oldpwd = resetUserOldPassword.getText().toString();
        newpwd = resetUserNewpassword.getText().toString();
        confirmpwd = resetUserConfirmPassword.getText().toString();

        if (account.equals("")){
            ToastUtils.showToast("请输入账号");
            return;
        }
        if (oldpwd.equals("")){
            ToastUtils.showToast("请输入旧密码");
            return;
        }
        if (newpwd.equals("")){
            ToastUtils.showToast("请输入新密码");
            return;
        }
        if (!newpwd.equals(confirmpwd) || confirmpwd.equals("")){
            ToastUtils.showToast("新密码不一致");
            return;
        }

        String json = ResetPwdUtil.getImpl(this).getUploadJson(account, oldpwd, newpwd, confirmpwd);
        ResetPwdUtil.getImpl(this).resetPwd(json, new UserCallback() {
            @Override
            public void successed(String content) {
                runOnUiThread(()->{
                    ToastUtils.showToast("密码修改成功");
                    resetUserAccount.setText("");
                    resetUserOldPassword.setText("");
                    resetUserNewpassword.setText("");
                    resetUserConfirmPassword.setText("");
                    finish();
                    Intent intent = new Intent(ResetPwdActivity.this, LoginActivity.class);
                    LoginUtils.getImpl(ResetPwdActivity.this).updatePwd(account, confirmpwd);
                    LoginUtils.getImpl(ResetPwdActivity.this).saveState(false);
                    startActivity(intent);
                    HomeActivity.INSTANCE.finish();
                });
            }

            @Override
            public void failed(String error) {
                runOnUiThread(()->{
                    if (error==null) {
                        ToastUtils.showToast("密码修改失败");
                    }else{
                        ToastUtils.showToast(error);
                    }
                });
            }
        });


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VApp.activityContainer.put(RESETPWD_ACTIVITY_TASK, this);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.qmui_config_color_transparent);
        setContentView(R.layout.activity_reset_psw_page);
        ButterKnife.bind(this);
        ToastUtils.init(this);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VApp.activityContainer.remove(RESETPWD_ACTIVITY_TASK);
    }
}
