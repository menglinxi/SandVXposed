package io.virtualapp.user.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import io.virtualapp.R;
import io.virtualapp.VApp;
import io.virtualapp.home.activity.BaseActivity;
import io.virtualapp.home.callback.IStringCallback;
import io.virtualapp.home.util.TimerDelayUtil;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.home.weight.AutoCleanEditText;
import io.virtualapp.tools.GsonUtil;
import io.virtualapp.user.models.FindPwdModel;
import io.virtualapp.user.models.LoginUtils;

import static io.virtualapp.home.WebConst.FORGET_ACTIVITY_TASK;

public class FindPwdPageActivity extends BaseActivity {

    @BindView(R.id.et_account)
    EditText etAccount;
    @BindView(R.id.et_new_pwd)
    EditText etNewPwd;
    @BindView(R.id.edit_phone)
    AutoCleanEditText etPhone;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;
    @BindView(R.id.edit_code)
    AutoCleanEditText etCode;

    @OnClick({R.id.tv_get_code, R.id.btn_reset})
    public void resetPwd(View view){
        switch (view.getId()){
            case R.id.tv_get_code:
                String user = etAccount.getText().toString();
                if (user.equals("")) return;

                setVilidateCodeBtn();

                FindPwdModel.get().getCode(user, new IStringCallback() {
                    @Override
                    public void getStrResp(String resp) {
                        if (resp==null || resp.equals("")){
                            ToastUtils.showToast("获取验证码失败");
                        }
                    }
                });
                break;

            case R.id.btn_reset:
                String code = etCode.getText().toString();
                String newPwd = etNewPwd.getText().toString();
                String userName = etAccount.getText().toString();

                if (code.equals("")){
                    ToastUtils.showToast("验证码为空");
                    return;
                }

                if (newPwd.length() < 6){
                    ToastUtils.showToast("密码不能少于6位");
                    return;
                }

                if (newPwd.length() > 16){
                    ToastUtils.showToast("密码不能多于16位");
                    return;
                }

                HashMap<String, String> params = new HashMap<>();
                params.put("code", code);
                params.put("pwd0", newPwd);
                params.put("uname", userName);
                String json = GsonUtil.getInstance().getGson().toJson(params);
                FindPwdModel.get().findBoxPwd(json, new IStringCallback() {
                    @Override
                    public void getStrResp(String resp) {
                        ToastUtils.showToast("密码重置成功.");
                        LoginUtils.getImpl(VApp.getApp()).updatePwd(userName, newPwd);
                        LoginActivity.INSTANCE.rgsCallback(userName, newPwd);
                        finish();
                    }
                });
                break;
        }
    }

    private void setVilidateCodeBtn() {
        tvGetCode.setTextColor(getResources().getColor(R.color.col_code_invali));
        TimerDelayUtil td = TimerDelayUtil.get();
        td.startTimeDelay(true, new TimerDelayUtil.ITimer() {
            @Override
            public void callback(int time) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvGetCode.setText(time + "s后再获取");
                        if (time==0){
                            tvGetCode.setText(getResources().getString(R.string.get_code));
                            tvGetCode.setTextColor(getResources().getColor(R.color.col_code));
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VApp.activityContainer.put(FORGET_ACTIVITY_TASK, this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_forget_psw_page;
    }

    @Override
    public void initData() {

    }

    @Override
    public void initViews() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimerDelayUtil.get().stopTimer();
        VApp.activityContainer.remove(FORGET_ACTIVITY_TASK);
    }
}
