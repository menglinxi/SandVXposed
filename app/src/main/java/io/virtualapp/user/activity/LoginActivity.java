package io.virtualapp.user.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lody.virtual.client.core.VirtualCore;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.virtualapp.R;
import io.virtualapp.VApp;
import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.home.activity.BaseActivity;
import io.virtualapp.home.activity.HomeActivity;
import io.virtualapp.home.callback.RegisterListener;
import io.virtualapp.home.callback.UserCallback;
import io.virtualapp.home.util.StatusBarUtils;
import io.virtualapp.home.util.TimerDelayUtil;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.user.models.LoginUtils;
import io.virtualapp.view.utils.CheckDoubleClickUtils;

import static io.virtualapp.home.WebConst.LOGIN_ACTIVITY_TASK;
import static io.virtualapp.home.WebConst.feedbackLink;

public class LoginActivity extends BaseActivity implements RegisterListener {

    public static LoginActivity INSTANCE;

    @BindView(R.id.login_logo)                  //用户头像
    ImageView loginLogo;
    @BindView(R.id.login_user_account)          //用户账号
    EditText loginUserAccount;
    @BindView(R.id.login_user_password)         //用户密码
    EditText loginUserPassword;
    @BindView(R.id.login_button)                //登录按钮
    Button loginButton;
    @BindView(R.id.login_to_register)           //去注册
    TextView loginToRegister;
    @BindView(R.id.forget_pwd)                  //忘记密码
    TextView forgetPwd;
    @BindView(R.id.checkbox_auto_login)         //自动登录
    CheckBox checkboxAutoLogin;
    @BindView(R.id.check_remember_password)     //记住密码
    CheckBox checkRememberPassword;
    @BindView(R.id.img_unfold_allusers)         //展开所有账户
    ImageView unfoldUsers;
    @BindView(R.id.ll_user)
    LinearLayout llUser;
    @BindView(R.id.ll_login_options)
    LinearLayout llLoginOptions;
    @BindView(R.id.lv_users)                    //所有用户
    ListView lvUsers;
    @BindView(R.id.feedback)
    TextView feedback;


    private String user = null, password = null; //用户账号和密码
    private LoginUtils loginUtils;
    private List<String> allUserInfos;
    private Gson gson = new Gson();
    private boolean isUnfolded = false;




    /**
     * 注册
     */
    @OnClick(R.id.login_to_register)
    public void toRegisterPage() {
        startActivity(new Intent(this, RegisterPageActivity.class));
    }

    /**
     * 忘记密码?
     */
    @OnClick(R.id.forget_pwd)
    public void toForgetPasswordPage() {
        startActivity(new Intent(this, FindPwdPageActivity.class));
    }

    /**
     * 登录
     */
    @OnClick(R.id.login_button)
    public void login() {

        if (CheckDoubleClickUtils.isDoubleClick(3000)) {
            return;
        }

        startTimeDelay();

        user = loginUserAccount.getText().toString();
        password = loginUserPassword.getText().toString();
        if (user.equals("") || password.equals("")) {
            ToastUtils.showToast("账号或密码为空");
            return;
        }

        String upLoadJson = loginUtils.getUploadJson(user, password);
        LogUtils.e("json:\n" + upLoadJson);
        if (upLoadJson != null) {
            loginUtils.login(upLoadJson, callback);
        }
    }



    /**
     * 展开所有用户
     */
    @OnClick(R.id.img_unfold_allusers)
    public void unfoldUser() {

        if (!isUnfolded){
            lvUsers.setVisibility(View.VISIBLE);
            llLoginOptions.setVisibility(View.GONE);
            unfoldUsers.setImageResource(R.drawable.less);
            isUnfolded = true;
        }
        else{
            lvUsers.setVisibility(View.GONE);
            llLoginOptions.setVisibility(View.VISIBLE);
            unfoldUsers.setImageResource(R.drawable.more_unfold);
            isUnfolded = false;
        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VApp.activityContainer.put(LOGIN_ACTIVITY_TASK, this);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.qmui_config_color_transparent);

    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void initData() {
        loginUtils = LoginUtils.getImpl(this);
        allUserInfos = loginUtils.getAllUserInfo();
    }

    @Override
    public void initViews() {
        INSTANCE = this;
        initUserList();
        //设置默认用户
        setDefaultUser();
    }

    private void initUserList() {
        UserAdapter adapter = new UserAdapter();
        lvUsers.setAdapter(adapter);

        String htmlRes = "<a href='" + feedbackLink +"'>"+ getResources().getString(R.string.feedback)+"</a>";
        feedback.setText(Html.fromHtml(htmlRes));
        feedback.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setDefaultUser() {
        if (allUserInfos != null && allUserInfos.size()!=0) {
            String uName = loginUtils.getCurrentUser().split(":")[0];
            LoginUtils.User defaultUser = loginUtils.getUserObj(uName);
            if (defaultUser!=null){
                loginUserAccount.setText(defaultUser.getUser());
                loginUserPassword.setText(defaultUser.getPwd());
                checkRememberPassword.setChecked(true);
            }

        } else {
            loginUserAccount.setText("");
            loginUserPassword.setText("");
        }
    }

    /**********************************************************************************
     **********************************************************************************
     ************************************登录相关业务***********************************
     **********************************************************************************
     **********************************************************************************
     */

    private UserCallback callback = new UserCallback() {
        @Override
        public void successed(String content) {
            VUiKit.defer().when(() -> {
                SharedPreferences sharedPreferences = VirtualCore.get().getContext().getSharedPreferences("postResult", Context.MODE_PRIVATE);
                String oldUserInfoJson = sharedPreferences.getString("info", null);
                loginUtils.checkUserInfo(oldUserInfoJson, content);
            }).done(res -> {
                Intent loginIntent = new Intent(LoginActivity.this, HomeActivity.class);
                loginIntent.putExtra("userinfo", content);
                startActivity(loginIntent);
                //保存用户信息
                loginUtils.saveUserInfo(user, password);
                loginUtils.saveCurrentUser(user, password);
                loginUtils.save(content);
                loginUtils.saveState(true);

                cancelTimer();
                finish();
            });
        }

        @Override
        public void failed(String error) {
            runOnUiThread(() -> {
                if (error == null) {
                    ToastUtils.showToast(getResources().getString(R.string.network_error_notice));
                } else {
                    ToastUtils.showToast(error);
                }
                loginUtils.saveCurrentUser(user);
                LogUtils.e("callback info -->" + error);
                loginUserPassword.setText("");
                cancelTimer();

            });
        }
    };

    @Override
    public void rgsCallback(String userName, String pwd) {
        loginUserAccount.setText(userName);
        loginUserPassword.setText(pwd);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        TimerDelayUtil.get().stopTimer();
        VApp.activityContainer.remove(LOGIN_ACTIVITY_TASK);

    }

    private class UserAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return allUserInfos!=null ? allUserInfos.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return allUserInfos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder=null;
            if (convertView==null){
                holder = new ViewHolder();
                convertView = getLayoutInflater().inflate(R.layout.user_item, null);
                holder.tvUser = convertView.findViewById(R.id.item_user);
                holder.item = convertView.findViewById(R.id.ll_user);
                convertView.setTag(holder);
            }else{
                holder= (ViewHolder) convertView.getTag();
            }

            final LoginUtils.User user = gson.fromJson(allUserInfos.get(position), LoginUtils.User.class);
            holder.tvUser.setText(user.getUser());
            holder.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loginUserAccount.setText(user.getUser());
                    loginUserPassword.setText(user.getPwd());
                    lvUsers.setVisibility(View.GONE);
                    llLoginOptions.setVisibility(View.VISIBLE);
                    unfoldUsers.setImageResource(R.drawable.more_unfold);
                    isUnfolded = false;
                }
            });
            return convertView;
        }

        public final class ViewHolder{
            public TextView tvUser;
            public LinearLayout item;
        }
    }

    /**
     * 响应时长提示
     */
    private void startTimeDelay(){

        TimerDelayUtil.get().startTimeDelay(new TimerDelayUtil.ITimer() {
            @Override
            public void callback(int time) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (time>0){
                            loginButton.setText(time + "s");
                        }else{
                            loginButton.setText(getResources().getString(R.string.login));
                        }
                    }
                });
            }
        });
    }

    private void cancelTimer(){
        TimerDelayUtil.get().cancelTimer();
    }
}
