package io.virtualapp.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.remote.InstalledAppInfo;
import com.meituan.android.walle.WalleChannelReader;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.virtualapp.BuildConfig;
import io.virtualapp.R;
import io.virtualapp.VApp;
import io.virtualapp.home.activity.BaseActivity;
import io.virtualapp.home.callback.IStringCallback;
import io.virtualapp.home.callback.UserCallback;
import io.virtualapp.home.util.MobileUtils;
import io.virtualapp.home.util.StatusBarUtils;
import io.virtualapp.home.util.TimerDelayUtil;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.home.webview.BaseWebViewActivity;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.user.models.RegisterUtils;
import io.virtualapp.view.link.Link;
import io.virtualapp.view.link.LinkBuilder;
import io.virtualapp.view.utils.CheckDoubleClickUtils;

import static io.virtualapp.home.WebConst.REGISTER_ACTIVITY_TASK;

public class RegisterPageActivity extends BaseActivity {

    @BindView(R.id.register_logo)
    ImageView registerLogo;
    @BindView(R.id.register_user_account)
    EditText registerUserAccount;
    @BindView(R.id.register_user_password)
    EditText registerUserPassword;
    @BindView(R.id.register_user_phone)
    EditText registerUserPhone;
    @BindView(R.id.register_user_invate_code)
    EditText registerUserInvateCode;
    @BindView(R.id.register_button)
    Button registerButton;
    @BindView(R.id.exist_user_tologin)
    TextView toLogin;
    @BindView(R.id.reg_code)
    EditText regCode;
    @BindView(R.id.cb_agree_rule)
    CheckBox checkBox;
    @BindView(R.id.tv_get_code)
    TextView tvGetCode;

    private String account, password, phone, code;
    private String channelId;
    TimerDelayUtil td = null;

    @OnClick({R.id.register_button, R.id.tv_get_code})
    public void register(View view) {
        switch (view.getId()){
            case R.id.register_button:
                reg();
                break;

            case R.id.tv_get_code:
                getCode();
                break;
        }
    }

    private void getCode() {

        if (CheckDoubleClickUtils.isDoubleClick()) return;

        account = registerUserAccount.getText().toString();
        phone = registerUserPhone.getText().toString();

        if (account.equals("") && phone.equals("")) return;

        setVilidateCodeBtn();
        RegisterUtils.getCode(account, phone, new IStringCallback() {
            @Override
            public void getStrResp(String str) {
                tvGetCode.setEnabled(false);
                ToastUtils.showToast("获取验证码成功");
            }
        });
    }

    private void setVilidateCodeBtn() {
        tvGetCode.setTextColor(getResources().getColor(R.color.col_code_invali));
        if (td == null){
           td = TimerDelayUtil.get();
        }
        td.startTimeDelay(true, new TimerDelayUtil.ITimer() {
            @Override
            public void callback(int time) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvGetCode.setText(time + "s后再获取");
                        if (time==0){
                            tvGetCode.setEnabled(true);
                            tvGetCode.setText(getResources().getString(R.string.get_code));
                            tvGetCode.setTextColor(getResources().getColor(R.color.col_code));
                        }
                    }
                });
            }
        });
    }

    private void reg() {

        try{
            if (CheckDoubleClickUtils.isDoubleClick(3000)) {
                return;
            }

            startTimeDelay();

            account = registerUserAccount.getText().toString();
            password = registerUserPassword.getText().toString();
            phone = registerUserPhone.getText().toString();
//        code = registerUserInvateCode.getText().toString();
            code = regCode.getText().toString();

            if (account.equals("")) {
                ToastUtils.showToast(getResources().getString(R.string.account_invalid));
                return;
            }

            if (password.equals("")) {
                ToastUtils.showToast(getResources().getString(R.string.password_invalid));
                return;
            }
            if (phone.equals("")) {
                ToastUtils.showToast(getResources().getString(R.string.mobile_invalid));
                return;
            }

            if (!MobileUtils.isMobileNO(phone)) {
                ToastUtils.showToast(getResources().getString(R.string.mobile_type_invalid));
                return;
            }

            if (code.equals("") || code.length()>6){
                ToastUtils.showToast(getResources().getString(R.string.invalidate_code));
                return;
            }

            //获取渠道channelid,即邀请码
            channelId = WalleChannelReader.getChannel(this);
            //设置channelId
            if (channelId == null || channelId.equals("")) {
                channelId = BuildConfig.CHANNEL;
//                channelId = "lanpanDEV9";
            }

//        ToastUtils.showToast("渠道：" + channelId);
            if (!checkBox.isChecked()){
                ToastUtils.showToast("请先阅读《用户游戏服务协议》");
                return;
            }

            RegisterUtils.register(account, password, phone, channelId, code, this, new UserCallback() {
                @Override
                public void successed(String content) {
                    runOnUiThread(() -> {
                        LogUtils.e("register successed -->" + content);
                        ToastUtils.showToast("注册成功");
                        List<InstalledAppInfo> apps = VirtualCore.get().getInstalledApps(0);
                        if (apps!=null && apps.size()>0){
                            for (InstalledAppInfo appInfo : apps) {
                                VirtualCore.get().uninstallPackage(appInfo.packageName);
                            }
                        }
                        LoginActivity.INSTANCE.rgsCallback(account, password);
                        cancelTimer();
                        finish();
                    });
                }

                @Override
                public void failed(String error) {
                    runOnUiThread(() -> {
                        tvGetCode.setText(getResources().getString(R.string.get_code));
                        tvGetCode.setTextColor(getResources().getColor(R.color.col_code));
                        LogUtils.e("register error -->" + error);
                        ToastUtils.showToast(error);
                        cancelTimer();
                    });
                }
            });
        }catch (Exception e){
            VLog.e("abc", "error = %s", e.toString());
        }

    }

    @OnClick(R.id.exist_user_tologin)
    public void toLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VApp.activityContainer.put(REGISTER_ACTIVITY_TASK, this);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.qmui_config_color_transparent);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_register_page;
    }

    @Override
    public void initData() {

    }

    @Override
    public void initViews() {
        ToastUtils.init(this);
        setLinkRule();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimerDelayUtil.get().stopTimer();
        VApp.activityContainer.remove(REGISTER_ACTIVITY_TASK);
    }

    public void setLinkRule() {
        Link link = new Link("《用户游戏服务协议》");
        link.setTextColor(getResources().getColor(R.color.blue_30e1fd));
        link.setUnderlined(false);
        link.setOnClickListener(new Link.OnClickListener() {
            @Override
            public void onClick(String clickedText) {
                Intent webIntent = new Intent(RegisterPageActivity.this,BaseWebViewActivity.class);
                startActivity(webIntent);
            }
        });
        LinkBuilder.on(checkBox).addLink(link).build();
    }

    /**
     * 响应时长提示
     */
    private void startTimeDelay(){

        cancelTimer();

        TimerDelayUtil.get().startTimeDelay(new TimerDelayUtil.ITimer() {
            @Override
            public void callback(int time) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (time>0){
                            registerButton.setText(time + "s");
                        }else{
                            registerButton.setText(getResources().getString(R.string.register));
                        }
                    }
                });
            }
        });
    }

    private void cancelTimer(){
        TimerDelayUtil.get().stopTimer();
    }

}
