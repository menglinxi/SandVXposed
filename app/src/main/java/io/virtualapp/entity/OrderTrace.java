package io.virtualapp.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/14
 * className:
 * <p/>
 * describe:
 */
@Entity
public class OrderTrace {

    /**
     * game_name：梦幻西游
     * game_id: aecfhavotmaaaacn-g-g17
     * status : 0
     * username : f1570477@163.com
     * account : f1570477@163.com
     * goods_name : 一小包仙玉
     * price : 1.00
     * tag :
     * discount_price : 1.00
     * currency : CNY
     * create_time : 1542170485
     * expire_time : 1542177685
     * login_type : 1
     * discount_reason :
     * id : aebvx25houi67pq5-o-181114124125
     * buyer_id : aebfxzb4di5luo5d
     */

    @Id(autoincrement = true)
    private Long id;
    @Property(nameInDb = "game_name")
    private String game_name;
    @Property(nameInDb = "game_id")
    private String game_id;
    @Property(nameInDb = "status")
    private int status;
    @Property(nameInDb = "username")
    private String username;
    @Property(nameInDb = "account")
    private String account;
    @Property(nameInDb = "goods_name")
    private String goods_name;
    @Property(nameInDb = "price")
    private String price;
    @Property(nameInDb = "tag")
    private String tag;
    @Property(nameInDb = "discount_price")
    private String discount_price;
    @Property(nameInDb = "currency")
    private String currency;
    @Property(nameInDb = "create_time")
    private int create_time;
    @Property(nameInDb = "expire_time")
    private int expire_time;
    @Property(nameInDb = "login_type")
    private int login_type;
    @Property(nameInDb = "discount_reason")
    private String discount_reason;
    @Property(nameInDb = "order_id")
    private String order_id;
    @Property(nameInDb = "buyer_id")
    private String buyer_id;
    @Property(nameInDb = "channel")
    private String channel;
    @Property(nameInDb = "channel_id")
    private String channel_id;
    @Property(nameInDb = "user_id")
    private String user_id;
    @Property(nameInDb = "device_id")
    private String device_id;

    @Property(nameInDb = "send_status")
    private int sendStatus;

    @Property(nameInDb = "app_name")
    private String app_name;
    @Property(nameInDb = "pay_method")
    private String pay_method;
    @Property(nameInDb = "pkg_name")
    private String pkg_name;

    @Generated(hash = 1405099891)
    public OrderTrace() {

    }


    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getPay_method() {
        return pay_method;
    }

    public void setPay_method(String pay_method) {
        this.pay_method = pay_method;
    }

    public String getPkg_name() {
        return pkg_name;
    }

    public void setPkg_name(String pkg_name) {
        this.pkg_name = pkg_name;
    }

    @Generated(hash = 2053747122)
    public OrderTrace(Long id, String game_name, String game_id, int status, String username, String account,
            String goods_name, String price, String tag, String discount_price, String currency, int create_time,
            int expire_time, int login_type, String discount_reason, String order_id, String buyer_id,
            String channel, String channel_id, String user_id, String device_id, int sendStatus, String app_name,
            String pay_method, String pkg_name) {
        this.id = id;
        this.game_name = game_name;
        this.game_id = game_id;
        this.status = status;
        this.username = username;
        this.account = account;
        this.goods_name = goods_name;
        this.price = price;
        this.tag = tag;
        this.discount_price = discount_price;
        this.currency = currency;
        this.create_time = create_time;
        this.expire_time = expire_time;
        this.login_type = login_type;
        this.discount_reason = discount_reason;
        this.order_id = order_id;
        this.buyer_id = buyer_id;
        this.channel = channel;
        this.channel_id = channel_id;
        this.user_id = user_id;
        this.device_id = device_id;
        this.sendStatus = sendStatus;
        this.app_name = app_name;
        this.pay_method = pay_method;
        this.pkg_name = pkg_name;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGame_id() {
        return game_id;
    }

    public void setGame_id(String game_id) {
        this.game_id = game_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getCreate_time() {
        return create_time;
    }

    public void setCreate_time(int create_time) {
        this.create_time = create_time;
    }

    public int getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(int expire_time) {
        this.expire_time = expire_time;
    }

    public int getLogin_type() {
        return login_type;
    }

    public void setLogin_type(int login_type) {
        this.login_type = login_type;
    }

    public String getDiscount_reason() {
        return discount_reason;
    }

    public void setDiscount_reason(String discount_reason) {
        this.discount_reason = discount_reason;
    }

    public String getOrderId() {
        return order_id;
    }

    public void setOrderId(String order_id) {
        this.order_id = order_id;
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getGame_name() {
        return this.game_name;
    }

    public void setGame_name(String game_name) {
        this.game_name = game_name;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }

    public String getOrder_id() {
        return this.order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public int getSendStatus() {
        return this.sendStatus;
    }

    public void setSendStatus(int sendStatus) {
        this.sendStatus = sendStatus;
    }
}
