package io.virtualapp.entity;

/**
 * Created by lanpan on 2018/4/17.
 */

public class DataItem {

    String imageUrl;
    String gameName;

    public DataItem() {
    }

    public DataItem(String imageUrl, String gameName) {
        this.imageUrl = imageUrl;
        this.gameName = gameName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
}
