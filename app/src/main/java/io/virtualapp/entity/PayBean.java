package io.virtualapp.entity;

import java.util.List;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/8
 * className:
 * <p/>
 * describe:
 */
public class PayBean {


    /**
     * pay_methods : [{"status":3,"name":"支付宝支付","icon_url":"","description":"","enabled":true,"hot":false,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"alipay","discount_reason":""},{"status":3,"name":"网易支付","icon_url":"","description":"","enabled":true,"hot":true,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"epay","discount_reason":"","support_sdk":null},{"status":3,"name":"微信支付","icon_url":"","description":"","enabled":true,"hot":false,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"weixinpay","discount_reason":""},{"status":3,"name":"QQ钱包","icon_url":"","description":"","enabled":true,"hot":false,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"tenpay","discount_reason":""},{"status":3,"name":"银行卡支付","icon_url":"","description":"","enabled":true,"hot":true,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"bankcard","discount_reason":""},{"status":3,"name":"手机充值卡支付","icon_url":"","description":"","enabled":true,"hot":false,"discount_price":"30.00","restrict":[[17,18,[30,50,100,200,300,500]],[15,19,[30,50,100,200,300,500]],[19,18,[30,50,100,200,500]]],"reason":"该支付方式暂不可用，请改用其他方式支付。","select_amounts":[10,20,30,50,100,200,300,500],"key":"mobilecard","discount_reason":""},{"status":3,"name":"点数支付","icon_url":"","description":"可用点数240，余额不足，点此充值","enabled":true,"hot":true,"discount_price":"30.00","ecard_insufficient":true,"reason":"该支付方式暂不可用，请改用其他方式支付。","deposite_url":"https://service.mkey.163.com/mpay/games/aecfitydamaaaadw-g-g10/devices/aiavxxklv2cyfnxa-d/users/aebfvrpr44zq7ilg/ecard_recharge","key":"ecard","balance":240,"faq_url":"https://game.163.com/web/pay/pay_include/help.html","discount_reason":""},{"status":3,"name":"支付宝扫码支付","icon_url":"","description":"","enabled":true,"hot":false,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"alipayqr","discount_reason":""},{"status":3,"name":"微信扫码支付","icon_url":"","description":"","enabled":true,"hot":false,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"weixinpayqr","discount_reason":""},{"status":3,"name":"","icon_url":"","description":"","enabled":true,"hot":false,"discount_price":"30.00","reason":"该支付方式暂不可用，请改用其他方式支付。","key":"paymentwall","discount_reason":""},{"status":3,"name":"银行卡支付","icon_url":"","enabled":false,"hot":false,"reason":"","key":"unionpay","description":""},{"status":3,"name":"QQ扫码支付","icon_url":"","enabled":false,"hot":false,"reason":"","key":"tenpayqr","description":""}]
     * game : {"id":"aecfitydamaaaadw-g-g10","name":"率土之滨"}
     * order : {"status":0,"username":"mcjds4oaug1l@163.com","account":"mcjds4oaug1l@163.com","goods_name":"300玉符贡品礼包","price":"30.00","tag":"","discount_price":"30.00","currency":"CNY","create_time":1541672037,"expire_time":1541679237,"login_type":1,"discount_reason":"","id":"aebvxzammui3ppb3-o-181108181357","buyer_id":"aebfvrpr44zq7ilg"}
     */

    private GameBean game;
    private OrderBean order;
    private List<PayMethodsBean> pay_methods;

    public GameBean getGame() {
        return game;
    }

    public void setGame(GameBean game) {
        this.game = game;
    }

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

    public List<PayMethodsBean> getPay_methods() {
        return pay_methods;
    }

    public void setPay_methods(List<PayMethodsBean> pay_methods) {
        this.pay_methods = pay_methods;
    }

    public static class GameBean {
        /**
         * id : aecfitydamaaaadw-g-g10
         * name : 率土之滨
         */

        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class OrderBean {
        /**
         * status : 0
         * username : mcjds4oaug1l@163.com
         * account : mcjds4oaug1l@163.com
         * goods_name : 300玉符贡品礼包
         * price : 30.00
         * tag :
         * discount_price : 30.00
         * currency : CNY
         * create_time : 1541672037
         * expire_time : 1541679237
         * login_type : 1
         * discount_reason :
         * id : aebvxzammui3ppb3-o-181108181357
         * buyer_id : aebfvrpr44zq7ilg
         */

        private int status;
        private String username;
        private String account;
        private String goods_name;
        private String price;
        private String tag;
        private String discount_price;
        private String currency;
        private int create_time;
        private int expire_time;
        private int login_type;
        private String discount_reason;
        private String id;
        private String buyer_id;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(String discount_price) {
            this.discount_price = discount_price;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getCreate_time() {
            return create_time;
        }

        public void setCreate_time(int create_time) {
            this.create_time = create_time;
        }

        public int getExpire_time() {
            return expire_time;
        }

        public void setExpire_time(int expire_time) {
            this.expire_time = expire_time;
        }

        public int getLogin_type() {
            return login_type;
        }

        public void setLogin_type(int login_type) {
            this.login_type = login_type;
        }

        public String getDiscount_reason() {
            return discount_reason;
        }

        public void setDiscount_reason(String discount_reason) {
            this.discount_reason = discount_reason;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBuyer_id() {
            return buyer_id;
        }

        public void setBuyer_id(String buyer_id) {
            this.buyer_id = buyer_id;
        }
    }

    public static class PayMethodsBean {
        /**
         * status : 3
         * name : 支付宝支付
         * icon_url :
         * description :
         * enabled : true
         * hot : false
         * discount_price : 30.00
         * reason : 该支付方式暂不可用，请改用其他方式支付。
         * key : alipay
         * discount_reason :
         * support_sdk : null
         * restrict : [[17,18,[30,50,100,200,300,500]],[15,19,[30,50,100,200,300,500]],[19,18,[30,50,100,200,500]]]
         * select_amounts : [10,20,30,50,100,200,300,500]
         * ecard_insufficient : true
         * deposite_url : https://service.mkey.163.com/mpay/games/aecfitydamaaaadw-g-g10/devices/aiavxxklv2cyfnxa-d/users/aebfvrpr44zq7ilg/ecard_recharge
         * balance : 240
         * faq_url : https://game.163.com/web/pay/pay_include/help.html
         */

        private int status;
        private String name;
        private String icon_url;
        private String description;
        private boolean enabled;
        private boolean hot;
        private String discount_price;
        private String reason;
        private String key;
        private String discount_reason;
        private Object support_sdk;
        private boolean ecard_insufficient;
        private String deposite_url;
        private int balance;
        private String faq_url;
        private List<List<Integer>> restrict;
        private List<Integer> select_amounts;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIcon_url() {
            return icon_url;
        }

        public void setIcon_url(String icon_url) {
            this.icon_url = icon_url;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public boolean isHot() {
            return hot;
        }

        public void setHot(boolean hot) {
            this.hot = hot;
        }

        public String getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(String discount_price) {
            this.discount_price = discount_price;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getDiscount_reason() {
            return discount_reason;
        }

        public void setDiscount_reason(String discount_reason) {
            this.discount_reason = discount_reason;
        }

        public Object getSupport_sdk() {
            return support_sdk;
        }

        public void setSupport_sdk(Object support_sdk) {
            this.support_sdk = support_sdk;
        }

        public boolean isEcard_insufficient() {
            return ecard_insufficient;
        }

        public void setEcard_insufficient(boolean ecard_insufficient) {
            this.ecard_insufficient = ecard_insufficient;
        }

        public String getDeposite_url() {
            return deposite_url;
        }

        public void setDeposite_url(String deposite_url) {
            this.deposite_url = deposite_url;
        }

        public int getBalance() {
            return balance;
        }

        public void setBalance(int balance) {
            this.balance = balance;
        }

        public String getFaq_url() {
            return faq_url;
        }

        public void setFaq_url(String faq_url) {
            this.faq_url = faq_url;
        }

        public List<List<Integer>> getRestrict() {
            return restrict;
        }

        public void setRestrict(List<List<Integer>> restrict) {
            this.restrict = restrict;
        }

        public List<Integer> getSelect_amounts() {
            return select_amounts;
        }

        public void setSelect_amounts(List<Integer> select_amounts) {
            this.select_amounts = select_amounts;
        }
    }
}
