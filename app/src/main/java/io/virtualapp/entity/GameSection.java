package io.virtualapp.entity;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * Created by lanpan on 2018/4/18.
 */

public class GameSection extends SectionEntity<GameEntity> {

    private boolean isMore;

    public GameSection(boolean isHeader, String header, boolean isMore) {
        super(isHeader, header);
        this.isMore = isMore;
    }

    public GameSection(GameEntity gameEntity) {
        super(gameEntity);
    }

    public boolean isMore() {
        return isMore;
    }

    public void setMore(boolean more) {
        isMore = more;
    }
}
