package io.virtualapp.entity.va;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.remote.InstalledAppInfo;

import java.util.ArrayList;
import java.util.List;


/**
 * * Created by chao on 2018/4/3.
 * app管理
 */
public class AppManager {
    private static Context context;
    //已安装app列表
    private List<AppInfo> mInstalledApps = new ArrayList<>();

    public AppManager(Context context) {
        this.context = context;
    }

    /**
     * 获得已安装app列表
     *
     * @return
     */
    public List<AppInfo> geInstalledApps() {
        loadInstalledApps();
        return mInstalledApps;
    }

    /**
     * 加载所有已安装app列表
     */
    private void loadInstalledApps() {
        mInstalledApps.clear();
        List<InstalledAppInfo> installedApps = VirtualCore.get().getInstalledApps(0);
        PackageManager packageManager = VirtualCore.getPM();
        for (InstalledAppInfo installedApp : installedApps) {
            int[] installedUsers = installedApp.getInstalledUsers();
            for (int installedUser : installedUsers) {
                AppInfo info = new AppInfo();
                info.userId = installedUser;
                ApplicationInfo applicationInfo = installedApp.getApplicationInfo(installedUser);
                info.name = applicationInfo.loadLabel(packageManager).toString();
                info.icon = applicationInfo.loadIcon(packageManager);
                info.pkgName = installedApp.packageName;
                info.path = applicationInfo.sourceDir;
                mInstalledApps.add(info);
            }
        }
    }
}
