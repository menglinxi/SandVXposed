package io.virtualapp.entity.va;

import android.graphics.drawable.Drawable;

/**
 * Created by chao on 2018/4/3.
 * app信息
 */
public class AppInfo {
    String name;
    int userId;
    Drawable icon;
    String pkgName;
    String path;

    public String getName() {
        if (userId == 0) {
            return name;
        } else {
            return name + "[" + (userId + 1) + "]";
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setName(String name) {

        this.name = name;
    }
}
