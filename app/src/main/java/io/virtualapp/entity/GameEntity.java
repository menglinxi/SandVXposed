package io.virtualapp.entity;

/**
 * Created by chao on 2018/3/18.
 * 游戏实体类
 */

public class GameEntity {

    /**
     * gamename : 楚留香
     * pic : http://www.17sy8.cn:80/uploadimg/1523601409170.png
     * apkpackage : com.netease.wyclx
     * dlurl : https://h42.gdl.netease.com/h42_netease_netease.lpwl_cps_dev_3.0.1_com.netease.wyclx_18aT6fj.apk
     * channel : 1
     * sort : 0
     * ishot : 0
     * createtime : 2018-04-13 14:36:50
     * id : 11
     */

    private String gamename;
    private String pic;
    private String apkpackage;
    private String dlurl;
    private int channel;
    private int sort;
    private int ishot;
    private String createtime;
    private int id;
    private String status;
    private float progress;

    public String getGamename() {
        return gamename;
    }

    public void setGamename(String gamename) {
        this.gamename = gamename;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getApkpackage() {
        return apkpackage;
    }

    public void setApkpackage(String apkpackage) {
        this.apkpackage = apkpackage;
    }

    public String getDlurl() {
        return dlurl;
    }

    public void setDlurl(String dlurl) {
        this.dlurl = dlurl;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getIshot() {
        return ishot;
    }

    public void setIshot(int ishot) {
        this.ishot = ishot;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }
}
