package io.virtualapp.dev.repo;

import android.content.Context;

import com.lody.virtual.remote.InstallResult;

import java.io.File;
import java.util.List;

import io.virtualapp.abs.Callback;
import io.virtualapp.dev.models.AppData;
import io.virtualapp.dev.models.AppInfoLite;


/**
 * @author Lody
 * @version 1.0
 */
public interface AppDataSource<T> {

    /**
     * @return All the Applications we Virtual.
     */
    void getVirtualApps(Callback<List<AppData>> callback);

    /**
     * @param context Context
     * @return All the Applications we Installed.
     */
    void getInstalledApps(Context context, Callback<T> callback);

    void getStorageApps(Context context, File rootDir, Callback<T> callback);

    InstallResult addVirtualApp(AppInfoLite info);

    boolean removeVirtualApp(String packageName, int userId);
}
