package io.virtualapp.dev.page;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;

import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.helper.utils.VLog;
import com.yinglan.alphatabs.AlphaTabView;
import com.yinglan.alphatabs.AlphaTabsIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.home.activity.BaseActivity;
import io.virtualapp.home.util.StatusBarUtils;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.home.weight.NoTouchViewPager;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/21
 * className:
 * <p/>
 * describe:
 */
public class DevActivity extends BaseActivity {

    private static final String PKG_NAME_ARGUMENT = "MODEL_ARGUMENT";
    private static final String KEY_INTENT = "KEY_INTENT";
    private static final String KEY_USER = "KEY_USER";

    @BindView(R.id.viewpager_vg)
    NoTouchViewPager mainViewpager;
    @BindView(R.id.dev_uninstalled)
    AlphaTabView devUninstalled;
    @BindView(R.id.dev_installed)
    AlphaTabView devInstalled;
    @BindView(R.id.main_alphatabs_indicator)
    AlphaTabsIndicator mainAlphatabsIndicator;

    private List<Fragment> mainfragments = new ArrayList<>();
    private DevAdapter mDevAdapter;
    private CloneFragment cloneFragment;
    private OriganteFragment unCloneFragment;
    private ActionBar actionBar;

    @Override
    public int getLayoutId() {
        return R.layout.activity_dev;
    }

    @Override
    public void initData() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.theme_color);
        actionBar = getSupportActionBar();
        actionBar.show();
    }

    @Override
    public void initViews() {
        mDevAdapter = new DevAdapter(getSupportFragmentManager());
        mainViewpager.setAdapter(mDevAdapter);
        mainViewpager.addOnPageChangeListener(mDevAdapter);
        mainViewpager.setOffscreenPageLimit(2);
        mainAlphatabsIndicator.setViewPager(mainViewpager);
    }

    class DevAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        public DevAdapter(FragmentManager fm) {
            super(fm);
            unCloneFragment = new OriganteFragment();
            cloneFragment = new CloneFragment();
            mainfragments.add(unCloneFragment);
            mainfragments.add(cloneFragment);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            VLog.e("abc", "onPageSelected = %s", position);
            switch (position) {

                case 0:
                    actionBar.setTitle("应用列表");
                    getItem(position).onResume();
                    break;

                case 1:
                    actionBar.setTitle("已克隆列表");
                    getItem(position).onResume();
                    break;

            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }

        @Override
        public Fragment getItem(int position) {
            return mainfragments.get(position);
        }

        @Override
        public int getCount() {
            return mainfragments.size();
        }

    }

    public static void launch(String packageName, int userId, String appName) {
        Intent intent = VirtualCore.get().getLaunchIntent(packageName, userId);
        VActivityManager.get().startActivity(intent, userId);
        VLog.e("abc","launch User id ="+userId);
        ToastUtils.showToast("正在启动：" + appName);
    }
}
