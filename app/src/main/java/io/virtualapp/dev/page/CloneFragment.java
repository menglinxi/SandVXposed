package io.virtualapp.dev.page;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VUserInfo;
import com.lody.virtual.os.VUserManager;
import com.lody.virtual.remote.InstallResult;
import com.lody.virtual.remote.InstalledAppInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.abs.Callback;
import io.virtualapp.abs.ui.VUiKit;
import io.virtualapp.dev.adapter.CloneAppAdapter;
import io.virtualapp.dev.models.AppData;
import io.virtualapp.dev.models.AppInfoLite;
import io.virtualapp.dev.models.EmptyAppData;
import io.virtualapp.dev.models.MultiplePackageAppData;
import io.virtualapp.dev.models.PackageAppData;
import io.virtualapp.dev.repo.AppRepository;
import io.virtualapp.dev.repo.PackageAppDataStorage;
import io.virtualapp.home.fragment.base.ThemeFragment;
import io.virtualapp.home.util.ToastUtils;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/21
 * className:
 * <p/>
 * describe:
 */
public class CloneFragment extends ThemeFragment implements Callback{

    @BindView(R.id.dev_app_list)
    RecyclerView rvAppList;
    @BindView(R.id.loadding)
    ProgressBar loadding;
    public static CloneFragment INSTANCE;

    private CloneAppAdapter adapter;
    private AppInfoLite appInfo;
    public List<AppData> appDatas = new ArrayList<>();
    private AppRepository appRepository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        INSTANCE = this;
    }

    @Override
    protected void initData() {
        appRepository = new AppRepository(getContext());
        appRepository.getVirtualApps(new Callback<List<AppData>>() {
            @Override
            public void callback(List<AppData> result) {
                loadding.setVisibility(View.GONE);
                appDatas.addAll(result);
                adapter.addData(appDatas);
                VLog.i("abc", "List<AppData> = %s", Arrays.toString(result.toArray()));
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_dev;
    }

    @Override
    protected void initViews() {
        VLog.i("abc", "text = %s", "initViews()");
        initAdapter();
    }

    public void initAdapter(){
        if (adapter==null){
            adapter = new CloneAppAdapter(R.layout.rv_game_repo_item);
            rvAppList.setLayoutManager(new GridLayoutManager(getContext(), 3));
            adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                }
            });
            rvAppList.setAdapter(adapter);
        }
    }

    @Override
    public void callback(Object result) {

        appInfo = (AppInfoLite) result;
        class AddResult {
            private PackageAppData appData;
            private int userId;
            private boolean justEnableHidden;
        }

        AddResult addResult = new AddResult();
        VUiKit.defer().when(() -> {
            InstalledAppInfo installedAppInfo = VirtualCore.get().getInstalledAppInfo(appInfo.packageName, 0);
            addResult.justEnableHidden = installedAppInfo != null;
            //** 判断是否已经安装
            //**  1.如果安装了, 则以userid为索引安装多个
            //**  2.否则安装新的app, userid=0
            if (addResult.justEnableHidden) {
                int[] userIds = installedAppInfo.getInstalledUsers();
                int nextUserId = userIds.length;
                /*
                  Input : userIds = {0, 1, 3}
                  Output: nextUserId = 2
                 */
                for (int i = 0; i < userIds.length; i++) {
                    if (userIds[i] != i) {
                        nextUserId = i;
                        break;
                    }
                }
                addResult.userId = nextUserId;
                addResult.appData.name = addResult.appData.getName() + "[" + nextUserId +  "]";
                if (VUserManager.get().getUserInfo(nextUserId) == null) {
                    // user not exist, create it automatically.
                    String nextUserName = "Space " + (nextUserId + 1);
                    VUserInfo newUserInfo = VUserManager.get().createUser(nextUserName, VUserInfo.FLAG_ADMIN);
                    if (newUserInfo == null) {
                        throw new IllegalStateException();
                    }
                }
                boolean success = VirtualCore.get().installPackageAsUser(nextUserId, appInfo.packageName);
                if (!success) {
                    throw new IllegalStateException();
                }
            } else {
                InstallResult res = appRepository.addVirtualApp(appInfo);
                if (!res.isSuccess) {
                    throw new IllegalStateException();
                }
            }
        }).then((res) -> {
            addResult.appData = PackageAppDataStorage.get().acquire(appInfo.packageName);
        }).done(res -> {
            boolean multipleVersion = addResult.justEnableHidden && addResult.userId != 0;
            if (!multipleVersion) {
                PackageAppData data = addResult.appData;
                data.isLoading = true;
                addAppToLauncher(data);
                handleOptApp(data, appInfo.packageName, true);
            } else {
                MultiplePackageAppData data = new MultiplePackageAppData(addResult.appData, addResult.userId);
                data.isLoading = true;
                addAppToLauncher(data);
                handleOptApp(data, appInfo.packageName, false);
            }
        });
    }

    private void handleOptApp(AppData data, String packageName, boolean needOpt) {
        VUiKit.defer().when(() -> {
            long time = System.currentTimeMillis();
            if (needOpt) {
                try {
                    VirtualCore.get().preOpt(packageName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            time = System.currentTimeMillis() - time;
            if (time < 1500L) {
                try {
                    Thread.sleep(1500L - time);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).done((res) -> {
            if (data instanceof PackageAppData) {
                ((PackageAppData) data).isLoading = false;
                ((PackageAppData) data).isFirstOpen = true;
            } else if (data instanceof MultiplePackageAppData) {
                ((MultiplePackageAppData) data).isLoading = false;
                ((MultiplePackageAppData) data).isFirstOpen = true;
            }
            ToastUtils.showToast(data.getName()+"安装成功");
//            refreshLauncherItem(data);
        });
    }

    private void refreshLauncherItem(AppData data) {
        if (!appDatas.contains(data)){
            adapter.addData(data);
        }
    }


    public void addAppToLauncher(AppData model) {
        List<AppData> dataList = appDatas;
        boolean replaced = false;
        for (int i = 0; i < dataList.size(); i++) {
            AppData data = dataList.get(i);
            if (data instanceof EmptyAppData) {
                adapter.addData(i, data);
                replaced = true;
                break;
            }
        }
        if (!replaced) {
            adapter.addData(model);
        }
    }

}
