package io.virtualapp.dev.page;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.lody.virtual.helper.utils.VLog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import io.virtualapp.R;
import io.virtualapp.abs.Callback;
import io.virtualapp.dev.adapter.OriganteAppAdapter;
import io.virtualapp.dev.models.AppInfo;
import io.virtualapp.dev.repo.AppRepository;
import io.virtualapp.home.fragment.base.ThemeFragment;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/21
 * className:
 * <p/>
 * describe:
 */
public class OriganteFragment extends ThemeFragment {

    @BindView(R.id.dev_app_list)
    RecyclerView rvAppList;
    @BindView(R.id.loadding)
    ProgressBar loadding;

    private OriganteAppAdapter adapter;
    private List<AppInfo> list;
    private List<AppInfo> clones = new ArrayList<>();

    @Override
    protected void initData() {
        VLog.i("abc", "text = %s", "initData()");
        AppRepository appRepository = new AppRepository(getContext());
        appRepository.getInstalledApps(getContext(), new Callback<List<AppInfo>>() {
            @Override
            public void callback(List<AppInfo> result) {
                list = result;
                if (result.size()>0 && adapter!=null){
                    loadding.setVisibility(View.GONE);
                    adapter.addData(result);
                    VLog.i("abc", "List<AppInfo> = %s", Arrays.toString(result.toArray()));
                }
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_dev;
    }

    @Override
    protected void initViews() {
        VLog.i("abc", "text = %s", "initViews()");
        initAdapter();
    }

    public void initAdapter(){
        if (adapter==null){
            adapter = new OriganteAppAdapter(R.layout.rv_game_repo_item);
            rvAppList.setLayoutManager(new GridLayoutManager(getContext(), 3));
            adapter.setOnItemChildLongClickListener(new BaseQuickAdapter.OnItemChildLongClickListener() {
                @Override
                public boolean onItemChildLongClick(BaseQuickAdapter adapter, View view, int position) {

                    return false;
                }
            });
            rvAppList.setAdapter(adapter);
        }
    }

}
