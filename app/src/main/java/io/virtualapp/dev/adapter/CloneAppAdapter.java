package io.virtualapp.dev.adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lid.lib.LabelImageView;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.helper.utils.VLog;

import io.virtualapp.R;
import io.virtualapp.dev.models.AppData;
import io.virtualapp.dev.models.MultiplePackageAppData;
import io.virtualapp.dev.models.PackageAppData;
import io.virtualapp.dev.page.DevActivity;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/22
 * className:
 * <p/>
 * describe:
 */
public class CloneAppAdapter extends BaseQuickAdapter<AppData, BaseViewHolder>{

    public CloneAppAdapter(int layoutResId) {
        super(layoutResId);

    }

    @Override
    protected void convert(BaseViewHolder helper, AppData item) {
        helper.itemView.setTag(item);
        helper.getView(R.id.download_progressbar).setVisibility(View.GONE);
        Glide.with(mContext).load(item.getIcon()).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                LabelImageView imageView =  helper.getView(R.id.game_logo);
                imageView.setLabelText("克隆");
                imageView.setLabelBackgroundColor(Color.parseColor("#67e667"));
                imageView.setImageDrawable(resource);
            }
        });
        helper.setText(R.id.installed_game_name, item.getName());
        helper.setTextColor(R.id.installed_game_name, mContext.getResources().getColor(R.color.col_code));

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppData data = (AppData) helper.itemView.getTag();
                if (data instanceof PackageAppData) {
                    PackageAppData appData = (PackageAppData) data;
                    appData.isFirstOpen = false;
                    DevActivity.launch(appData.packageName, 0, data.getName());
                } else if (data instanceof MultiplePackageAppData) {
                    MultiplePackageAppData multipleData = (MultiplePackageAppData) data;
                    multipleData.isFirstOpen = false;
                    DevActivity.launch(multipleData.appInfo.packageName, ((MultiplePackageAppData) data).userId, data.getName());
                }
            }
        });

        helper.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AppData data = (AppData) helper.itemView.getTag();
                VLog.i("abc", "position = %s", helper.getLayoutPosition());

                new MaterialDialog.Builder(mContext)
                        .title("提示")
                        .content("是否卸载\""+ data.getName() + "\"?")
                        .positiveText("卸载")
                        .negativeText("取消")
                        .positiveColor(mContext.getResources().getColor(R.color.holo_red_dark))
                        .negativeColor(mContext.getResources().getColor(R.color.bootstrap_gray_light))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                if (data instanceof PackageAppData) {
                                    PackageAppData appData = (PackageAppData) data;
                                    appData.isFirstOpen = false;
                                    VirtualCore.get().uninstallPackage(appData.packageName);
                                    remove(helper.getLayoutPosition());
                                } else if (data instanceof MultiplePackageAppData) {
                                    MultiplePackageAppData multipleData = (MultiplePackageAppData) data;
                                    multipleData.isFirstOpen = false;
                                    VirtualCore.get().uninstallPackage(multipleData.appInfo.packageName);
                                    remove(helper.getLayoutPosition());
                                }
                            }
                        })
                        .build()
                        .show();
                return false;
            }
        });

    }
}
