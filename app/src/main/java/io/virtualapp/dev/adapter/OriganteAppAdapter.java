package io.virtualapp.dev.adapter;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lid.lib.LabelImageView;
import com.lody.virtual.client.core.VirtualCore;

import io.virtualapp.R;
import io.virtualapp.dev.models.AppData;
import io.virtualapp.dev.models.AppInfo;
import io.virtualapp.dev.models.AppInfoLite;
import io.virtualapp.dev.page.CloneFragment;
import io.virtualapp.home.util.ToastUtils;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/22
 * className:
 * <p/>
 * describe:
 */
public class OriganteAppAdapter extends BaseQuickAdapter<AppInfo, BaseViewHolder>{

    public OriganteAppAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, AppInfo item) {
        helper.itemView.setTag(item);
        helper.getView(R.id.download_progressbar).setVisibility(View.GONE);
        Glide.with(mContext).load(item.icon).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                LabelImageView imageView =  helper.getView(R.id.game_logo);
                if (VirtualCore.get().isAppInstalled(item.packageName) && resource instanceof BitmapDrawable) {
                    imageView.setLabelText("克隆");
                    imageView.setLabelBackgroundColor(Color.parseColor("#67e667"));
                    imageView.setImageDrawable(resource);
                } else {
                    imageView.setLabelText("原始");
                    imageView.setLabelBackgroundColor(Color.parseColor("#C2185B"));
                    imageView.setImageDrawable(resource);
                }
            }
        });
        helper.setText(R.id.installed_game_name, item.name);
        helper.setTextColor(R.id.installed_game_name, mContext.getResources().getColor(R.color.col_code));

        helper.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AppInfo item = (AppInfo) v.getTag();

                new MaterialDialog.Builder(mContext)
                        .title("提示")
                        .content("是否添加\""+ item.name+ "\"到虚拟内存中?")
                        .positiveText("添加")
                        .negativeText("取消")
                        .positiveColor(mContext.getResources().getColor(R.color.col_code))
                        .negativeColor(mContext.getResources().getColor(R.color.bootstrap_gray_light))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                //禁止安装多个同样的app
                                if (CloneFragment.INSTANCE.appDatas!=null){
                                    for (AppData appData : CloneFragment.INSTANCE.appDatas) {
                                        if (appData.getName().equals(item.name) && !appData.isFirstOpen()){
                                            ToastUtils.showToast(appData.getName()+"已安装");
                                            return;
                                        }
                                    }
                                }

                                AppInfoLite infoLite = new AppInfoLite(item.packageName, item.path, item.fastOpen);
                                CloneFragment.INSTANCE.callback(infoLite);
                            }
                        })
                        .build()
                        .show();
                return false;
            }
        });

    }
}
