package io.virtualapp.delegate.db;

import java.util.List;

/**
 * author: gecollsn
 * <p/>
 * email: zhougx@gamezs.cn
 * time:  2017-01-11 下午 16:14
 * className: IDaoHandle
 * <p/>
 * describe:
 */
public interface IDaoHandle<T> {

    void inspectDirtyData();

    void insert(T t);

    void update(T t);

    List<T> query();

    void deleteAll();

    void delete(T t);

    void insertAll(List<T> dataList);

    T queryKey(String str);
}
