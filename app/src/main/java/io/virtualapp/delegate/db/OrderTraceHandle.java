package io.virtualapp.delegate.db;
import android.content.Context;
import android.database.Cursor;

import com.lody.virtual.helper.utils.VLog;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import io.virtualapp.VApp;
import io.virtualapp.delegate.db.compile.DaoMaster;
import io.virtualapp.delegate.db.compile.OrderTraceDao;
import io.virtualapp.entity.OrderTrace;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/14
 * className:
 * <p/>
 * describe:
 */
public class OrderTraceHandle implements IDaoHandle<OrderTrace> {

    private static final int DAY = 1000 * 60 * 60 * 24;
    private static final int MONTH = 1000 * 60 * 60 * 24 * 30;

    private static final String dbName = "order_db";
    private static OrderTraceHandle mInstance;
    private DaoMaster.DevOpenHelper openHelper;
    private Context context;

    public OrderTraceHandle() {
//        inspectDirtyData();
    }

    /**
     * 获取单例
     * @return
     */
    public static OrderTraceHandle get() {
        if (mInstance == null) {
            synchronized (OrderTraceHandle.class) {
                if (mInstance == null) {
                    mInstance = new OrderTraceHandle();
                }
            }
        }
        return mInstance;
    }

    /**
     * 定期清理
     */
    @Override
    public void inspectDirtyData() {
        List<Long> dirtyData = new ArrayList<>();
        List<OrderTrace> originData = query();
        for (OrderTrace item : originData) {
            boolean timeOut = item.getExpire_time()-System.currentTimeMillis() <= 0;
            if (timeOut) {
                dirtyData.add(item.getId());
            }
        }

        if (dirtyData.size() > 0) {
            getDao().deleteByKeyInTx(dirtyData);
        }
    }

    /**
     * 插入订单数据
     * 如果没有数据则插入新数据
     * 否则更新数据
     * @param orderTrace
     */
    @Override
    public void insert(OrderTrace orderTrace) {
        VLog.i("abc", "insert order id = %s", orderTrace.getOrder_id());
        boolean hasRecord = getDao().queryBuilder()
                .where(OrderTraceDao.Properties.Order_id.eq(orderTrace.getOrder_id()))
                .list()
                .size() > 0;

        VLog.i("abc", "hasRecord = %s", hasRecord);
        if (!hasRecord) {
            getDao().insert(orderTrace);
        }
        update(orderTrace);
    }



    /**
     * 插入订单列表
     * @param dataList
     */
    @Override
    public void insertAll(List<OrderTrace> dataList) {
        Cursor cursor = getDao().queryBuilder().buildCursor().query();
        boolean hasNext = cursor.moveToFirst();
        List<Long> ids = new ArrayList<>();
        while (hasNext) {
            int id_key = cursor.getColumnIndex("id");
            Long id = cursor.getLong(id_key);
            ids.add(id);
            hasNext = cursor.moveToNext();
        }
        cursor.close();

        List<OrderTrace> insertData = new ArrayList<>();
        List<OrderTrace> updateData = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            OrderTrace item = dataList.get(i);
            if (ids.contains(item.getId()))
                updateData.add(item);
            else
                insertData.add(item);
        }

        getDao().insertInTx(insertData);
        getDao().updateInTx(updateData);
    }


    @Override
    public void update(OrderTrace orderTrace) {
        getDao().update(orderTrace);
    }

    /**
     * 查询所有订单
     * @return
     */
    @Override
    public List<OrderTrace> query() {

        if (getDao()!=null){
            return getDao().queryBuilder().list();
        }
        return null;
    }

    public List<OrderTrace>  getSendFail(){
        List<OrderTrace> rt = null;
        if (getDao()!=null){
            rt = getDao().queryBuilder().where(OrderTraceDao.Properties.SendStatus.eq(-1)).list();
        }
        return rt;
    }

    public List<OrderTrace>  getNoSend(){
        List<OrderTrace> rt = null;
        if (getDao()!=null){
            rt = getDao().queryBuilder().where(OrderTraceDao.Properties.SendStatus.lt(0)).list();
        }
        return rt;
    }

    /**
     * 查询所有订单
     * @return
     */
    @Override
    public OrderTrace queryKey(String str) {
        OrderTrace orderTrace = null;
        if (getDao()!=null){
            orderTrace = getDao().queryBuilder().where(OrderTraceDao.Properties.Order_id.eq(str)).unique();
        }
        return orderTrace;
    }



    /**
     * 删除所有订单
     */
    @Override
    public void deleteAll() {
        getDao().deleteAll();
    }

    /**
     * 删除某条订单
     * @param orderTrace
     */
    @Override
    public void delete(OrderTrace orderTrace) {
        getDao().delete(orderTrace);
    }

    private OrderTraceDao getDao() {
        return VApp.getApp().getDaoSession().getOrderTraceDao();
    }

}
