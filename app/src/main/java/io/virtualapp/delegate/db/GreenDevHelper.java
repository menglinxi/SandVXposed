package io.virtualapp.delegate.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.database.Database;

import io.virtualapp.delegate.db.compile.DaoMaster;
import io.virtualapp.delegate.db.compile.OrderTraceDao;

/**
 * author: MQZ
 * <p>
 * email: 82498301@qq.com
 * className: GreenDevHelper
 * <p>
 * describe: 预留的数据库升级操作类
 */
public class GreenDevHelper extends DaoMaster.DevOpenHelper {
    private static final String DB_NAME = "order_db.db";

    public GreenDevHelper(Context context) {
        super(context, DB_NAME);
    }

    public GreenDevHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DB_NAME, factory);
    }

    @Override
    public void onCreate(Database db) {
        DaoMaster.createAllTables(db, true);


    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        // TODO: GreenDao数据库升级时，默认会将原数据库删除，如果需要自定义升级时的操作，应该重写该方法
        // 数据库操作时，数据库版本对比的是已发布的app的数据库版本，并非开发中的数据库版本
        Log.d("abc", "Data Base Upload");
        switch (oldVersion) {
            case 1:
                upgradeDBFrom1To2(db);
            case 2:
                upgradeDBFrom2To3(db);
            case 3:
                upgradeDBFrom3To4(db);
        }
    }

    public boolean isTableExists(Database db, String tableName) {


        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    private void upgradeDBFrom1To2(Database db) {

        final String OPEN_LOG_TABLE = "LaunchLogCache";
        if (!isTableExists(db, OPEN_LOG_TABLE)) {
            return;
        }
        String sql_imei = "ALTER TABLE " + OPEN_LOG_TABLE + " ADD COLUMN imei TEXT";
        String sql_number = "ALTER TABLE " + OPEN_LOG_TABLE + " ADD COLUMN number TEXT";
        db.execSQL(sql_imei);
        db.execSQL(sql_number);
    }

    private void upgradeDBFrom3To4(Database db) {
        final String ORDER_TRACE_TB = OrderTraceDao.TABLENAME;

        Cursor dbCursor = db.rawQuery("SELECT sql FROM sqlite_master " +
                "WHERE tbl_name = '"+ORDER_TRACE_TB+"' AND type = 'table';",null);
        String[] columnNames = dbCursor.getColumnNames();

        Log.d("abc", "Show Table col:");
        for (String col : columnNames) {
            Log.d("abc", "#" + col);
        }

        String sql_send_status = "ALTER TABLE " + ORDER_TRACE_TB + " ADD COLUMN send_status INTEGER";
        db.execSQL(sql_send_status);

        String sql_app_name = "ALTER TABLE " + ORDER_TRACE_TB + " ADD COLUMN app_name TEXT";
        db.execSQL(sql_app_name);

        String sql_pay_method = "ALTER TABLE " + ORDER_TRACE_TB + " ADD COLUMN pay_method TEXT";
        db.execSQL(sql_pay_method);

        String sql_pkg_name = "ALTER TABLE " + ORDER_TRACE_TB + " ADD COLUMN pkg_name TEXT";
        db.execSQL(sql_pkg_name);
    }


    private void upgradeDBFrom2To3(Database db) {
        OrderTraceDao.createTable(db, true);
        final String INSTALL_GAME_TABLE = "InstalledGameData";
        if (!isTableExists(db, INSTALL_GAME_TABLE)) {
            return;
        }
        String share_url = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN share_url TEXT";
        String is_subscribe = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN is_subscribe TEXT";
        String is_collection = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN is_collection TEXT";
        String colligate_score = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN colligate_score TEXT";
        String app_status = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN app_status TEXT";
        String subscribe_count = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN subscribe_count TEXT";
        String comment_count = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN comment_count TEXT";
        String cover_img = "ALTER TABLE " + INSTALL_GAME_TABLE + " ADD COLUMN cover_img TEXT";

        db.execSQL(share_url);
        db.execSQL(is_collection);
        db.execSQL(is_subscribe);
        db.execSQL(colligate_score);
        db.execSQL(app_status);
        db.execSQL(subscribe_count);
        db.execSQL(comment_count);
        db.execSQL(cover_img);
    }
}
