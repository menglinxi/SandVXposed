package io.virtualapp.delegate.models;

import com.lody.virtual.helper.utils.VLog;

import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.OkHttpCallback;
import io.virtualapp.home.callback.RespCallback;
import io.virtualapp.tools.LogUtils;
import io.virtualapp.user.models.SendInfoWithOkHttp;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/6
 * className:
 * <p/>
 * describe:
 */
public class TraceLoginModel {

    private static TraceLoginModel instance = new TraceLoginModel();

    public static TraceLoginModel get(){
        return instance;
    }

    public void traceLoginState(String param, OkHttpCallback<String> callback){
        VLog.i("abc", "auto login params = %s", param);
        SendInfoWithOkHttp.sendHttpPostRequest(param, WebConst.webUrlForGameLogin, new RespCallback() {
            @Override
            public void response(String resp) {
                VLog.i("abc", "auto login content = %s", resp);
                callback.response(resp);
            }

            @Override
            public void errors(Throwable t) {
                LogUtils.e("receive auto login errors : " + t.toString());
                callback.errors(t);
            }
        });
    }
}
