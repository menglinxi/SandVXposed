package io.virtualapp.delegate.models;

import com.lody.virtual.helper.utils.VLog;

import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.OkHttpCallback;
import io.virtualapp.home.callback.RespCallback;
import io.virtualapp.user.models.SendInfoWithOkHttp;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/6
 * className:
 * <p/>
 * describe:
 */
public class TracePayStateModel {

    private static TracePayStateModel instance = new TracePayStateModel();

    public static TracePayStateModel get(){
        return instance;
    }

    public void traceUserPayState(String param, OkHttpCallback<String> callback){
        VLog.i("abc", "user pay params = %s", param);
        SendInfoWithOkHttp.sendHttpPostRequest(param, WebConst.webUrlForPay, new RespCallback() {
            @Override
            public void response(String resp) {
                callback.response(resp);
            }

            @Override
            public void errors(Throwable t) {
                callback.errors(t);
            }
        });
    }
}
