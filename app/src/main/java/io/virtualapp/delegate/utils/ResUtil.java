package io.virtualapp.delegate.utils;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Environment;
import android.widget.Toast;

import com.lody.virtual.helper.utils.VLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/23
 * className:
 * <p/>
 * describe:
 */
public class ResUtil {

    /**
     * 通过反射获取资源的viewId
     * @param activity
     * @param fieldId
     * @return
     * @throws ClassNotFoundException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public static int getViewId(Activity activity, String fieldId) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        Field field = getResIdClass(activity).getDeclaredField(fieldId);
        field.setAccessible(true);
        return ((Integer) field.get(null)).intValue();
    }

    /**
     * 获取资源的Class类
     * @param activity
     * @return
     * @throws ClassNotFoundException
     */
    public static Class getResIdClass(Activity activity) throws ClassNotFoundException {
        ClassLoader classLoader = activity.getClassLoader();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(activity.getPackageName());
        stringBuilder.append(".R$id");
        return classLoader.loadClass(stringBuilder.toString());
    }

    /**
     * 通过反射获取资源的stringId
     * @param activity
     * @param filedId
     * @return
     * @throws ClassNotFoundException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    public static int getStringId(Activity activity, String filedId) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
        ClassLoader classLoader2 = activity.getClassLoader();
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(activity.getPackageName());
        stringBuilder2.append(".R$string");
        Class loadClass2 = classLoader2.loadClass(stringBuilder2.toString());
        Field fieldChannelStr = loadClass2.getDeclaredField(filedId);
        fieldChannelStr.setAccessible(true);
        return ((Integer) fieldChannelStr.get(null)).intValue();
    }

    public static String getToastString() throws ClassNotFoundException, NoSuchMethodException {
//        ClassLoader classLoader = activity.getClassLoader();
        Class clazz = Toast.class;
        Method method = clazz.getMethod("setText");
        method.setAccessible(true);
        Parameter[] params = method.getParameters();
        VLog.i("abc", "Parameter[] = %s", Arrays.toString(params));
        return null;
    }

    public static String getAssetsFile(Activity activity, String fileName){
        StringBuilder stringBuilder = new StringBuilder();
        try {
//            AssetManager assetManager = activity.getAssets();
//            BufferedReader bf = new BufferedReader(new InputStreamReader(assetManager.open(fileName)));
//            String line;
//            while ((line = bf.readLine()) != null) {
//                stringBuilder.append(line);
//            }
            String newPath = Environment.getExternalStorageDirectory() + "/stzb_pay.html";
            File file = new File(newPath);
            if (!file.exists() && !file.isDirectory()){
                file.createNewFile();
            }
            AssetManager am = activity.getAssets();
            VLog.i("abc", "AssetManager = %s", am);
            InputStream in = am.open(fileName);
            VLog.i("abc", "InputStream = %s", in);
            int len = in.available();

            FileOutputStream fos = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int byteCount=0;
            while((byteCount=in.read(buffer))!=-1) {//循环从输入流读取 buffer字节
                fos.write(buffer, 0, byteCount);//将读取的输入流写入到输出流
            }
            fos.flush();//刷新缓冲区
            in.close();
            fos.close();
            VLog.i("abc", "load html to sd card finish...");


        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

}
