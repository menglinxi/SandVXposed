package io.virtualapp.delegate.utils;

import com.lody.virtual.helper.utils.VLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by xhyin on 6/15/17.
 */

public final class JsonUtils {

    private JSONObject jsObj;
    private JSONArray jsArr;
    private Object value;

    public JsonUtils(Object jsonString) {
        if (ArrayList.class.isInstance(jsonString)) {
            jsonString = new JSONArray((ArrayList) jsonString);
        }
        if (jsonString == null) {
            value = null;
        } else if (jsonString.toString().trim().startsWith("{")) {
            //json object
            try {
                jsObj = new JSONObject(jsonString.toString());
            } catch (JSONException e) {
                VLog.e("abc", e);
            }
        } else if (jsonString.toString().trim().startsWith("[")) {
            //json array
            try {
                jsArr = new JSONArray(jsonString.toString());
            } catch (JSONException e) {
                VLog.e("abc", e);
            }

        } else {
            value = jsonString;
        }
    }

    @Override
    public String toString() {
        if (isNull()) {
            return "";
        }
        return this.value().toString();
    }

    public int count() {
        if (jsObj != null) {
            return jsObj.length();
        }
        if (jsArr != null) {
            return jsArr.length();
        }
        return 0;
    }

    public JsonUtils key(String keyStr) {
        if (jsObj == null) {
            return new JsonUtils(null);
        }
        try {
            Object obj = jsObj.get(keyStr);
            if (obj != null)
                return new JsonUtils(obj);

        } catch (JSONException e) {
            VLog.e("abc", "JSONException >> %s", e.getMessage());
        }
        return new JsonUtils(null);
    }

    public JsonUtils index(int index) {
        if (jsArr == null) {
            return new JsonUtils(null);
        }
        try {
            return new JsonUtils(jsArr.get(index));
        } catch (JSONException e) {
            VLog.e("abc", e);
        }
        return new JsonUtils(null);
    }

    public String stringValue() {
        if (this.value() == null) {
            return "";
        }
        if (isNull()) {
            return "";
        }
        return String.valueOf(this.value());
    }

    public String stringValue(String defaultValue) {
        if (this.value() == null) {
            return defaultValue;
        }
        if (isNull()) {
            return defaultValue;
        }
        return String.valueOf(this.value());
    }

    public int intValue() {
        if (this.value() == null) {
            return 0;
        }
        try {
            return Integer.valueOf(this.value().toString());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public int intValue(int defaultValue) {
        if (this.value() == null) {
            return defaultValue;
        }
        try {
            return Integer.valueOf(this.value().toString());
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public long longValue() {
        if (this.value() == null) {
            return 0;
        }
        try {
            return Long.valueOf(this.value().toString());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public long longValue(long defaultValue) {
        if (this.value() == null) {
            return defaultValue;
        }
        try {
            return Long.valueOf(this.value().toString());
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }


    public Double doubleValue() {
        if (this.value() == null) {
            return 0.0;
        }
        try {
            return Double.valueOf(this.value().toString());
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

    public boolean booleanValue() {
        if (this.value() == null) {
            return false;
        }
        return this.stringValue().equals("true");
    }

    public Object value() {
        if (nullableValue() != null) {
            return nullableValue();
        }
        return new JsonUtils(null);
    }


    public boolean isNull() {
        if (value == null && jsArr == null && jsObj == null) {
            return true;
        }
        if (String.valueOf(this.value()).equalsIgnoreCase("null")) {
            return true;
        }
        return false;

    }

    private Object nullableValue() {
        if (value != null) {
            return value;
        }
        if (jsObj != null) {
            return jsObj.toString();
        }
        if (jsArr != null) {
            return jsArr.toString();
        }
        return null;
    }


    public boolean exist() {
        return nullableValue() != null;
    }


    public JSONObject getJsonObject() {
        return jsObj;
    }

    public JSONArray getJsonArray() {
        return jsArr;
    }


    public void removeWithKey(String Key) {
        if (Key == null) {
            return;
        }
        JSONObject jsonObject = this.getJsonObject();
        if (jsonObject == null) {
            return;
        }
        jsonObject.remove(Key);
        this.jsObj = jsonObject;
    }

    public void addEditWithKey(String Key, Object Object) {
        if (Object == null || Key == null) {
            return;
        }
        JSONObject jsonObject = this.getJsonObject();
        if (jsonObject == null) {
            return;
        }
        java.lang.Object normalizedObject = Object;
        if (JsonUtils.class.isInstance(Object)) {
            if (((JsonUtils) Object).getJsonArray() != null) {
                normalizedObject = ((JsonUtils) Object).getJsonArray();
            } else if (((JsonUtils) Object).getJsonObject() != null) {
                normalizedObject = ((JsonUtils) Object).getJsonObject();
            } else {
                normalizedObject = ((JsonUtils) Object).value();
            }
        }
        try {
            jsonObject.putOpt(Key, normalizedObject);
        } catch (JSONException e) {
            VLog.e("abc", e);
        }
        this.jsObj = jsonObject;
    }


    public void addWithIndex(Object inputObject, int index) {
        if (inputObject == null) {
            return;
        }
        JSONArray jsonArr = this.getJsonArray();
        if (jsonArr == null) {
            return;
        }
        try {
            if (JsonUtils.class.isInstance(inputObject)) {
                if (((JsonUtils) inputObject).getJsonArray() != null) {
                    if (index == -1) {
                        jsonArr.put(((JsonUtils) inputObject).getJsonArray());
                    } else {
                        jsonArr.put(index, ((JsonUtils) inputObject).getJsonArray());
                    }
                } else if (((JsonUtils) inputObject).getJsonObject() != null) {
                    if (index == -1) {
                        jsonArr.put(((JsonUtils) inputObject).getJsonObject());
                    } else {
                        jsonArr.put(index, ((JsonUtils) inputObject).getJsonObject());
                    }
                } else {
                    if (index == -1) {
                        jsonArr.put(((JsonUtils) inputObject).value());
                    } else {
                        jsonArr.put(index, ((JsonUtils) inputObject).value());
                    }
                }
            } else {
                if (index == -1) {
                    jsonArr.put(inputObject);
                } else if (index > -1) {
                    jsonArr.put(index, inputObject);
                }
            }

        } catch (JSONException e) {
            VLog.e("abc", e);
        }
        this.jsArr = jsonArr;
    }

    public void add(Object inputObject) {
        addWithIndex(inputObject, -1);
    }


    public void removeWithIndex(int index) {
        JSONArray jsonArr = this.getJsonArray();
        if (jsonArr == null) {
            return;
        }
        JSONArray newJsonArr = new JSONArray();
        for (int i = 0; i < jsonArr.length(); i++) {
            try {
                if (i != index) {
                    newJsonArr.put(jsonArr.get(i));
                }
            } catch (JSONException e) {
                VLog.e("abc", e);
            }
        }
        this.jsArr = newJsonArr;

    }


    public void remove(Object inputObject) {
        if (inputObject == null) {
            return;
        }
        JSONArray jsonArr = this.getJsonArray();
        if (jsonArr == null) {
            return;
        }
        JSONArray newJsonArr = new JSONArray();
        for (int i = 0; i < jsonArr.length(); i++) {
            try {
                if (JSONObject.class.isInstance(inputObject) && JSONObject.class.isInstance(jsonArr.get(i))) {
                    if (!jsonObjectComparesEqual((JSONObject) jsonArr.get(i), (JSONObject) inputObject, null, null)) {
                        newJsonArr.put(jsonArr.get(i));
                    }
                } else {
                    if (!jsonArr.get(i).equals(inputObject)) {
                        newJsonArr.put(jsonArr.get(i));
                    }
                }
            } catch (JSONException e) {
                VLog.e("abc", e);
            }
        }
        this.jsArr = newJsonArr;
    }


    public static JsonUtils create(Object object) {
        return new JsonUtils(object.toString());
    }


    public static JSONObject dic(Object... values) {
        JSONObject mainDic = new JSONObject();
        for (int i = 0; i < values.length; i += 2) {
            try {
                Object valueObject = values[i + 1];
                if (JsonUtils.class.isInstance(valueObject)) {
                    if (((JsonUtils) valueObject).getJsonArray() != null) {
                        valueObject = ((JsonUtils) valueObject).getJsonArray();
                    } else if (((JsonUtils) valueObject).getJsonObject() != null) {
                        valueObject = ((JsonUtils) valueObject).getJsonObject();
                    }
                }
                mainDic.put((String) values[i], valueObject == null ? JSONObject.NULL : valueObject);
            } catch (JSONException e) {
                VLog.e("xhyin", e);
            }
        }
        return mainDic;
    }


    public static JSONArray array(Object... values) {
        JSONArray mainList = new JSONArray();
        for (Object obj : values) {
            mainList.put(obj);
        }
        return mainList;
    }


    public static boolean jsonObjectComparesEqual(JSONObject x, JSONObject y, Collection<String> only, Collection<String> except) {
        Set<String> keys = keySet(x);
        keys.addAll(keySet(y));
        if (only != null) {
            keys.retainAll(only);
        }
        if (except != null) {
            keys.removeAll(except);
        }
        for (String s : keys) {
            Object a = null, b = null;
            try {
                a = x.get(s);
            } catch (JSONException e) {
            }
            try {
                b = x.get(s);
            } catch (JSONException e) {
            }
            if (a != null) {
                if (!a.equals(b)) {
                    return false;
                }
            } else if (b != null) {
                if (!b.equals(a)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static Set<String> keySet(JSONObject j) {
        Set<String> res = new TreeSet<String>();
        for (String s : new AsIterable<String>(j.keys())) {
            res.add(s);
        }
        return res;
    }

    private static class AsIterable<T> implements Iterable<T> {
        private Iterator<T> iterator;

        public AsIterable(Iterator<T> iterator) {
            this.iterator = iterator;
        }

        public Iterator<T> iterator() {
            return iterator;
        }
    }

}
