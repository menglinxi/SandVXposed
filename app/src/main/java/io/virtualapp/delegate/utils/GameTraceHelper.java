package io.virtualapp.delegate.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.lody.virtual.helper.utils.VLog;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.virtualapp.VApp;
import io.virtualapp.VCommends;
import io.virtualapp.delegate.MyGameHookDelegate;
import io.virtualapp.delegate.db.OrderTraceHandle;
import io.virtualapp.delegate.models.TraceLoginModel;
import io.virtualapp.delegate.models.TracePayStateModel;
import io.virtualapp.entity.OrderTrace;
import io.virtualapp.home.callback.OkHttpCallback;
import io.virtualapp.home.callback.TraceCallback;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/6
 * className:
 * <p/>
 * describe:
 */
public class GameTraceHelper {

    private int index = -1;
    private int viewCount = -1;
    private HashMap<Integer, String> titleMap = new HashMap<>();


    private static GameTraceHelper instance = new GameTraceHelper();
    private static Activity activity;

    public static GameTraceHelper get() {
        return instance;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    /**
     * 自动登录
     *
     * @param version
     * @param gameUser
     * @param gamePwd
     */
    public void autoLogin(String version, String gameUser, String gamePwd) {
        ViewGroup viewGroup = (ViewGroup) ((ViewGroup) activity.getWindow().getDecorView()
                .findViewById(android.R.id.content)).getChildAt(0);

        switch (version) {
            case "1":
                autoLoginToGame1(viewGroup, gameUser, gamePwd, version);
                break;

            case "2":
                autoLoginToGame2(viewGroup, gameUser, gamePwd);
                break;

            case "3":
                autoLoginToGame1(viewGroup, gameUser, gamePwd, version);
                break;
        }

    }

    /**
     * 自动登录到游戏.
     * notice: 当获取不到网易游戏SDK版本号的时候，默认设为2.
     * 版本设为2时，使用此方法
     *
     * @param viewGroup
     */
    private void autoLoginToGame2(ViewGroup viewGroup, String game_user, String game_pwd) {
        VLog.e("abc", "rootView = %s", viewGroup.getClass().getName());
        try {
            Button button;
            //1.自动选择邮箱登录类型
            int viewId = ResUtil.getViewId(activity, "netease_mpay__login_channels");
            int stringId = ResUtil.getStringId(activity, "netease_mpay__login_channel_urs");
            if (viewGroup.findViewById(viewId) instanceof GridView) {
                GridView gridView = viewGroup.findViewById(viewId);
                if (gridView.getVisibility() == View.VISIBLE) {
                    int finalIntValue = stringId;
                    gridView.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
                        @Override
                        public void onChildViewAdded(View view, View view2) {
                            if (view2 instanceof ViewGroup) {
                                ViewGroup viewGroup = (ViewGroup) view2;
                                VLog.i("abc", "mainButton = %s", activity.getString(finalIntValue));
                                if (activity.getString(finalIntValue).equals(((TextView) viewGroup.getChildAt(1)).getText().toString())) {
                                    ((ViewGroup) viewGroup.getChildAt(0)).getChildAt(0).performClick();
                                }
                            }
                        }

                        @Override
                        public void onChildViewRemoved(View view, View view2) {

                        }
                    });
                }
            }

            //2.自动输入账号, 并进行下一步
            int etViewId = ResUtil.getViewId(activity, "netease_mpay__login_urs");
            int btnId = ResUtil.getViewId(activity, "netease_mpay__login_login");
            VLog.e("abc", "urs_view_Id = %s", Integer.valueOf(viewId));
            if ((viewGroup.findViewById(etViewId) instanceof EditText) && (viewGroup.findViewById(btnId) instanceof Button)) {
                EditText etUser = (EditText) viewGroup.findViewById(etViewId);
                VLog.e("abc", "auto complete textview = %s", etUser);
                if (etUser.getVisibility() == View.VISIBLE) {
                    VLog.e("abc", "auto complete textview = visible");
                    etUser.setText(game_user);
                    etUser.setFocusable(false);
                }
                button = (Button) viewGroup.findViewById(btnId);
                if (button.getVisibility() == View.VISIBLE) {
                    button.performClick();
                }
            }

            //3.自动输入密码, 并执行自动登录业务
            etViewId = ResUtil.getViewId(activity, "netease_mpay__login_password");
            btnId = ResUtil.getViewId(activity, "netease_mpay__login_login");
            VLog.e("abc", "pwdId = %s", Integer.valueOf(stringId));
            if ((viewGroup.findViewById(etViewId) instanceof EditText) && (viewGroup.findViewById(btnId) instanceof Button)) {
                EditText etPwd = (EditText) viewGroup.findViewById(etViewId);
                if (etPwd.getVisibility() == View.VISIBLE) {
                    etPwd.setText(new String(Base64.decode(game_pwd.getBytes(Charset.defaultCharset()), 0)));
                    etPwd.setFocusable(false);
                }
                button = (Button) viewGroup.findViewById(btnId);
                if (button.getVisibility() == View.VISIBLE) {
                    button.performClick();
                }
            }
        } catch (Throwable e) {
            VLog.e("abc", e);
        }
    }

    /**
     * 游戏自动登录
     *
     * @param viewGroup
     */
    private void autoLoginToGame1(ViewGroup viewGroup, String game_user, String game_pwd, String version) {
        try {
            TextView textView;
            //1.获取控件id
            int group2ChsId = ResUtil.getViewId(activity, "netease_mpay__login_other_channel_two");
            int firstChannel = ResUtil.getViewId(activity, "netease_mpay__login_other_channel_first");
            int groupMoreChsId = ResUtil.getViewId(activity, "netease_mpay__login_other_channels");
            int groupSecondChsId = ResUtil.getViewId(activity, "netease_mpay__login_other_channel_second");
//            int typeIconId = ResUtil.getViewId(activity, "netease_mpay__login_type_icon");
            int typeNameId = ResUtil.getViewId(activity, "netease_mpay__login_type_name");
            int userEmailId = ResUtil.getViewId(activity, "netease_mpay__urs_email");
            int userPwdId = ResUtil.getViewId(activity, "netease_mpay__input");
            final int loginBtnId = ResUtil.getViewId(activity, "netease_mpay__login");
            int otherTypeStringId = ResUtil.getViewId(activity, "netease_mpay__login_with_other");
            int mobileId = ResUtil.getViewId(activity, "netease_mpay__mobile_editor");
            int btnId = ResUtil.getViewId(activity, "netease_mpay__login_login");
            int btnLoginId = ResUtil.getViewId(activity, "netease_mpay__login");
            int tvOutTimeId = ResUtil.getViewId(activity, "netease_mpay__login_time_text");

            int childCount = viewGroup.getChildCount();
            VLog.i("abc", "rootview child count = %s", viewGroup.getChildCount());
            //为何要判断子view的数量？
            //因为在用户修改游戏密码后，盒子无法实现自动登录。在这种情况下，activity会加载3个容器存放不同情况的view
            if (childCount >= 3) {
                ViewGroup child1 = (ViewGroup) viewGroup.getChildAt(0);
                VLog.i("abc", "child1 count = %s", child1.getChildCount());
                Button btnLogin = child1.findViewById(btnLoginId);
                VLog.i("abc", "child1 btnLogin = %s", btnLogin);
                TextView tvOutTime = child1.findViewById(tvOutTimeId);
                if (tvOutTime != null) {
                    String outTimeText = tvOutTime.getText().toString();
                    VLog.i("abc", "outTimeText = %s", outTimeText);
                    if (outTimeText.contains("上次登录")) {
//                    btnLogin.setText("请更新密码");
//                    btnLogin.setBackgroundColor(Color.GRAY);
//                    btnLogin.setClickable(false);
//                    child1.getChildAt(1).setEnabled(false);
                        android.widget.Toast.makeText(activity, "请在宝盒更新邮箱的密码", android.widget.Toast.LENGTH_LONG).show();
                    }
                }
            }

            //EditText：手机号登录输入框
            EditText etMobile = viewGroup.findViewById(mobileId);
            VLog.i("abc", "etMobile = %s, mobileId = %s", etMobile, mobileId);

            if (etMobile != null) {
                etMobile.setFocusable(false);
            } else {
                //Button:下一步
                Button next = viewGroup.findViewById(btnId);
                VLog.i("abc", "next button = %s", next);
                if (next != null) {
                    next.setClickable(false);
                }
            }

            //TextView：其他登录方式
            TextView otherLoginView = viewGroup.findViewById(otherTypeStringId);
            if (otherLoginView != null) {
                otherLoginView.setClickable(false);
            }

            //2.通过获取id获取value/string目录下的字符串
            int channelStrId = ResUtil.getStringId(activity, "netease_mpay__login_channel_urs");

            ViewGroup fastGameViewGroup = viewGroup.findViewById(firstChannel);
            VLog.i("abc", "fastGameViewGroup = %s", fastGameViewGroup);
            if (fastGameViewGroup != null) {
                fastGameViewGroup.getChildAt(0).setEnabled(false);
            }

            ViewGroup viewGroup2 = null;
            try {
                viewGroup2 = (ViewGroup) viewGroup.findViewById(group2ChsId);
            } catch (Exception e) {
                viewGroup2 = (ViewGroup) viewGroup.findViewById(groupSecondChsId);
                VLog.v("abc", "current is second channel id = %s", groupSecondChsId);
            }

            //当游戏的登录页面为2种登录模式时，实现自动点击邮箱登录
            if (viewGroup2 != null && viewGroup2.getVisibility() == View.VISIBLE) {
                VLog.e("abc", "otherChannelTwoViewGroup count = %s", viewGroup2.getChildCount());

                viewGroup2 = (ViewGroup) viewGroup2.findViewById(groupSecondChsId);
                if (viewGroup2 != null && viewGroup2.getVisibility() == View.VISIBLE) {
                    textView = (TextView) viewGroup2.findViewById(typeNameId);
                    VLog.v("abc", "textView = %s", textView.getText().toString());
                    VLog.v("abc", "value/string = ", activity.getString(channelStrId));
                    if (textView.getVisibility() == View.VISIBLE && activity.getString(channelStrId).equals(textView.getText().toString())) {
                        VLog.e("abc", "channelTextView = %s >> channelViewGroup = %s >> channelTextView = %s", textView.getText().toString(), Boolean.valueOf(viewGroup2.isClickable()), Boolean.valueOf(textView.isClickable()));
                        ViewGroup finalViewGroup = viewGroup2;
                        new Handler(activity.getMainLooper()).postDelayed(
                                new Runnable() {
                                    public void run() {
                                        finalViewGroup.performClick();
                                    }
                                }, 1000);
                    }
                }
            }

            //当游戏的登录页面为多种登录模式时，实现自动点击邮箱登录
            ViewGroup viewGroupMore;
            try {
                viewGroupMore = (ViewGroup) viewGroup.findViewById(groupMoreChsId);
            } catch (Exception e) {
                viewGroupMore = (ViewGroup) viewGroup.findViewById(groupSecondChsId);
                VLog.v("abc", "current is second channel id = %s", groupSecondChsId);
            }
            if (viewGroupMore != null && viewGroupMore.getVisibility() == View.VISIBLE) {
                List arrayList = new ArrayList(viewGroupMore.getChildCount());
                VLog.i("abc", "Channel ViewGroups count = %s", viewGroupMore.getChildCount());
                addViews(arrayList, viewGroupMore, TextView.class);
                VLog.i("abc", "size = %s", arrayList.size());
                for (int i = 0; i < arrayList.size(); i++) {
                    textView = (TextView) arrayList.get(i);
                    VLog.i("abc", "channel text (textview) = %s", textView.getText().toString());
                    VLog.i("abc", "channel text (value/string) = %s", activity.getString(channelStrId));
                    if (textView.getVisibility() == View.VISIBLE && activity.getString(channelStrId).equals(textView.getText().toString())) {
                        VLog.i("abc", "i = %s", i);
                        ViewGroup finalViewGroupMore = viewGroupMore;
                        ViewGroup childViewGroup = (ViewGroup) viewGroupMore.getChildAt(i);
                        new Handler(activity.getMainLooper()).postDelayed(
                                new Runnable() {
                                    public void run() {
                                        VLog.i("abc", "do click for login");
                                        if (version.equals("3") && arrayList.size() == 1) {
                                            finalViewGroupMore.performClick();  //为了适配《大话西游》
                                        } else {
                                            childViewGroup.performClick();
                                        }
                                    }
                                }, 1000);
                        break;
                    }
                }
            }
            AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) viewGroup.findViewById(userEmailId);
            if (autoCompleteTextView != null && autoCompleteTextView.getVisibility() == View.VISIBLE) {
                autoCompleteTextView.setText(game_user);
                autoCompleteTextView.setFocusable(false);
                EditText editText = (EditText) viewGroup.findViewById(userPwdId);
                editText.setText(new String(Base64.decode(game_pwd.getBytes(Charset.defaultCharset()), Base64.DEFAULT)));
                editText.setFocusable(false);
                final ViewGroup viewGroup3 = viewGroup;
                new Handler(activity.getMainLooper()).postDelayed(
                        new Runnable() {
                            final MyGameHookDelegate delegate = null;

                            public void run() {
                                viewGroup3.findViewById(loginBtnId).performClick();
                            }
                        }, 1000);
            }
        } catch (Throwable e) {
            VLog.e("abc", e);
        }
    }

    /**
     * 遍历所有控件
     * 将所有View添加到集合中
     *
     * @param list
     * @param viewGroup
     * @param cls
     */
    public void addViews(List<View> list, ViewGroup viewGroup, Class cls) {
        if (viewGroup != null) {
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(i);
                if (isInstance(childAt, cls)) {
                    list.add(childAt);
                } else if (childAt instanceof ViewGroup) {
                    addViews((List) list, (ViewGroup) childAt, cls);
                }
            }
        }
        VLog.e("abc", "add views to list");
    }

    /**
     * 判断类是否相同
     *
     * @param obj
     * @param cls
     * @return
     */
    private boolean isInstance(Object obj, Class cls) {
        try {
            return cls.isInstance(obj);
        } catch (Throwable e) {
            VLog.e("abc", e);
            return false;
        }
    }

    /**
     * 隐藏切换账号和修改密码Button
     */
    public void hideAccountAndPwdButton() {

        ViewGroup rootView = (ViewGroup) ((ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content)).getChildAt(0);
        VLog.i("abc", "root view = %s", rootView);

        // 1.隐藏部分控件
        try {
            //隐藏<切换账号>
            int btnId = ResUtil.getViewId(activity, "netease_mpay__switch_account");
            Button button = (Button) rootView.findViewById(btnId);
            if (button != null) {
                button.setVisibility(View.GONE);
            }

            //帐号中间部分字符改为'***'
            int userTextId = ResUtil.getViewId(activity, "netease_mpay__login_center_username");
            TextView tvUserName = rootView.findViewById(userTextId);
            if (tvUserName != null) {
                shieldPartContent(tvUserName);
            }

            int activity_content_id = ResUtil.getViewId(activity, "netease_mpay__activity_content");
            ViewGroup flViewGroup = rootView.findViewById(activity_content_id);
            VLog.i("abc", "flViewGroup = %s", flViewGroup);

            View view = flViewGroup.getChildAt(0);
            if (view instanceof WebView) {
                VLog.i("abc", "tvAccWebViewount = %s", view);
                WebView webView = (WebView) view;
                webView.getSettings().setJavaScriptEnabled(true);
                webView.addJavascriptInterface(new InJavaScriptLocalObj(), "java_obj");
                Class clsWebView = activity.getClassLoader().loadClass("android.webkit.WebView");
                Method method = clsWebView.getMethod("setWebViewClient", WebViewClient.class);
                method.invoke(webView, new Object[]{new HookWebViewClient()}); // 见下面的 HookWebViewClient 代码说明
                VLog.i("abc", "init WebViewClient");
            }

            //隐藏<账号充值><修改密码><账号管理>
            int gridViewId = ResUtil.getViewId(activity, "netease_mpay__entry_selector_options");
            final GridView gridView = (GridView) rootView.findViewById(gridViewId);
            if (gridView != null) {
                final int tvTitleId = ResUtil.getViewId(activity, "netease_mpay__channel_option_title");
                int resetPwdId = ResUtil.getStringId(activity, "netease_mpay__change_password");
                final Handler handler = new Handler(activity.getMainLooper()) {
                    public void dispatchMessage(Message message) {
                        VLog.e("abc", "message = %s", Integer.valueOf(message.what));
                        viewCount = -1;
                        index = -1;
                        ((BaseAdapter) gridView.getAdapter()).notifyDataSetChanged();
                    }
                };
                gridView.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {

                    @Override
                    public void onChildViewAdded(View parent, View child) {
                        if (child instanceof ViewGroup) {
                            index = index + 1;
                            String title = ((TextView) ((ViewGroup) child).findViewById(tvTitleId)).getText().toString();
                            titleMap.put(index, title);
                            VLog.i("abc", "title = %s , index = %s", title, index);

                            ArrayList list = getGridViewList((GridView) parent);

                            try {
                                if (gridView.getChildCount() == list.size()) {
                                    for (int i = 0; i < gridView.getChildCount(); i++) {
                                        if (titleMap.get(i).equals("帐号充值")) {
                                            VLog.e("abc", "current item is  = %s, index = %s", titleMap.get(i), i);
                                            list.remove(i);
                                        }
                                        if (titleMap.get(i).equals("帐号管理")) {
                                            VLog.e("abc", "current item is  = %s, index = %s", titleMap.get(i), i);
                                            list.remove(i - 1);
                                        }
                                        if (titleMap.get(i).equals("修改密码")) {
                                            VLog.e("abc", "current item is  = %s, index = %s", titleMap.get(i), i);
                                            list.remove(i - 2);
                                        }
                                    }
                                    VLog.e("abc", "view count = %s", list.size());
                                    handler.sendEmptyMessage(1);
//                                    ((BaseAdapter) gridView.getAdapter()).notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                VLog.e("abc", "remove exception = %s", e.toString());
                            }
                        }
                    }

                    @Override
                    public void onChildViewRemoved(View parent, View child) {
                        VLog.e("abc", "remove view");
                    }
                });

            }
        } catch (Throwable e2) {
            VLog.e("abc", e2);
        }
    }

    /**
     * 屏蔽账号部分字段
     *
     * @param textView
     */
    private void shieldPartContent(TextView textView) {
        String userName = textView.getText().toString();
        if (!userName.equals("")) {
            String shield = StringUtils.shieldContent(userName);
            VLog.e("abc", "shield = %s", shield);
            String newContent = StringUtils.escape(userName, shield);
            textView.setText(newContent);
        }
    }

    /**
     * 获取GridView的所有子View
     *
     * @param gridView
     */
    private ArrayList getGridViewList(GridView gridView) {
        try {
            for (Field field : gridView.getAdapter().getClass().getDeclaredFields()) {
                if (field.getType().getName().equals("java.util.ArrayList")) {
                    field.setAccessible(true);
                    ArrayList arrayList = (ArrayList) field.get(gridView.getAdapter());
                    return arrayList;
                }
            }
        } catch (Exception e) {
            VLog.e("abc", "remove view exception == %s", e.toString());
        }
        return null;
    }

    /**
     * 追踪上报登录状态
     *
     * @param json
     */
    public void traceLoginInfo(String json) {

//        VLog.i("abc", "upload login state");
        TraceLoginModel.get().traceLoginState(json, new OkHttpCallback<String>() {
            @Override
            public void response(String string) {
                if (!TextUtils.isEmpty(string)
                        && JsonValidator.validate(string)
                        && new JsonUtils(string).key("operationState").stringValue("FALSE").equalsIgnoreCase("SUCCESS")) {
                    VLog.e("abc", "LOGIN SUCCESS", new Object[0]);
                }
            }

            @Override
            public void errors(Throwable t) {
                VLog.e("abc", "LOGIN FAILED", new Object[0]);
                VLog.e("abc", t.getMessage());
            }
        });
    }

    /**
     * 追踪上报充值状态
     *
     * @param json
     */
    public void traceUserPayState(String order_id, String json, TraceCallback callback) {
        VLog.e("abc", "upload pay info json = %s", json);
        TracePayStateModel.get().traceUserPayState(json, new OkHttpCallback<String>() {
            @Override
            public void response(String string) {
                VLog.e("abc", "From XKLM Remote server response = %s", string);
                if (TextUtils.isEmpty(string) ||! JsonValidator.validate(string)) {
                    VLog.i("abc", "response error");
                    return;
                }

                JsonUtils jsonUtils = new JsonUtils(string);
                OrderTrace order = OrderTraceHandle.get().queryKey(order_id);
                String rt = jsonUtils.key("operationState").stringValue("FALSE");
                //提交成功，删除记录
                if (!rt.equalsIgnoreCase("SUCCESS")) {

                    VLog.i("abc", "FAIL");
                    order.setSendStatus(-1);//发送失败
                    OrderTraceHandle.get().update(order);
                    return;
                }

                order.setSendStatus(1);//发送成功
                OrderTraceHandle.get().update(order);
                VLog.i("abc", "SUCCESS");
                String str = jsonUtils.key("data").key("sysorderid").stringValue();
                if (TextUtils.isEmpty(str)) return;

                OrderTraceHandle.get().delete(order);
                VApp.getApp().removeToSharedPreferences(VCommends.SP_VBOX_RECHARGE_RECORD, str);

                if (OrderTraceHandle.get().queryKey(str) == null) {
                    VLog.i("abc", "text = %s", "ready finish");
                    callback.resp();
                }
            }

            @Override
            public void errors(Throwable t) {
                VLog.e("abc", "TRACE PAY STATE FAILED", new Object[0]);
                VLog.e("abc", t.getMessage());
                OrderTrace order = OrderTraceHandle.get().queryKey(order_id);
                order.setSendStatus(-1);
                OrderTraceHandle.get().update(order);
            }
        });
    }


    public class HookWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            VLog.i("abc", "load url = %s", url);
//            view.loadUrl("javascript:(function(){" + StartJSCode + "})();"); // 页面打开时想做的事情
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            // 通过内部类定义的方法获取html页面加载的内容，这个需要添加在webview加载完成后的回调中
            VLog.i("abc", "onPageFinished load url = %s", url);
            if (url.contains("asset")) {
                ResUtil.getAssetsFile(activity, url);
            }
            // 获取页面内容
            view.loadUrl("javascript:window.java_obj.showSource(document.getElementsByTagName('html')[0].innerHTML);");
//            view.loadUrl("javascript:window.java_obj.showSource('<head>'+"
//                    + "document.getElementsByTagName('html')[0].innerHTML+'</head>');");
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

            VLog.i("abc", "header json = %s", url);
//            String json = GsonUtil.getInstance().getGson().toJson(map);

//            WebResourceResponse response = new WebResourceResponse("text/", "UTF-8", localCopy);
            return super.shouldInterceptRequest(view, url);
        }
    }

    public class InJavaScriptLocalObj {
        @JavascriptInterface
        public void showSource(String html) {
            VLog.i("abc", "====>html=", html);

//            System.out.println("====>html=" + html);
        }

        @JavascriptInterface
        public void showDescription(String str) {
            VLog.i("abc", "====>html=");

//            System.out.println("====>html=" + str);
        }
    }


}
