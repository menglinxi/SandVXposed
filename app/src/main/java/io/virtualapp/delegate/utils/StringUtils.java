package io.virtualapp.delegate.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.lody.virtual.helper.utils.VLog;

import java.io.InputStream;
import java.util.Arrays;

/**
 * Created by xhyin on 2017/4/26.
 */

public final class StringUtils {

    private static final String END_SUBFFIX = "@163.com";

    public static String toStringByStream(InputStream input) {
        if (input == null) {
            return null;
        }
        byte[] buf = new byte[1024];
        int bytesRead = 0;
        StringBuffer sb = new StringBuffer();
        try {
            while ((bytesRead = input.read(buf)) > 0) {
                sb.append(new String(buf, 0, bytesRead));
            }
        } catch (Exception e) {
            return null;
        }
        return sb.toString();
    }

    public static String getAppName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            int labelRes = packageInfo.applicationInfo.labelRes;
            return context.getResources().getString(labelRes);
        } catch (Exception e) {
            VLog.e("abc",e);
        }
        return null;
    }

    public static String shieldContent(String resString){
        return resString.substring(3, resString.lastIndexOf("@")-3);
    }


    /**
     * 编码
     * @param expression 要编码的原始字符串
     * @param find 要编码的字符串片段，该参数为正则表达式，注意转义
     * @return 如果出现异常，返回空字符串
     */
    public static String escape(String expression,String find){

        StringBuilder newExpression = new StringBuilder();
        String[] expressionArray;
        try {
            //按照指定字符串分割原始字符串
            expressionArray = expression.split(find);
            VLog.i("abc", "Expression Array = %s", Arrays.toString(expressionArray));
            newExpression.append(expressionArray[0]);
            newExpression.append("******");
            newExpression.append(expressionArray[1]);
        } catch (Exception e) {
            VLog.e("abc", "split str exception..");
        }
        return newExpression.toString();
    }

}
