package io.virtualapp.delegate.utils;

import android.text.TextUtils;

import com.lody.virtual.helper.utils.VLog;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by xhyin on 2017/4/26.
 */

public final class HttpUtils {

    private static final String TAG = HttpUtils.class.getSimpleName();

    private static final int TIMEOUT_IN_MILLIONS = 6000;

    public interface CallBack {
        void onRequestComplete(String result);
    }

    public synchronized static void sendGetRequestAsync(final String urlStr) {
        sendGetRequestAsync(urlStr, null);
    }

    public synchronized static void sendGetRequestAsync(final String urlStr, final CallBack callBack) {
        new Thread() {
            public void run() {
                try {
                    String result = sendGetRequest(urlStr);
                    if (callBack != null) {
                        callBack.onRequestComplete(result);
                    }
                } catch (Exception e) {
                    VLog.e("abc", e);
                }

            }
        }.start();
    }

    public synchronized static void sendPostRequestAsync(final String urlStr, final String postData) {
        sendPostRequestAsync(urlStr, postData, null);
    }

    public synchronized static void sendPostRequestAsync(final String urlStr, final String postData, final CallBack callBack) {
        new Thread() {
            public void run() {
                try {
                    String result = sendPostRequest(urlStr, postData);
                    if (callBack != null) {
                        callBack.onRequestComplete(result);
                    }
                } catch (Exception e) {
                    VLog.e("abc", e);
                }
            }
        }.start();
    }

    /**
     * 通过url获取数据
     *
     * @param urlStr
     * @return 返回数据
     */
    public synchronized static String sendGetRequest(String urlStr) {
        String result = null;
        HttpURLConnection connection = null;
        try {
            VLog.e(TAG, urlStr);
            URL url = new URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();
            // 设置链接时间
            connection.setConnectTimeout(TIMEOUT_IN_MILLIONS);
            // 设置请求方法，默认是GET
            connection.setRequestMethod("GET");
            // 设置字符集
            connection.setRequestProperty("Charset", "UTF-8");
            // 设置文件类型
            connection.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
            // 设置接收文件类型
            connection.setRequestProperty("Accept", "application/json");
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                result = StringUtils.toStringByStream(connection.getInputStream());
            }
        } catch (Exception e) {
            VLog.e("abc", e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    /**
     * 通过url获取数据
     *
     * @param urlStr
     * @return 返回数据
     */
    public synchronized static String sendPostRequest(String urlStr, String postData) {
        String result = null;
        HttpURLConnection connection = null;
        OutputStream outWriteStream = null;
        try {
            VLog.e(TAG, urlStr);
            URL url = new URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();
            // 设置链接时间
            connection.setConnectTimeout(TIMEOUT_IN_MILLIONS);
            // 设置请求方法，默认是POST
            connection.setRequestMethod("POST");
            // 支持输出流
            connection.setDoOutput(true);
            // 支持输入流
            connection.setDoInput(true);
            // 不使用缓存
            connection.setUseCaches(false);
            // 保持连接
            connection.setRequestProperty("Connection", "Keep-Alive");
            // 设置字符集
            connection.setRequestProperty("Charset", "UTF-8");
            // 设置文件类型
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            // 设置接收文件类型
            connection.setRequestProperty("Accept", "application/json");
            // 往服务器里面发送数据
            if (!TextUtils.isEmpty(postData)) {
                byte[] writeBytes = postData.getBytes();
                // 设置文件长度
                connection.setRequestProperty("Content-Length", String.valueOf(writeBytes.length));
                outWriteStream = connection.getOutputStream();
                outWriteStream.write(writeBytes);
                outWriteStream.flush();
            }
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                result = StringUtils.toStringByStream(connection.getInputStream());
            }
        } catch (Exception e) {
            VLog.e("abc", e);
        } finally {
            if (outWriteStream != null) {
                try {
                    outWriteStream.close();
                } catch (IOException e) {
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    public static void parseParameters(Map map, String data, String encoding)
            throws UnsupportedEncodingException {
        if ((data == null) || (data.length() <= 0)) {
            return;
        }
        byte[] bytes = null;
        try {
            if (encoding == null)
                bytes = data.getBytes();
            else
                bytes = data.getBytes(encoding);
        } catch (UnsupportedEncodingException uee) {
        }
        parseParameters(map, bytes, encoding);
    }

    public static String urlDecode(String url) {
        if (url != null && !"".equals(url)) {
            url = URLDecoder.decode(url);
        }
        return url;
    }

    public static String getParameterUrl(String url) {
        String parametersStr = null;
        if (url != null && !"".equals(url)) {
            int index = url.indexOf("?") + 1;
            parametersStr = url.substring(index);
//            VLog.e("abc", "index = %s >> parametersStr = %s", index, parametersStr);
        }
        return parametersStr;
    }

    public static void parseParameters(Map map, byte[] data, String encoding) throws UnsupportedEncodingException {
        if ((data != null) && (data.length > 0)) {
            int ix = 0;
            int ox = 0;
            String key = null;
            String value = null;
            while (ix < data.length) {
                byte c = data[(ix++)];
                switch ((char) c) {
                    case '&':
                        value = new String(data, 0, ox, encoding);
                        if (key != null) {
                            putMapEntry(map, key, value);
                            key = null;
                        }
                        ox = 0;
                        break;
                    case '=':
                        if (key == null) {
                            key = new String(data, 0, ox, encoding);
                            ox = 0;
                        } else {
                            data[(ox++)] = c;
                        }
                        break;
                    case '+':
                        data[(ox++)] = 32;
                        break;
                    case '%':
                        data[(ox++)] = (byte) ((convertHexDigit(data[(ix++)]) << 4) + convertHexDigit(data[(ix++)]));
                        break;
                    default:
                        data[(ox++)] = c;
                }
            }

            if (key != null) {
                value = new String(data, 0, ox, encoding);
                putMapEntry(map, key, value);
            }
        }
    }

    private static void putMapEntry(Map map, String name, String value) {
        String[] newValues = null;
        String[] oldValues = (String[]) map.get(name);
        if (oldValues == null) {
            newValues = new String[1];
            newValues[0] = value;
        } else {
            newValues = new String[oldValues.length + 1];
            System.arraycopy(oldValues, 0, newValues, 0, oldValues.length);
            newValues[oldValues.length] = value;
        }
        map.put(name, newValues);
    }

    private static byte convertHexDigit(byte b) {
        if ((b >= 48) && (b <= 57)) return (byte) (b - 48);
        if ((b >= 97) && (b <= 102)) return (byte) (b - 97 + 10);
        if ((b >= 65) && (b <= 70)) return (byte) (b - 65 + 10);
        return 0;
    }

    public static String replaceAccessToken(String url, Map<String, String> parametersMap) {
        String urlStr = url;
        if (parametersMap != null) {
            Set<String> parametersKey = parametersMap.keySet();
            for (String parameterKey : parametersKey) {
                urlStr = replaceAccessToken(urlStr, parameterKey, parametersMap.get(parameterKey));
            }
        }
        return urlStr;
    }

    public static String replaceAccessToken(String url, String name, String accessToken) {
        if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(accessToken)) {
            int index = url.indexOf(name + "=");
            if (index != -1) {
                StringBuilder sb = new StringBuilder();
                sb.append(url.substring(0, index)).append(name + "=")
                        .append(accessToken);
                int idx = url.indexOf("&", index);
                if (idx != -1) {
                    sb.append(url.substring(idx));
                }
                url = sb.toString();
            }
        }
        return url;
    }

    public static Map<String, String> getParameterMap(String param) {
        Map<String, String> map = new HashMap<>();
        if (TextUtils.isEmpty(param)) {
            return map;
        }
        String[] params = param.split("&");
        for (int i = 0; i < params.length; i++) {
            String[] p = params[i].split("=");
            if (p.length == 2) {
                map.put(p[0], String.valueOf(p[1]));
            }
        }
        return map;
    }

}
