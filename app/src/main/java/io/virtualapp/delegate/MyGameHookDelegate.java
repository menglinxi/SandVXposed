package io.virtualapp.delegate;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Message;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.lody.virtual.client.VClientImpl;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.hook.delegate.ComponentDelegate;
import com.lody.virtual.client.ipc.VDeviceManager;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.remote.VDeviceInfo;
import com.strkont.XProxyHelper;
import com.strkont.proxy.ProxyCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import io.virtualapp.VApp;
import io.virtualapp.VCommends;
import io.virtualapp.delegate.db.OrderTraceHandle;
import io.virtualapp.delegate.utils.GameTraceHelper;
import io.virtualapp.delegate.utils.HttpUtils;
import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.delegate.utils.JsonValidator;
import io.virtualapp.delegate.utils.StringUtils;
import io.virtualapp.entity.OrderTrace;
import io.virtualapp.entity.PayBean;
import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.TraceCallback;
import io.virtualapp.tools.AESUtil;
import io.virtualapp.tools.GsonUtil;
import io.virtualapp.tools.MD5Utils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/10/15
 * className:
 * <p/>
 * describe: 网易游戏插件
 */
public class MyGameHookDelegate implements ComponentDelegate {

    public static final String NETEASE_GAMES_URL = "https://service.mkey.163.com/mpay/games/";
    private String mSendingOrder;
    private String order_id;
    private String game_id;
    private String price;
    private String pay_status = "-1";
    private String pay_method;
    private String app_channel;
    private String channel_id;
    private String id;
    private String token;
    private String cv;
    private String game_user;
    private String game_pwd;
    private String user_id;
    private String version;
    private String device_id;

    private static String TAG = MyGameHookDelegate.class.getSimpleName();
    private Application application;
    private Intent service;
    private boolean isRecord;
    private List<String> unCheckOrders = new ArrayList<>();
    public int bbc;



    public void setBbc(int bbc1){
        bbc =bbc1;
        VLog.e("abcd","bbc ="+bbc);
    }
    /**
     * Log 显示设备信息
     *
     * @param context
     */
    private void logDeviceInfo(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            VLog.e("abc", "imei = %s", telephonyManager.getDeviceId());
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            VLog.i("abc", "### print system device info. ### \n" +
                            ">> buildManufacturer = %s \n" +
                            ">> buildModel = %s \n" +
                            ">> buildSerial = %s \n" +
                            ">> buildSerial8 = %s \n" +
                            ">> buildDevice = %s \n" +
                            ">> buildVersionRelease = %s \n" +
                            ">> buildVersionSdk = %s \n" +
                            ">> buildVersionSdkInt = %s \n" +
                            ">> androidId0 =%s \n>> androidId1 =%s \n" +
                            ">> deviceId =%s \n" +
                            ">> line1Number = %s \n" +
                            ">> simNo = %s \n" +
                            ">> iccid = %s \n" +
                            ">> networkType = %s \n" +
                            ">> ssid = %s \n>> bssid = %s \n" +
                            ">> macaddress = %s",
                    Build.MANUFACTURER,
                    Build.MODEL,
                    Build.SERIAL,
                    Build.VERSION.SDK_INT >= 26 ? Build.getSerial() : Build.SERIAL,
                    Build.DEVICE,
                    Build.VERSION.RELEASE,
                    Build.VERSION.SDK,
                    Build.VERSION.SDK_INT,
                    Settings.Secure.getString(context.getContentResolver(), "android_id"),
                    Settings.System.getString(context.getContentResolver(), "android_id"),
                    telephonyManager.getDeviceId(),
                    telephonyManager.getLine1Number(),
                    telephonyManager.getSubscriberId(),
                    telephonyManager.getSimSerialNumber(),
                    connectivityManager.getActiveNetworkInfo().getType(),
                    wifiManager.getConnectionInfo().getSSID(),
                    wifiManager.getConnectionInfo().getBSSID(),
                    wifiManager.getConnectionInfo().getMacAddress());

        } catch (Throwable e) {
            VLog.e("abc", e);
        }
    }

    /**
     * 从SharedPreference删除字段
     */
    private void removeInfo(String key) {
        if (!TextUtils.isEmpty(key)) {
            VApp.getApp().removeToSharedPreferences(VCommends.SP_VBOX_RECHARGE_RECORD, key);
        }
    }

    /**
     * 从SharedPreference添加字段
     */
    private void addInfo(String key, String value) {
        if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(value)) {
            VApp.getApp().putStringToSharedPreferences(VCommends.SP_VBOX_RECHARGE_RECORD, key, value);
        }
    }


    /**
     * 通过反射hook网易SDK获得的设备ID
     *
     * @param context
     * @return
     */
    private String hookNeteaseDeviceId(Context context) {
        try {
            Method declaredMethod = context.getClassLoader().loadClass("com.netease.ntunisdk.base.SdkMgr")
                    .getDeclaredMethod("getInst", new Class[0]);
            declaredMethod.setAccessible(true);
            Object invoke = declaredMethod.invoke(null, new Object[0]);
            Method declaredMethod2 = context.getClassLoader().loadClass("com.netease.ntunisdk.base.SdkBase")
                    .getDeclaredMethod("getPropStr", new Class[]{String.class});
            declaredMethod2.setAccessible(true);
            String str = (String) declaredMethod2.invoke(invoke, new Object[]{"DEVICE_ID"});
            Object[] objArr = new Object[2];
            objArr[0] = str;
            objArr[1] = (String) declaredMethod2.invoke(invoke, new Object[]{"UDID"});
            VLog.i("abc", "deviceId = %s --> udid = %s", objArr);
            return str;
        } catch (Throwable e) {
            VLog.e("abc", e);
            return null;
        }
    }

    @Override
    public void bindApplicationNoCheck() {
        MyGameHookDelegate delegate = this;
        VLog.e("abc", "MyGameHookDelegate bindApplicationNoCheck()..");
        String info = VirtualCore.get().getContext().getSharedPreferences("postResult", 0).getString("info", null);
        if (!TextUtils.isEmpty(info) && JsonValidator.validate(info)) {
            VLog.e("abc", "userInfo = %s", info);
            JsonUtils jsonUtils = new JsonUtils(info);
            if (!"FALSE".equals(jsonUtils.key("operationState").stringValue("FALSE"))) {
                //真实的设备信息
                SharedPreferences preferences = VApp.getApp().getPreferences(VCommends.SP_VBOX_DEVICE_INFO);
                String v_android_id = preferences.getString("V_ANDROID_ID", "");
                String v_device_id = preferences.getString("V_DEVICE_ID", null);
                String v_wifimac = preferences.getString("V_MAC_ADDRESS", "");
                String v_serial = preferences.getString("V_BUILD_SERIAL", "");
                String v_iccid = preferences.getString("V_ICC_ID", "");
                String v_manufacturer = preferences.getString("V_BUILD_MANUFACTURER", "");
                String v_buildmodel = preferences.getString("V_BUILD_MODEL", "");
                VLog.e("abc", "--> realAndroidId = %s \n" +
                                "--> realDeviceId = %s \n" +
                                "--> realManufacturer = %s \n" +
                                "--> realModel = %s \n" +
                                "--> realSerial =%s \n" +
                                "--> realMac = %s \n" +
                                "--> realIccid = %s \n",
                        v_android_id, v_device_id, v_manufacturer, v_buildmodel, v_serial, v_wifimac, v_iccid);

                JsonUtils jsonUtils2 = new JsonUtils(AESUtil.decrypt(jsonUtils.key("data").key("login").stringValue(),
                        new StringBuffer(AESUtil.genKey(v_android_id, v_device_id)).reverse().toString()));
                delegate.game_user = jsonUtils2.key("gameuser").stringValue("");
                delegate.game_pwd = Base64.encodeToString(jsonUtils2.key("gamepwd").stringValue("")
                        .getBytes(Charset.defaultCharset()), 0);
                delegate.channel_id = jsonUtils2.key("channelid").stringValue();
                delegate.user_id = jsonUtils2.key("userid").stringValue("");
                VLog.e("abc", "userName = %s >> password = %s >> userId = %s >> channelId = %s",
                        delegate.game_user, delegate.game_pwd, delegate.user_id, delegate.channel_id);

                //虚拟的设备信息(从服务器获取)
                jsonUtils = jsonUtils2.key("vd");
                String x_androidid = jsonUtils.key("androidId").stringValue("");
                String x_deviceid = jsonUtils.key("deviceId").stringValue("");
                String x_deviceid2 = jsonUtils.key("deviceId2").stringValue("");
                String x_serial = jsonUtils.key("buildSerial").stringValue("");
                String x_meid = jsonUtils.key("meid").stringValue("");
                String x_line1number = jsonUtils.key("line1Number").stringValue("");
                String x_imsi = jsonUtils.key("imsi").stringValue("");
                String x_iccid = jsonUtils.key("iccid").stringValue("");
                String x_ssid = jsonUtils.key("ssid").stringValue("");
                String x_bssid = jsonUtils.key("bssid").stringValue("");
                String x_macaddress = jsonUtils.key("macaddress").stringValue("");
                String x_buildmanufacturer = jsonUtils.key("buildManufacturer").stringValue("");
                String x_buildmodel = jsonUtils.key("buildModel").stringValue("");
                VLog.e("abc", "steadAndroidId = %s \n" +
                                "--> steadDeviceId = %s \n" +
                                "--> steadManufacturer = %s \n" +
                                "--> steadModel = %s \n" +
                                "--> steadSerial = %s \n" +
                                "--> steadMac = %s \n" +
                                "--> steadIccid = %s \n" +
                                "--> steadLine1Number = %s",
                        x_androidid, x_deviceid, x_buildmanufacturer, x_buildmodel, x_serial, x_macaddress, x_iccid, x_line1number);

                VDeviceInfo deviceInfo = VClientImpl.get().getDeviceInfo();

                //判断是否hook掉当前的设备信息，如果没有，则替换
                if (!v_manufacturer.equalsIgnoreCase(x_buildmanufacturer) && !v_buildmodel.equalsIgnoreCase(x_buildmodel)) {

                    if (!TextUtils.isEmpty(x_buildmanufacturer)) {
                        deviceInfo.manufacturer = x_buildmanufacturer;
                    }
                    if (!TextUtils.isEmpty(x_buildmodel)) {
                        deviceInfo.model = x_buildmodel;
                    }
                    if (!TextUtils.isEmpty(x_serial)) {
                        deviceInfo.serial = x_serial;
                    }
                    if (!TextUtils.isEmpty(x_androidid)) {
                        deviceInfo.androidId = x_androidid;
                    }
                    if (!TextUtils.isEmpty(x_deviceid)) {
                        deviceInfo.deviceId = x_deviceid;
                        deviceInfo.imei1 = x_deviceid;
                    }
                    if (!TextUtils.isEmpty(x_deviceid2)) {
                        deviceInfo.imei2 = x_deviceid2;
                    }
                    if (!TextUtils.isEmpty(x_meid)) {
                        deviceInfo.meid = x_meid;
                    }
                    if (!TextUtils.isEmpty(x_line1number)) {
                        deviceInfo.line1Number = x_line1number;
                    }
                    if (!TextUtils.isEmpty(x_imsi)) {
                        deviceInfo.imsi = x_imsi;
                    }
                    if (!TextUtils.isEmpty(x_iccid)) {
                        deviceInfo.iccId = x_iccid;
                    }
                    if (!TextUtils.isEmpty(x_ssid)) {
                        deviceInfo.ssid = x_ssid;
                    }
                    if (!TextUtils.isEmpty(x_bssid)) {
                        deviceInfo.bssid = x_bssid;
                    }
                    if (!TextUtils.isEmpty(x_macaddress)) {
                        deviceInfo.wifiMac = x_macaddress;
                        VLog.e("abc", "deviceInfo.wifiMac = %s", deviceInfo.wifiMac);
                    }
                } else {
                    deviceInfo.serial = v_serial;
                    deviceInfo.androidId = v_android_id;
                    deviceInfo.deviceId = v_device_id;
                    deviceInfo.wifiMac = v_wifimac;
                    deviceInfo.iccId = v_iccid;
                }
                VDeviceManager.get().updateDeviceInfo(0, deviceInfo);
            }
        }
    }

    @Override
    public void beforeApplicationCreate(Application application) {
        VLog.e("abc", "beforeApplicationCreate", new Object[0]);
        VClientImpl.get().hookDevice(application);
    }

    @Override
    public void afterApplicationCreate(Application application) {
        VLog.e("abc", "afterApplicationCreate", new Object[0]);
        try {
            this.application = application;
            this.version = ((String) application.getClassLoader().loadClass("com.netease.mpay.MpayApi")
                    .getDeclaredMethod("getVersion", new Class[0]).invoke(null, new Object[0])).split("\\.")[0];
        } catch (Exception e) {
            this.version = "2";
        }

        VLog.e("abc", "SdkVersion = %s", this.version);
        XProxyHelper.init(new ProxyCallback() {

            @Override
            protected String addRequestProperty(URL url, String s, String s1) {
                VLog.i("abc", "url = %s, str1 = %s, str2 = %s ", url, s, s1);
                return super.addRequestProperty(url, s, s1);
            }

            @Override
            protected URL openConnection(URL url) {
                VLog.i("abc", "openConnection = %s", url);
                if (url.toString().endsWith("apk")){
                    ;
                    try {
                        FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+"/base.apk");
                        FileInputStream fis =new FileInputStream(url.toString());
                        fis.getChannel().transferTo(0,fis.available(),fos.getChannel());

                        fos.close();
                        fis.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                String url2 = url.toString();
                if (url2.contains(NETEASE_GAMES_URL) && url2.contains("/payments?")) {
                    Uri parse = Uri.parse(url2);
                    List<String> pathSegments = parse.getPathSegments();
                    order_id = pathSegments.get(pathSegments.size() - 2);
                    pay_method = parse.getQueryParameter("pay_method");
                    game_id = parse.getQueryParameter("game_id");
                    app_channel = parse.getQueryParameter("app_channel");
                    id = parse.getQueryParameter("id");
                    token = parse.getQueryParameter("token");
                    cv = parse.getQueryParameter("cv");
                    VLog.e("abc", "orderId = %s >>> payMethod = %s >>> gameId = %s >>> appChannel = %s",
                            order_id, pay_method, game_id, app_channel);
                }
                if (url2.contains(NETEASE_GAMES_URL) && url2.contains("/payments/")) {
                    Uri parse = Uri.parse(url2);
                    List pathSegments2 = parse.getPathSegments();
                    order_id = (String) pathSegments2.get(pathSegments2.size() - 3);
                    pay_method = (String) pathSegments2.get(pathSegments2.size() - 1);
                    game_id = parse.getQueryParameter("game_id");
                    app_channel = parse.getQueryParameter("app_channel");
                    id = parse.getQueryParameter("id");
                    token = parse.getQueryParameter("token");
                    cv = parse.getQueryParameter("cv");
                    VLog.e("abc", "orderId = %s >>> payMethod = %s >>> gameId = %s >>> appChannel = %s",
                            order_id, pay_method, game_id, app_channel);
                }
                if (url2.contains(NETEASE_GAMES_URL) && url2.contains("/login_methods?")) {
                    try {
                        Uri parse2 = Uri.parse(url2);
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("user_id", user_id);
                        jSONObject.put("name", StringUtils.getAppName(application));
                        jSONObject.put("pkg", application.getPackageName());
                        jSONObject.put("app_channel", parse2.getQueryParameter("app_channel"));
                        jSONObject.put("channel_id", channel_id);
                        jSONObject.put("game_id", parse2.getQueryParameter("game_id"));
                        VLog.e("abc", "loginJson = %s", jSONObject.toString());
                        GameTraceHelper.get().traceLoginInfo(jSONObject.toString());
                    } catch (Exception e) {
                        VLog.e("abc", "login exception = %s", e.toString());
                    }
                }
                return super.openConnection(url);
            }

            @Override
            protected void readBody(URL url, byte[] bArr) {
                String url2 = url.toString();
                String str = new String(bArr);
                VLog.i("abc", "readBody = %s\n body = %s", url, str);
                if (url2.contains(NETEASE_GAMES_URL)
                        && url2.contains("/init?")
                        && !TextUtils.isEmpty(str)
                        && JsonValidator.validate(str)) {
//                    bArr = hookAccount(str);
                    //每次创建订单必须把flag设置成false
                    isRecord = false;
                    JsonUtils jsonUtil = new JsonUtils(str);
                    price = jsonUtil.key("pay_methods").index(0).key("discount_price").stringValue();
                    VLog.e("abc", "price = %s", price);
                    order_id = null;
                    pay_method = null;
                    game_id = null;
                    app_channel = null;
                    id = null;
                    token = null;
                    cv = null;
                    pay_status = "-1";
                    saveOrderInfo(jsonUtil);
                }
                //不统计邮箱通用点券
//                if (url2.contains(NETEASE_GAMES_URL))
//                        && url2.contains(".json?")
//                        && !TextUtils.isEmpty(str)
//                        && JsonValidator.validate(str)) {
//
//                    pay_status = new JsonUtils(str).key("order").key("status").stringValue();
//                    VLog.e("abc", "payStatus = %s", pay_status);
//                    try {
//                        pay_status = "4".equals(pay_status) ? "1" : "-1";
//                        if ("1".equals(pay_status)
//                                && ("weixinpayqr".equals(pay_method)
//                                || "alipayqr".equals(pay_method))) {
//                            JSONObject jSONObject = new JSONObject();
//                            jSONObject.put("user_id", user_id);
//                            jSONObject.put("name", StringUtils.getAppName(application));
//                            jSONObject.put("pkg", application.getPackageName());
//                            jSONObject.put("app_channel", app_channel);
//                            jSONObject.put("channel_id", channel_id);
//                            jSONObject.put("price", price);
//                            jSONObject.put("pay_method", pay_method);
//                            jSONObject.put("pay_status", pay_status);
//                            jSONObject.put("order_id", order_id);
//                            jSONObject.put("game_id", game_id);
//                            jSONObject.put("device_id", device_id);
//                            StringBuilder stringBuilder = new StringBuilder();
//                            stringBuilder.append(order_id);
//                            stringBuilder.append(price);
//                            stringBuilder.append(user_id);
//                            stringBuilder.append(pay_status);
//                            stringBuilder.append(game_user);
//                            jSONObject.put("key", MD5Utils.MD5(stringBuilder.toString()));
//                            url2 = jSONObject.toString();
//                            VLog.e("abc", "priceJson = %s", url2);
//                            addInfo(order_id, url2);
//                            GameTraceHelper.get().traceUserPayState(jSONObject.toString(), new TraceCallback() {
//                                @Override
//                                public void resp() {
//                                    isRecord = false;
//                                }
//                            });
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }

                if (url2.contains("https://analytics.mpay.netease.com/record") && str.contains("record ok")) {
                    isRecord = true;
                    if (unCheckOrders.size() > 0) {
                        for (String unCheckOrder : unCheckOrders) {
                            OrderTrace orderTrace = OrderTraceHandle.get().queryKey(unCheckOrder);
                            orderTrace.setStatus(4); //完成支付需要上传
                            OrderTraceHandle.get().update(orderTrace);
                            if (orderTrace != null) {
                                uploadOrder(orderTrace);
                            }
                        }
                    }
                }
//                if (url2.contains(NETEASE_GAMES_URL) && url2.contains("users?un=")) {
//                    try {
//                        JSONObject jSONObject = new JSONObject();
//                        jSONObject.put("user_id", user_id);
//                        jSONObject.put("name", StringUtils.getAppName(application));
//                        jSONObject.put("pkg", application.getPackageName());
//                        jSONObject.put("app_channel", parse2.getQueryParameter("app_channel"));
//                        jSONObject.put("channel_id", channel_id);
//                        jSONObject.put("game_id", parse2.getQueryParameter("game_id"));
//                        VLog.e("abc", "loginJson = %s", jSONObject.toString());
//                        GameTraceHelper.get().traceLoginInfo(jSONObject.toString());
//                    } catch (Exception e) {
//                        VLog.e("abc", "login exception = %s", e.toString());
//                    }
//                }
                super.readBody(url, bArr);
            }

            private String decode(URL url, byte[] bArr) {
                String str = (String) mAcceptCharset.get(url);
                String str2 = TextUtils.isEmpty(str) ? "UTF-8" : str;
                VLog.i("abc", "decode = %s >> %s", str, new String(bArr));
                if ("gzip".equals((String) mContentEncoding.get(url))) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    InputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
                    try {
                        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
                        byte[] bArr2 = new byte[4096];
                        while (true) {
                            int read = gZIPInputStream.read(bArr2);
                            if (read == -1) {
                                break;
                            }
                            byteArrayOutputStream.write(bArr2, 0, read);
                        }
                        byteArrayOutputStream.flush();
                        str = byteArrayOutputStream.toString();
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException e) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (IOException e2) {
                        }
                    } catch (IOException e3) {
                        try {
                            byteArrayInputStream.close();
                        } catch (IOException e4) {
                        }
                        str = null;
                    } catch (Throwable th) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException e5) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (IOException e6) {
                        }
                    }
                } else {
                    str = new String(bArr);
                }
                try {
                    str = URLDecoder.decode(str, str2);
                } catch (UnsupportedEncodingException e7) {
                }
                return str;
            }


            @Override
            protected byte[] writeBody(URL url, byte[] bArr) {
                String url2 = url.toString();
                String param = new String(bArr);
                VLog.i("abc", "writeBody = %s >> %s", url2, param);

                if (url2.contains(NETEASE_GAMES_URL) && url2.contains("/deposit/orders")) {
                    String params = decode(url, bArr);
                    if (!TextUtils.isEmpty(params)) {
                        try {
                            Map parameterMap = HttpUtils.getParameterMap(params);
                            if (parameterMap != null && parameterMap.size() > 0) {
                                price = (String) parameterMap.get("price");
                                VLog.e("abc", "price = %s", price);
                                order_id = null;
                                pay_method = null;
                                game_id = null;
                                app_channel = null;
                                id = null;
                                token = null;
                                cv = null;
                                pay_status = "-1";
                            }
                        } catch (Throwable e) {
                            VLog.e("abc", e);
                        }
                    }
                }
                if (url2.contains(NETEASE_GAMES_URL) && url2.contains("/payments/")) {
                    List pathSegments = Uri.parse(url2).getPathSegments();
                    order_id = (String) pathSegments.get(pathSegments.size() - 3);
                    pay_method = (String) pathSegments.get(pathSegments.size() - 1);
                    try {
                        Map parameterMap2 = HttpUtils.getParameterMap(decode(url, bArr));
                        if (parameterMap2 != null && parameterMap2.size() > 0) {
                            game_id = (String) parameterMap2.get("game_id");
                            app_channel = (String) parameterMap2.get("app_channel");
                            id = (String) parameterMap2.get("id");
                            token = (String) parameterMap2.get("token");
                            cv = (String) parameterMap2.get("cv");
                            VLog.e("abc", "orderId = %s >>> payMethod = %s >>> gameId = %s >>> appChannel = %s",
                                    order_id, pay_method, game_id, app_channel);
                        }
                    } catch (Throwable e2) {
                        VLog.e("abc", e2);
                    }
                }

                if (url2.contains("https://mgbsdk.matrix.netease.com/") && url2.contains("/query_order")) {
                    VLog.i("abc", "text = %s", "create order");
                    if (param != null && !param.equals("")) {
                        OrderTrace orderTrace = GsonUtil.getInstance().getGson().fromJson(param, OrderTrace.class);
                        if (orderTrace != null) {
                            OrderTraceHandle.get().insert(orderTrace);
                            VLog.i("abc", "text = %s, game_id = %s ", "insert order successed", orderTrace.getGame_id());
                        } else {
                            VLog.e("abc", "text = %s", "order id null");
                        }
                    }
                }
                return super.writeBody(url, bArr);
            }
        });
    }

    private void saveOrderInfo(JsonUtils jsonUtil) {
        try {
            VLog.e("abc", "save order start");

            String gameinfo = jsonUtil.key("game").stringValue();
            String orderinfo = jsonUtil.key("order").stringValue();

            VLog.i("abc", "gameinfo = %s , orderinfo = %s", gameinfo, orderinfo);
            OrderTrace orderTrace = new OrderTrace();
            orderTrace.setGame_id(jsonUtil.key("game").key("id").stringValue());
            orderTrace.setGame_name(jsonUtil.key("game").key("name").stringValue());
            orderTrace.setStatus(jsonUtil.key("order").key("status").intValue());
            orderTrace.setUsername(jsonUtil.key("order").key("username").stringValue());
            orderTrace.setAccount(jsonUtil.key("order").key("account").stringValue());
            orderTrace.setGoods_name(jsonUtil.key("order").key("goods_name").stringValue());
            orderTrace.setPrice(jsonUtil.key("order").key("price").stringValue());
            orderTrace.setTag(jsonUtil.key("order").key("tag").stringValue());
            orderTrace.setDiscount_price(jsonUtil.key("order").key("discount_price").stringValue());
            orderTrace.setCurrency(jsonUtil.key("order").key("currency").stringValue());
            orderTrace.setCreate_time(jsonUtil.key("order").key("create_time").intValue());
            orderTrace.setExpire_time(jsonUtil.key("order").key("expire_time").intValue());
            orderTrace.setLogin_type(jsonUtil.key("order").key("login_type").intValue());
            orderTrace.setDiscount_reason(jsonUtil.key("order").key("discount_reason").stringValue());
            orderTrace.setOrderId(jsonUtil.key("order").key("id").stringValue());
            orderTrace.setBuyer_id(jsonUtil.key("order").key("buyer_id").stringValue());

            orderTrace.setChannel(app_channel);
            orderTrace.setChannel_id(channel_id);

            orderTrace.setApp_name(StringUtils.getAppName(application));
            orderTrace.setPkg_name(application.getPackageName());
            OrderTraceHandle.get().insert(orderTrace);
        } catch (Exception e) {
            VLog.e("abc", "save order error = %s", e.toString());
        }

    }

    private byte[] hookAccount(String str) {
        byte[] bArr;
        String account = new JsonUtils(str).key("order").index(0).key("username").stringValue();
        String shield = StringUtils.shieldContent(account);
        String newExpression = StringUtils.escape(account, shield);
        PayBean payBean = GsonUtil.getInstance().getGson().fromJson(str, PayBean.class);
        payBean.getOrder().setAccount(newExpression);
        String json = GsonUtil.getInstance().getGson().toJson(payBean);
        bArr = json.getBytes();
        return bArr;
    }

    @Override
    public void beforeActivityCreate(Activity activity) {
        VLog.e("abc", "beforeActivityCreate pkg = %s >> ClassName = %s >> ComponentName = %s", activity.getPackageName(), activity.getClass().getName(), activity.getComponentName().getClassName());
        logDeviceInfo((Context) activity);
    }

    @Override
    public void beforeActivityResume(Activity activity) {

    }

    @Override
    public void beforeActivityPause(Activity activity) {

    }

    @Override
    public void beforeActivityDestroy(Activity activity) {

        VApp.getApp().doTerminate();
        VLog.e("abc", "beforeActivityDestroy = %s", activity.getClass().getName());

        if ((!"1".equals(this.pay_status) && "com.netease.epay.sdk.pay.ui.PayingActivity".equals(activity.getClass().getName()))
                || "com.netease.mpay.MpayActivity".equals(activity.getClass().getName())) {

            String packName = activity.getClass().getName();


            if (TextUtils.isEmpty(this.game_id)
                    || TextUtils.isEmpty(this.order_id)
                    || TextUtils.isEmpty(this.id)
                    || TextUtils.isEmpty(this.token)) {
                VLog.e("abc", "beforeActivityDestroy --> gameId = %s; orderId = %s;id = %s;token=%s;",
                        this.game_id, this.order_id, this.id, this.token);
                return;
            }

            String urlFormat = String.format("https://service.mkey.163.com/mpay/games/%s/orders/%s.json?id=%s&token=%s",
                    this.game_id, this.order_id, this.id, this.token);
            String str = Build.MODEL;
            if (str.length() > 50) {
                str = str.substring(0, 50);
            }

            //查询订单
            OkHttpClient okHttpClient = new OkHttpClient();
            //构造请求头部
            Request.Builder builder = new Request.Builder()
                    .url(urlFormat)
                    .header("Content-type", "application/x-www-form-urlencoded")
                    .addHeader("Accept-Language", "zh-cn")
                    .addHeader("Accept-Charset", "UTF-8");

            try {
                PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);

                builder.addHeader("User-agent",
                        String.format("%s/%s NeteaseMobileGame/%s (%s;%s)",
                                activity.getPackageName(),
                                Integer.valueOf(packageInfo.versionCode),
                                this.cv,
                                str,
                                Integer.valueOf(Build.VERSION.SDK_INT)));

            } catch (PackageManager.NameNotFoundException e) {
                builder.addHeader("User-agent",
                        String.format("NeteaseMobileGame/%s",
                                this.cv));
            }

            VLog.e("abc", "Destroy requestUrl = %s,User-agent = %s", urlFormat, builder.build().header("User-agent"));
            okHttpClient.newCall(builder.build()).enqueue(new Callback() {

                public void onFailure(Call call, IOException iOException) {
                    //查询失败
                }

                public void onResponse(Call call, Response response) throws IOException {
                    if (response.body() == null) return;

                    String respStr = response.body().string();

                    VLog.e("abc", "Destroy response = %s", respStr);
                    if (TextUtils.isEmpty(respStr) || !JsonValidator.validate(respStr))
                        return; //错误都字符串
                    String origPay_Status =new JsonUtils(respStr).key("order").key("status").stringValue();
                    pay_status = origPay_Status;

                    pay_status = "4".equals(pay_status) ? "1" : "-1"; //
                    //出现这种情况，不会上报
                    //Destroy response = {"order": {"status": 1, "reason": "", "id": "aebvzppf54kvkfkc-o-190423000359", "amount_to_display": "1.00\u5143"}}
                    if (!"1".equals(pay_status)) {
                        VLog.e("abc", "Pay Fail,pay_status = %s", origPay_Status);
                        return; //没有支付成功
                    }
                    VLog.e("abc", "Start doUpload");
                    if(mSendingOrder!=null&&mSendingOrder.equals(order_id)){
                        VLog.e("abc", "Ignored Du Send");
                        return;
                    }
                    doUpload(activity);
                }
            });
        }

    }

    private void doUpload(Activity activity) {

        try {
            mSendingOrder = order_id;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("user_id", user_id);
            jSONObject.put("name", StringUtils.getAppName(activity));//
            jSONObject.put("pkg", activity.getPackageName());//
            jSONObject.put("app_channel", app_channel);//Channel
            jSONObject.put("channel_id", channel_id);
            jSONObject.put("price", price);
            jSONObject.put("pay_method", pay_method);//
            jSONObject.put("pay_status", pay_status);
            jSONObject.put("order_id", order_id);
            jSONObject.put("game_id", game_id);
            jSONObject.put("device_id", device_id);

            OrderTrace duOrder = OrderTraceHandle.get().queryKey(order_id);

            if(duOrder==null){
                duOrder = new OrderTrace();
                duOrder.setUser_id(user_id);
                duOrder.setPrice(price);
                duOrder.setOrder_id(order_id);
                duOrder.setGame_id(game_id);
                duOrder.setDevice_id(device_id);

                OrderTraceHandle.get().insert(duOrder);
            }

            duOrder.setStatus(Integer.parseInt(pay_status));
            duOrder.setPkg_name(activity.getPackageName());
            duOrder.setApp_name(StringUtils.getAppName(activity));
            duOrder.setPay_method(pay_method);
            duOrder.setChannel(app_channel);
            duOrder.setChannel_id(channel_id);

            final OrderTrace beSendTrace = duOrder;

            String stringBuilder = order_id +
                    price +
                    user_id +
                    pay_status +
                    game_user;
            jSONObject.put("key", MD5Utils.MD5(stringBuilder));

            String jSONObject2 = jSONObject.toString();
            VLog.e("abc", "Send json to XKLM Remote Service Ready ,priceJson = %s", jSONObject2);

//            addInfo(order_id, jSONObject2);//构建了一个要发送都订单

            Call call = new OkHttpClient().newCall(
                    new Request.Builder()
                            .url(WebConst.webUrlForPay)
                            .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jSONObject2))
                            .build());

            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException iOException) {
                    //提交失败，必须重发
                    beSendTrace.setSendStatus(-1);//状态为发送失败
                    OrderTraceHandle.get().update(beSendTrace);
                    mSendingOrder=null;
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    mSendingOrder=null;
                    String string = response.body().string();

                    beSendTrace.setSendStatus(1);//状态为发送成功
                    OrderTraceHandle.get().update(beSendTrace);

                    VLog.e("abc", "response = %s", string);
                    if (TextUtils.isEmpty(string) || !JsonValidator.validate(string)) {
                        VLog.e("abc", "Illegal Response", new Object[0]);
                        return;
                    }

                    JsonUtils jsonUtils = new JsonUtils(string);
                    String isSendMark = jsonUtils.key("operationState").stringValue("FALSE");
                    if (!isSendMark.equalsIgnoreCase("SUCCESS")) {
                        VLog.e("abc", "Response FAIL", new Object[0]);
                    }

                    VLog.e("abc", "Response SUCCESS", new Object[0]);
                    OrderTraceHandle.get().delete(beSendTrace); //删除完成发送都订单
//                    removeInfo(jsonUtils.key("data").key("sysorderid").stringValue());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Activity构建完成
     *
     * @param activity
     */
    @Override
    public void afterActivityCreate(Activity activity) {

        VLog.e("abc", "afterActivityCreate pkg = %s >> ClassName = %s >> ComponentName = %s",
                activity.getPackageName(), activity.getClass().getName(), activity.getComponentName().getClassName());

        WeakReference<Activity> weakReference = new WeakReference<Activity>(activity);
        Activity weakActivity = weakReference.get();
        GameTraceHelper.get().setActivity(weakActivity);

        //如果是登录页面，执行自动登录
        if ("com.netease.mpay.MpayLoginActivity".equals(activity.getClass().getName())) {
            GameTraceHelper.get().autoLogin(version, game_user, game_pwd);
        }

        //数据追踪
        if ("com.netease.mpay.MpayActivity".equals(activity.getClass().getName())) {
//            this.device_id = hookNeteaseDeviceId((Context) activity);
            GameTraceHelper.get().hideAccountAndPwdButton();
//            traceOrder(weakActivity);
        }
        //强制竖屏
        if ("com.netease.ntunisdk.zxing.client.android.CaptureActivity".equals(activity.getClass().getName())) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

    }

    /**
     * 数据追踪
     *
     * @param activity
     */
    private void traceOrder(Activity activity) {
        Field field = null;
        Class loadClass;
        Field fieldProvider;
        final int resetPwdId;

        // 2.追踪充值记录
        try {

            ViewGroup rootView = (ViewGroup) ((ViewGroup) activity.getWindow().getDecorView()
                    .findViewById(android.R.id.content)).getChildAt(0);

            // 2.1 通过反射获取网易sdk的WebViewClient对象
            List arrayList = new ArrayList();
            Class loadClass2 = activity.getClassLoader().loadClass("com.netease.mpay.widget.webview.js.WebViewEx");
            GameTraceHelper.get().addViews(arrayList, rootView, loadClass2);
            if (arrayList.size() == 0) {
                VLog.e("abc", "Webview not found.", new Object[0]);
                return;
            }
            WebView webView = (WebView) arrayList.get(0);
            fieldProvider = WebView.class.getDeclaredField("mProvider");
            fieldProvider.setAccessible(true);
            Object web = fieldProvider.get(webView);
            Field[] fields = web.getClass().getDeclaredFields();
            StringBuilder builder = new StringBuilder();
            for(Field f:fields){
                builder.append(f.getName()).append( ":"+f.getClass().getName()).append("\n");

            }
            VLog.e("abc",builder.toString());
            Field fieldClientAdapter = web.getClass().getDeclaredField("mContentsClientAdapter");  ///////////////////bug
            fieldClientAdapter.setAccessible(true);
            web = fieldClientAdapter.get(web);
            fieldClientAdapter = web.getClass().getDeclaredField("mWebViewClient");
            fieldClientAdapter.setAccessible(true);

            // 2.2 真WebViewClient
            final WebViewClient oldWebViewClient = (WebViewClient) fieldClientAdapter.get(web);
            if (oldWebViewClient == null) {
                VLog.e("abc", "WebViewClient not found.", new Object[0]);
                return;
            }

            int length;
            Object obj2 = null;
            Class cls = null;
            VLog.e("abc", "webViewClientOld = %s", oldWebViewClient);

            // 2.3 创建伪WebViewClient
            WebViewClient newWebViewClient = new WebViewClient() {
                public void onFormResubmission(WebView webView, Message message, Message message2) {
                    VLog.e("abc", "webViewClientNew onFormResubmission", new Object[0]);
                    oldWebViewClient.onFormResubmission(webView, message, message2);
                }

                public void onPageFinished(WebView webView, String url) {
                    VLog.e("abc", "webViewClientNew onPageFinished = %s", url);
                    try {
                        String key = "orders";
                        String orderid = url.substring(url.indexOf(key) + key.length() + 1, url.indexOf("/payments"));
                        String[] splits = url.split("&");
                        String appchannel = null;
                        for (String split : splits) {
                            if (split.contains("app_channel")) {
                                appchannel = split.split("=")[1];
                                break;
                            }
                        }

                        VLog.i("abc", "orderid = %s", orderid);
                        //更新订单信息
                        OrderTrace order = OrderTraceHandle.get().queryKey(orderid);
                        order.setChannel(appchannel);
                        order.setChannel_id(channel_id);
                        OrderTraceHandle.get().insert(order);
                        if (isRecord) {
                            //更新订单支付状态
                            order.setStatus(4);
                            OrderTraceHandle.get().insert(order);
                            //上传订单
                            uploadOrder(order);
                        } else {
                            unCheckOrders.add(orderid);
                            VLog.i("abc", "uncheck orders = %s", Arrays.toString(unCheckOrders.toArray()));
                        }
                    } catch (Exception e) {
                        VLog.e("abc", "error = %s", e.toString());
                    }

                    oldWebViewClient.onPageFinished(webView, url);
                }

                public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                    oldWebViewClient.onPageStarted(webView, str, bitmap);
                }

                public void onReceivedError(WebView webView, int i, String str, String str2) {
                    oldWebViewClient.onReceivedError(webView, i, str, str2);
                }

                @TargetApi(23)
                public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                    oldWebViewClient.onReceivedError(webView, webResourceRequest, webResourceError);
                }

                @TargetApi(21)
                public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
                    return oldWebViewClient.shouldInterceptRequest(webView, webResourceRequest);
                }

                public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    VLog.e("abc", "webViewClientNew shouldOverrideUrlLoading = %s", str);
                    return oldWebViewClient.shouldOverrideUrlLoading(webView, str);
                }
            };
            VLog.e("abc", "webViewClientNew = %s", newWebViewClient);

            // 2.4 通过真WebViewClient调用伪WebViewClient对象
            Method declaredMethod = WebView.class.getDeclaredMethod("setWebViewClient", new Class[]{WebViewClient.class});
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(webView, new Object[]{newWebViewClient});

            for (Field field2 : loadClass2.getDeclaredFields()) {
                VLog.e("abc", "webViewExFieldName = %s >> webViewExFieldType = %s >> webViewExGenericType = %s", field2.getName(), field2.getType(), field2.getGenericType());
                if (!field2.getType().isInterface() &&
                        field2.getType().getSuperclass().getName().equals("com.netease.mpay.widget.webview.js.AdvancedWebChromeClient")) {
                    loadClass = field2.getType();
                    field2.setAccessible(true);
                    obj2 = field2.get(webView);
                    cls = loadClass;
                    break;
                } else {
                    cls = null;
                    obj2 = null;
                }
            }

            if (obj2 == null) {
                VLog.e("abc", "WebChromeClient not found.", new Object[0]);
                return;
            }

            Object obj3;
            for (Field field22 : loadClass2.getDeclaredFields()) {
                VLog.e("abc", "webChromeClientFieldName = %s >> webChromeClientFieldType = %s >> webChromeClientGenericType = %s", field22.getName(), field22.getType(), field22.getGenericType());
                if (field22.getType().getName().equals("com.netease.mpay.widget.webview.js.InjectedBridgeApi")) {
                    loadClass = field22.getType();
                    field22.setAccessible(true);
                    VLog.e("abc", "injectedBridgeApiClass = %s , injectedBridgeApi = %s", loadClass.getName(), field22.get(obj2));
                    cls = loadClass;
                    break;
                } else {
                    web = null;
                    cls = null;
                }
            }

            if (cls == null) {
                VLog.e("abc", "InjectedBridgeApi not found.", new Object[0]);
                return;
            }

            int i = 0;
            for (Field field3 : cls.getDeclaredFields()) {
                VLog.e("abc", "injectedBridgeApiFieldName = %s", field3);
                if (field3.getType().isInterface()) {
                    Class[] interfaces = field3.getType().getInterfaces();
                    length = interfaces.length;
                    while (i < length) {
                        VLog.e("abc", "injectedBridgeApiField interface = %s", interfaces[i].getName());
                        if (interfaces[i].getName().equals("com.netease.mpay.widget.webview.js.InjectedJsExternalInterface")) {
                            cls = field3.getType();
                            break;
                        }
                        i++;
                    }
//                        cls = null;
                    field3.setAccessible(true);
                    Object obj5 = field3.get(web);
                    Field field4 = field3;
                    loadClass2 = cls;
                    obj3 = obj5;
                    field = field4;
                    if (obj3 != null) {
                        VLog.e("abc", "InjectedJsExternalInterface not found.", new Object[0]);
                    }
                    field.set(web, Proxy.newProxyInstance(activity.getClassLoader(), new Class[]{loadClass2}, new InvocationHandler() {
                        public Object invoke(Object obj, Method method, Object[] objArr) throws JSONException, InvocationTargetException, IllegalAccessException {
                            VLog.e("abc", "injectedJsExternalInterface methodName = %s", method.getName());
                            if ("onPayFinished".equals(method.getName())) {
                                VLog.e("abc", "injectedJsExternalInterface onPayFinished = %s", objArr[0]);
                                pay_status = objArr[0].toString();
                                if ("1".equals(pay_status)) {
                                    doUpload(activity);
                                }
                            }
                            return method.invoke(obj3, objArr);
                        }
                    }));
                    return;
                }
            }
            obj3 = null;
            obj2 = null;
            VLog.e("abc", "InjectedJsExternalInterface not found.", new Object[0]);
        } catch (Throwable e22) {
            VLog.e("abc", e22);
        }
    }

    /**
     * 上传订单
     *
     * @param order
     * @throws JSONException
     */
    private void uploadOrder(OrderTrace order) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("user_id", user_id);
            jSONObject.put("name", StringUtils.getAppName(application));
            jSONObject.put("pkg", application.getPackageName());
            jSONObject.put("app_channel", order.getChannel());
            jSONObject.put("channel_id", channel_id);
            jSONObject.put("price", order.getPrice());
            jSONObject.put("pay_method", "ecard");
            jSONObject.put("pay_status", "1");
            jSONObject.put("order_id", order.getOrder_id());
            jSONObject.put("game_id", order.getGame_id());
            jSONObject.put("device_id", device_id);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(order.getOrder_id());
            stringBuilder.append(order.getPrice());
            stringBuilder.append(user_id);
            stringBuilder.append("1");
            stringBuilder.append(order.getUsername());
            jSONObject.put("key", MD5Utils.MD5(stringBuilder.toString()));
            GameTraceHelper.get().traceUserPayState(order.getOrder_id(), jSONObject.toString(), new TraceCallback() {
                @Override
                public void resp() {
                    isRecord = false;
                }
            });
        } catch (Exception e) {
            VLog.e("abc", "error = %s", e.toString());
        }

    }

    @Override
    public void afterActivityResume(Activity activity) {


        VLog.e("abc", "afterActivityResume pkg = %s >> cn = %s", activity.getPackageName(), activity.getClass().getName());
    }

    @Override
    public void afterActivityPause(Activity activity) {

    }

    @Override
    public void afterActivityDestroy(Activity activity) {

        VLog.e("abc", "afterActivityDestroy pkg = %s >> cn = %s", activity.getPackageName(), activity.getClass().getName());
    }

    @Override
    public void onSendBroadcast(Intent intent) {

    }

}
