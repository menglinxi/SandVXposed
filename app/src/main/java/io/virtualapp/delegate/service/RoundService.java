package io.virtualapp.delegate.service;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.lody.virtual.helper.utils.VLog;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.virtualapp.VApp;
import io.virtualapp.delegate.db.OrderTraceHandle;
import io.virtualapp.delegate.utils.GameTraceHelper;
import io.virtualapp.delegate.utils.JsonUtils;
import io.virtualapp.delegate.utils.JsonValidator;
import io.virtualapp.delegate.utils.StringUtils;
import io.virtualapp.entity.OrderTrace;
import io.virtualapp.home.WebConst;
import io.virtualapp.home.callback.TraceCallback;
import io.virtualapp.tools.MD5Utils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * author: MQZ
 * <p/>
 * email: 82498301@qq.com
 * time:  2018/11/14
 * className:
 * <p/>
 * describe:
 */
public class RoundService extends Service{

    private static final int DELAY_TIME = 1000*60*5;
    private static final int TEN = 60*10;
//    private static final int DELAY_TIME = 1000*5;
    private static final int PAY_OK = 4;
    private static final int PAY_FAIL = 0;
    private OrderTraceHandle orderHandle;


    @Override
    public void onCreate() {
        super.onCreate();
        orderHandle = OrderTraceHandle.get();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        VLog.i("abc", "RoundService = %s", "onStartCommand");
        sendRemainOrderTrance();
        return super.onStartCommand(intent, flags, startId);
    }
    private void sendRemainOrderTrance(){
        final List<OrderTrace> beSendTrace = OrderTraceHandle.get().query();
        for(OrderTrace record : beSendTrace){
            int createTime =record.getCreate_time();
            Date now = new Date();
            if(now.getTime()/1000-createTime>TEN && (record.getStatus()==PAY_FAIL) ){
                //超时为完成支付,删除
                OrderTraceHandle.get().delete(record);
                continue;
            }

            if(record.getStatus()==1||record.getStatus()==4){

                if(record.getSendStatus()<0){
                    //需要发送
                    doUpload(record);
                }

                if(record.getSendStatus()==1){
                    //发送成功,执行
                    OrderTraceHandle.get().delete(record);
                }
            }
        }
    }

    private void doUpload(OrderTrace trace) {

        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("user_id", trace.getUser_id());
            jSONObject.put("name", trace.getApp_name());//
            jSONObject.put("pkg", trace.getPkg_name());//
            jSONObject.put("app_channel", trace.getChannel());//Channel
            jSONObject.put("channel_id", trace.getChannel_id());
            jSONObject.put("price", trace.getPrice());
            jSONObject.put("pay_method", trace.getPay_method());//
            jSONObject.put("pay_status", trace.getStatus());
            jSONObject.put("order_id", trace.getOrder_id());
            jSONObject.put("game_id", trace.getGame_id());
            jSONObject.put("device_id", trace.getDevice_id());



            String stringBuilder = trace.getOrder_id() +
                    trace.getPrice() +
                    trace.getUser_id() +
                    trace.getStatus() +
                    trace.getAccount();
            jSONObject.put("key", MD5Utils.MD5(stringBuilder));

            String jSONObject2 = jSONObject.toString();
            VLog.e("abc", "In Service Send json to XKLM Remote Service Ready ,priceJson = %s", jSONObject2);

//            addInfo(order_id, jSONObject2);//构建了一个要发送都订单

            Call call = new OkHttpClient().newCall(
                    new Request.Builder()
                            .url(WebConst.webUrlForPay)
                            .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jSONObject2))
                            .build());

            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException iOException) {
                    //提交失败，必须重发
                    trace.setSendStatus(-1);//状态为发送失败
                    OrderTraceHandle.get().update(trace);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String string = response.body().string();

                    trace.setSendStatus(1);//状态为发送失败
                    OrderTraceHandle.get().update(trace);

                    VLog.e("abc", "response = %s", string);
                    if (TextUtils.isEmpty(string) || !JsonValidator.validate(string)) {
                        VLog.e("abc", "Illegal Response", new Object[0]);
                        return;
                    }

                    JsonUtils jsonUtils = new JsonUtils(string);
                    String isSendMark = jsonUtils.key("operationState").stringValue("FALSE");
                    if (!isSendMark.equalsIgnoreCase("SUCCESS")) {
                        VLog.e("abc", "Response FAIL", new Object[0]);
                    }

                    VLog.e("abc", "Response SUCCESS", new Object[0]);
                    OrderTraceHandle.get().delete(trace); //删除完成发送都订单
//                    removeInfo(jsonUtils.key("data").key("sysorderid").stringValue());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }
}
