package io.virtualapp.down;

import android.os.Environment;

import com.liulishuo.filedownloader.BaseDownloadTask;
import com.liulishuo.filedownloader.FileDownloadListener;
import com.liulishuo.filedownloader.FileDownloader;

/**
 * @Author:陈海楠
 * @Date:2018/4/11
 * @Description:下载工具类,需要网络和SD卡权限
 */
public class DownloadUtil {

    private static BaseDownloadTask task;

    /**
     * @Date: 2018/4/12 12:40
     * @Param:downloadUrl,下载链接，listener，下载的回调监听
     * @Description:下载单个文件,使用默认保存路径
     * @Return:返回任务的ID
     */
    public static void downSingleTask(String downloadUrl, boolean isPause, FileDownloadListener listener) {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + downloadUrl.substring(downloadUrl.lastIndexOf("/"));
        //安卓6.0以上注意手动申请权限，否则报错
        task = FileDownloader.getImpl()
                .create(downloadUrl)
                .setPath(path)
                .setCallbackProgressTimes(500) //进度返回次数
                .setAutoRetryTimes(3)   //自动重试次数
                .setSyncCallback(true)  //设置回调在子线程
                .setListener(listener);
        if (isPause){
            task.pause();
        }
        else{
            task.start();
        }
    }


    /**
     * @Date: 2018/4/13 13:51
     * @Param:downloadUrl,下载链接，listener，下载的回调监听,path,传入的文件保存路径,直接到目录
     * @Description:
     */
    public static void downSingleTask(String downloadUrl, FileDownloadListener listener, String path) {
        path += downloadUrl.substring(downloadUrl.lastIndexOf("/"));
        //安卓6.0以上注意手动申请权限，否则报错
        FileDownloader.getImpl()
                .create(downloadUrl)
                .setPath(path)
                .setAutoRetryTimes(3)   //自动重试次数
                .setListener(listener)
                .start();

    }

    /**
     * @Date: 2018/4/12 12:40
     * @Param:downloadUrls,下载链接，listener，下载的回调监听
     * @Description:下载多个文件
     */
    public static void downSomeTask(FileDownloadListener queueTarget, String... downloadUrls) {
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        for (String downloadUrl : downloadUrls) {
            path += downloadUrl.substring(downloadUrl.lastIndexOf("/"));
            FileDownloader.getImpl()
                    .create(downloadUrl)
                    .setPath(path)
                    .setCallbackProgressTimes(0)
                    .setAutoRetryTimes(3)
                    .setListener(queueTarget)
                    .asInQueueTask()
                    .enqueue();
        }
        FileDownloader.getImpl().start(queueTarget, true);  // 串行执行该队列
    }

    /**
     * @Date: 2018/4/12 15:00
     * @Param:任务ID
     * @Description:暂停某个任务
     */
    public static void pauseSingleTask(int downloadID) {
        FileDownloader.getImpl().pause(downloadID);
    }

    /**
     * @Date: 2018/4/12 15:01
     * @Param:
     * @Description:暂停全部任务
     */
    public static void pauseAllTask() {
        FileDownloader.getImpl().pauseAll();
    }

    /**
     * @Date: 2018/4/13 13:53
     * @Param:任务ID,目标文件的保存路径,直接到文件名
     * @Description:删除某个任务
     * @Return:返回是否成功删除
     */
    public boolean deleteSingleTask(int downloadId, String targetFilePath) {
        return FileDownloader.getImpl().clear(downloadId, targetFilePath);
    }

    /**
     * @Date: 2018/4/13 13:57
     * @Param:
     * @Description:删除全部下载任务
     */
    public void deleteAllTask() {
        FileDownloader.getImpl().clearAllTaskData();
    }
}
