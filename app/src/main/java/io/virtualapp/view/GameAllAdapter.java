package io.virtualapp.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import io.virtualapp.R;

/**
 * * Created by chao on 2018/4/3.
 * 游戏item适配器(所有游戏界面)
 */

public class GameAllAdapter extends BaseAdapter {

    private Context context;
    private List<String> urls = new ArrayList<>();
    private List<String> names = new ArrayList<>();

    public GameAllAdapter(Context context, List<String> names, List<String> urls) {
        this.context = context;
        this.urls = urls;
        this.names = names;
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public String getItem(int i) {
        return urls.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_game, null);
            vh = new ViewHolder();

            vh.imageView = (ImageView) view.findViewById(R.id.item_img_game);
            vh.textView = (TextView) view.findViewById(R.id.item_txv_gamename);
            view.setTag(vh);
        }
        vh = (ViewHolder) view.getTag();
        if (urls != null && urls.size() > 0) {
            vh.textView.setText(names.get(i));
            Glide.with(context).load(urls.get(i)).into(vh.imageView);
        }
        return view;
    }

    class ViewHolder {
        ImageView imageView;
        TextView textView;
    }
}