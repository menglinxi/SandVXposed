package io.virtualapp.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.virtualapp.R;


/**
 * * Created by chao on 2018/4/3.
 * 游戏item适配器(我的游戏界面)
 */

public class GameMyAdapter extends BaseAdapter {

    private Context context;
    private List<Drawable> drawables = new ArrayList<>();
    private List<String> names = new ArrayList<>();

    public GameMyAdapter(Context context, List<String> names, List<Drawable> drawabless) {
        this.context = context;
        this.drawables = drawabless;
        this.names = names;
    }

    @Override
    public int getCount() {
        return drawables.size();
    }

    @Override
    public Drawable getItem(int i) {
        return drawables.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder vh = null;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_game, null);
            vh = new ViewHolder();

            vh.imageView = (ImageView) view.findViewById(R.id.item_img_game);
            vh.textView = (TextView) view.findViewById(R.id.item_txv_gamename);
            view.setTag(vh);
        }
        vh = (ViewHolder) view.getTag();
        if (drawables != null && drawables.size() > 0) {
            vh.textView.setText(names.get(i));
            vh.imageView.setImageDrawable(drawables.get(i));
        }
        return view;
    }

    class ViewHolder {
        ImageView imageView;
        TextView textView;
    }
}