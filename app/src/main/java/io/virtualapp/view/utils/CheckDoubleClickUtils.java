package io.virtualapp.view.utils;

public class CheckDoubleClickUtils {

    private static long lastClickTime = 0;

    private static final int SPACE_TIME = 500;

    private CheckDoubleClickUtils() {
        // DO NOTHING...
    }

    public synchronized static boolean isDoubleClick() {
        long currentTime = System.currentTimeMillis();
        boolean isClick = currentTime - lastClickTime < SPACE_TIME;
        lastClickTime = currentTime;
        return isClick;
    }


    public synchronized static boolean isDoubleClick(int delaymillis) {
        long currentTime = System.currentTimeMillis();
        boolean isClick = currentTime - lastClickTime < delaymillis;
        lastClickTime = currentTime;
        return isClick;
    }
}
