package io.virtualapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.liulishuo.filedownloader.FileDownloader;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.stub.VASettings;
import com.lody.virtual.helper.utils.VLog;
import com.lody.virtual.os.VEnvironment;
import com.lody.virtual.sandxposed.SandHookHelper;
import com.lody.virtual.sandxposed.SandXposed;
import com.service.MyMessageCode;
import com.service.MyService;
import com.swift.sandhook.SandHook;
import com.swift.sandhook.annotation.ThisObject;
import com.swift.sandhook.wrapper.HookErrorException;
import com.swift.sandhook.wrapper.HookWrapper;
import com.trend.lazyinject.buildmap.Auto_ComponentBuildMap;
import com.trend.lazyinject.lib.LazyInject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import io.virtualapp.delegate.MyAppRequestListener;
import io.virtualapp.delegate.MyGameHookDelegate;
import io.virtualapp.delegate.MyPhoneInfoDelegate;
import io.virtualapp.delegate.MyTaskDescriptionDelegate;
import io.virtualapp.delegate.db.GreenDevHelper;
import io.virtualapp.delegate.db.compile.DaoMaster;
import io.virtualapp.delegate.db.compile.DaoSession;
import io.virtualapp.home.util.ToastUtils;
import io.virtualapp.tools.FileUtil;
import io.virtualapp.tools.GameGson;
import io.virtualapp.tools.PhoneInfoCollect;
import jonathanfinerty.once.Once;

import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.crashreport.CrashReport;

/**
 * @author Lody
 */
public class VApp extends MultiDexApplication {


    private static final String TAG = "VApp";
    private static VApp gApp;
    private SharedPreferences mPreferences;
    public static SparseArray<Activity> activityContainer = new SparseArray<>();
    private SQLiteDatabase db;
    private DaoSession daoSession;
    private Intent service;
    public static final String XPOSED_INSTALLER_PACKAGE = "de.robv.android.xposed.installer";


    private void initialDaoSession(Context context) {
        DaoMaster.DevOpenHelper helper = new GreenDevHelper(context, null);
        db = helper.getWritableDatabase();

        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();

    }

    public Messenger mHomeServiceMessenger;
    public ServiceConnection mHomeServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("lhh","HomeService bind");

            mHomeServiceMessenger =  new Messenger(service);
            //hook.setMessenger(mHomeServiceMessenger);
            //VirtualCore.get().setHomeServiceMessenger(mHomeServiceMessenger);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("lhh","HomeService disconnected");
        }
    };

    private void initHomeService(){
        Intent serviceIntent = new Intent(this.getBaseContext(), MyService.class);
        boolean r =bindService(serviceIntent,mHomeServiceConn,Context.BIND_AUTO_CREATE);

        VLog.d("lhh","startService "+r);

    }

    public static VApp getApp() {
        return gApp;
    }

    @Override
    protected void attachBaseContext(Context base) {
        Log.e("abc","vastart up");
        super.attachBaseContext(base);
        SandXposed.init();

        mPreferences = base.getSharedPreferences("va", Context.MODE_MULTI_PROCESS);
        VASettings.ENABLE_IO_REDIRECT = true;
        VASettings.ENABLE_INNER_SHORTCUT = false;
        try {
            VirtualCore.get().startup(base);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void doTerminate(){
        if(mHomeServiceMessenger!=null){

            unbindService(mHomeServiceConn);

            mHomeServiceMessenger=null;
        }

    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    private void initBugly() {
        Context context = getApplicationContext();
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        // 上报策略设置：由主进程上报
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        // 初始化bugly
        Bugly.init(getApplicationContext(), "83b945173b", BuildConfig.DEBUG, strategy);
        Beta.autoInit = true;
        Beta.autoCheckUpgrade = true;
    }
    private void requestNetworkConnectChangedReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(new NetworkConnectChangedReceiver(), filter);
    }

    private void initConfig() {
        PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
        SharedPreferences.Editor edit = getPreferences(VCommends.SP_VBOX_DEVICE_INFO).edit();
        edit.putString("V_ANDROID_ID", phoneInfoCollect.ANDROID_ID);
        edit.putString("V_DEVICE_ID", phoneInfoCollect.IMEI1);
        edit.putString("V_BUILD_MANUFACTURER", phoneInfoCollect.phoneManufacturer);
        edit.putString("V_BUILD_MODEL", phoneInfoCollect.phoneModel);
        edit.putString("V_BUILD_SERIAL", phoneInfoCollect.serial);
        edit.putString("V_MAC_ADDRESS", phoneInfoCollect.wifiInfoGetMacAddress);
        edit.putString("V_ICC_ID", phoneInfoCollect.telephonyGetSimSerialNumber);
        edit.commit();
    }


    private static class NetworkConnectChangedReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                PhoneInfoCollect phoneInfoCollect = PhoneInfoCollect.get();
                String ipAddress = phoneInfoCollect.getNetworkIpAddress();
                VLog.e("abc", "ipAddress = %s", ipAddress);
                VApp.getApp().putStringToSharedPreferences(VCommends.SP_VBOX_DEVICE_INFO,"V_IP_ADDRESS", ipAddress);
            }
        }
    }

    @Override
    public void onCreate() {
        gApp = this;
        super.onCreate();
        lazyInjectInit();

        VLog.e("abc","onCreate()");
        //delete ../io.va.exposed folder
        FileUtil.checkFixedFolder();
        // 初始化bugly
        initBugly();
        // 初始化下载器
        FileDownloader.setup(getApplicationContext());
        initialDaoSession(getApp());


        VirtualCore virtualCore = VirtualCore.get();
        virtualCore.initialize(new VirtualCore.VirtualInitializer() {

            //主进程
            @Override
            public void onMainProcess() {
                Once.initialise(VApp.this);
                new FlurryAgent.Builder()
                        .withLogEnabled(true)
                        .withListener(() -> {
                            // nothing
                        })
                        .build(VApp.this, "48RJJP7ZCZZBB6KMMWW5");
                // 获取游戏页数
                GameGson.getPageCount();
                // 初始化Toast
                ToastUtils.init(getApp());
                initConfig();
                // demo
//                WifiManager wifiManager = (WifiManager) gApp.getSystemService(Context.WIFI_SERVICE);
//                List<ScanResult> scanResults = wifiManager.getScanResults();
//                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//                if (scanResults != null && wifiInfo != null) {
//                    if (scanResults.size() > 0) {
//                        for (ScanResult scanResult : scanResults) {
//                            if (wifiInfo.getBSSID().equals(scanResult.BSSID)) {
//                                VLog.e("abc", "bssid = %s", scanResult.BSSID);
//                            }
//                        }
//                    }
                // 网络监听
                requestNetworkConnectChangedReceiver();
//                LogcatService.start(VApp.this, VEnvironment.getDataUserPackageDirectory(0, XPOSED_INSTALLER_PACKAGE));

            }

            //客户端进程
            @Override
            public void onVirtualProcess() {
                VLog.e("adc", "run virtual process");




                MyGameHookDelegate delegate = new MyGameHookDelegate();


                virtualCore.setComponentDelegate(delegate);
//                //listener components
//                virtualCore.setComponentDelegate(new MyComponentDelegate());
                //fake phone imei,macAddress,BluetoothAddress
                virtualCore.setPhoneInfoDelegate(new MyPhoneInfoDelegate());
                //fake task description's icon and title
                virtualCore.setTaskDescriptionDelegate(new MyTaskDescriptionDelegate());
                //
                virtualCore.setCrashHandler((t, e) -> {
                    VLog.i(TAG, "uncaught :" + t, e);
                    if (t == Looper.getMainLooper().getThread()) {
                        System.exit(0);
                    } else {
                        VLog.e(TAG, "ignore uncaught exception of thread: " + t);
                    }
                });
                // ensure the logcat service alive when every virtual process start.
//                LogcatService.start(VApp.this, VEnvironment.getDataUserPackageDirectory(0, XPOSED_INSTALLER_PACKAGE));
            }

            //服务端进程
            @Override
            public void onServerProcess() {
                virtualCore.setAppRequestListener(new MyAppRequestListener(VApp.this));
                virtualCore.addVisibleOutsidePackage("com.tencent.mobileqq");
                virtualCore.addVisibleOutsidePackage("com.tencent.mobileqqi");
                virtualCore.addVisibleOutsidePackage("com.tencent.minihd.qq");
                virtualCore.addVisibleOutsidePackage("com.tencent.qqlite");
                virtualCore.addVisibleOutsidePackage("com.facebook.katana");
                virtualCore.addVisibleOutsidePackage("com.whatsapp");
                virtualCore.addVisibleOutsidePackage("com.tencent.mm");
                virtualCore.addVisibleOutsidePackage("com.immomo.momo");
                virtualCore.addVisibleOutsidePackage("com.eg.android.AlipayGphone");
            }
        });

    }

    public static SharedPreferences getPreferences() {
        return getApp().mPreferences;
    }

    private void lazyInjectInit() {
        LazyInject.init(this);
        LazyInject.addBuildMap(Auto_ComponentBuildMap.class);
    }

    /**
     * 获取数据库的管理类
     *
     * @return
     */
    public static DaoSession getDaoSession() {
        return getApp().daoSession;
    }

    public void removeToSharedPreferences(String str, String str2) {
        SharedPreferences.Editor edit = getPreferences(str).edit();
        edit.remove(str2);
        edit.commit();
    }

    public static SharedPreferences getPreferences(String preName) {
        return getApp().getSharedPreferences(preName, MODE_MULTI_PROCESS);
    }

    public void putStringToSharedPreferences(String preName, String key, String value) {
        SharedPreferences.Editor editor = getPreferences(preName).edit();
        editor.putString(key, value);
        editor.commit();
    }

}
