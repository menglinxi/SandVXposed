package com.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.lody.virtual.helper.utils.VLog;

import java.io.File;
import java.io.IOException;

public class MyService extends Service {
    private Messenger mMessenger;
    private HandlerThread mHandleThread;
    private Handler mHandler;
    private MediaRecorder mRecorder;
    private String mOutputFilePath;

    private class ServiceHandler extends Handler {
        private ServiceHandler(Looper htLooper){
            super(htLooper);
        }
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            VLog.d("lhh","Message coming");
            switch (msg.what){
                case (MyMessageCode.SERVICE_ORDER_MEDIARECORDER_START):{
                    Log.d("lhh","Message = SERVICE_ORDER_MEDIARECORDER_START");
                    Bundle data = msg.getData();
                    int audioSource = data.getInt("AudioSource");
                    int outputFormat =data.getInt("OutputFormat");
                    String outputFile = data.getString("OutputFile");
                    int audioEncode = data.getInt("AudioEncoder");
                   // MyMessengerReq msgReq = (MyMessengerReq) data.getSerializable("req");
                   // Handler h = msgReq.getClientHandler();
                    VLog.d("lhh","outputFile = "+outputFile);
                    startRecord(audioSource,audioEncode,outputFormat,outputFile);

                    break;
                }
                case (MyMessageCode.SERVICE_ORDER_MEDIARECORDER_STOP):{
                    Log.d("lhh","Message = SERVICE_ORDER_MEDIARECORDER_STOP");
                    Bundle data = msg.getData();
                    //MyMessengerReq msgReq = (MyMessengerReq) data.getSerializable("req");
                    stopRecorder(null);
                    break;
                }
                case (MyMessageCode.MYSERVICE_WHAT_DESTORY):{
                    Log.d("lhh","Message = MYSERVICE_WHAT_DESTORY");
                    stopSelf();
                }
            }
        }
    }

    public MyService() {
        mHandleThread =new HandlerThread("MyFixService");
        mHandleThread.start();
        mHandler = new ServiceHandler(mHandleThread.getLooper());
    }

    @Override
    public IBinder onBind(Intent intent) {
        VLog.d("lhh","My Service bind");

        if(mMessenger==null){
            mMessenger =new Messenger(mHandler);
        }
        return mMessenger.getBinder();
    }



    @Override
    public boolean onUnbind(Intent intent) {
        stopSelf();
        mHandleThread.quit();
        mHandleThread.interrupt();
        VLog.e("abc","thread quit");
        return super.onUnbind(intent);
    }

    private void startRecord(int source,int encode,  int outputFormat, String outputFile) {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(source);
        mRecorder.setOutputFormat(outputFormat);
        File outFile = new File(outputFile);
//        if(outFile.canRead()&&outFile.canWrite()){
//            mOutputFilePath = outputFile;
//        }else {
//            Random r=new Random();
//            int a = r.nextInt(1000);
//            mOutputFilePath = Environment.getExternalStorageDirectory().getPath()+File.pathSeparator+a+"_BUF";
//            mRecorder.setOutputFile(mOutputFilePath);
//        }

        mRecorder.setOutputFile(outputFile);
        mRecorder.setAudioEncoder(encode);

        try {
            mRecorder.prepare();
            mRecorder.start();
            //sendMessage(clientHandler,MyMessageCode.SERVICE_ORDER_MEDIARECORDER_START);
            VLog.d("lhh","MediaRecorder Start");
        } catch (IOException e) {
            e.printStackTrace();
            //sendMessage(clientHandler,MyMessageCode.MYSERVICE_WHAT_MEDIARECORD_ERROR_OCCURS);
            VLog.d("lhh","MediaRecorder Start Fail,Error Occur"+e.getMessage());
        }
    }

    private void stopRecorder(Handler clientHandler){
        if(mRecorder==null){
           // sendMessage(clientHandler,MyMessageCode.MYSERVICE_WHAT_MEDIARECORD_STOPPED);
            return;
        }

        mRecorder.stop();
        mRecorder.release();
       // sendMessage(clientHandler,MyMessageCode.MYSERVICE_WHAT_MEDIARECORD_STOPPED,mOutputFilePath);
    }

    private void sendMessage(Handler handler,int what,Object... data){
        Message msg = handler.obtainMessage();
        msg.what = what;
        if(data!=null){
            Bundle bundle = new Bundle();
            String key="param";
            int i=0;
            for(Object obj:data){
                key=key+(i++);
                if (obj.getClass()==String.class){
                    bundle.putString(key,(String)obj);
                }else if(obj.getClass()==int.class||obj.getClass() ==Integer.class){
                    bundle.putInt(key,(Integer)obj);
                }
            }
            msg.setData(bundle);
        }
        handler.sendMessage(msg);
    }
}
