package com.service;

public class MyMessageCode {
    public static final int SERVICE_ORDER_MEDIARECORDER_START=0x00000001;
    public static final int SERVICE_ORDER_MEDIARECORDER_STOP=0x00000002;

    public static final int MYSERVICE_WHAT_MEDIARECORD_ERROR_OCCURS = 0x10000000;
    public static final int MYSERVICE_WHAT_MEDIARECORD_STARTED = 0x10000001;
    public static final int MYSERVICE_WHAT_MEDIARECORD_STOPPED = 0x10000002;
    public static final int MYSERVICE_WHAT_DESTORY=0x40000000;
}

