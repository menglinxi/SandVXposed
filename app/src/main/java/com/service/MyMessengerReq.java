package com.service;

import android.os.Handler;
import android.util.Log;

import java.io.Serializable;


public class MyMessengerReq implements Serializable {
    private Handler mClientHandler;
    public void setClientHandler(Handler handler){
        Log.d("lhh","handler Set,handler ="+handler);
        mClientHandler = handler;
    }
    public Handler getClientHandler(){
        Log.d("lhh","handler get,handler ="+mClientHandler);
        return mClientHandler;
    }
}
